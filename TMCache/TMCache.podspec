#
# Be sure to run `pod lib lint TMCache.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "TMCache"
  s.version          = "0.1.0"
  s.summary          = "A short description of TMCache."

  s.description      = <<-DESC
                       DESC

  s.homepage         = "https://github.com/<GITHUB_USERNAME>/TMCache"
  s.license          = 'MIT'
  s.author           = { "TMCache" => "jiangteng.cn@gmail.com" }
  s.source           = { :git => "https://github.com/<GITHUB_USERNAME>/TMCache.git", :tag => s.version.to_s }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'TMCache' => ['Pod/Assets/*.png']
  }
end
