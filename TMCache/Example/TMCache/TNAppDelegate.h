//
//  TNAppDelegate.h
//  TMCache
//
//  Created by TMCache on 03/06/2016.
//  Copyright (c) 2016 TMCache. All rights reserved.
//

@import UIKit;

@interface TNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
