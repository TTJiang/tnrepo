# TMCache

[![CI Status](http://img.shields.io/travis/TMCache/TMCache.svg?style=flat)](https://travis-ci.org/TMCache/TMCache)
[![Version](https://img.shields.io/cocoapods/v/TMCache.svg?style=flat)](http://cocoapods.org/pods/TMCache)
[![License](https://img.shields.io/cocoapods/l/TMCache.svg?style=flat)](http://cocoapods.org/pods/TMCache)
[![Platform](https://img.shields.io/cocoapods/p/TMCache.svg?style=flat)](http://cocoapods.org/pods/TMCache)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TMCache is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "TMCache"
```

## Author

TMCache, jiangteng.cn@gmail.com

## License

TMCache is available under the MIT license. See the LICENSE file for more info.
