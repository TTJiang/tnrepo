Pod::Spec.new do |s|
s.name         = "Foundation"
s.version      = "0.0.1"
s.summary      = "Foundation"

s.description  = <<-DESC
Foundation
DESC

s.homepage     = "http://code.jiangteng.com/"
s.license      = 'MIT'
s.author             = { "teng.jiang" => "jiangteng.cn@gmail.com" }
s.platform     = :ios, "6.0"
s.source       = {:git => "http://code", :tag => s.version}

s.source_files  = 'Foundation/**/*.{h,m}'

s.requires_arc = true
end
