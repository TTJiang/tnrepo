//
//  UGCModel.h
//  Pods
//
//  Created by Johnny on 15/8/1.
//
//

#import <Foundation/Foundation.h>

@class UGCDraft;

@interface UGCModel : NSObject

@property (nonatomic, readonly, strong) NSLock *lock;

+ (id)getplistObjectWithObject:(id)object;
+ (id)getUGCModelObjectFromPlistObject:(id)object;

+ (BOOL)needSavetoplistForKey:(NSString *)string;

- (NSDictionary *)allValidKeys;

- (void)relesetResource;

@end
