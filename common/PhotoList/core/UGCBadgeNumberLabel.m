//
//  UGCBadgeNumberLabel.m
//  Pods
//
//  Created by johnny on 15/8/7.
//
//

#import "UGCBadgeNumberLabel.h"

@interface UGCBadgeNumberLabel ()

@property (nonatomic, strong) UIColor *internalBackgroundColor;


@end

@implementation UGCBadgeNumberLabel

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [super setBackgroundColor:[UIColor clearColor]];
        
        self.textAlignment = NSTextAlignmentCenter;
        
        _internalBackgroundColor = [UIColor whiteColor];
        _borderColor = [UIColor clearColor];
        _borderWidth = 0;
    }
    return self;
}

- (void)setBorderColor:(UIColor *)borderColor{
    _borderColor = borderColor;
    [self setNeedsDisplay];
}

- (void)setBorderWidth:(CGFloat)borderWidth{
    _borderWidth = borderWidth;
    [self setNeedsDisplay];
}


- (void)setText:(NSString *)text WithAnimation:(BOOL)animation{
    self.text = text;
    CGRect bounds = self.bounds;
    if (animation) {
        self.contentMode = UIViewContentModeScaleAspectFit;
        [UIView animateWithDuration:0.1 animations:^{
            self.bounds = CGRectMake(0, 0, bounds.size.width * 0.75, bounds.size.height * 0.75);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.1 initialSpringVelocity:0.1 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                self.bounds = bounds;
            } completion:^(BOOL internalFinished) {
                self.contentMode = UIViewContentModeRedraw;
            }];
        }];
    }
}

- (void)setBackgroundColor:(UIColor *)backgroundColor{
    self.internalBackgroundColor = backgroundColor;
}

- (void)drawRect:(CGRect)rect{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGMutablePathRef path = CGPathCreateMutable();
    CGFloat cornerRadius = MIN(self.bounds.size.width, self.bounds.size.height) / 2;
    CGPathAddRoundedRect(path, NULL, self.bounds, cornerRadius, cornerRadius);
    CGContextAddPath(ctx, path);
    CGPathRelease(path);
    CGContextSetFillColorWithColor(ctx, self.internalBackgroundColor.CGColor);
    CGContextFillPath(ctx);
    path = CGPathCreateMutable();
    CGRect newBounds = CGRectMake(self.borderWidth / 2, self.borderWidth / 2, self.bounds.size.width - self.borderWidth, self.bounds.size.height - self.borderWidth);
    cornerRadius = MIN(newBounds.size.width, newBounds.size.height) / 2;
    if (newBounds.size.width < 0 || newBounds.size.height < 0) {
        [super drawRect:rect];
        return;
    }
    CGPathAddRoundedRect(path, NULL, newBounds, cornerRadius, cornerRadius);
    CGContextAddPath(ctx, path);
    CGPathRelease(path);
    CGContextSetStrokeColorWithColor(ctx, self.borderColor.CGColor);
    CGContextSetLineWidth(ctx, self.borderWidth + 0.5);
    CGContextStrokePath(ctx);
    [super drawRect:rect];
    
}


@end
