//
//  UGCPhotoInfo.m
//  Nova
//
//  Created by johnny on 15/5/26.
//  Copyright (c) 2015年 dianping.com. All rights reserved.
//

#import "UGCUploadPhotoInfo.h"
#import "UGCUserContentHelper.h"
#import <ImageIO/ImageIO.h>

@implementation UGCUploadPhotoInfo

@synthesize filePath = _filePath;

+ (BOOL)needSavetoplistForKey:(NSString *)string{
    if ([string isEqualToString:@"fileSize"]) return NO;
    if ([string isEqualToString:@"image"]) return NO;
    if ([string isEqualToString:@"photoAsset"]) return NO;
    if ([string isEqualToString:@"filePath"]) return NO;
    if ([string isEqualToString:@"remoteUrl"]) return NO;
    if ([string isEqualToString:@"thumbUrl"]) return NO;
    return [super needSavetoplistForKey:string];
}

+ (instancetype)photoInfoWithFilePath:(NSString *)filePath{
    return [[self alloc] initWithFilePath:filePath];
}

- (instancetype)initWithFilePath:(NSString *)filePath{
    if (!filePath.length) {
        return nil;
    }
    if (self = [super init]) {
        _filePath = filePath;
        _binded = NO;
        _photoSize = CGSizeZero;
        _processFlag = UGCPhotoProcessTypeCompression;
        _clipRect = CGRectMake(0, 0, 1, 1);
    }
    return self;
}

- (instancetype)init{
    if (self = [super init]) {
        _binded = NO;
        _photoSize = CGSizeZero;
        _processFlag = UGCPhotoProcessTypeCompression;
    }
    return self;
}

- (UIImage *)image{
    if (!_image) {
        return [UIImage imageWithContentsOfFile:self.filePath];
    }
    return _image;
}

- (NSString *)fileName{
    if (!_fileName.length) {
        _fileName = @"ind";
    }
    return _fileName;
}

- (NSString *)filePath{
    if (!_filePath.length) {
        _filePath = [[[UGCUserContentHelper imageDraftPath] stringByAppendingPathComponent:self.fileName] stringByAppendingString:@".jpg"];
    }
    return _filePath;
}

- (CGSize)photoSize{
    if (_photoSize.height == 0 || _photoSize.width == 0) {
        UIImage *image = [UIImage imageWithContentsOfFile:self.filePath];
        if (!image) {
            return CGSizeZero;
        }
        self.photoSize = image.size;
    }
    return _photoSize;
}

- (unsigned long long)fileSize{
    NSFileManager *fm = [NSFileManager defaultManager];
    if ([fm fileExistsAtPath:self.filePath]){
        return [[fm attributesOfItemAtPath:self.filePath error:nil] fileSize];
    }
    return 0;
}

- (NSString *)callID{
    if (!_callID || !_callID.length){
        self.callID = @"ind";
    }
    return _callID;
}

- (void)saveImageToFilePathOnBackground{
    if (!self.image && !self.photoAsset) return;
    [self performSelectorInBackground:@selector(saveImageToFilePathWithImage:) withObject:nil];
}

- (void)saveImageToFilePathWithImage:(UIImage *)image{
    if (!self.photoAsset && !image) return;
    self.image = nil;
    if (!image){
        image = [self.photoAsset getImage];
        self.exif = self.photoAsset.exif;
        self.photoAsset = nil;
    }
    CFMutableDataRef data = CFDataCreateMutable(kCFAllocatorDefault, 0);
    CGImageDestinationRef imageDest = CGImageDestinationCreateWithData(data, CFSTR("public.jpeg"), 0, NULL);
    NSMutableDictionary *extroinfo = [NSMutableDictionary dictionaryWithDictionary:self.exif?:[NSDictionary new]];
    [extroinfo setObject:@([self getValueWithUIImageOrientation:image.imageOrientation]) forKey:(__bridge NSString *)kCGImagePropertyOrientation];
    NSMutableDictionary *tiffinfo = [NSMutableDictionary dictionaryWithDictionary:extroinfo[(__bridge NSString *)kCGImagePropertyTIFFDictionary]];
    [tiffinfo setObject:@([self getValueWithUIImageOrientation:image.imageOrientation]) forKey:(__bridge NSString *)kCGImagePropertyTIFFOrientation];
    [extroinfo setObject:tiffinfo forKey:(__bridge NSString *)kCGImagePropertyTIFFDictionary];
    CGImageDestinationAddImage(imageDest, image.CGImage, (__bridge CFDictionaryRef)extroinfo);
    CGImageDestinationFinalize(imageDest);
    CFRelease(imageDest);
    [(__bridge NSData *)data writeToFile:self.filePath atomically:YES];
    CFRelease(data);
}

- (NSInteger)getValueWithUIImageOrientation:(UIImageOrientation)imageOrientation{
    switch (imageOrientation) {
        case UIImageOrientationUp: return 1;
        case UIImageOrientationDown: return 3;
        case UIImageOrientationLeft: return 8;
        case UIImageOrientationRight: return 6;
        case UIImageOrientationUpMirrored: return 2;
        case UIImageOrientationDownMirrored: return 4;
        case UIImageOrientationLeftMirrored: return 5;
        case UIImageOrientationRightMirrored: return 7;
    }
    return 1;
}

- (void)deleteImageFromFilePath{
    if (!self.filePath.length) return;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:self.filePath error:NULL];
}

- (NSURL *)remoteUrl{
    return [NSURL URLWithString:[self valueForKey:@"_remoteUrl"]];
}

- (void)setRemoteUrl:(NSURL *)remoteUrl{
    if (!remoteUrl) return;
    return[self setValue:remoteUrl.absoluteString forKey:@"_remoteUrl"];
}

- (NSURL *)thumbUrl{
    return [NSURL URLWithString:[self valueForKey:@"_thumbUrl"]];
}

- (void)setThumbUrl:(NSURL *)thumbUrl{
    if (!thumbUrl) return;
    return[self setValue:thumbUrl.absoluteString forKey:@"_thumbUrl"];
}

- (void)dealloc{
}


@end
