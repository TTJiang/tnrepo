//
//  NVImagePickerViewController.m
//  Nova
//
//  Created by zhou cindy on 12-7-2.
//  Copyright (c) 2012年 dianping.com. All rights reserved.
//

#import "NVImagePickerViewController.h"

@implementation NVImagePickerViewController

- (id)init
{
    self = [super init];
    if (self) {
        UIView *backgroundView = [[UIView alloc] initWithFrame:self.view.frame];
        backgroundView.backgroundColor = [UIColor blackColor];
        [self.view insertSubview:backgroundView atIndex:0];
    }
    return self;
}

- (NSString *)modalGroup
{
    return @"Photo";
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    [[UINavigationBar appearanceWhenContainedIn:[self class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                          [UIColor whiteColor], UITextAttributeTextColor,
                                                          [UIFont fontWithName:@"Helvetica" size:18], UITextAttributeFont,
                                                          [UIColor redColor], UITextAttributeTextShadowColor,
                                                          [NSValue valueWithCGSize:CGSizeMake(0, -1)], UITextAttributeTextShadowOffset,
                                                          nil]];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)prefersStatusBarHidden NS_AVAILABLE_IOS(7_0)
{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
