//
//  UGCPhotoInfo.h
//  Nova
//
//  Created by johnny on 15/5/26.
//  Copyright (c) 2015年 dianping.com. All rights reserved.
//

#import "UGCModel.h"
#import <UIKit/UIKit.h>
#import "UGCPhotoLibrary.h"
#import "UGCPhotoAsset.h"
#import "UGCPhotoCollection.h"

typedef enum {
    UGCPhotoProcessTypeClip        = 1,
    UGCPhotoProcessTypeCompression = 1<<1,
    UGCPhotoProcessTypeCompose     = 1<<2,
    UGCPhotoProcessTypeSaveImage   = 1<<3,
} UGCPhotoProcessType;

@interface UGCUploadPhotoInfo : UGCModel

@property (nonatomic, assign)   CGSize              photoSize;
@property (nonatomic, strong)   NSString           *filePath;
@property (nonatomic, strong)   NSString           *fileName;
@property (nonatomic, readonly) unsigned long long  fileSize;
@property (nonatomic, strong)   NSString           *fileID;
@property (nonatomic, assign)   NSInteger           picID;
@property (nonatomic, strong)   NSNumber           *lng;
@property (nonatomic, strong)   NSNumber           *lat;
@property (nonatomic, assign)   BOOL                isUploadByQCloud;
@property (nonatomic, strong)   NSString           *callID;
@property (nonatomic, strong)   NSNumber           *result;
@property (nonatomic, assign)   BOOL                binded;
@property (nonatomic, strong)   UIImage            *image;
@property (nonatomic, strong)   NSURL              *remoteUrl;
@property (nonatomic, strong)   NSURL              *thumbUrl;
@property (nonatomic, assign)   NSInteger           processFlag;
@property (nonatomic, assign)   NSInteger           processResultFlag;
@property (nonatomic, assign)   BOOL                savedInAlbum;
@property (nonatomic, assign)   CGRect              clipRect;
@property (nonatomic, assign)   CGSize              maxSize;
@property (nonatomic, strong)   UGCPhotoAsset      *photoAsset;
@property (nonatomic, strong)   NSDictionary       *exif;


//Temp
+ (instancetype)photoInfoWithFilePath:(NSString *)filePath;

- (void)saveImageToFilePathOnBackground;
- (void)saveImageToFilePathWithImage:(UIImage *)image;
- (void)deleteImageFromFilePath;

@end
