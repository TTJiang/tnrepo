//
//  UGCButtonWithBadgeNumber.h
//  Pods
//
//  Created by johnny on 15/8/7.
//
//

#import <UIKit/UIKit.h>
#import "UGCBadgeNumberLabel.h"

@interface UGCButtonWithBadgeNumber : UIButton

@property (nonatomic, readonly) UGCBadgeNumberLabel *badgeLabel;
@property (nonatomic, assign)   CGPoint              labelCenterPoint;

- (void)setBadgeText:(NSString *)text WithAnimation:(BOOL)animation;

@end
