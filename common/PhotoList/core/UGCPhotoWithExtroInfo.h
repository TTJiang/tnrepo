//
//  UGCPhotoWithExtroInfo.h
//  Pods
//
//  Created by Johnny on 15/8/2.
//
//

#import "UGCUploadPhotoInfo.h"

@interface UGCPhotoWithExtroInfo : UGCUploadPhotoInfo

@property (nonatomic, strong) NSMutableArray *tags;
@property (nonatomic, strong) NSMutableArray *stickers;
@property (nonatomic, strong) NSString       *identifier;
@property (nonatomic, strong) NSString       *title;
@property (nonatomic, assign) CGSize         viewSize;

@end
