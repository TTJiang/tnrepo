//
//  UGCPictureProcesser.m
//  Pods
//
//  Created by Johnny on 15/11/27.
//
//

#import <QuartzCore/QuartzCore.h>

#import "UGCPictureProcesser.h"
#import "UGCPhotoWithExtroInfo.h"
#import "UGCSticker.h"

@implementation UGCPictureProcesser

- (UIImage *)processImageWithImageInfo:(UGCUploadPhotoInfo *)photoInfo{
    UIImage *inputImage = photoInfo.image;
    if (!inputImage) return nil;
    UIImage *resultImage = inputImage;
    if ((photoInfo.processFlag ^ photoInfo.processResultFlag) & UGCPhotoProcessTypeCompression) {
        resultImage = [self compressedInputImage:resultImage ToMaxSize:photoInfo.maxSize];
        photoInfo.processResultFlag = photoInfo.processResultFlag | UGCPhotoProcessTypeCompression;
    }
    if ((photoInfo.processFlag ^ photoInfo.processResultFlag) & UGCPhotoProcessTypeClip) {
        CGRect targetRect;
        targetRect.origin.x = photoInfo.clipRect.origin.x * resultImage.size.width;
        targetRect.origin.y = photoInfo.clipRect.origin.y * resultImage.size.height;
        targetRect.size.width = photoInfo.clipRect.size.width * resultImage.size.width;
        targetRect.size.height = photoInfo.clipRect.size.height * resultImage.size.height;
        resultImage = [self cropInputImage:resultImage WithRect:targetRect];
        photoInfo.processResultFlag = photoInfo.processResultFlag | UGCPhotoProcessTypeClip;
    }
    if ((photoInfo.processFlag ^ photoInfo.processResultFlag) & UGCPhotoProcessTypeCompose) {
        if ([photoInfo isKindOfClass:[UGCPhotoWithExtroInfo class]] && ((UGCPhotoWithExtroInfo *)photoInfo).stickers.count) {
            resultImage = [self composeImage:resultImage WithPhotoInfo:(UGCPhotoWithExtroInfo *)photoInfo];
            photoInfo.processResultFlag = photoInfo.processResultFlag | UGCPhotoProcessTypeCompose;
        }
    }
    if (resultImage.imageOrientation != UIImageOrientationUp) {
        resultImage = [self fixImageOrientationWithInputImage:resultImage];
    }
    if (inputImage != resultImage) {
        [photoInfo saveImageToFilePathWithImage:resultImage];
        photoInfo.photoSize = photoInfo.image.size;
    }
    return resultImage;
}

- (UIImage *)fixImageOrientationWithInputImage:(UIImage *)inputImage {
    @autoreleasepool {
        if (inputImage.imageOrientation == UIImageOrientationUp) return inputImage;
        CGAffineTransform transform = CGAffineTransformIdentity;
        size_t width = (size_t)inputImage.size.width;
        size_t height = (size_t)inputImage.size.height;
        
        switch (inputImage.imageOrientation) {
            case UIImageOrientationDown:
            case UIImageOrientationDownMirrored:
                transform = CGAffineTransformTranslate(transform, width, height);
                transform = CGAffineTransformRotate(transform, M_PI);
                break;
                
            case UIImageOrientationLeft:
            case UIImageOrientationLeftMirrored:
                transform = CGAffineTransformTranslate(transform, width, 0);
                transform = CGAffineTransformRotate(transform, M_PI_2);
                break;
                
            case UIImageOrientationRight:
            case UIImageOrientationRightMirrored:
                transform = CGAffineTransformTranslate(transform, 0, height);
                transform = CGAffineTransformRotate(transform, -M_PI_2);
                break;
            default:
                break;
        }
        
        switch (inputImage.imageOrientation) {
            case UIImageOrientationUpMirrored:
            case UIImageOrientationDownMirrored:
                transform = CGAffineTransformTranslate(transform, width, 0);
                transform = CGAffineTransformScale(transform, -1, 1);
                break;
                
            case UIImageOrientationLeftMirrored:
            case UIImageOrientationRightMirrored:
                transform = CGAffineTransformTranslate(transform, height, 0);
                transform = CGAffineTransformScale(transform, -1, 1);
                break;
            default:
                break;
        }
        
        CGContextRef ctx = CGBitmapContextCreate(NULL, width, height,
                                                 CGImageGetBitsPerComponent(inputImage.CGImage), 0,
                                                 CGImageGetColorSpace(inputImage.CGImage),
                                                 CGImageGetBitmapInfo(inputImage.CGImage));
        CGContextConcatCTM(ctx, transform);
        switch (inputImage.imageOrientation) {
            case UIImageOrientationLeft:
            case UIImageOrientationLeftMirrored:
            case UIImageOrientationRight:
            case UIImageOrientationRightMirrored:
                CGContextDrawImage(ctx, CGRectMake(0,0,height,width), inputImage.CGImage);
                break;
                
            default:
                CGContextDrawImage(ctx, CGRectMake(0,0,width,height), inputImage.CGImage);
                break;
        }
        
        CGImageRef resultCGImage = CGBitmapContextCreateImage(ctx);
        UIImage *image = [UIImage imageWithCGImage:resultCGImage];
        CGContextRelease(ctx);
        CGImageRelease(resultCGImage);
        return image;
    }
}

- (UIImage *)composeImage:(UIImage *)inputImage WithPhotoInfo:(UGCPhotoWithExtroInfo *)extroInfo{
//    @autoreleasepool {
//        UIGraphicsBeginImageContextWithOptions(inputImage.size, NO, 1);
//        CGContextRef ctx = UIGraphicsGetCurrentContext();
//        [inputImage drawInRect:CGRectMake(0, 0, inputImage.size.width, inputImage.size.height)];
//        for (UGCSticker *sticker in extroInfo.stickers) {
//            UGCDecal *decal = sticker.decal;
//            UIImage *targetImage = decal.image;
//            CGSize targetSize = CGSizeMake(sticker.relativeWidth * inputImage.size.width, sticker.relativeHeight * inputImage.size.height);
//            CGLayerRef layer = CGLayerCreateWithContext(ctx, targetSize, NULL);
//            CGContextRef layerCtx = CGLayerGetContext(layer);
//            UIGraphicsPushContext(layerCtx);
//            [targetImage drawInRect:CGRectMake(0, 0, targetSize.width, targetSize.height)];
//            UIGraphicsPopContext();
//            CGContextSaveGState(ctx);
//            CGContextRotateCTM(ctx, sticker.angle);
//            CGPoint newCenter = CGContextConvertPointToDeviceSpace(ctx, CGPointMake( targetSize.width / 2, targetSize.height / 2));
//            CGContextRestoreGState(ctx);
//            CGContextSaveGState(ctx);
//            CGContextTranslateCTM(ctx, sticker.position.x * inputImage.size.width - newCenter.x, sticker.position.y  * inputImage.size.height - newCenter.y );
//            CGContextRotateCTM(ctx, sticker.angle);
//            CGContextDrawLayerInRect(ctx, CGRectMake(0, 0, targetSize.width, targetSize.height), layer);
//            CGContextRestoreGState(ctx);
//            CGLayerRelease(layer);
//        }
//        UIImage *resutlImage = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        return  resutlImage;
//    }
    return nil;
}

- (UIImage *)compressedInputImage:(UIImage *)inputImage ToMaxSize:(CGSize)maxSize
{
    @autoreleasepool {
        CGSize imageSize = inputImage.size;
        CGFloat width = imageSize.width;
        CGFloat height = imageSize.height;
        
        if (width == 0 || height == 0 || maxSize.width == 0 || maxSize.height == 0) {
            [NSException exceptionWithName:@"Compress Image parameter error" reason:@"parameter error" userInfo:nil];
            return nil;
        }
        if (width <= maxSize.width && height <= maxSize.height) {
            return inputImage;
        }
        CGFloat widthScale = maxSize.width / width;
        CGFloat heightScale = maxSize.height / height;
        
        CGSize targetSize;
        if (widthScale < heightScale) {
            targetSize.width = maxSize.width;
            targetSize.height = widthScale * height;
        } else {
            targetSize.height = maxSize.height;
            targetSize.width = heightScale * width;
        }
        
        UIGraphicsBeginImageContext(targetSize);
        
        CGRect drawRect = CGRectZero;
        drawRect.size.width  = targetSize.width;
        drawRect.size.height = targetSize.height;
        
        [inputImage drawInRect:drawRect];
        
        UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        return result;
    }
}

-(UIImage*)cropInputImage:(UIImage *)inputImage WithRect:(CGRect)rect
{
    @autoreleasepool {
        UIGraphicsBeginImageContextWithOptions(rect.size, NO, 1);
        [inputImage drawAtPoint:(CGPoint){-rect.origin.x, -rect.origin.y}];
        UIImage *croppedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return croppedImage;
    }
}

@end
