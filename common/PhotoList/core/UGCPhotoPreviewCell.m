//
//  UGCPhotoPreviewCell.m
//  Pods
//
//  Created by Johnny on 15/8/9.
//
//

#import "UGCPhotoPreviewCell.h"
#import "UIView+Layout.h"

@interface UGCPhotoPreviewCell ()

@end

@implementation UGCPhotoPreviewCell

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        _imageView = [UIImageView new];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        _imageView.backgroundColor = [UIColor blackColor];
        [self.contentView addSubview:_imageView];
        
        _selectButton = [UIButton new];
        [_selectButton setImage:[UIImage imageNamed:@"addpic_icon_pitchon_rest"] forState:UIControlStateNormal];
        [_selectButton setImage:[UIImage imageNamed:@"addpic_icon_pitchon"] forState:UIControlStateSelected];
        [_selectButton addTarget:self action:@selector(buttonClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_selectButton];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    self.imageView.frame = self.bounds;
    
    self.selectButton.size = CGSizeMake(100, 100);
    self.selectButton.right = self.contentView.width;
    self.selectButton.top = 0;
}


- (void)buttonClicked{
    if (self.selectButtonClicked) {
        BOOL result =  self.selectButtonClicked(!self.selectButton.isSelected);
        if (result) {
            self.selectButton.selected = !self.selectButton.isSelected;
        }
    }
}

- (void)prepareForReuse{
    [super prepareForReuse];
    
    self.imageView.image = nil;
}

@end
