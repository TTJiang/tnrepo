//
//  UGCPhotoAlbumCell.h
//  Pods
//
//  Created by johnny on 15/8/24.
//
//

#import <UIKit/UIKit.h>

@interface UGCPhotoAlbumCell : UITableViewCell

@property (nonatomic, strong, readonly) UIImageView *postImage;
@property (nonatomic, strong, readonly) UILabel     *nameLabel;
@property (nonatomic, strong, readonly) UILabel     *countLabel;

@end
