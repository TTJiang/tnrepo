//
//  UGCPhotoListPhotoCell.h
//  Pods
//
//  Created by Johnny on 15/8/2.
//
//

#import <UIKit/UIKit.h>

@interface UGCPhotoListPhotoCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) BOOL (^selectButtonClicked)(BOOL);
@property (nonatomic, readonly) UIButton *selectButton;

@end

@interface UGCPhotoListCameraCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *imageView;

@end
