//
//  UGCPhotoPreviewController.h
//  Pods
//
//  Created by johnny on 15/7/13.
//
//
#import <UIKit/UIKit.h>
#import "UGCPhotoCollection.h"

@class UGCPhotoListViewController;

@interface UGCPhotoPreviewController : UIViewController

@property (nonatomic, strong) UGCPhotoCollection         *collection;
@property (nonatomic, assign) NSInteger                   beginIndex;
@property (nonatomic, assign) NSInteger                   maxNum;
@property (nonatomic, strong) NSString                   *next;
@property (nonatomic, strong) NSString                   *nextButtonTitle;
@property (nonatomic, strong) dispatch_block_t            nextBlock;
@property (nonatomic, weak)   UGCPhotoListViewController *listVC;
@property (nonatomic, assign) NSInteger                   oldSelectNum;

@end
