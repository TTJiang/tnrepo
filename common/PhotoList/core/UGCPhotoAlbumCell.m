//
//  UGCPhotoAlbumCell.m
//  Pods
//
//  Created by johnny on 15/8/24.
//
//

#import "UGCPhotoAlbumCell.h"
#import "UIView+Layout.h"
#import "UIColor+Ext.h"

@implementation UGCPhotoAlbumCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _postImage = [UIImageView new];
        _postImage.contentMode = UIViewContentModeScaleAspectFill;
        _postImage.clipsToBounds = YES;
        [self addSubview:_postImage];
        
        _nameLabel = [UILabel new];
        [self addSubview:_nameLabel];
        
        _countLabel = [UILabel new];
        _countLabel.textColor = [UIColor colorWithHexString:@"8e8e93"];
        _countLabel.font = [UIFont systemFontOfSize:17];
        [self addSubview:_countLabel];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    self.postImage.size = CGSizeMake(self.height, self.height);
    self.postImage.left = 0;
    self.postImage.top = 0;
    
    [self.nameLabel sizeToFit];
    self.nameLabel.centerY = self.height / 2;
    self.nameLabel.left = self.postImage.right + 10;
    
    [self.countLabel sizeToFit];
    self.countLabel.centerY = self.height / 2;
    self.countLabel.right = self.width - 40;
    
    
}

@end
