//
//  UGCPhotoAlbumSelectView.m
//  Pods
//
//  Created by johnny on 15/8/23.
//
//

#import "UGCPhotoAlbumSelectView.h"
#import "UIView+Layout.h"
#import "UGCPhotoCollection.h"

#import "UGCPhotoAlbumCell.h"

@interface UGCPhotoAlbumSelectView ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *tableView;

@end

@implementation UGCPhotoAlbumSelectView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _tableView = [UITableView new];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self addSubview:_tableView];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    self.tableView.frame = self.bounds;
}

- (void)setAlbums:(NSArray *)albums{
    _albums = albums;
    [self.tableView reloadData];
}

#pragma mark- Table View Delegate and DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return (NSInteger)self.albums.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UGCPhotoAlbumCell *cell = [tableView dequeueReusableCellWithIdentifier:@"albumcell"];
    if (!cell) {
        cell = [[UGCPhotoAlbumCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"albumcell"];
    }
    UGCPhotoCollection *collection = [self.albums objectAtIndex:indexPath.row];
    [collection getPostImageWithComplete:^(UIImage *result) {
        if (indexPath.row == [tableView indexPathForCell:cell].row) {
            cell.postImage.image = result;
            if (result == nil) {
                cell.postImage.image = [UIImage imageNamed:@"NoPicFrame"];
            }
            [cell setNeedsLayout];
        }
    }];
    cell.nameLabel.text = collection.name;
    cell.countLabel.text = [@(collection.count) stringValue];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 72;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (self.photoAlbumSelected) {
        self.photoAlbumSelected(self.albums[indexPath.row]);
    }
}

@end
