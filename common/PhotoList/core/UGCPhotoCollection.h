//
//  UGCPhotoCollection.h
//  HelloPhotoLibrary
//
//  Created by Johnny on 15/7/12.
//  Copyright © 2015年 Johnny. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class UGCPhotoAsset;

@interface UGCPhotoCollection : NSObject

@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSInteger count;

- (UGCPhotoAsset *)objectAtIndex:(NSInteger)index;
- (void)preloadPicWithCompletion:(void(^)())completion;
- (void)getPostImageWithComplete:(void(^)(UIImage *))completion;

@end
