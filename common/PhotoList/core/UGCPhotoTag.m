//
//  UGCPhotoTag.m
//  Pods
//
//  Created by Johnny on 15/8/1.
//
//

#import "UGCPhotoTag.h"


@implementation UGCPhotoTag

+ (BOOL)needSavetoplistForKey:(NSString *)string{
    if ([string isEqualToString:@"superTag"]) return NO;
    return [super needSavetoplistForKey:string];
}


- (void)setSubTags:(NSArray *)subTags{
    _subTags = subTags;
    for (UGCPhotoTag *photoTag in subTags) {
        photoTag->_superTag = self;
    }
}


@end
