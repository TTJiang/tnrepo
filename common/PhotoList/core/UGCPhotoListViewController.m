//
//  UGCPhotoListViewController.m
//  Pods
//
//  Created by johnny on 15/7/13.
//
//

#import "UGCPhotoListViewController.h"
//#import "UGCPhotoLibraryHeader.h"
#import "UGCPhotoLibrary.h"
#import "UGCPhotoAsset.h"
#import "UGCPhotoCollection.h"
#import "UGCPhotoListPhotoCell.h"
//#import "EXTScope.h"
#import "UIView+Layout.h"
#import "UGCUserContentHelper.h"
//#import "NSString+UUID.h"
#import "UGCPhotoWithExtroInfo.h"
//#import "UGCPhotoSelectURLParametersPraser.h"
#import "UGCButtonWithBadgeNumber.h"
#import "UIColor+Ext.h"
#import "UGCPhotoPreviewController.h"
#import "MVPopoverView.h"
//#import "UIBarButtonItem+Addition.h"
#import "UGCPhotoAlbumSelectView.h"
//#import "NVAlertDelegate.h"
//#import "LegoAlertController.h"
//#import "LegoAlertView.h"
//#import "R.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreLocation/CoreLocation.h>

#import "UGCButtonWithBadgeNumber.h"

@interface UGCPhotoListViewController()<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) UICollectionView                  *collectionView;
@property (nonatomic, strong) UGCPhotoCollection                *activeCollection;
@property (nonatomic, strong) UGCPhotoLibrary                   *photoLibrary;
@property (nonatomic, strong) NSArray                           *photoAlbums;
@property (nonatomic, weak)   UGCUserContentHelper              *ucHelper;
//@property (nonatomic, strong) UGCPhotoSelectURLParametersPraser *parameterPraser;
@property (nonatomic, strong) UGCButtonWithBadgeNumber          *nextButton;
@property (nonatomic, strong) UIView                            *toolBar;
@property (nonatomic, strong) UIButton                          *previewButton;
@property (nonatomic, assign) CGFloat                            cellWidth;
@property (nonatomic, assign) NSInteger                          oldSelectNum;

@end

@implementation UGCPhotoListViewController

NSString * const UGCPhotoListCancelNotification = @"UGCPhotoListCancelNotification";
NSString * const UGCPhotoListSubmitNotification = @"UGCPhotoListSubmitNotification";

#pragma mark- LifeCycle

NSMutableArray *selectArray;
NSMutableArray *selectPhotoInfos;


- (instancetype)init{
    if (self = [super init]) {
//        _parameterPraser = [[UGCPhotoSelectURLParametersPraser alloc] initWithPhotoListViewController:self];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectPicCountChange) name:UGCUserContentHelperSelectPhotoChangedNotification object:nil];
        _maxnum = 9;
        selectArray = [NSMutableArray new];
        selectPhotoInfos = [NSMutableArray new];
        CGFloat balanceValue = 100;
        NSInteger viewCount = [UIScreen mainScreen].bounds.size.width / balanceValue;
        CGFloat width = [UIScreen mainScreen].bounds.size.width -( viewCount - 1 ) * 10 - 30;
        width = width / viewCount;
        _cellWidth = width;
    }
    return self;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    selectArray = nil;
    selectPhotoInfos = nil;
}

- (void)backToPreviousViewController{
//    [super backToPreviousViewController];
    for (UGCPhotoWithExtroInfo *photoInfo in selectPhotoInfos) {
        [self.ucHelper deselectPhoto:photoInfo];
    }
}

//- (BOOL)handleWithURLAction:(NVURLAction *)urlAction{
//    BOOL praseResult =[self.parameterPraser prase];
//    if (!praseResult) return NO;
//    if ([urlAction stringForKey:@"next"].length) {
//        self.next = [urlAction stringForKey:@"next"];
//    }
//    if ([urlAction integerForKey:@"maxnum"]) {
//        self.maxnum = [urlAction integerForKey:@"maxnum"];
//    }
//    self.nextBlock = [urlAction anyObjectForKey:@"nextblock"];
//    self.nextButtonTitle = [urlAction stringForKey:@"nextbuttontitle"];
//    return [super handleWithURLAction:urlAction];
//}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    self.toolBar.size = CGSizeMake(self.view.width, 50);
    self.toolBar.left = 0;
    self.toolBar.bottom = self.view.height;
    
    self.previewButton.centerY = self.toolBar.height / 2;
    self.previewButton.left = 15;
    
    
    self.collectionView.width = self.view.width;
    self.collectionView.height = self.view.height - self.toolBar.height;
    
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    
    CGFloat margin = 15;
    layout.itemSize = CGSizeMake(self.cellWidth, self.cellWidth);
    layout.sectionInset = UIEdgeInsetsMake(margin, margin, margin, margin);
    layout.minimumInteritemSpacing = 10;
    layout.minimumLineSpacing = layout.minimumInteritemSpacing;
    self.collectionView.collectionViewLayout = layout;
    
}

- (void)selectPicCountChange{
    if (!selectArray.count) {
        self.nextButton.enabled = NO;
        self.previewButton.enabled = NO;
        self.previewButton.layer.borderWidth = 0;
        self.previewButton.backgroundColor = [UIColor colorWithHexString:@"cccccc"];
        [self.nextButton setBadgeText:nil WithAnimation:NO];
        return;
    }
    self.nextButton.enabled = YES;
    self.previewButton.enabled = YES;
    self.previewButton.layer.borderWidth = 1;
    self.previewButton.backgroundColor = [UIColor whiteColor];
    [self.nextButton setBadgeText:[@(selectArray.count) stringValue] WithAnimation:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self selectPicCountChange];

}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    NSArray *cells = [self.collectionView visibleCells];
    for (UGCPhotoListPhotoCell *cell in cells) {
        if (![cell isKindOfClass:[UGCPhotoListPhotoCell class]]) continue;
        NSIndexPath *indexPath  = [self.collectionView indexPathForCell:cell];
        UGCPhotoAsset *asset = [self.activeCollection objectAtIndex:indexPath.row - 1];
        cell.selectButton.selected = [selectArray containsObject:asset.identifier];
    }
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [self createNavigationItem];
    
    [self createToolBar];
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:[UICollectionViewFlowLayout new]];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerClass:[UGCPhotoListPhotoCell class] forCellWithReuseIdentifier:@"photoCell"];
    [self.collectionView registerClass:[UGCPhotoListCameraCell class] forCellWithReuseIdentifier:@"cameraCell"];
    self.collectionView.backgroundColor = [UIColor colorWithHexString:@"f4f4f4"];
    [self.view addSubview:self.collectionView];
    
    [self requestPermissionAndFetchData];
    
    self.ucHelper = [UGCUserContentHelper shareInstance];
    
    self.oldSelectNum = self.ucHelper.currentSelectedPhotos.count;
    
    [self updateTitleView];
    
}

- (void)updateTitleView{
    UIButton *button = [UIButton new];
    button.backgroundColor = [UIColor clearColor];
    button.titleLabel.font= [UIFont systemFontOfSize:16];
    [button setImage:[UIImage imageNamed:@"dropdown_arrow_d"] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(showAlbumSelectPage:) forControlEvents:UIControlEventTouchUpInside];
    [button sizeToFit];
    button.height = 44;
    self.navigationItem.titleView = button;
}

- (void)updateTitle{
    UIButton *button = (UIButton *)self.navigationItem.titleView;
    [button setTitle:self.activeCollection.name forState:UIControlStateNormal];
    [button sizeToFit];
    button.height = 44;
    button.titleEdgeInsets = UIEdgeInsetsMake(0, -button.imageView.width, 0, button.imageView.width);
    button.imageEdgeInsets = UIEdgeInsetsMake(0, button.titleLabel.width, 0, -button.titleLabel.width);
}

- (void)showAlbumSelectPage:(UIButton *)sender{
    if ([MVPopoverView popoverIsShowed]) {
        [MVPopoverView dismissPopoverViewAnimated:YES];
        [sender setImage:[UIImage imageNamed:@"dropdown_arrow_d"] forState:UIControlStateNormal];
        return;
    }
    [sender setImage:[UIImage imageNamed:@"dropdown_arrow_u"] forState:UIControlStateNormal];
    MVPopoverView *popView = [MVPopoverView sharedPopoverView];
    UGCPhotoAlbumSelectView *selectView = [[UGCPhotoAlbumSelectView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 220)];
    selectView.albums = self.photoAlbums;
    __weak typeof(self) __weakSelf = self;
    selectView.photoAlbumSelected = ^(UGCPhotoCollection *collection){
        __strong typeof(__weakSelf) __strongSelf = __weakSelf;
        [MVPopoverView dismissPopoverViewAnimated:YES];
        [sender setImage:[UIImage imageNamed:@"dropdown_arrow_d"] forState:UIControlStateNormal];
        if (__strongSelf.activeCollection != collection) {
            __strongSelf.activeCollection = collection;
            [__strongSelf preloadAssetsFromActiveCollection];
        }
    };
    popView.componentsView = selectView;
    [popView showPopoverViewInView:self.navigationItem.titleView position:NVPopoverPositionBelow animated:YES];
}

- (void)showLimitToast{
    if (self.ucHelper.currentSelectedPhotos.count) {
//        [self showSplash:[NSString stringWithFormat:@"最多可以选择上传%@张图片", @(self.maxnum - self.oldSelectNum)]];
    }
}

- (void)createNavigationItem{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];

    UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
    button.titleLabel.font = font;
    button.titleLabel.minimumScaleFactor = 8.f / 15.f;
    [button setTitle:@"取消" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    button.exclusiveTouch = YES;
    CGSize size = [self sizeWithstring:@"取消" font:font constrainedToSize:CGSizeMake(140, 34) lineBreakMode:NSLineBreakByWordWrapping];
    button.frame = CGRectMake(0, 0, size.width, size.height);
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = item;
    
    
    self.nextButton = [UGCButtonWithBadgeNumber new];
    self.nextButton.size = CGSizeMake(60, 30);
    self.nextButton.labelCenterPoint = CGPointMake(60, 5);
    self.nextButton.titleLabel.font = [UIFont boldSystemFontOfSize:11];
    self.nextButton.badgeLabel.font = [UIFont systemFontOfSize:13];
    [self.nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    if (self.nextButtonTitle.length) {
        [self.nextButton setTitle:self.nextButtonTitle forState:UIControlStateNormal];
    } else {
        [self.nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    }
    [self.nextButton setBackgroundImage:[self getPureColorImageWithSize:self.nextButton.size andColor:[UIColor colorWithHexString:@"fc673d"] andCornerRadius:2] forState:UIControlStateNormal];
    [self.nextButton setBackgroundImage:[self getPureColorImageWithSize:self.nextButton.size andColor:[UIColor colorWithHexString:@"ffc2ad"] andCornerRadius:2] forState:UIControlStateDisabled];
    
    [self.nextButton addTarget:self action:@selector(nextButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    self.nextButton.badgeLabel.backgroundColor = [UIColor colorWithHexString:@"6cc26a"];
    self.nextButton.badgeLabel.textColor = [UIColor whiteColor];
    self.nextButton.badgeLabel.borderColor = [UIColor whiteColor];
    self.nextButton.badgeLabel.borderWidth = 1;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.nextButton];
}

-(CGSize)sizeWithstring:(NSString *)string font:(UIFont *)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)mode
{
    if (!self || string.length == 0) {
        return CGSizeZero;
    }
    NSDictionary * attrs = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    CGSize sbSize = [string boundingRectWithSize:size options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
    sbSize.height = ceilf(sbSize.height);
    sbSize.width = ceilf(sbSize.width);
    return sbSize;
}

- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
    for (UGCPhotoWithExtroInfo *photoInfo in selectPhotoInfos) {
        [self.ucHelper deselectPhoto:photoInfo];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:UGCPhotoListCancelNotification object:nil];
}

- (void)createToolBar{
    self.toolBar = [UIView new];
    self.toolBar.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.toolBar];
    
    UIView *seperator = [UIView new];
    seperator.backgroundColor = [UIColor colorWithHexString:@"e8e8e8"];
    [self.toolBar addSubview:seperator];
    seperator.top = 0;
    seperator.height = 1;
    seperator.width = self.toolBar.width;
    seperator.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin|UIViewAutoresizingFlexibleWidth;
    
    self.previewButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    self.previewButton.enabled = NO;
    [self.toolBar addSubview:self.previewButton];
    [self.previewButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.previewButton setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
    self.previewButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.previewButton setTitle:@"预览" forState:UIControlStateNormal];
    self.previewButton.layer.borderWidth = 1;
    self.previewButton.layer.cornerRadius = 2;
    [self.previewButton addTarget:self action:@selector(openPreview) forControlEvents:UIControlEventTouchUpInside];
    self.previewButton.layer.borderColor = [UIColor colorWithHexString:@"afb0b1"].CGColor;
}

- (UIImage *)getPureColorImageWithSize:(CGSize)size andColor:(UIColor *)color andCornerRadius:(CGFloat)radius{
    UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRoundedRect(path, NULL, CGRectMake(0, 0, size.width, size.height), radius, radius);
    CGContextAddPath(ctx, path);
    CGPathRelease(path);
    CGContextSetFillColorWithColor(ctx, color.CGColor);
    CGContextFillPath(ctx);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

#pragma mark - Getdata

- (void)requestPermissionAndFetchData{
    [UGCPhotoLibrary requestAuthorizationWithCompletionBlock:^(BOOL result) {
        if (result) {
            self.photoLibrary = [UGCPhotoLibrary sharePhotoLibrary];
            [self performSelectorOnMainThread:@selector(getAlbums) withObject:nil waitUntilDone:NO];
            self.previewButton.enabled = YES;
        } else {
//            if (NSFoundationVersionNumber >= NSFoundationVersionNumber_iOS_8_0){
//                NVAlertView alertView = [NVAlertViewController alertViewWithTitle:nil message:@"请允许大众点评访问您的手机相册"];
//                [alertView addButtonWithTitle:@"取消" action:^{
//                }];
//                [alertView addButtonWithTitle:@"设置" action:^{
//                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
//                }];
//                [alertView show];
//            }
//            else {
//                NVAlertView alertView = [NVAlertViewController alertViewWithTitle:nil message:@"请在iPhone的“设置－隐私－照片”选项中，允许大众点评访问您的相册"];
//                [alertView addButtonWithTitle:@"确定" action:^{
//                }];
//                [alertView show];
//            }
        }
    }];
}

- (void)getAlbums{
    [self.photoLibrary fetchCollectionsWithCompletionBlock:^(NSArray *albums) {
        self.photoAlbums = albums;
        [self setFirstActiveCollection];
        [self preloadAssetsFromActiveCollection];
    }];
}

- (void)setFirstActiveCollection{
    if (self.photoAlbums) {
        self.activeCollection = self.photoAlbums[0];
    }
}

- (void)preloadAssetsFromActiveCollection{
    [self updateTitle];
    [self.activeCollection preloadPicWithCompletion:^{
        [self.collectionView reloadData];
    }];
}

#pragma mark - UICollectionViewDelegate and DataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.activeCollection.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (!indexPath.row) {
        UGCPhotoListPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cameraCell" forIndexPath:indexPath];
        return cell;
    }
    else {
        UGCPhotoListPhotoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
        UGCPhotoAsset *asset = [self.activeCollection objectAtIndex:indexPath.row - 1];
        [asset getThumbImageWithSize:CGSizeMake(self.cellWidth * [UIScreen mainScreen].scale, self.cellWidth * [UIScreen mainScreen].scale) withComplete:^(UIImage *result) {
            cell.imageView.image = result;
        }];
        cell.selectButton.selected = [selectArray containsObject:asset.identifier];
        __weak typeof(self) __weakSelf = self;
        cell.selectButtonClicked = ^(BOOL selected){
            __strong typeof(__weakSelf) __strongSelf = __weakSelf;
            UGCPhotoAsset *selectAsset = [__strongSelf.activeCollection objectAtIndex:indexPath.row - 1];
            if (selected) {
                return [__weakSelf selectAsset:selectAsset];
            }
            else {
                return [__weakSelf deselectAsset:selectAsset];
            }
            
        };
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    if (!indexPath.row) {
        if (self.ucHelper.currentSelectedPhotos.count == self.maxnum) {
        } else {
            [self openCamera];
        }
        return;
    }
    [self showPreviewWithIndex:@(indexPath.row - 1)];
}


- (BOOL)selectAsset:(UGCPhotoAsset *)asset{
    if (self.ucHelper.currentSelectedPhotos.count == self.maxnum) {
        [self showLimitToast];
        return NO;
    };
    UGCPhotoWithExtroInfo *photoInfo = [UGCPhotoWithExtroInfo new];
    photoInfo.photoAsset = asset;
    photoInfo.identifier = asset.identifier;
    [photoInfo.photoAsset getDisplayImagewithComplete:^(UIImage *result) {
        photoInfo.image = result;
    }];
    if (asset.location) {
        CLLocation *loc = asset.location;
        photoInfo.lat = @(loc.coordinate.latitude);
        photoInfo.lng = @(loc.coordinate.longitude);
    }
    [selectArray addObject:asset.identifier];
    [selectPhotoInfos addObject:photoInfo];
    [self.ucHelper selectPhoto:photoInfo];
    return YES;
}

- (BOOL)deselectAsset:(UGCPhotoAsset *)asset{
    UGCPhotoWithExtroInfo *photoInfo = [self.ucHelper photoWithSelectedIdentifier:asset.identifier];
    [selectPhotoInfos removeObject:photoInfo];
    [selectArray removeObject:asset.identifier];
    [self.ucHelper deselectPhoto:photoInfo];
    return YES;
}

#pragma mark UserAction

- (void)openPreview{
    [self showPreviewWithIndex:nil];
}

- (void)openCamera{
    if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
        return;
    }
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus == AVAuthorizationStatusDenied || authStatus == AVAuthorizationStatusRestricted) {
        if (NSFoundationVersionNumber >= NSFoundationVersionNumber_iOS_8_0){
//            NVAlertView alertView = [NVAlertViewController alertViewWithTitle:nil message:@"请允许大众点评访问您的相机"];
//            [alertView addButtonWithTitle:@"取消" action:^{
//            }];
//            [alertView addButtonWithTitle:@"设置" action:^{
//                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
//            }];
//            [alertView show];
            return;
        }
        else {
//            NVAlertView alertView = [NVAlertViewController alertViewWithTitle:nil message:@"请在iPhone的“设置－隐私－相机”选项中，允许大众点评访问您的相机"];
//            [alertView addButtonWithTitle:@"确定" action:^{
//            }];
//            [alertView show];
            return;
        }
        
    }
    UIImagePickerController *camaraPicker = [NSClassFromString(@"NVImagePickerViewController") new];
    camaraPicker.delegate = self;
    camaraPicker.allowsEditing = NO;
    camaraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:camaraPicker animated:YES completion:nil];
}

- (void)popSelfWithAnimation:(NSNumber *)animation{
    NSInteger index = [self.navigationController.viewControllers indexOfObject:self];
    [self.navigationController popToViewController:self.navigationController.viewControllers[index - 1] animated:[animation boolValue]];
}

- (void)nextButtonClicked{
    [self processImage];
    BOOL needDelay = NO;
    for (UGCPhotoWithExtroInfo *extroinfo in self.ucHelper.currentSelectedPhotos){
        if (![extroinfo.remoteUrl absoluteString].length && !extroinfo.image) {
            needDelay = YES;
            break;
        }
    }
    if (needDelay) {
        self.nextButton.userInteractionEnabled = NO;
//        [self showWaitingNoCancel:@"正在处理图片" voidKeyboard:NO];
        [self performSelector:@selector(jumpToNextpage) withObject:nil afterDelay:1];
        return;
    }
    [self jumpToNextpage];
}

- (void)jumpToNextpage{
//    [self hideNotify];
    if (self.nextBlock){
        self.nextBlock();
        [[NSNotificationCenter defaultCenter] postNotificationName:UGCPhotoListSubmitNotification object:nil];
        return;
    }
    NSString *nextPath = self.next;
    if (nextPath.length) {
        [self popSelfWithAnimation:@(NO)];
//        [[NVNavigator navigator] openURLString:nextPath];
    } else {
        [self popSelfWithAnimation:@(YES)];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:UGCPhotoListSubmitNotification object:nil];
}

- (void)showPreviewWithIndex:(NSNumber *)index{
    if (!index && !selectArray.count) {
//        [self showSplash:@"请至少选择一张图片"];
        return;
    }
    UGCPhotoPreviewController *controller = [UGCPhotoPreviewController new];
    if (index) {
        controller.beginIndex = index.integerValue;
        controller.collection = self.activeCollection;
    }
    controller.nextBlock = self.nextBlock;
    controller.nextButtonTitle = self.nextButtonTitle;
    controller.maxNum = self.maxnum;
    controller.next = self.next;
    controller.oldSelectNum = self.oldSelectNum;
    controller.listVC = self;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    __weak typeof(self) __weakSelf = self;
    [self dismissViewControllerAnimated:YES completion:^{
        __strong typeof(__weakSelf) __strongSelf = __weakSelf;
        UGCPhotoWithExtroInfo *photoInfo = [UGCPhotoWithExtroInfo new];
        photoInfo.identifier = @"identifier";
        [photoInfo saveImageToFilePathWithImage:info[UIImagePickerControllerOriginalImage]];
        [__strongSelf.ucHelper selectPhoto:photoInfo];
        [__strongSelf nextButtonClicked];
    }];
}

- (void)processImage{
//    [self.parameterPraser processImage];
}



@end
