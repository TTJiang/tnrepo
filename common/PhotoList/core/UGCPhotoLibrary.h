//
//  UGCPhotoLibrary.h
//  HelloPhotoLibrary
//
//  Created by Johnny on 15/7/12.
//  Copyright © 2015年 Johnny. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class UGCPhotoCollection;


typedef enum : NSInteger {
    UGCPLAuthorizationStatusNotDetermined = 0,
    UGCPLAuthorizationStatusDenied,
    UGCPLAuthorizationStatusAuthorized
} UGCPLAuthorizationStatus;

@interface UGCPhotoLibrary : NSObject

@property (nonatomic, readonly) UGCPhotoCollection *dianpingAlbum;

+ (instancetype)sharePhotoLibrary;

+ (UGCPLAuthorizationStatus)authorizationStatus;

+ (void)requestAuthorizationWithCompletionBlock:(void(^)(BOOL))completion;

- (void)fetchCollectionsWithCompletionBlock:(void(^)(NSArray *))completion;

- (void)createAlbumWithName:(NSString *)name withCallBack:(void(^)(BOOL result))callback;

- (void)saveImageToDianpingAlbum:(UIImage *)image WithCallBack:(void(^)(BOOL result))callback;

@end
