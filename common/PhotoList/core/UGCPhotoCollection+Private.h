//
//  UGCPhotoCollection+Private.h
//  HelloPhotoLibrary
//
//  Created by Johnny on 15/7/12.
//  Copyright © 2015年 Johnny. All rights reserved.
//

#import "UGCPhotoCollection.h"

@interface UGCPhotoCollection (Private)

@property (nonatomic, strong) ALAssetsGroup *assetsGroup;
@property (nonatomic, strong) PHAssetCollection *assetCollection;

- (instancetype)initWithCollectionObject:(id)object;

@end
