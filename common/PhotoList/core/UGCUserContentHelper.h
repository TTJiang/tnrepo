//
//  UGCUserContentHelper.h
//  Pods
//
//  Created by Johnny on 15/8/2.
//
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    UGCContentTypeShopPhoto = 0,
    UGCContentTypeCommunityPhoto,
    UGCContentTypeReview,
    UGCContentTypeCheckIn,
} UGCContentType;

extern NSString * const UGCUserContentHelperSelectPhotoChangedNotification;
extern NSString * const UGCUserContentUploadFinishedNotification;

@class UGCPhotoWithExtroInfo;
@class UGCDraft;

@interface UGCUserContentHelper : NSObject

@property (nonatomic)         NSArray   *currentSelectedPhotos;
@property (nonatomic, assign) NSInteger  maxSelectNum;

+ (instancetype)shareInstance;

- (void)createNewPhotoSelectContextAndRemoveOldOneWithMaxNum:(NSInteger)maxNum;
- (void)invalidateSelectPhotoContext;

- (void)pushUserContent:(id)ugcobject;
- (void)popUserContent;
- (id)topActiveUserContentWithObjectClass:(Class)class;

- (void)selectPhoto:(UGCPhotoWithExtroInfo *)extroInfo;
- (void)deselectPhoto:(UGCPhotoWithExtroInfo *)extroinfo;
- (void)notifyPicChanged;
- (void)addPhotosToContextFromArray:(NSArray *)array;
- (void)deletePhotosFromContextInArray:(NSArray *)array;
- (UGCPhotoWithExtroInfo *)photoWithSelectedIdentifier:(NSString *)key;

+ (NSString *)imageDraftPath;
+ (NSString *)userContentDraftPath;

- (void)addDraft:(UGCDraft *)draft;
- (void)removeDraft:(UGCDraft *)draft;
- (void)updateDraft:(UGCDraft *)draft;
- (NSArray *)draftWithId:(NSString *)identifier andContentType:(NSNumber *)type andUserSaved:(NSNumber *)saved;
- (UGCDraft *)draftWithDraftid:(NSString *)draftid;



@end
