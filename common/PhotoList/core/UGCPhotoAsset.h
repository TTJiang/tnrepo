//
//  UGCPhotoAsset.h
//  HelloPhotoLibrary
//
//  Created by Johnny on 15/7/12.
//  Copyright © 2015年 Johnny. All rights reserved.
//

#import "UGCModel.h"
#import <UIKit/UIKit.h>

@class UIImage;
@class CLLocation;

@interface UGCPhotoAsset : UGCModel

@property (nonatomic, readonly) BOOL          isVideo;
@property (nonatomic, readonly) CLLocation   *location;
@property (nonatomic, readonly) NSString     *identifier;
@property (nonatomic, readonly) NSDictionary *exif;

- (void)getThumbImageWithSize:(CGSize)size withComplete:(void(^)(UIImage *))resultBlock;
- (void)getDisplayImagewithComplete:(void(^)(UIImage *))resultBlock;
- (UIImage *)getDisplayImage;
- (void)getImagewithComplete:(void (^)(UIImage *))resultBlock;
- (UIImage *)getImage;

@end
