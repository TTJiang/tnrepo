//
//  UGCPhotoLibraryTools.m
//  HelloPhotoLibrary
//
//  Created by Johnny on 15/7/12.
//  Copyright © 2015年 Johnny. All rights reserved.
//

#import "UGCPhotoLibraryTools.h"

@implementation UGCPhotoLibraryTools

+ (BOOL)photoFrameworkIsValid{
    return NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1;
}

@end
