//
//  UGCPhotoWithExtroInfo.m
//  Pods
//
//  Created by Johnny on 15/8/2.
//
//

#import "UGCPhotoWithExtroInfo.h"
#import "UGCPhotoTag.h"

@implementation UGCPhotoWithExtroInfo

- (NSString *)title{
    for (UGCPhotoTag *tag in self.tags) {
        if (tag.type == UGCPhotoTagTypeKind) {
            return [tag valueForKey:kUGCPhotoTagKeyKindName];
        }
    }
    return nil;
}

- (NSString *)identifier{
    if (!_identifier) {
        _identifier = @"identifier";
    }
    return _identifier;
}

@end
