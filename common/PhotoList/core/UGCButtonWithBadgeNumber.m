//
//  UGCButtonWithBadgeNumber.m
//  Pods
//
//  Created by johnny on 15/8/7.
//
//

#import "UGCButtonWithBadgeNumber.h"
#import "UIView+Layout.h"

@interface UGCButtonWithBadgeNumber ()

@property (nonatomic, strong, readwrite) UGCBadgeNumberLabel *badgeLabel;

@end

@implementation UGCButtonWithBadgeNumber

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        _badgeLabel = [UGCBadgeNumberLabel new];
        [self addSubview:_badgeLabel];
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    [self.badgeLabel sizeToFit];
    self.badgeLabel.height += 6;
    self.badgeLabel.width = MAX(self.badgeLabel.height, self.badgeLabel.width + 10);
    self.badgeLabel.hidden = !self.badgeLabel.text.length;
    self.badgeLabel.center = self.labelCenterPoint;
}

- (void)setBadgeText:(NSString *)text WithAnimation:(BOOL)animation{
    [self.badgeLabel setText:text WithAnimation:NO];
    [self layoutIfNeeded];
    [self.badgeLabel setText:text WithAnimation:animation];
}



@end
