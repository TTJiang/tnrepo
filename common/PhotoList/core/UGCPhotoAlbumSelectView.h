//
//  UGCPhotoAlbumSelectView.h
//  Pods
//
//  Created by johnny on 15/8/23.
//
//

#import <UIKit/UIKit.h>

@class UGCPhotoCollection;

@interface UGCPhotoAlbumSelectView : UIView

@property (nonatomic, strong) NSArray *albums;
@property (nonatomic, copy) void(^photoAlbumSelected)(UGCPhotoCollection *collection);

@end
