//
//  UGCSticker.h
//  Pods
//
//  Created by 薛琳 on 15/9/15.
//
//

#import <UIKit/UIKit.h>
#import "UGCModel.h"

@interface UGCSticker : UGCModel
<NSCopying>
@property (nonatomic, strong) NSString  *identifier;
@property (nonatomic, assign) CGFloat   originWidth;
@property (nonatomic, assign) CGFloat   originHeight;
//Value from 0 - 1.
@property (nonatomic, assign) CGFloat   relativeWidth;
@property (nonatomic, assign) CGFloat   relativeHeight;

@property (nonatomic, assign) CGFloat   scaleFactor;
@property (nonatomic, assign) CGPoint   position;
@property (nonatomic, assign) CGFloat   angle;
//@property (nonatomic, strong) UGCDecal  *decal;

@end
