//
//  UGCPhotoPreviewCell.h
//  Pods
//
//  Created by Johnny on 15/8/9.
//
//

#import <UIKit/UIKit.h>

@interface UGCPhotoPreviewCell : UICollectionViewCell

@property (nonatomic, readonly) UIImageView *imageView;
@property (nonatomic, strong) BOOL (^selectButtonClicked)(BOOL);
@property (nonatomic, readonly) UIButton *selectButton;

@end
