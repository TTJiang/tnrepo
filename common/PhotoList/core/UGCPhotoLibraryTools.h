//
//  UGCPhotoLibraryTools.h
//  HelloPhotoLibrary
//
//  Created by Johnny on 15/7/12.
//  Copyright © 2015年 Johnny. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UGCPhotoLibraryTools : NSObject

+ (BOOL)photoFrameworkIsValid;

@end
