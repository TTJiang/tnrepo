//
//  UGCBadgeNumberLabel.h
//  Pods
//
//  Created by johnny on 15/8/7.
//
//

#import <UIKit/UIKit.h>

@interface UGCBadgeNumberLabel : UILabel

@property (nonatomic, strong) UIColor *borderColor;
@property (nonatomic, assign) CGFloat borderWidth;

- (void)setText:(NSString *)text WithAnimation:(BOOL)animation;

@end
