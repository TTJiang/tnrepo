//
//  UGCPhotoTag.h
//  Pods
//
//  Created by Johnny on 15/8/1.
//
//

#import "UGCModel.h"

#define kUGCPhotoTagKeyPOIName       @"UGCPhotoTagKeyPOIName"
#define kUGCPhotoTagKeyShopID        @"UGCPhotoTagKeyShopID"        //NSNumber
#define kUGCPhotoTagKeyInterestName  @"UGCPhotoTagKeyInterestName"
#define kUGCPhotoTagKeyPasterName    @"UGCPhotoTagKeyPasterName"




#define kUGCPhotoTagXPos             @"kUGCPhotoTagXPos"
#define kUGCPhotoTagYPos             @"kUGCPhotoTagYPos"
#define kUGCPhotoTagTitle            @"kUGCPhotoTagTitle"
#define kUGCPhotoTagDirection        @"kUGCPhotoTagDirection"       //NVTagDirection

#define kUGCPhotoTagType             @"kUGCPhotoTagType"
#define kUGCPhotoTagDisable          @"kUGCPhotoTagDisable"
#define kUGCPhotoTagPOIValue         @"kUGCPhotoTagPOIValue"
#define kUGCPhotoTagInterestValue    @"kUGCPhotoTagInterestValue"
#define kUGCPhotoTagKeyKindName      @"UGCPhotoTagKeyKindName"
#define kUGCPhotoTagKeyKindValue     @"kUGCPhotoTagKeyKindValue"
#define kUGCPhotoTagKeyDishPrice     @"kUGCPhotoTagKeyDishPrice"
#define kUGCPhotoTagKeyPasterValue     @"kUGCPhotoTagKeyPasterValue"

#define kUGCPhotoInfo                @"kUGCPhotoInfo"



typedef enum : NSUInteger {
    UGCPhotoTagTypePOI = 0,      //keys : kUGCPhotoTagKeyPOIName, kUGCPhotoTagKeyShopID
    UGCPhotoTagTypeInterest,     //keys : kUGCPhotoTagKeyInterestName
    UGCPhotoTagTypeKind,
    UGCPhotoTagTypePaster, //
} UGCPhotoTagType;

@interface UGCPhotoTag : UGCModel

@property (nonatomic, assign) UGCPhotoTagType type;
@property (nonatomic, strong) NSArray *subTags;
@property (nonatomic, readonly) UGCPhotoTag *superTag;

@end
