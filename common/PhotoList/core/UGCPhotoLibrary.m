//
//  UGCPhotoLibrary.m
//  HelloPhotoLibrary
//
//  Created by Johnny on 15/7/12.
//  Copyright © 2015年 Johnny. All rights reserved.
//

#import "UGCPhotoLibrary.h"
#import <Photos/Photos.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "UGCPhotoLibraryTools.h"
#import "UGCPhotoCollection+Private.h"

static UGCPhotoLibrary *_photoLibrary;

@interface UGCPhotoLibrary ()

@property (nonatomic, strong)            ALAssetsLibrary    *assertLibrary;
@property (nonatomic, strong, readwrite) UGCPhotoCollection *dianpingAlbum;

@end

@implementation UGCPhotoLibrary

#pragma mark -- property 


+ (instancetype)sharePhotoLibrary{
    @synchronized(self) {
        if (_photoLibrary) return _photoLibrary;
        else {
            UGCPhotoLibrary *library = [self new];
            _photoLibrary = library;
            return library;
        }
    }
}

+ (UGCPLAuthorizationStatus)authorizationStatus{
    if ([UGCPhotoLibraryTools photoFrameworkIsValid]) {
        switch ([PHPhotoLibrary authorizationStatus]) {
            case PHAuthorizationStatusDenied:
                return UGCPLAuthorizationStatusDenied;
                break;
            case PHAuthorizationStatusRestricted:
                return UGCPLAuthorizationStatusDenied;
                break;
            case PHAuthorizationStatusAuthorized:
                return UGCPLAuthorizationStatusAuthorized;
                break;
            default:
                return UGCPLAuthorizationStatusNotDetermined;
                break;
        }
    }
    switch ([ALAssetsLibrary authorizationStatus]) {
        case ALAuthorizationStatusDenied:
            return UGCPLAuthorizationStatusDenied;
            break;
        case ALAuthorizationStatusRestricted:
            return UGCPLAuthorizationStatusDenied;
            break;
        case ALAuthorizationStatusAuthorized:
            return UGCPLAuthorizationStatusAuthorized;
            break;
        default:
            return UGCPLAuthorizationStatusNotDetermined;
            break;
    }
    
}

- (instancetype)init{
    if (self = [super init]) {
        if ([UGCPhotoLibraryTools photoFrameworkIsValid]){
        }
        else {
            _assertLibrary = [ALAssetsLibrary new];
        }
    }
    return self;
}

+ (void)requestAuthorizationWithCompletionBlock:(void(^)(BOOL))completion{
    if ([self authorizationStatus] == UGCPLAuthorizationStatusNotDetermined) {
        if ([UGCPhotoLibraryTools photoFrameworkIsValid]) {
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                completion(status == PHAuthorizationStatusAuthorized);
            }];
        }
        else {
            ALAssetsLibrary *library = [ALAssetsLibrary new];
            [library enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                *stop = YES;
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES);
                });
            } failureBlock:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(NO);
                });
            }];
        }
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        completion([self authorizationStatus] == UGCPLAuthorizationStatusAuthorized);
    });
}

- (void)fetchCollectionsWithCompletionBlock:(void(^)(NSArray *))completion{
    if ([UGCPhotoLibraryTools photoFrameworkIsValid]) {
        [self performSelectorInBackground:@selector(fetchPhotoFrameworkCollectionInBackground:) withObject:completion];
        return;
    }
    else {
        NSMutableArray *array = [NSMutableArray array];
        [self.assertLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos|ALAssetsGroupAlbum usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
            if (!group) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UGCPhotoCollection *defaultCollectiion = [array firstObject];
                    NSMutableArray *newResult = [NSMutableArray new];
                    for (UGCPhotoCollection *collection in array) {
                        if (collection.count) {
                            [newResult addObject:collection];
                        }
                    }
                    if (!newResult.count) {
                        [newResult addObject:defaultCollectiion];
                    }
                    completion([NSArray arrayWithArray:newResult]);
                });
                return;
            }
            UGCPhotoCollection *collection = [[UGCPhotoCollection alloc] initWithCollectionObject:group];
            if ([collection.name isEqualToString:@"大众点评"]) {
                self.dianpingAlbum = collection;
            }
            [array insertObject:collection atIndex:0];
        } failureBlock:^(NSError *error) {
            NSMutableArray *resultArray = [NSMutableArray new];
            NSEnumerator *enumrator = [array reverseObjectEnumerator];
            id object = nil;
            while (object) {
                [resultArray addObject:object];
                object = enumrator.nextObject;
            }
            completion([NSArray arrayWithArray:array]);
        }];
    }
}

- (void)fetchPhotoFrameworkCollectionInBackground:(void(^)(NSArray *))completion{
    NSMutableArray *array = [NSMutableArray array];
    PHFetchResult *result = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumUserLibrary options:nil];
    [result enumerateObjectsUsingBlock:^(PHAssetCollection *obj, NSUInteger idx, BOOL * __nonnull stop) {
        [array addObject:[[UGCPhotoCollection alloc] initWithCollectionObject:obj]];
    }];
    result = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum subtype:PHAssetCollectionSubtypeAlbumRegular options:nil];
    [result enumerateObjectsUsingBlock:^(PHAssetCollection *obj, NSUInteger idx, BOOL * __nonnull stop) {
        if (!obj.estimatedAssetCount) return;
        UGCPhotoCollection *collection = [[UGCPhotoCollection alloc] initWithCollectionObject:obj];
        if ([collection.name isEqualToString:@"大众点评"]) {
            self.dianpingAlbum = collection;
        }
        [array addObject:collection];
    }];
    dispatch_async(dispatch_get_main_queue(), ^{
        completion([NSArray arrayWithArray:array]);
    });
}

- (void)createAlbumWithName:(nonnull NSString *)name withCallBack:(nonnull void(^)(BOOL result))callback{
    if (self.assertLibrary) {
        [self.assertLibrary addAssetsGroupAlbumWithName:name resultBlock:^(ALAssetsGroup *group) {
            if (callback) {
                callback(group?YES:NO);
            }
        } failureBlock:^(NSError *error) {
            if (callback) {
                callback(NO);
            }
        }];
    }
    else {
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            PHAssetCollectionChangeRequest *request = [PHAssetCollectionChangeRequest creationRequestForAssetCollectionWithTitle:name];
            self.dianpingAlbum = [[UGCPhotoCollection alloc] initWithCollectionObject:request.placeholderForCreatedAssetCollection.localIdentifier];
    
        } completionHandler:^(BOOL success, NSError * _Nullable error) {
            if (callback) {
                callback(success);
            }
        }];
    }
}

- (void)saveImageToDianpingAlbum:(UIImage *)image WithCallBack:(void(^)(BOOL result))callback{
    CFRunLoopRef callRunLoop = CFRunLoopGetCurrent();
    if (!self.dianpingAlbum) {
        [self fetchCollectionsWithCompletionBlock:^(NSArray *collections) {
            CFRunLoopPerformBlock(callRunLoop, NSDefaultRunLoopMode, ^{
                if (!self.dianpingAlbum) {
                    [self createAlbumWithName:@"大众点评" withCallBack:^(BOOL result) {
                        if (!self.dianpingAlbum) {
                            callback(NO);
                            return;
                        }
                        [self saveImageToDianpingAlbum:image WithCallBack:callback];
                    }];
                    return;
                }
                [self saveImageToDianpingAlbum:image WithCallBack:callback];
            });
        }];
        return;
    }
    if (self.assertLibrary) {
        [self.assertLibrary writeImageToSavedPhotosAlbum:image.CGImage orientation:(ALAssetOrientation)image.imageOrientation completionBlock:^(NSURL *assetURL, NSError *error) {
            CFRunLoopPerformBlock(callRunLoop, NSDefaultRunLoopMode, ^{
                if (error) {
                    callback(NO);
                    return;
                }
                
                [self.assertLibrary assetForURL:assetURL resultBlock:^(ALAsset *asset) {
                    if (!assetURL) {
                        callback(NO);
                        return;
                    }
                    callback([self.dianpingAlbum.assetsGroup addAsset:asset]);
                } failureBlock:^(NSError *aserror) {
                    callback(NO);
                }];
            });
            
        }];
    } else {
        [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
            PHAssetChangeRequest *assetsRequest = [PHAssetChangeRequest creationRequestForAssetFromImage:image];
            PHAssetCollectionChangeRequest *request = [PHAssetCollectionChangeRequest changeRequestForAssetCollection:self.dianpingAlbum.assetCollection];
            [request addAssets:@[assetsRequest.placeholderForCreatedAsset]];
        } completionHandler:^(BOOL success, NSError * _Nullable error) {
            CFRunLoopPerformBlock(callRunLoop, NSDefaultRunLoopMode, ^{
                callback(success);
            });
        }];
    }
}

@end
