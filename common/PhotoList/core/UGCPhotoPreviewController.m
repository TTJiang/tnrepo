//
//  UGCPhotoPreviewController.m
//  Pods
//
//  Created by johnny on 15/7/13.
//
//

#import "UGCPhotoPreviewController.h"
#import "UGCPhotoPreviewCell.h"
#import "UIView+Layout.h"
//#import "EXTScope.h"
#import "UGCUserContentHelper.h"
#import "UGCPhotoWithExtroInfo.h"
#import "UGCButtonWithBadgeNumber.h"
#import "UIColor+Ext.h"
//#import "UIBarButtonItem+Addition.h"
#import "UGCPhotoListViewController.h"

@interface UGCPhotoPreviewController ()<UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, weak)   UGCUserContentHelper *ucHelper;
@property (nonatomic, strong) NSMutableArray *logicDeleted;
@property (nonatomic, strong) UGCButtonWithBadgeNumber *nextButton;

@end

@implementation UGCPhotoPreviewController

extern NSMutableArray *selectArray;
extern NSMutableArray *selectPhotoInfos;

- (instancetype)init{
    if (self = [super init]) {
        _logicDeleted = [NSMutableArray new];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectPicCountChange) name:UGCUserContentHelperSelectPhotoChangedNotification object:nil];
    }
    return self;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    
    self.collectionView.frame = self.view.bounds;
    
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = self.view.size;
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = layout.minimumInteritemSpacing;
    self.collectionView.collectionViewLayout = layout;
    
    if (self.beginIndex) {
        [self scrollToIndex:@(self.beginIndex)];
        self.beginIndex = 0;
    }
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:[UICollectionViewFlowLayout new]];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.pagingEnabled = YES;
    [self.collectionView registerClass:[UGCPhotoPreviewCell class] forCellWithReuseIdentifier:@"photoCell"];
    self.collectionView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.collectionView];
    
    [self addNavigationBarItem];
    
    self.ucHelper = [UGCUserContentHelper shareInstance];
}

- (void)addNavigationBarItem{
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
    button.titleLabel.font = font;
    button.titleLabel.minimumScaleFactor = 8.f / 15.f;
    [button setTitle:@"返回" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    button.exclusiveTouch = YES;
    CGSize size = [self sizeWithstring:@"返回" font:font constrainedToSize:CGSizeMake(140, 34) lineBreakMode:NSLineBreakByWordWrapping];
    button.frame = CGRectMake(0, 0, size.width, size.height);
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    self.navigationItem.leftBarButtonItem = item;
    
    self.nextButton = [UGCButtonWithBadgeNumber new];
    self.nextButton.size = CGSizeMake(60, 30);
    self.nextButton.labelCenterPoint = CGPointMake(60, 5);
    self.nextButton.titleLabel.font = [UIFont boldSystemFontOfSize:11];
    self.nextButton.badgeLabel.font = [UIFont systemFontOfSize:13];
    [self.nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.nextButton setTitle:self.nextButtonTitle.length?self.nextButtonTitle:@"下一步" forState:UIControlStateNormal];
    [self.nextButton setBackgroundImage:[self getPureColorImageWithSize:self.nextButton.size andColor:[UIColor colorWithHexString:@"fc673d"] andCornerRadius:2] forState:UIControlStateNormal];
    [self.nextButton setBackgroundImage:[self getPureColorImageWithSize:self.nextButton.size andColor:[UIColor grayColor] andCornerRadius:2] forState:UIControlStateDisabled];
    
    [self.nextButton addTarget:self action:@selector(nextButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    self.nextButton.badgeLabel.backgroundColor = [UIColor colorWithHexString:@"6cc26a"];
    self.nextButton.badgeLabel.textColor = [UIColor whiteColor];
    self.nextButton.badgeLabel.borderColor = [UIColor whiteColor];
    self.nextButton.badgeLabel.borderWidth = 2;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.nextButton];
}

-(CGSize)sizeWithstring:(NSString *)string font:(UIFont *)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)mode
{
    if (!self || string.length == 0) {
        return CGSizeZero;
    }
    NSDictionary * attrs = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    CGSize sbSize = [string boundingRectWithSize:size options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
    sbSize.height = ceilf(sbSize.height);
    sbSize.width = ceilf(sbSize.width);
    return sbSize;
}

- (UIImage *)getPureColorImageWithSize:(CGSize)size andColor:(UIColor *)color andCornerRadius:(CGFloat)radius{
    UIGraphicsBeginImageContextWithOptions(size, NO, [UIScreen mainScreen].scale);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRoundedRect(path, NULL, CGRectMake(0, 0, size.width, size.height), radius, radius);
    CGContextAddPath(ctx, path);
    CGPathRelease(path);
    CGContextSetFillColorWithColor(ctx, color.CGColor);
    CGContextFillPath(ctx);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self selectPicCountChange];
}
                                             
- (void)back{
    if ([self isSHowFromPreviewButton]) {
        for (UGCPhotoWithExtroInfo *photoInfo in self.logicDeleted) {
            [photoInfo deleteImageFromFilePath];
            [selectArray removeObject:photoInfo.identifier];
            [self.ucHelper deselectPhoto:photoInfo];
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - ColllectionView delegate and dataSource;

- (void)scrollToIndex:(NSNumber *)index{
    [self.collectionView setContentOffset:CGPointMake(index.integerValue * self.view.width, 0)];
    [self performSelector:@selector(updateTitle) withObject:nil afterDelay:0.1];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if ([self isSHowFromPreviewButton]) return selectArray.count;
    return self.collection.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UGCPhotoPreviewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    if ([self isSHowFromPreviewButton]) {
        UGCPhotoWithExtroInfo *photoInfo = [self.ucHelper photoWithSelectedIdentifier:selectArray[indexPath.row]];
        cell.imageView.image = photoInfo.image;
        cell.selectButton.selected = ![self.logicDeleted containsObject:photoInfo];
        __weak typeof(self) __weakSelf = self;
        cell.selectButtonClicked = ^(BOOL selected){
            __strong typeof(__weakSelf) __strongSelf =__weakSelf;
            if (selected) {
                [__strongSelf.logicDeleted removeObject:photoInfo];
            } else {
                [__strongSelf.logicDeleted addObject:photoInfo];
            }
            [__strongSelf selectPicCountChange];
            return YES;
        };

    }
    else {
        UGCPhotoAsset *asset = [self.collection objectAtIndex:indexPath.row];
        [asset getDisplayImagewithComplete:^(UIImage *result) {
            if ([[collectionView indexPathForCell:cell] isEqual:indexPath]) {
                cell.imageView.image = result;
            }
        }];
        cell.selectButton.selected = [selectArray containsObject:asset.identifier];
        __weak typeof(self) __weakSelf = self;
        cell.selectButtonClicked = ^(BOOL selected){
            __strong typeof(__weakSelf) __strongSelf =__weakSelf;
            UGCPhotoAsset *selectAsset = [__strongSelf.collection objectAtIndex:indexPath.row];
            if (selected) {
                return [__strongSelf selectAsset:selectAsset];
            }
            else {
                return [__strongSelf deselectAsset:selectAsset];
            }
            
        };
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self updateTitle];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (!decelerate) {
        [self updateTitle];
    }
}

- (void)updateTitle{
    NSInteger index = [self.collectionView indexPathForCell:self.collectionView.visibleCells.firstObject].row + 1;
    if ([self isSHowFromPreviewButton]) {
        self.title = [NSString stringWithFormat:@"%@/%@", @(index), @(selectArray.count)];
    } else {
        self.title = [NSString stringWithFormat:@"%@/%@", @(index), @(self.collection.count)];
    }
}

- (BOOL)selectAsset:(UGCPhotoAsset *)asset{
    if (self.ucHelper.currentSelectedPhotos.count == self.maxNum) {
        [self showLimitToast];
        return NO;
    };
    UGCPhotoWithExtroInfo *photoInfo = [UGCPhotoWithExtroInfo new];
    photoInfo.photoAsset = asset;
    photoInfo.identifier = asset.identifier;
    photoInfo.image = [asset getDisplayImage];
    [selectArray addObject:asset.identifier];
    [self.ucHelper selectPhoto:photoInfo];
    [selectPhotoInfos addObject:photoInfo];
    return YES;
}

- (void)selectPicCountChange{
    NSInteger selectCount = [self isSHowFromPreviewButton]?selectArray.count- self.logicDeleted.count:selectArray.count;
    if (!selectCount) {
        self.nextButton.enabled = NO;
        [self.nextButton setBadgeText:nil WithAnimation:NO];
        return;
    }
    self.nextButton.enabled = YES;
    [self.nextButton setBadgeText:[@(selectCount) stringValue] WithAnimation:YES];
}

- (BOOL)deselectAsset:(UGCPhotoAsset *)asset{
    UGCPhotoWithExtroInfo *photoInfo = [self.ucHelper photoWithSelectedIdentifier:asset.identifier];
    [photoInfo deleteImageFromFilePath];
    [selectArray removeObject:asset.identifier];
    [selectPhotoInfos removeObject:photoInfo];
    [self.ucHelper deselectPhoto:photoInfo];
    return YES;
}

- (void)popSelfWithAnimation:(NSNumber *)animation{
    NSInteger index = [self.navigationController.viewControllers indexOfObject:self];
    [self.navigationController popToViewController:self.navigationController.viewControllers[index - 2] animated:[animation boolValue]];
}

- (void)nextButtonClicked{
    if ([self isSHowFromPreviewButton]) {
        for (UGCPhotoWithExtroInfo *photoInfo in self.logicDeleted) {
            [photoInfo deleteImageFromFilePath];
            [self.ucHelper deselectPhoto:photoInfo];
        }
    }
    [self.listVC processImage];
    
    BOOL needDelay = NO;
    for (UGCPhotoWithExtroInfo *extroinfo in self.ucHelper.currentSelectedPhotos){
        if (![extroinfo.remoteUrl absoluteString].length && !extroinfo.image) {
            needDelay = YES;
            break;
        }
    }
    if (needDelay) {
        self.nextButton.userInteractionEnabled = NO;
//        [self showWaitingNoCancel:@"正在处理图片" voidKeyboard:NO];
        [self performSelector:@selector(jumpToNextpage) withObject:nil afterDelay:1];
        return;
    }
    [self jumpToNextpage];
}

- (void)jumpToNextpage{
    if (self.nextBlock){
        [self.navigationController popViewControllerAnimated:NO];
        self.nextBlock();
        return;
    }
    NSString *nextPath = self.next;
    if (nextPath.length) {
        [self popSelfWithAnimation:@(NO)];
//        [[NVNavigator navigator] openURLString:nextPath];
    } else {
        [self popSelfWithAnimation:@(YES)];
    }
}

- (BOOL)isSHowFromPreviewButton{
    return !self.collection;
}

- (void)showLimitToast{
    if (self.ucHelper.currentSelectedPhotos.count) {
//        [self showSplash:[NSString stringWithFormat:@"最多可以选择上传%@张图片", @(self.maxNum - self.oldSelectNum)]];
    }
}

@end
