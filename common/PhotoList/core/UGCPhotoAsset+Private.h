//
//  UGCPhotoAsset+Private.h
//  HelloPhotoLibrary
//
//  Created by Johnny on 15/7/12.
//  Copyright © 2015年 Johnny. All rights reserved.
//

#import "UGCPhotoAsset.h"

@interface UGCPhotoAsset (Private)

- (instancetype)initWithAsset:(id)object;

@end
