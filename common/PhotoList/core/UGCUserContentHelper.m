//
//  UGCUserContentHelper.m
//  Pods
//
//  Created by Johnny on 15/8/2.
//
//

#import "UGCUserContentHelper.h"
//#import "NVAccountHelper.h"
#import "UGCPhotoWithExtroInfo.h"
//#import "NSString+UUID.h"
//#import "UGCDraftIndex.h"

static UGCUserContentHelper *__contentHelper;
static dispatch_once_t _onceFlag;

NSString *  const UGCUserContentHelperSelectPhotoChangedNotification = @"UGCUserContentHelperSelectPhotoChangedNotification";
NSString * const UGCUserContentUploadFinishedNotification = @"UGCUserContentUploadFinishedNotification";

@interface UGCUserContentHelper ()

@property (nonatomic, strong) NSMutableArray       *selectedPhotos;
@property (nonatomic, strong) NSMutableDictionary  *selectPhotosDic;
@property (nonatomic, strong) NSMutableArray       *ugcObjectStack;
//@property (nonatomic, strong) UGCDraftIndex        *draftIndex;
@property (nonatomic, assign) NSInteger             currentDraftVersion;

@end


@implementation UGCUserContentHelper

+ (instancetype)shareInstance{
    if (!__contentHelper) {
        dispatch_once(&_onceFlag, ^{
            __contentHelper = [self new];
        });
    }
    return __contentHelper;
}

- (instancetype)init{
    if (self = [super init]) {
        _ugcObjectStack = [NSMutableArray new];
        _currentDraftVersion = 1;
    }
    return self;
}

#pragma mark const value

- (NSString *)userContentpath{
    NSString *documentpath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *userContentPath = [documentpath stringByAppendingPathComponent:@"UserContent"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:userContentPath]) {
        [fileManager createDirectoryAtPath:userContentPath withIntermediateDirectories:YES attributes:nil error:NULL];
    }
    return userContentPath;
}


- (NSString *)publicPath{
    NSString *userContentpath = [self userContentpath];
    NSString *publicPath = [userContentpath stringByAppendingPathComponent:@"Public"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:publicPath]) {
        [fileManager createDirectoryAtPath:publicPath withIntermediateDirectories:YES attributes:nil error:NULL];
    }
    return publicPath;
}

- (NSString *)currentUserDraftPath{
    NSString *userContentpath = [self userContentpath];
//    NSString *userPath = [userContentpath stringByAppendingPathComponent:[@([NVAccountHelper userId]) stringValue]];
    NSString *userPath = [userContentpath stringByAppendingPathComponent:@"path"];

    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:userPath]) {
        [fileManager createDirectoryAtPath:userPath withIntermediateDirectories:YES attributes:nil error:NULL];
    }
    return userPath;
}

- (void)createNewPhotoSelectContextAndRemoveOldOneWithMaxNum:(NSInteger)maxNum{
    [self invalidateSelectPhotoContext];
    self.selectedPhotos = [NSMutableArray new];
    self.selectPhotosDic = [NSMutableDictionary new];
    self.maxSelectNum = maxNum;
}

- (void)invalidateSelectPhotoContext{
    self.selectedPhotos = nil;
    self.selectPhotosDic = nil;
    self.maxSelectNum = 0;
    
}

- (void)pushUserContent:(id)ugcobject{
    if (ugcobject) {
        [self.ugcObjectStack addObject:ugcobject];
    }
}

- (void)popUserContent{
    [self.ugcObjectStack removeLastObject];
}

- (id)topActiveUserContentWithObjectClass:(Class)class{
    id result = [self.ugcObjectStack lastObject];
    if (result && class) {
        if (![result isKindOfClass:class]) return nil;
    }
    return result;
}

- (NSArray *)currentSelectedPhotos{
    if (!self.selectedPhotos) return nil;
    return [NSArray arrayWithArray:self.selectedPhotos];
}

- (void)addPhotosToContextFromArray:(NSArray *)array{
    for (UGCPhotoWithExtroInfo *photoInfo in array) {
        [self.selectedPhotos addObject:photoInfo];
        if (![self.selectPhotosDic objectForKey:photoInfo.identifier]) {
            [self.selectPhotosDic setObject:[NSMutableArray new] forKey:photoInfo.identifier];
        }
        [[self.selectPhotosDic objectForKey:photoInfo.identifier] addObject:photoInfo];
    }
    [self notifyPicChanged];
}

- (void)deletePhotosFromContextInArray:(NSArray *)array{
    for (UGCPhotoWithExtroInfo *photoInfo in array) {
        [self.selectedPhotos removeObject:photoInfo];
        NSMutableArray *photoArray = [self.selectPhotosDic objectForKey:photoInfo.identifier];
        [photoArray removeObject:photoInfo];
    }
    [self notifyPicChanged];
}

- (void)setCurrentSelectedPhotos:(NSArray *)currentSelectedPhotos{
    [self.selectedPhotos removeAllObjects];
    [self.selectPhotosDic removeAllObjects];
    for (UGCPhotoWithExtroInfo *photoInfo in currentSelectedPhotos) {
        [self.selectedPhotos addObject:photoInfo];
        if (![self.selectPhotosDic objectForKey:photoInfo.identifier]) {
            [self.selectPhotosDic setObject:[NSMutableArray new] forKey:photoInfo.identifier];
        }
        [[self.selectPhotosDic objectForKey:photoInfo.identifier] addObject:photoInfo];
    }
    [self notifyPicChanged];
}

- (void)selectPhoto:(UGCPhotoWithExtroInfo *)extroInfo{
    [self.selectedPhotos addObject:extroInfo];
    if (![self.selectPhotosDic objectForKey:extroInfo.identifier]) {
        [self.selectPhotosDic setObject:[NSMutableArray new] forKey:extroInfo.identifier];
    }
    [[self.selectPhotosDic objectForKey:extroInfo.identifier] addObject:extroInfo];
    [self notifyPicChanged];
}

- (void)deselectPhoto:(UGCPhotoWithExtroInfo *)extroinfo{
    if (extroinfo) {
        [self.selectedPhotos removeObject:extroinfo];
        [[self.selectPhotosDic objectForKey:extroinfo.identifier] removeLastObject];
        [self notifyPicChanged];
    }
}

- (void)notifyPicChanged{
    [[NSNotificationCenter defaultCenter] postNotificationName:UGCUserContentHelperSelectPhotoChangedNotification object:nil];
}

- (UGCPhotoWithExtroInfo *)photoWithSelectedIdentifier:(NSString *)key{
    return [[self.selectPhotosDic objectForKey:key] lastObject];
}

+ (NSString *)imageDraftPath{
    NSString *documentpath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    NSString *dratPath = [documentpath stringByAppendingPathComponent:@"ImageDraft"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:dratPath]) {
        [fileManager createDirectoryAtPath:dratPath withIntermediateDirectories:YES attributes:nil error:NULL];
    }
    return dratPath;
}

+ (NSString *)userContentDraftPath{
    NSString *dratPath = [self ugcDraftPath];
//    if ([NVAccountHelper isLogin]) {
//        dratPath = [dratPath stringByAppendingPathComponent:[@([NVAccountHelper userId]) stringValue]];
//    } else {
        dratPath = [dratPath stringByAppendingPathComponent:@"public"];
//    }
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:dratPath]) {
        [fileManager createDirectoryAtPath:dratPath withIntermediateDirectories:YES attributes:nil error:NULL];
    }
    return dratPath;
}

+ (NSString *)ugcDraftPath{
    NSString *documentpath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
    return [documentpath stringByAppendingPathComponent:@"UGCDraft"];
}

//- (UGCDraftIndex *)draftIndex{
//    @synchronized(self) {
//        if (!_draftIndex) {
//            NSString *filePath = [[[self class] ugcDraftPath] stringByAppendingPathComponent:@"draftindex"];
//            NSDictionary *dictionary = [[NSDictionary alloc] initWithContentsOfFile:filePath];
//            if (!dictionary) {
//                _draftIndex = [UGCDraftIndex new];
//                _draftIndex.drafts = [NSMutableArray new];
//                _draftIndex.draftDic = [NSMutableDictionary new];
//                _draftIndex.lastChangeDate = [NSDate date];
//                _draftIndex.version = self.currentDraftVersion;
//                [_draftIndex writeTofile];
//            } else {
//                _draftIndex = [UGCDraftIndex getUGCModelObjectFromPlistObject:dictionary];
//                if (_draftIndex.version < self.currentDraftVersion) {
//                    _draftIndex = nil;
//                }
//                if (!_draftIndex.drafts){
//                    _draftIndex.drafts = [NSMutableArray new];
//                    _draftIndex.draftDic = [NSMutableDictionary new];
//                }
//            }
//        }
//    }
//    return _draftIndex;
//}

- (void)addDraft:(UGCDraft *)draft{
//    return [self.draftIndex addDraft:draft];
}
- (void)removeDraft:(UGCDraft *)draft{
//    return [self.draftIndex removeDraft:draft];
}
- (void)updateDraft:(UGCDraft *)draft{
//    return [self.draftIndex updateDraft:draft];
}

- (NSArray *)draftWithId:(NSString *)identifier andContentType:(NSNumber *)type andUserSaved:(NSNumber *)saved{
//    return [self.draftIndex draftWithId:identifier andContentType:type andUserSaved:saved];
    return nil;
}

- (UGCDraft *)draftWithDraftid:(NSString *)draftid{
//    return [self.draftIndex draftWithDraftid:draftid];
    return nil;
}


@end
