//
//  UGCSticker.m
//  Pods
//
//  Created by 薛琳 on 15/9/15.
//
//

#import "UGCSticker.h"

@implementation UGCSticker

- (NSString *)identifier
{
    if (!_identifier) {
//        _identifier = [NSString NV_uuidString];
        _identifier = @"identifier";
    }
    return _identifier;
}

- (id)copyWithZone:(NSZone *)zone
{
    UGCSticker *newSticker = [[self class] allocWithZone:zone];
    newSticker.originHeight = _originHeight;
    newSticker.originWidth = _originWidth;
    newSticker.identifier = _identifier;
    newSticker.scaleFactor = _scaleFactor;
    newSticker.angle = _angle;
    newSticker.position = _position;
//    newSticker.decal = _decal;
    newSticker.relativeHeight = _relativeHeight;
    newSticker.relativeWidth = _relativeWidth;
    return newSticker;
}

@end
