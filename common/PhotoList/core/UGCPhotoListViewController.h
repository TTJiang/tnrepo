//
//  UGCPhotoListViewController.h
//  Pods
//
//  Created by johnny on 15/7/13.
//
//

#import <UIKit/UIKit.h>

extern NSString * const UGCPhotoListCancelNotification;
extern NSString * const UGCPhotoListSubmitNotification;

@interface UGCPhotoListViewController : UIViewController

@property (nonatomic, strong) NSString                          *next;
@property (nonatomic, assign) NSUInteger                         maxnum;
@property (nonatomic, strong) dispatch_block_t                   nextBlock;
@property (nonatomic, strong) NSString                          *nextButtonTitle;

- (void)processImage;

@end
