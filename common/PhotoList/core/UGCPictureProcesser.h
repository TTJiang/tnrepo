//
//  UGCPictureProcesser.h
//  Pods
//
//  Created by Johnny on 15/11/27.
//
//

#import "UGCModel.h"
#import <UIKit/UIKit.h>

@class UGCPhotoWithExtroInfo;

@interface UGCPictureProcesser : UGCModel

- (UIImage *)processImageWithImageInfo:(UGCPhotoWithExtroInfo *)photoInfo;

- (UIImage *)fixImageOrientationWithInputImage:(UIImage *)inputImage;

- (UIImage *)composeImage:(UIImage *)inputImage WithPhotoInfo:(UGCPhotoWithExtroInfo *)extroInfo;

- (UIImage *)compressedInputImage:(UIImage *)inputImage ToMaxSize:(CGSize)maxSize;

- (UIImage *)cropInputImage:(UIImage *)inputImage WithRect:(CGRect)rect;

@end
