//
//  UGCModel.m
//  Pods
//
//  Created by Johnny on 15/8/1.
//
//

#import "UGCModel.h"
#import <objc/runtime.h>
#import <UIKit/UIKit.h>

NSString * const objectClass = @"__Object__Class__";
NSString * const objectValue = @"__Object__Value__";

@interface OCProperty : UGCModel

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSArray *attributes;


@end

@implementation OCProperty

@end

@interface UGCModel ()

@property (nonatomic, strong) NSMutableDictionary *valueMap;

@end

@implementation UGCModel

@synthesize lock = _lock;

+ (NSDictionary *)properties{
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    Class currentClass = [self class];
    Class ugcModel = NSClassFromString(@"UGCModel");
    if (currentClass != ugcModel) {
        Class superClass = [self superclass];
        [dictionary addEntriesFromDictionary:[superClass properties]];
    }
    unsigned int outCount;
    objc_property_t *properties = class_copyPropertyList(self, &outCount);
    for (NSInteger index = 0; index != outCount; index++) {
        OCProperty *property = [OCProperty new];
        const char *name = property_getName(*(properties + index));
        property.name = [NSString stringWithCString:name encoding:NSUTF8StringEncoding];
        unsigned int attributeCount;
        objc_property_attribute_t *attriList = property_copyAttributeList(*(properties+index), &attributeCount);
        for (NSInteger interindex = 0; interindex != attributeCount; interindex++) {
            objc_property_attribute_t attribute = *(attriList + interindex);
            if (!strcmp("T", attribute.name)) {
                if (!strcmp(attribute.value, @encode(int))) {
                    property.type = @"int";
                }
                else if (!strcmp(attribute.value, @encode(long))){
                    property.type = @"long";
                }
                else if (!strcmp(attribute.value, @encode(double))){
                    property.type = @"double";
                }
                else if (!strcmp(attribute.value, @encode(float))){
                    property.type = @"float";
                }
                else if (!strcmp(attribute.value, @encode(BOOL))){
                    property.type = @"BOOL";
                }
                else if (!strcmp(attribute.value, @encode(CGRect))){
                    property.type = @"CGRect";
                }
                else if (!strcmp(attribute.value, @encode(CGSize))){
                    property.type = @"CGSize";
                }
                else if (!strcmp(attribute.value, @encode(CGPoint))){
                    property.type = @"CGPoint";
                }
                else if ([[NSString stringWithCString:attribute.value encoding:NSUTF8StringEncoding] hasPrefix:@"@"]){
                    NSString *type = [NSString stringWithCString:attribute.value encoding:NSUTF8StringEncoding];
                    type = [type stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                    property.type = [type stringByReplacingOccurrencesOfString:@"@" withString:@""];
                }
                break;
            }
        }
        free(attriList);
        [dictionary setObject:property forKey:property.name];
    }
    free(properties);
    return [NSDictionary dictionaryWithDictionary:dictionary];
}

+ (id)getplistObjectWithObject:(id)object withClassInfo:(BOOL)classInfo{
    if ([self needReturnNil:object]) return nil;
    if ([self needReturnObjectSelf:object]) return object;
    if ([object isKindOfClass:[NSArray class]]) return [self getplistObjectFromArray:object withClassInfo:classInfo];
    if ([object isKindOfClass:[NSDictionary class]]) return [self getplistObjectFromDictionary:object withClassInfo:classInfo];
    if ([object isKindOfClass:[UGCModel class]]) return [self getplistObjectFromUGCModel:object withClassInfo:classInfo];
    return nil;
}

+ (id)getplistObjectWithObject:(id)object{
    return [self getplistObjectWithObject:object withClassInfo:YES];
}

+ (id)getUGCModelObjectFromPlistObject:(id)object{
    if ([self needReturnObjectSelf:object]) return object;
    if ([object isKindOfClass:[NSDictionary class]]) {
        if ([object objectForKey:objectClass]) {
            return [self getUGCModelWithDictionary:object];
        }
        return [self getUGCModelDicWithDictionary:object];
    }
    if ([object isKindOfClass:[NSArray class]]) return [self getUGCModelArrayWithArray:object];
    return nil;
}

+ (NSDictionary *)getUGCModelDicWithDictionary:(NSDictionary *)dic{
    NSMutableDictionary *result = [NSMutableDictionary new];
    for (id key in [dic allKeys]) {
        [result setObject:[self getUGCModelObjectFromPlistObject:dic[key]] forKey:key];
    }
    return result;
}

+ (UGCModel *)getUGCModelWithDictionary:(NSDictionary *)dic{
    UGCModel *result = [NSClassFromString(dic[objectClass]) new];
    NSDictionary *valueDic = dic[objectValue];
    for (NSString * key in [valueDic allKeys]) {
        if ([valueDic[key] isKindOfClass:[NSString class]]) {
            NSString *value = valueDic[key];
            NSValue *strctValue = nil;
            if ([value rangeOfString:@"__CGRect__"].location == 0) {
                strctValue = [NSValue valueWithCGRect:CGRectFromString([value stringByReplacingOccurrencesOfString:@"__CGRect__" withString:@""])];
            }else if ([value rangeOfString:@"__CGSize__"].location == 0) {
                strctValue = [NSValue valueWithCGSize:CGSizeFromString([value stringByReplacingOccurrencesOfString:@"__CGSize__" withString:@""])];
            }else if ([value rangeOfString:@"__CGPoint__"].location == 0) {
                strctValue = [NSValue valueWithCGPoint:CGPointFromString([value stringByReplacingOccurrencesOfString:@"__CGPoint__" withString:@""])];
            }
            if (strctValue) {
                [result setValue:strctValue forKey:key];
                continue;
            }
        }
        [result setValue:[self getUGCModelObjectFromPlistObject:valueDic[key]] forKey:key];
    }
    return result;
}

+ (NSArray *)getUGCModelArrayWithArray:(NSArray *)array{
    NSMutableArray *result = [NSMutableArray new];
    for (id object in array) {
        [result addObject:[self getUGCModelObjectFromPlistObject:object]];
    }
    return result;
}



- (NSString *)description{
    return [[[self class] getplistObjectWithObject:self withClassInfo:NO] description];
}

+ (NSArray *)getplistObjectFromArray:(NSArray *)array withClassInfo:(BOOL)classInfo{
    NSMutableArray *result = [NSMutableArray new];
    for (id object in array) {
        id targetObject = [self getplistObjectWithObject:object withClassInfo:classInfo];
        if (targetObject) [result addObject:targetObject];
    }
    if (result.count) return [NSArray arrayWithArray:result];
    return nil;
}

+ (NSDictionary *)getplistObjectFromDictionary:(NSDictionary *)dictionary withClassInfo:(BOOL)classInfo{
    NSMutableDictionary *result = [NSMutableDictionary new];
    for (id key in [dictionary allKeys]) {
        id targetObject = [self getplistObjectWithObject:dictionary[key] withClassInfo:classInfo];
        if (targetObject) [result setObject:targetObject forKey:key];
    }
    if (result.count) return [NSDictionary dictionaryWithDictionary:result];
    return nil;
}

+ (NSDictionary *)getplistObjectFromUGCModel:(UGCModel *)model withClassInfo:(BOOL)classInfo{
    NSArray *properties = [[[model class] properties] allValues];
    NSMutableDictionary *result = [NSMutableDictionary new];
    for (OCProperty *property in properties){
        if (![[model class] needSavetoplistForKey:property.name]) continue;
        id targetObject = [self getplistObjectWithObject:[model valueForKey:property.name] withClassInfo:classInfo];
        if ([targetObject isKindOfClass:[NSValue class]] && ![targetObject isKindOfClass:[NSNumber class]]) {
            if ([property.type isEqualToString:@"CGRect"]) {
                targetObject = NSStringFromCGRect([targetObject CGRectValue]);
                targetObject = [targetObject stringByReplacingCharactersInRange:NSMakeRange(0, 0) withString:@"__CGRect__"];
            } else if ([property.type isEqualToString:@"CGPoint"]) {
                targetObject = NSStringFromCGPoint([targetObject CGPointValue]);
                targetObject = [targetObject stringByReplacingCharactersInRange:NSMakeRange(0, 0) withString:@"__CGPoint__"];
            } else if ([property.type isEqualToString:@"CGSize"]) {
                targetObject = NSStringFromCGSize([targetObject CGSizeValue]);
                targetObject = [targetObject stringByReplacingCharactersInRange:NSMakeRange(0, 0) withString:@"__CGSize__"];
            }
        }
        if (targetObject) [result setObject:targetObject forKey:property.name];
    }
    if (result.count) {
        if (classInfo) return @{objectClass : NSStringFromClass([model class]),
                                objectValue : [NSDictionary dictionaryWithDictionary:result]};
        return [NSDictionary dictionaryWithDictionary:result];
    }
    return nil;
}

+ (BOOL)needReturnObjectSelf:(id)object{
    return [object isKindOfClass:[NSNumber class]]
    || [object isKindOfClass:[NSString class]]
    || [object isKindOfClass:[NSDate class]]
    || [object isKindOfClass:[NSValue class]];
}


+ (BOOL)needReturnNil:(id)object{
    return NO;
}

+ (BOOL)needSavetoplistForKey:(NSString *)string{
    if ([string isEqualToString:@"lock"]) return NO;
    if ([string isEqualToString:@"draft"]) return NO;
    return YES;
}

#pragma mark Property

- (instancetype)init{
    if (self = [super init]) {
        _valueMap = [NSMutableDictionary new];
    }
    return self;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if (!value) {
        [self.valueMap removeObjectForKey:key];
        return;
    }
    [self.valueMap setObject:value forKey:key];
}

- (id)valueForUndefinedKey:(NSString *)key{
    return [self.valueMap objectForKey:key];
}

- (NSArray *)allValidKeys{
    return [self.valueMap allKeys];
}

- (void)relesetResource{
}

@end

