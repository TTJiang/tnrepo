//
//  TNAppDelegate.h
//  StoreKit
//
//  Created by StoreKit on 03/06/2016.
//  Copyright (c) 2016 StoreKit. All rights reserved.
//

@import UIKit;

@interface TNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
