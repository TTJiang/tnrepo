//
//  DPOperationQueue.h
//  DPLib
//
//  Created by Tu Yimin on 10-1-8.
//  Copyright 2010 dianping.com. All rights reserved.
//

#import <Foundation/Foundation.h>


//
// the NSOperationQueue is buggy (especially on iPhone SDK), so we use our own queue.
// the direct operation on the queue is only safe on the main thread.
// the queue is using a mutable array which cause the low speed. you can optimize with a linked list.
//

@protocol NVOperation <NSObject>

- (void)main;

@end


@interface NVOperationQueue : NSObject

- (id)initWithThreadCount:(NSInteger)count;
- (id)initWithThreadCount:(NSInteger)count withFIFO:(BOOL)isFIFO;

- (NSInteger)threadCount;
- (NSArray *)operations;
- (NSInteger)busyThreads;

- (void)addOperation:(id<NVOperation>)operation;

- (void)cancelAllOperations;
- (void)waitUntilAllOperationsAreFinished;

@end
