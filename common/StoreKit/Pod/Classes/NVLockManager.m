//
//  DPLockManager.m
//  DPScope
//
//  Created by ZhouHui on 11-6-8.
//  Copyright 2011年 dianping.com. All rights reserved.
//

#import "NVLockManager.h"


@interface NVLockManager () {
    NSMutableDictionary *map;
}

@end

NVLockManager *_instance;

@implementation NVLockManager
//
//+ (void)initialize
//{
//    _instance = [[NVLockManager alloc] init];
//}
//
//+ (NVLockManager *)sharedManager
//{
//    return _instance;
//}
//
//- (id)init {
//    self = [super init];
//    if (self) {
//        map = [[NSMutableDictionary alloc] init];
//    }
//    return self;
//}
//
//- (NSRecursiveLock *)retainLock:(id)key {
//    @synchronized (map) {
//        NSRecursiveLock *lock = [map objectForKey:key];
//        if (lock == nil) {
//            lock = [[NSRecursiveLock alloc] init];
//            [map setObject:lock forKey:key];
//        } else {
//            [lock retain]; 
//        }
//        return lock;
//    }
//}
//
//- (NSRecursiveLock *)releaseLock:(id)key {
//    @synchronized (map) {
//        NSRecursiveLock *lock = [map objectForKey:key];
//        if (lock == nil) {
//            return nil;
//        }
//        if ([lock retainCount] <= 2) {
//            [map removeObjectForKey:key];
//            return [lock autorelease];
//        } else {
//            [lock release];
//            return lock;
//        }
//    }
//}
//
//- (void)lock:(id)key {
//	if (key == nil) return;
//    NSRecursiveLock *lock = [self retainLock:key];
//    [lock lock];
//}
//
//- (void)unlock:(id)key {
//	if (key == nil) return;
//    NSRecursiveLock *lock = [self releaseLock:key];
//    assert(lock);
//    [lock unlock];
//}
//
//- (void)dealloc {
//    [map release];
//    [super dealloc];
//}

@end
