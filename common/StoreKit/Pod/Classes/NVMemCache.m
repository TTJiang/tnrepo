//
//  DPMemCache.m
//  DPLib
//
//  Created by Tu Yimin on 10-1-8.
//  Copyright 2010 dianping.com. All rights reserved.
//

#import "NVMemCache.h"


#define kOkayUseage			0.85
#define kMaxSingleUseage	0.5


@interface NVCont : NSObject {
@public
	id obj;
    size_t size;
	NVLinkedListNode *accessNode;
	NVLinkedListNode *ageNode;
	NSInteger hitCount;
}

@end


@implementation NVCont

@end




@implementation NVMemCache

- (id)init {
    return [self initWithMaxSize:-1 maxLifetime:-1];
}

- (id)initWithMaxSize:(size_t)ms maxLifetime:(time_t)ml {
	if(self = [super init]) {
		dict = [[NSMutableDictionary alloc] initWithCapacity:64];
		accessList = [[NVLinkedList alloc] init];
		ageList = [[NVLinkedList alloc] init];
		maxCacheSize = ms;
		maxLifetime = ml;
	}
	return self;
}

- (id)putObject:(id)obj forKey:(id)key {
    time_t t = time(0);
	size_t valSize = [self sizeOf:obj];
	size_t oldSize = 0;
	if(maxCacheSize > 0 && valSize > maxCacheSize * kMaxSingleUseage) {
		// 如果对象尺寸过大（size>maxCacheSize*kMaxSingleUseage）则不存入memcache，并把memcache中原来的数据清除。
		// 这里代码永远不会调用（因为对象的size都是1）
		NVCont *removed = [dict objectForKey:key];
		if(removed) {
			id obj = removed->obj;
			[dict removeObjectForKey:key];
			return obj;
		} else {
			return nil;
		}
	}
	NVCont *vc = [[NVCont alloc] init];
    vc->obj = obj;
    vc->size = valSize;
	NVCont *removed = [dict objectForKey:key];
	id removedObj = nil;
	[dict setObject:vc forKey:key];
	NVLinkedListNode *accessNode = nil;
	NVLinkedListNode *ageNode = nil;
	if(removed) {
		[removed->accessNode remove];
		accessNode = removed->accessNode;
		removed->accessNode = nil;
		[removed->ageNode remove];
		ageNode = removed->ageNode;
		removed->ageNode = nil;
		oldSize = removed->size;
		cacheSize -= oldSize;
		removedObj = removed->obj;
		removed = nil;
	}
	cacheSize += valSize;
	
	if(!accessNode)
		accessNode = [[NVLinkedListNode alloc] init];
	accessNode.obj = key;
	accessNode.time = t;
	[accessList addFirst:accessNode];
	vc->accessNode = accessNode;
    accessNode = nil;
	
	if(!ageNode)
		ageNode = [[NVLinkedListNode alloc] init];
	ageNode.obj = key;
	ageNode.time = t;
	[ageList addFirst:ageNode];
	vc->ageNode = ageNode;
	ageNode = nil;
	
	if(oldSize < valSize)
		[self cleanFull];
	
	return removedObj;
}

- (id)objectForKey:(id)key {
	[self cleanExpired];
	NVCont *vc = [dict objectForKey:key];
	if(!vc) {
		++cacheMisses;
		return nil;
	} else {
		++cacheHits;
		++(vc->hitCount);
		vc->accessNode.time = time(0);
		
		[vc->accessNode remove];
		[accessList addFirst:vc->accessNode];
		
		return vc->obj;
	}
}

- (id)removeObjectForKey:(id)key {
	NVCont *vc = [dict objectForKey:key];
	if(!vc)
		return nil;
	
	[vc->accessNode remove];
	vc->accessNode = nil;
	[vc->ageNode remove];
	vc->ageNode = nil;
	cacheSize -= vc->size;
	
	id obj = vc->obj;
	[dict removeObjectForKey:key];
	
	return obj;
}

- (void)clear {
	[accessList clear];
	[ageList clear];
	[dict removeAllObjects];
	cacheSize = 0;
}


- (NSInteger)count {
	return cacheSize;
}

- (BOOL)isEmpty {
	return cacheSize == 0;
}


- (int32_t)cacheHits {
	return cacheHits;
}

- (int32_t)cacheMisses {
	return cacheMisses;
}


- (size_t)maxCacheSize {
	return maxCacheSize;
}

- (size_t)cacheSize {
	return cacheSize;
}

- (void)setCacheSize:(size_t)size {
	cacheSize = size;
	[self cleanFull];
}


- (time_t)maxLifetime {
	return maxLifetime;
}

- (void)setMaxLifetime:(time_t)lifetime {
	maxLifetime = lifetime;
	[self cleanExpired];
}


- (size_t)sizeOf:(id)obj {
	return 1;
}


- (void)cleanExpired {
	if(maxLifetime <= 0)
		return;
	
	time_t expireTime = time(0) - maxLifetime;
	NVLinkedListNode *node;
	while((node = [ageList last])) {
		if(expireTime > node.time) {
			[self removeObjectForKey:node.obj];
		} else {
			break;
		}
	}
}

- (void)cleanFull {
	if(maxCacheSize <= 0)
		return;
	
	if(cacheSize >= maxCacheSize) {
		[self cleanExpired];
		size_t okaySize = (size_t)(maxCacheSize * kOkayUseage);
		while(cacheSize > okaySize) {
			[self removeObjectForKey:[accessList last].obj];
		}
	}
}

- (void)dealloc {
	[self clear];
}

@end
