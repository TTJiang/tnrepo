//
//  DPLinkedList.h
//  DPLib
//
//  Created by Tu Yimin on 10-1-8.
//  Copyright 2010 dianping.com. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NVLinkedListNode : NSObject

@property (nonatomic, retain) id obj;
@property (nonatomic, assign) time_t time;

- (void)remove;

@end


@interface NVLinkedList : NSObject

- (NVLinkedListNode *)first;
- (NVLinkedListNode *)last;

- (NVLinkedListNode *)addFirst:(NVLinkedListNode *)node;

- (NVLinkedListNode *)addLast:(NVLinkedListNode *)node;

- (void)clear;

@end
