//
//  DPLinkedList.m
//  DPLib
//
//  Created by Tu Yimin on 10-1-8.
//  Copyright 2010 dianping.com. All rights reserved.
//

#import "NVLinkedList.h"


@interface NVLinkedListNode () {
@public
    __unsafe_unretained NVLinkedListNode *prev;
    __unsafe_unretained NVLinkedListNode *next;
}

@end


@implementation NVLinkedListNode

@synthesize obj;
@synthesize time;

- (void)remove {
	prev->next = next;
	next->prev = prev;
}

@end


#pragma mark -


@interface NVLinkedList () {
	NVLinkedListNode *head;
}

@end


@implementation NVLinkedList

- (id)init {
	if(self = [super init]) {
		head = [[NVLinkedListNode alloc] init];
		head->next = head->prev = head;
	}
	return self;
}

- (NVLinkedListNode *)first {
	NVLinkedListNode *node = head->next;
	if(node == head)
		return nil;
	return node;
}

- (NVLinkedListNode *)last {
	NVLinkedListNode *node = head->prev;
	if(node == head)
		return nil;
	return node;
}

- (NVLinkedListNode *)addFirst:(NVLinkedListNode *)node {
	node->next = head->next;
	node->prev = head;
	node->prev->next = node;
	node->next->prev = node;
	return node;
}

- (NVLinkedListNode *)addLast:(NVLinkedListNode *)node {
	node->next = head;
	node->prev = head->prev;
	node->prev->next = node;
	node->next->prev = node;
	return node;
}

- (void)clear {
	NVLinkedListNode *node = [self last];
	while (node != nil) {
		[node remove];
		node = [self last];
	}
	head->next = head->prev = head;
}

@end
