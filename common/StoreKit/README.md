# StoreKit

[![CI Status](http://img.shields.io/travis/StoreKit/StoreKit.svg?style=flat)](https://travis-ci.org/StoreKit/StoreKit)
[![Version](https://img.shields.io/cocoapods/v/StoreKit.svg?style=flat)](http://cocoapods.org/pods/StoreKit)
[![License](https://img.shields.io/cocoapods/l/StoreKit.svg?style=flat)](http://cocoapods.org/pods/StoreKit)
[![Platform](https://img.shields.io/cocoapods/p/StoreKit.svg?style=flat)](http://cocoapods.org/pods/StoreKit)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

StoreKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "StoreKit"
```

## Author

StoreKit, jiangteng.cn@gmail.com

## License

StoreKit is available under the MIT license. See the LICENSE file for more info.
