//
//  NVFilterButton.h
//  ZZCool
//
//  Created by JiangTeng on 16/1/22.
//  Copyright © 2016年 dianping. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NVFilterButton : UIButton
@property (nonatomic, assign) BOOL isFilterNeedOn;      // 是否开或关状态

- (instancetype)initWithFrame:(CGRect)frame isHideGapView:(BOOL)isHideGapView;
- (void)adjustArrow;
- (void)adjustArrowToRotate;
- (void)updateFilterButton;
@end
