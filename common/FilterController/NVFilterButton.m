//
//  NVFilterButton.m
//  ZZCool
//
//  Created by JiangTeng on 16/1/22.
//  Copyright © 2016年 dianping. All rights reserved.
//

#import "NVFilterButton.h"
#import "UIColor+Ext.h"
#import "UIView+layout.h"
#import "NSString+Ext.h"
#import "UIImage+Ext.h"
#import "R.h"

@interface NVFilterButton()
@property (nonatomic, strong) UIImageView *dropImage;                // 上下箭头
@property (nonatomic, strong) UIImageView *gapImage;                 // 分割线
@property (nonatomic, strong) UIImageView *selectedArrowImage;     // 选中时，下面显示的箭头
@end

@implementation NVFilterButton
- (instancetype)initWithFrame:(CGRect)frame isHideGapView:(BOOL)isHideGapView
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateNormal];
        [self setTitleColor:[UIColor colorWithHexString:@"#333333"] forState:UIControlStateHighlighted];
        [self setBackgroundImage:[UIImage imageNamed:R_FilterTab_Normal] forState:UIControlStateNormal];
        
        self.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        self.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;//lmc change
        self.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 15);
        
        // 创建箭头
        self.dropImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:R_tuangou_list_icon_arrow_down]];
        self.dropImage.frame = CGRectMake(self.width - 25, (self.height - 13) / 2, 13, 13);
        [self addSubview:self.dropImage];
        
        // 创建分割线
        if (!isHideGapView) {
            self.gapImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:R_FilterGap]];
            self.gapImage.frame =  CGRectMake(0, 0, 0.5, self.height - 1);
            [self addSubview:self.gapImage];
        }
        
        // 创建当前选中时的箭头
        self.selectedArrowImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:R_Tab_Selected_Arrow]];
        self.selectedArrowImage.center = CGPointMake(0.5 * CGRectGetWidth(self.bounds) ,CGRectGetHeight(self.bounds) - self.selectedArrowImage.image.size.height * 0.5);
        self.selectedArrowImage.hidden = YES;
        [self addSubview:self.selectedArrowImage];
        
        [self setBackgroundImage:[UIImage imageNamed:R_FilterTab_Normal] forState:UIControlStateNormal];
        [self setBackgroundImage:[[UIImage imageNamed:R_FilterTab_Normal] highLightedImage:ButtonHighLightStyleGray] forState:UIControlStateHighlighted];
    }
    return self;
}

// 根据文本调整箭头位置
- (void)adjustArrow {
    self.dropImage.frame = CGRectMake(self.width - 25, (self.height - 13) / 2, 13, 13);
    self.gapImage.frame =  CGRectMake(0, 0, 0.5, self.height - 1);
    self.selectedArrowImage.center = CGPointMake(0.5 * CGRectGetWidth(self.bounds) ,CGRectGetHeight(self.bounds) - self.selectedArrowImage.image.size.height * 0.5);
    
    NSString *title = [self titleForState:UIControlStateNormal];
    if (title.length > 0) {
        CGSize sizeTitle = CGSizeMake(self.width - self.dropImage.width,18);
        CGSize expectedNameSize = [title nvSizeWithFont:self.titleLabel.font constrainedToSize:sizeTitle lineBreakMode:NSLineBreakByClipping];
        self.dropImage.left = (self.width - expectedNameSize.width) / 2 + expectedNameSize.width - 5;
    }
}

// 调整箭头转向
- (void)adjustArrowToRotate {
    [self adjustArrow];
    self.dropImage.layer.transform = CATransform3DIdentity;
}

// 更新filter按钮
-(void)updateFilterButton {
    self.selectedArrowImage.hidden = (self.isFilterNeedOn) ? NO : YES;
    self.dropImage.layer.transform = (self.isFilterNeedOn) ? CATransform3DMakeRotation((M_PI / 180.0) * 180.0f, 0.0f, 0.0f, 1.0f) : CATransform3DIdentity;
}
@end
