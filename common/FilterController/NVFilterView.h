//
//  NVFilterView.h
//  Nova
//
//  Created by MengWang on 15-4-27.
//  Copyright (c) 2015年 dianping.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NVFilterButton;
@protocol NVFilterViewDelegate;

@interface NVFilterView : UIView
@property (nonatomic) NSUInteger filterCount;    // filter按钮数量
@property (nonatomic, assign) id<NVFilterViewDelegate> delegate;
@property (nonatomic, assign) BOOL disable;      // 控制filter上按钮状态

- (id)initWithFrame:(CGRect)frame withFilterCount:(NSInteger)filterCount;
- (void)resetFilterBtnsStates;

// 配置button的文本和index
- (void)setCurrentFilterButton:(NSString *)filterText index:(NSInteger)index;
// 通过index，获取button的title
- (NSString *)currentFilterButtonOfIndex:(NSInteger)index;
// 传index取回当前button
- (NVFilterButton *)filterButtonWithindex:(NSInteger)index;
@end

@protocol NVFilterViewDelegate <NSObject>
@optional
// 点击按钮的回调
- (void)filterView:(NVFilterView *)view touchedFilter:(NSInteger)index filterNeedsOn:(BOOL)filterNeedsOn;
@end


@interface NVPickerObject : NSObject
@property (nonatomic, retain) id leftObject;
@property (nonatomic, retain) NSArray *rightArray;
+ (id)pickerObjectWithLeftObject:(id)left withRightArray:(NSArray *)right;
- (id)initWithLeftObject:(id)left withRightArray:(NSArray *)right;
@end



