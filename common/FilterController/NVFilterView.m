//
//  NVFilterView.m
//  Nova
//
//  Created by MengWang on 15-4-27.
//  Copyright (c) 2015年 dianping.com. All rights reserved.
//

#import "NVFilterView.h"
#import "MVPopoverView.h"
#import "NVFilterButton.h"
#import "UIScreen+Adaptive.h"
#import "UIView+layout.h"

#import "Navigator.h"
#import "ZZCAcountHelper.h"


@interface NVFilterView()
@property (nonatomic, strong) NSArray *filterButtonsArray;   // 保存筛选按钮的array
@property (nonatomic, strong) NSMutableArray *filterBtnStatesArray;   // 记录按钮状态数组
@end

@implementation NVFilterView
@synthesize filterCount = _filterCount;

- (id)initWithFrame:(CGRect)frame {
    return [self initWithFrame:frame withFilterCount:[self defaultFilterCount]];
}

- (id)initWithFrame:(CGRect)frame withFilterCount:(NSInteger)filterCount {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.filterCount = filterCount;
    }
    return self;
}

- (void)awakeFromNib {
    self.backgroundColor = [UIColor clearColor];
    self.filterCount = [self defaultFilterCount];
}

- (void)setDisable:(BOOL)disable {
    _disable = disable;
    for (NVFilterButton *filterButton in self.filterButtonsArray) {
        filterButton.enabled = !disable;
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat eachButtonWidth = [self width] / (CGFloat)_filterCount;
    CGFloat left = 0.0f;
    
    for (NVFilterButton *button in self.filterButtonsArray) {
        button.frame = CGRectMake(left, 0, eachButtonWidth, self.height);
        left += eachButtonWidth;
        
        [button adjustArrow];
    }
}

// 默认个数
- (NSInteger)defaultFilterCount {
    return 3;
}

-(void)updateFilterButtonAppearance {
    for (NVFilterButton *button in self.filterButtonsArray) {
        NSNumber *status = self.filterBtnStatesArray[[self.filterButtonsArray indexOfObject:button]];
        button.isFilterNeedOn = [status boolValue];
        [button updateFilterButton];
    }
}

- (void)setFilterCount:(NSUInteger)filterCount {
    if (filterCount == _filterCount || filterCount < 1) {
        return;
    }
    _filterCount = filterCount;
    
    // 首先删除所有界面元素
    NSArray *subViews = self.subviews;
    for (UIView *subView in subViews) {
        [subView removeFromSuperview];
    }
    
    NSMutableArray *filterButtonsArr = [[NSMutableArray alloc] initWithCapacity:0];
    
    self.filterBtnStatesArray = [[NSMutableArray alloc] initWithCapacity:0];
    for (NSInteger i = 0; i < filterCount; i++) {
        NVFilterButton *filterButton = [[NVFilterButton alloc] initWithFrame:CGRectZero isHideGapView:(i == 0) ? YES : NO];
        filterButton.tag = i + 1;
        filterButton.isFilterNeedOn = NO;
        [filterButton addTarget:self action:@selector(filterButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:filterButton];
        
        [filterButtonsArr addObject:filterButton];
        [self.filterBtnStatesArray addObject:[NSNumber numberWithBool:NO]];
    }
    
    if (filterButtonsArr.count > 0) {
        self.filterButtonsArray = [NSArray arrayWithArray:filterButtonsArr];
    }
}

- (void)setCurrentFilterButton:(NSString *)filterText index:(NSInteger)index {
    NVFilterButton *button = (NVFilterButton *)self.filterButtonsArray[index - 1];
    [button setTitle:filterText forState:UIControlStateNormal];
    [button adjustArrowToRotate];
}

- (NSString *)currentFilterButtonOfIndex:(NSInteger)index {
    NVFilterButton *button = (NVFilterButton *)self.filterButtonsArray[index - 1];
    return button.titleLabel.text ? button.titleLabel.text : @"";
}

- (void)filterButtonGA:(NSString *)label index:(NSInteger)index {
    NVFilterButton *button = (NVFilterButton *)self.filterButtonsArray[index - 1];
    if (label.length > 0) {
    }
}

// 传index取回当前button
- (NVFilterButton *)filterButtonWithindex:(NSInteger)index {
    NVFilterButton *button = (NVFilterButton *)self.filterButtonsArray[index - 1];
    return button;
}

// 重置状态为0
- (void)resetFilterBtnsStates {
    for (NSInteger i = 0; i < self.filterBtnStatesArray.count; i++) {
        [self.filterBtnStatesArray replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:NO]];
    }
    [self updateFilterButtonAppearance];
}

- (void)filterButtonAction:(id)sender {
    
    if (![[ZZCAcountHelper instance] isLogined]) {
        MVURLAction *action;
        NSString *url = @"zzcool://login";
        action = [[MVURLAction alloc] initWithURL:[NSURL URLWithString:url]];
        [[MVNavigator navigator] openURLAction:action];
        return ;
    }
    
    if([MVPopoverView popoverIsShowed]) {
        [MVPopoverView sharedPopoverView].delegate = nil;
        [MVPopoverView dismissPopoverViewAnimated:NO];
    }
    
    NVFilterButton *button = (NVFilterButton *)sender;
    NSInteger index = button.tag;
    BOOL isFilterNeedsOn = NO;
    
    for (NSInteger i = 0; i < self.filterBtnStatesArray.count; i++) {
        if (i == index - 1) {
            isFilterNeedsOn = ![self.filterBtnStatesArray[i] boolValue];
            [self.filterBtnStatesArray replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:isFilterNeedsOn]];
        } else {
            [self.filterBtnStatesArray replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:NO]];
        }
    }
    
    [self updateFilterButtonAppearance];
    if (self.delegate && [self.delegate respondsToSelector:@selector(filterView:touchedFilter:filterNeedsOn:)]) {
        [self.delegate filterView:self touchedFilter:index filterNeedsOn:isFilterNeedsOn];
    }
}

@end


@implementation NVPickerObject
@synthesize leftObject, rightArray;
+ (id)pickerObjectWithLeftObject:(id)left withRightArray:(NSArray *)right {
    return [[self alloc] initWithLeftObject:left withRightArray:right];
}

- (id)initWithLeftObject:(id)left withRightArray:(NSArray *)right {
    self = [super init];
    if (self) {
        self.leftObject = left;
        self.rightArray = right;
    }
    return self;
}

- (void)dealloc {
    self.leftObject = nil;
    self.rightArray = nil;
}
@end
