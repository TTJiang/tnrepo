//
//  NVAlertDelegate.h
//  Lego
//
//  Created by Johnson Zhang on 14-9-28.
//  Copyright (c) 2014年 dianping.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef NVAlertViewController
#define NVAlertViewController [UIAlertController class] ? [LegoAlertController class] : [LegoAlertView class]
#endif

#ifndef NVAlertView
#define NVAlertView id<NVAlertDelegate>
#endif


typedef void (^NVUIBlock)(void);
typedef void (^NVActionBlock)(id sender);


@protocol NVAlertDelegate <NSObject>

/** Creates and returns a new alert view with only a title.
 
 @param title The title of the alert view.
 @return A newly created alert view.
 */
+ (NVAlertView)alertViewWithTitle:(NSString *)title;

/** Creates and returns a new alert view with only a title, message, and cancel button.
 
 @param title The title of the alert view.
 @param message The message content of the alert.
 @return A newly created alert view.
 */
+ (NVAlertView)alertViewWithTitle:(NSString *)title message:(NSString *)message;

/** Add a new button with an associated code block.
 
 @param title The text of the button.
 @param block A block of code.
 */
- (void)addButtonWithTitle:(NSString *)title action:(NVUIBlock)block;

- (void)addButtonWithTitle:(NSString *)title;

- (void)addTextField;

- (UITextField *)textField;

- (UITextField *)textFieldAtIndex:(NSInteger)textFieldIndex;

/** show alert view
 */
- (void)showByViewController:(UIViewController *)vc;

/** Set the cancel button with an associated code block.
 
 @warning Because buttons cannot be removed from an alert view,
 be aware that the effects of calling this method are cumulative.
 Previously added cancel buttons will become normal buttons.
 
 @param title The text of the button.
 @param block A block of code.
 */
- (void)addCancelButtonWithTitle:(NSString *)title action:(NVUIBlock)block;

@optional

- (void)show;
@end

