//
//  NVAlertView.h
//  NVScope
//
//  Created by ZhouHui on 12-3-13.
//  Copyright (c) 2012年 dianping.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NVAlertDelegate.h"

@interface LegoAlertView : UIAlertView <NVAlertDelegate>

@property (nonatomic, strong) id data;

@end
