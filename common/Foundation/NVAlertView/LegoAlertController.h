//
//  LegoAlertController.h
//  Lego
//
//  Created by Johnson Zhang on 14-9-28.
//  Copyright (c) 2014年 dianping.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NVAlertDelegate.h"
#import "NVActionSheetDelegate.h"

@interface LegoAlertController : UIAlertController <NVAlertDelegate, NVActionSheetDelegate>

@end
