//
//  LegoAlertController.m
//  Lego
//
//  Created by Johnson Zhang on 14-9-28.
//  Copyright (c) 2014年 dianping.com. All rights reserved.
//

#import "LegoAlertController.h"
//#import "EXTScope.h"

@interface LegoAlertController ()

@end

@implementation LegoAlertController

#pragma mark NVAlertDelegate

+ (instancetype)alertViewWithTitle:(NSString *)title
{
    return [self alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleAlert];
}

+ (instancetype)alertViewWithTitle:(NSString *)title message:(NSString *)message
{
    return [self alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
}

#pragma mark NVActionSheetDelegate

+ (instancetype)actionSheetWithTitle:(NSString *)title
{
    return [self alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
}

- (void)addDestructiveButtonWithTitle:(NSString *)title action:(NVUIBlock)block
{
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleDestructive handler:^(UIAlertAction *pAction) {
        if (block) {
            block();
        }
    }];
    [self addAction:action];
}

#pragma mark Shared

- (void)addButtonWithTitle:(NSString *)title action:(NVUIBlock)block
{
    UIAlertAction *action = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction *pAction) {
        if (block) {
            block();
        }
    }];
    [self addAction:action];
}

- (void)addButtonWithTitle:(NSString *)title
{
    [self addButtonWithTitle:title action:nil];
}

- (void)addCancelButtonWithTitle:(NSString *)title action:(NVUIBlock)block
{
    UIAlertAction *action = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleCancel handler:^(UIAlertAction *pAction) {
        if (block) {
            block();
        }
    }];
    [self addAction:action];
}

- (void)addTextField
{
    [self addTextFieldWithConfigurationHandler:nil];
}

- (UITextField *)textFieldAtIndex:(NSInteger)textFieldIndex
{
    return self.textFields.count > textFieldIndex ? [self.textFields objectAtIndex:textFieldIndex] : nil;
}

- (UITextField *)textField
{
    return [self textFieldAtIndex:0];
}

- (void)showByViewController:(UIViewController *)vc;
{
    self.popoverPresentationController.sourceView = vc.view;
    self.popoverPresentationController.sourceRect = CGRectMake(vc.view.bounds.size.width / 2.0, vc.view.bounds.size.height, 1.0, 1.0);
    
    [vc presentViewController:self animated:YES completion:nil];
}

- (void)show
{
    //[[NVAppDelegate instance].navigationController.topViewController presentViewController:self animated:YES completion:nil];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:self animated:YES completion:nil];//lmc add
}

- (void)showInView:(UIView *)view
{
    //[self showByViewController:[NVAppDelegate instance].navigationController.topViewController];
    [self showByViewController:[UIApplication sharedApplication].keyWindow.rootViewController];//lmc add
}


#pragma mark Other

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
