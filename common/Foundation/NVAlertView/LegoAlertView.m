//
//  NVAlertView.m
//  NVScope
//
//  Created by ZhouHui on 12-3-13.
//  Copyright (c) 2012年 dianping.com. All rights reserved.
//

#import "LegoAlertView.h"
#import <objc/runtime.h>

@interface LegoAlertView ()<UIAlertViewDelegate>
@property (nonatomic, strong) NSArray *actions;
@end

@implementation LegoAlertView

- (void)showByViewController:(UIViewController *)vc
{
    [self show];
}

- (instancetype)initWithTitle:(NSString *) title message:(NSString *) message{
    self = [super initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
    if (self == nil) return nil;
    _actions = @[];
    return self;
}

+ (instancetype)alertViewWithTitle:(NSString *)title{
    return [[self alloc] initWithTitle:title message:nil];
}

+ (instancetype)alertViewWithTitle:(NSString *)title message:(NSString *)message{
    return [[self alloc] initWithTitle:title message:message];
}

- (void)addButtonWithTitle:(NSString *)title action:(NVUIBlock)block{
    NSParameterAssert(title);
    
    [super addButtonWithTitle:title];
    id blockOrNull = [block copy] ?: [NSNull null];
    self.actions = [self.actions arrayByAddingObject:blockOrNull];
}

- (void)addButtonWithTitle:(NSString *)title {
    self.actions = [self.actions arrayByAddingObject:[NSNull null]];
    [super addButtonWithTitle:title];
}

- (void)addCancelButtonWithTitle:(NSString *)title action:(NVUIBlock)block {
    [self addButtonWithTitle:title action:block];
    self.cancelButtonIndex = [self.actions count] - 1;
}

- (void)addTextField
{
    [self setAlertViewStyle:UIAlertViewStylePlainTextInput];
}

- (UITextField *)textField
{
    return [self textFieldAtIndex:0];
}

- (const void *)associateKey{
    return "retainKey";
}

- (void)show{
    [self addRetainCircle];
    [super show];
}

- (void)addRetainCircle{
    objc_setAssociatedObject(self, [self associateKey], self, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)breakCircle{
    objc_setAssociatedObject(self, [self associateKey], nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSParameterAssert(buttonIndex < [self.actions count]);
    if (buttonIndex >= [self.actions count]) return;
    
    id block = self.actions[buttonIndex];
    if (block != [NSNull null]) {
        ((NVUIBlock)block)();
    }
    [self breakCircle];
}

- (void)alertViewCancel:(UIAlertView *)alertView{
    [self breakCircle];
}


@end
