//
//  MVVersion.h
//  Pods
//
//  Created by JiangTeng on 15/12/13.
//
//

#import <Foundation/Foundation.h>

@interface MVVersion : NSObject
@property (strong, nonatomic, readonly) NSString *version;

+ (MVVersion *)mainBundleVersion;

+ (MVVersion *)osVersion;

- (id)initWithVersion:(NSString *)v;

- (BOOL)isNewerThan:(MVVersion *)ver;

- (BOOL)isOlderThan:(MVVersion *)ver;

@end
