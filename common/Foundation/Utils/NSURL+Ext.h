//
//  NSURL+Ext.h
//  Pods
//
//  Created by JiangTeng on 15/12/8.
//
//

#import <Foundation/Foundation.h>

@interface NSURL (Ext)

/**
 将query解析为NSDictionary
 
 @return 返回参数字典对象，参数的值已经进行了decode.
 (stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding)
 */
- (NSDictionary *)parseQuery;

@end
