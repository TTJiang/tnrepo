//
//  NSString+Ext.h
//  Pods
//
//  Created by JiangTeng on 15/12/9.
//
//

#import <Foundation/Foundation.h>

@interface NSString (Ext)

+ (instancetype)generateUUID;

- (NSData *)decodeHexString;

- (NSString *)md5;

- (NSString *)stringByAddingPercentEscapesUsingEncodingExt:(NSStringEncoding)enc;
- (NSString *)stringByReplacingPercentEscapesUsingEncodingExt:(NSStringEncoding)enc;

/**
 url escape
 */
- (NSString *)stringByAddingPercentEscapes;
/**
 url unescape
 */
- (NSString *)stringByReplacingPercentEscapes;

- (BOOL)isValidEmail;

/**
 去掉头尾的冒号，包括中文和英文
 [@"价格：" trimColon] = @"价格"
 */
- (NSString *)trimColon;

// Standard Base64 decoder
- (NSData *)decodeBase64String;

// URL Base64 decoder (avoid url escape)
- (NSData *)decodeUrlBase64String;


- (NSString *)urlEncode;

- (NSString *)urlDecode;

-(CGSize)nvSizeWithFont:(UIFont *)font forWidth:(CGFloat)width lineBreakMode:(NSLineBreakMode)lineBreakMode;

-(CGSize) nvSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)mode;
/**
 *  验证是不是手机号
 *
 *  @return Yes是手机号
 */
- (BOOL)isCellPhoneNumber;
@end




