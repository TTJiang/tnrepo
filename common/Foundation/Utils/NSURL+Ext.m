//
//  NSURL+Ext.m
//  Pods
//
//  Created by JiangTeng on 15/12/8.
//
//

#import "NSURL+Ext.h"

@implementation NSURL (Ext)

- (NSDictionary *)parseQuery {
    NSString *query = [self query];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:6];
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    
    for (NSString *pair in pairs) {
        NSArray *elements = [pair componentsSeparatedByString:@"="];
        
        if ([elements count] <= 1) {
            continue;
        }
        
        NSString *key = [[elements objectAtIndex:0] stringByRemovingPercentEncoding];
        CFStringRef originValue = CFURLCreateStringByReplacingPercentEscapes(NULL, (CFStringRef)([elements objectAtIndex:1]),  CFSTR(""));
        [dict setObject:(__bridge NSString*)originValue forKey:key];
        CFRelease(originValue);
    }
    return dict;
}

@end
