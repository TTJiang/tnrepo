//
//  UIImage+Ext.h
//  Pods
//
//  Created by JiangTeng on 16/1/22.
//
//

#import <UIKit/UIKit.h>

typedef enum {
    ButtonHighLightStyleLight,
    ButtonHighLightStyleDark,
    ButtonHighLightStyleGray
} ButtonHighLightStyle;

@interface UIImage (Ext)

- (UIImage *)highLightedImage:(ButtonHighLightStyle)highLightStyle;


- (CGRect)convertCropRect:(CGRect)cropRect;
- (UIImage *)croppedImage:(CGRect)cropRect scale:(CGFloat)scale;
- (UIImage *)resizedImage:(CGSize)size imageOrientation:(UIImageOrientation)imageOrientation;
- (UIImage *)imageByScalingProportionallyToMinimumSize:(CGSize)targetSize;
- (UIImage *)imageByScalingProportionallyToSize:(CGSize)targetSize;
- (UIImage *)imageByScalingToSize:(CGSize)targetSize;
- (UIImage *)imageRotatedByRadians:(CGFloat)radians;
- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees;
+ (UIImage *)imageWithColor:(UIColor *)color;
@end
