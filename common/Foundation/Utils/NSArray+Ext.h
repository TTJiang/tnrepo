//
//  NSArray+Ext.h
//  Pods
//
//  Created by JiangTeng on 15/12/8.
//
//

#import <Foundation/Foundation.h>

typedef void(^EnumerateBlock)(NSInteger index, id obj);
typedef id (^TransformBlock)(id obj);
typedef BOOL (^ValidationBlock)(id obj);
typedef id (^AccumulationBlock)(id sum, id obj);

@interface NSArray (Ext)
/**
 *  enumerate each object in array with index
 *  @warning if index is not needed, prefer forin loop; consider map select reduce first
 *  @param block side effect logic
 */
- (void)eachWithIndex:(EnumerateBlock)block;

/**
 *  functional map method
 *
 *  @param block specify mapping relation
 *
 *  @return mapped array
 */
- (NSArray *)map:(TransformBlock)block;

/**
 *  function select method
 *
 *  @param block logic to specify which to select
 *
 *  @return new array with selectecd objects
 */
- (NSArray *)select:(ValidationBlock)block;

/**
 *  functional reject, similar with select
 *
 *  @param block specify which to reject
 *
 *  @return new array with filterd objects
 */
- (NSArray *)reject:(ValidationBlock)block;

/**
 *  functional reduce method
 *
 *  @param initial sum base
 *  @param block   add logic
 *
 *  @return sum
 */
- (id)reduce:(id)initial withBlock:(AccumulationBlock)block;

/**
 *  take first n objects as array, if n > array length, return self
 *
 *  @param n number to take
 *
 *  @return array
 */
- (instancetype)take:(NSUInteger)n;

/**
 *  find the object match condition in array
 *
 *  @param block condition
 *
 *  @return matched object or nil
 */
- (id)find:(ValidationBlock)block;

/**
 *  check whether all objects in array match condition
 *
 *  @param block condition logic
 *
 *  @return
 */
- (BOOL)allObjectsMatched:(ValidationBlock)block;



/**
 *  check whether any object in array match condition
 *
 *  @param block condition logic
 *
 *  @return
 */
- (BOOL)anyObjectMatched:(ValidationBlock)block;

/**
 *  join array of string to a string
 *
 *  @param seperator
 *
 *  @return string
 */
- (NSString *)join:(NSString *)seperator;

/**
 *  return the first matched object in array
 *
 *  @param block match logic
 *
 *  @return the first matched object, if not found, return nil
 */
- (id)match:(ValidationBlock)block;

/**
 *  check whether array contain matched object
 *
 *  @param block match logic
 *
 *  @return bool
 */
- (BOOL)existObjectMatch:(ValidationBlock)block;

/**
 *  check whether all objects in array match the validation
 *
 *  @param block validation
 *
 *  @return bool
 */
- (BOOL)allObjectMatch:(ValidationBlock)block;


- (NSArray *)groupBy:(TransformBlock)block;

- (NSArray *)zip:(NSArray *)array;

- (NSString *)insertIntoPlaceHolderString:(NSString *)placeHolder;


@end
