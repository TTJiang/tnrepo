//
//  NSDate+Ext.h
//  Pods
//
//  Created by JiangTeng on 16/2/3.
//
//

#import <Foundation/Foundation.h>

@interface NSDate (Ext)
- (NSDate *)TC_dateByAddingCalendarUnits:(NSCalendarUnit)calendarUnit amount:(NSInteger)amount;
@end
