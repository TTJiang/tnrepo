//
//  MVVersion.m
//  Pods
//
//  Created by JiangTeng on 15/12/13.
//
//

#import "MVVersion.h"

@implementation MVVersion
@synthesize version;

+ (MVVersion *)mainBundleVersion {
    NSString* current = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    return [[self alloc] initWithVersion:current];
}

+ (MVVersion *)osVersion {
    NSString *os = [[UIDevice currentDevice] systemVersion];
    return [[self alloc] initWithVersion:os];
}

- (id)init {
    return [self initWithVersion:nil];
}

- (id)initWithVersion:(NSString *)v {
    if(self = [super init]) {
        version = v;
    }
    return self;
}

- (BOOL)isNewerThan:(MVVersion *)ver {
    if(!ver)
        return YES;
    NSScanner *scanner1 = [[NSScanner alloc] initWithString:version];
    NSScanner *scanner2 = [[NSScanner alloc] initWithString:ver.version];
    BOOL result = NO;
    for(int i = 0; i < 4; i++) {
        NSInteger i1 = 0, i2 = 0;
        [scanner1 scanInteger:&i1];
        [scanner2 scanInteger:&i2];
        if(i1 > i2) {
            result = YES;
            break;
        } else if(i1 < i2) {
            result = NO;
            break;
        }
        [scanner1 scanString:@"." intoString:nil];
        [scanner2 scanString:@"." intoString:nil];
    }
    return result;
}

- (BOOL)isOlderThan:(MVVersion *)ver {
    if(!ver)
        return NO;
    return ![ver isNewerThan:self];
}
@end
