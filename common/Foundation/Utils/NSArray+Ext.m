//
//  NSArray+Ext.m
//  Pods
//
//  Created by JiangTeng on 15/12/8.
//
//

#import "NSArray+Ext.h"

@implementation NSArray (Ext)

- (void)eachWithIndex:(EnumerateBlock)block {
    NSInteger index = 0;
    for (id obj in self) {
        block(index, obj);
        index++;
    }
}

- (NSArray *)map:(TransformBlock)block{
    NSParameterAssert(block != nil);
    NSMutableArray *ret = [NSMutableArray arrayWithCapacity:self.count];
    for (id obj in self) {
        [ret addObject:block(obj)];
    }
    return ret;
}

- (NSArray *)select:(ValidationBlock)block{
    NSParameterAssert(block != nil);
    return [self objectsAtIndexes:[self indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return block(obj);
    }]];
}

- (NSArray *)reject:(ValidationBlock)block{
    NSParameterAssert(block != nil);
    
    return [self objectsAtIndexes:[self indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return !block(obj);
    }]];
}

- (id)reduce:(id)initial withBlock:(AccumulationBlock)block {
    NSParameterAssert(block != nil);
    id result = initial;
    
    for (id obj in self) {
        result = block(result, obj);
    }
    return result;
}

- (instancetype)take:(NSUInteger)n {
    if ([self count] <= n) return self;
    return [self subarrayWithRange:NSMakeRange(0, n)];
}

- (id)find:(ValidationBlock)block {
    for (id obj in self) {
        if (block(obj)) {
            return obj;
        }
    }
    return nil;
}

- (id)match:(ValidationBlock)block {
    for (id object in self) {
        if (block(object)) {
            return object;
        }
    }
    return nil;
}

- (BOOL)allObjectsMatched:(ValidationBlock)block {
    for (id obj in self) {
        if (!block(obj)) {
            return NO;
        }
    }
    return YES;
}

- (BOOL)anyObjectMatched:(ValidationBlock)block {
    for (id obj in self) {
        if (block(obj)) {
            return YES;
        }
    }
    return NO;
}

- (NSString *)join:(NSString *)seperator {
    NSMutableString *string = [NSMutableString string];
    [self eachWithIndex:^(NSInteger index, id obj) {
        if (index != 0) {
            [string appendString:seperator];
        }
        [string appendString:obj];
    }];
    return string;
    
}

- (BOOL)existObjectMatch:(ValidationBlock)block {
    return [self match:block] != nil;
}

- (BOOL)allObjectMatch:(ValidationBlock)block {
    return [self match:^BOOL(id obj) {
        return !block(obj);
    }] == nil;
}

- (NSArray *)groupBy:(TransformBlock)block {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    for (id obj in self) {
        NSString *key = block(obj);
        if (dic[key] == nil) {
            dic[key] = [NSMutableArray array];
        }
        [dic[key] addObject:obj];
    }
    return [dic allValues];
}

- (NSArray *)zip:(NSArray *)array {
    NSMutableArray *result = [NSMutableArray array];
    [self eachWithIndex:^(NSInteger index, id obj) {
        [result addObject:obj];
        if (index >= array.count) return;
        [result addObject:array[index]];
    }];
    return result;
}

- (NSString *)insertIntoPlaceHolderString:(NSString *)placeHolder {
    NSArray *components = [placeHolder componentsSeparatedByString:@"%%"];
    if ([components count] < 2) return placeHolder;
    return [[components zip:self] join:@""];
}
@end
