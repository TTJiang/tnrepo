//
//  MVCompatible.m
//  Pods
//
//  Created by JiangTeng on 15/12/13.
//
//

#import "MVCompatible.h"
#import "MVVersion.h"

double IPHONE_OS_MAIN_VERSION() {
    static double __iphone_os_main_version = 0.0;
    if(__iphone_os_main_version == 0.0) {
        NSString *sv = [[UIDevice currentDevice] systemVersion];
        NSScanner *sc = [[NSScanner alloc] initWithString:sv];
        if(![sc scanDouble:&__iphone_os_main_version])
            __iphone_os_main_version = -1.0;
    }
    return __iphone_os_main_version;
}


BOOL IPHONE_OS_3() {
    return IPHONE_OS_MAIN_VERSION() >= 3.0;
}


BOOL IPHONE_OS_4() {
    return IPHONE_OS_MAIN_VERSION() >= 4.0;
}


BOOL IPHONE_OS_4_2() {
    return IPHONE_OS_MAIN_VERSION() >= 4.2;
}

BOOL IPHONE_OS_4_3() {
    return IPHONE_OS_MAIN_VERSION() >= 4.3;
}

BOOL IPHONE_OS_5() {
    return IPHONE_OS_MAIN_VERSION() >= 5.0;
}

BOOL IPHONE_OS_6() {
    return IPHONE_OS_MAIN_VERSION() >= 6.0;
}

BOOL IPHONE_OS_7() {
    return IPHONE_OS_MAIN_VERSION() >= 7.0;
}

BOOL IPHONE_OS_8() {
    return IPHONE_OS_MAIN_VERSION() >= 8.0;
}

BOOL IPHONE_OS_9() {
    return IPHONE_OS_MAIN_VERSION() >= 9.0;
}

BOOL IPHONE_OS_EQUILORHIGHERTHAN(NSString *compareVersionString) {
    MVVersion *osVersion = [MVVersion osVersion];
    MVVersion *compVersion = [[MVVersion alloc] initWithVersion:compareVersionString];
    return ![compVersion isNewerThan:osVersion];
}

BOOL IPHONE_OS_SUPPORTMULTITASK() {
    UIDevice *device = [UIDevice currentDevice];
    if ([device respondsToSelector:@selector(isMultitaskingSupported)] && [device isMultitaskingSupported]) {
        return YES;
    } else {
        return NO;
    }
    
}
