//
//  NVActionSheetDelegate.h
//  Lego
//
//  Created by Johnson Zhang on 14-10-11.
//  Copyright (c) 2014年 dianping.com. All rights reserved.
//

#import "NVUIActionBlock.h"

#ifndef NVActionSheetController
#define NVActionSheetController [UIAlertController class] ? [LegoAlertController class] : [LegoActionSheet class]
#endif

#ifndef NVActionSheet
#define NVActionSheet id<NVActionSheetDelegate>
#endif

@protocol NVActionSheetDelegate <NSObject>

/** Creates and returns a new action sheet with only a title button.
 
 @warning You should add a cancel button manually by
 [addCancelButtonWithTitle:action:] at last.
 
 @param title The header of the action sheet.
 @return A newly created action sheet.
 */
+ (NVActionSheet)actionSheetWithTitle:(NSString *)title;

///-----------------------------------
/// @name Adding buttons
///-----------------------------------

/** Add a new button with an associated code block.
 
 @param title The text of the button.
 @param block A block of code.
 */
- (void)addButtonWithTitle:(NSString *)title action:(NVUIBlock)block;

- (void)addButtonWithTitle:(NSString *)title;

/** Set the destructive (red) button with an associated code block.
 
 @warning Because buttons cannot be removed from an action sheet,
 be aware that the effects of calling this method are cumulative.
 Previously added destructive buttons will become normal buttons.
 
 @param title The text of the button.
 @param block A block of code.
 */
- (void)addDestructiveButtonWithTitle:(NSString *)title action:(NVUIBlock)block;


/** Set the cancel button with an associated code block.
 
 @warning Because buttons cannot be removed from an action sheet,
 be aware that the effects of calling this method are cumulative.
 Previously added cancel buttons will become normal buttons.
 
 @param title The text of the button.
 @param block A block of code.
 */
- (void)addCancelButtonWithTitle:(NSString *)title action:(NVUIBlock)block;

/** show alert view
 */
- (void)showByViewController:(UIViewController *)vc;

@optional

- (void)show;

- (void)showInView:(UIView *)view;

- (void)setGa_label:(NSString *)label;

@end
