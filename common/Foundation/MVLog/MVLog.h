//
//  MVLog.h
//  Pods
//
//  Created by JiangTeng on 15/12/8.
//
//

#import <Foundation/Foundation.h>

#define MVLOG(format, ...) __MVLog(@__FILE__, __LINE__, [NSString stringWithFormat:format, ## __VA_ARGS__])

void __MVLog(NSString *file, NSInteger line, NSString * content);
