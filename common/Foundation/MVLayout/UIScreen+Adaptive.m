//
//  UIScreen+Adaptive.m
//  Nova
//
//  Created by dawei on 9/28/14.
//  Copyright (c) 2014 dianping.com. All rights reserved.
//

#import "UIScreen+Adaptive.h"

@implementation UIScreen (Adaptive)

+ (CGFloat)width {
    return [self mainScreen].bounds.size.width;
}

+ (CGFloat)height {
    return [self mainScreen].bounds.size.height;
}

+ (CGFloat)scale {
    return [self width] / 320.0f;
}

@end
