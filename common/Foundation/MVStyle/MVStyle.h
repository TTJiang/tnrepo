//
//  MVStyle.h
//  Pods
//
//  Created by JiangTeng on 15/12/9.
//
//

#import <Foundation/Foundation.h>

@interface UIColor (TenMovie)

/**
 * 主体的背影颜色
 */
+ (UIColor *)styleBackgroundColor;

/**
 * 主体的字体颜色
 */
+ (UIColor *)styleFontColor;

/**
 * 默认的字体颜色
 */
+ (UIColor *)defaultFontColor;

/**
 * 选中时字体颜色
 */
+ (UIColor *)selectFontColor;

@end

@interface UIFont (MVFontStyle)

//18pt
+ (UIFont *)headerTitleFont;

//17pt
+ (UIFont *)buttonTitleFont;

//10pt
+ (UIFont *)tabBarItemTitleFont;

// 11pt
+ (UIFont *)travelFooterFont;

//17pt
+ (UIFont *)subTitleFont1;

//15pt
+ (UIFont *)subTitleFont2;

//13pt
+ (UIFont *)contentFont1;

//12pt
+ (UIFont *)contentFont2;

@end


@interface UIColor (MVColorSytle)

// 255,132,0
+ (UIColor *)orangeStyleColor;

// 245,207,168
+ (UIColor *)orangeStyleHighlightedColor;

// 50,50,50
+ (UIColor *)normalTitleColor;

+ (UIColor *)orangeTitleColor;

// 135,135,135
+ (UIColor *)grayTitleColor1;

// 200,200,200
+ (UIColor *)graySeperateLineColor;

+ (UIColor *)topLineColor;

+ (UIColor *)middleLineColor;

+ (UIColor *)bottomLineColor;

// 248,248,248
+ (UIColor *)viewBackgroundColor;

// 248,248,248
+ (UIColor *)tableViewBackgroundColor;

// 0,0,0,0.05
+ (UIColor *)tableViewCellSelectedBackgroundColor;

// 217,217,217
+ (UIColor *)grayHightedColor;

// 228, 228, 228
+ (UIColor *)scoreReviewColor;

//橙色，alpha为1.0
+ (UIColor *)buttonTitleNormalColor;

//橙色，alpha为0.3
+ (UIColor *)buttonTitleHighlightedColor;

//商户详情页酒店“价格”“促销活动”等内容文本的红色
+ (UIColor *)hotelHighLightRedColor;

// 230,230,230
+ (UIColor *)lightGraySeperateLineColor;

//＃666666
+ (UIColor *)barTitleColor;

// 246,246,246
+ (UIColor *)grayBackgroundColor;

#pragma mark -
#pragma mark Button Style

+ (UIColor *)orangeButtonBackColor;

+ (UIColor *)whiteButtonBorderColor;

+ (UIColor *)whiteButtonTextColor;

+ (UIColor *)grayDisableButtonColor;

+ (UIColor *)orangeDisableButtonColor;

#pragma mark -

@end

@interface UIColor (Movie)

//f63 橙色 需要突出的价格颜色
+(UIColor*)mvColorWithf63;
//333 黑色 栏目大小标题，重要文字颜色
+(UIColor*)mvColorWith333;
//666 所有页面无须特殊处理的文字颜色
+(UIColor*)mvColorWith666;
//999 灰色 说明备注性文字的颜色
+(UIColor*)mvColorWith999;
//ccc 次要文字，表单内提示文字、不可操作的文字
+(UIColor*)mvColorWithccc;
//80b57e 绿色
+(UIColor*)mvColorWith80b57e;
//bbb
+(UIColor*)mvColorWithbbb;
//65c07a
+(UIColor*)mvColorWith65c07a;
+(UIColor*)mvColorWithff8400;
//3388bb
+(UIColor*)mvColorWIth3388bb;
//adadad
+(UIColor*)mvColorWithadadad;
//f0f0f0
+(UIColor*)mvColorWithf0;


@end

@interface UIFont (Movie)

//40 加粗
+ (UIFont *)mvFontWithSizeBold40;
//18 加粗 商户详情页团购详情页商户标题
+ (UIFont *)mvFontWithSizeBold18;
//18 商户详情页团购详情页商户标题
+ (UIFont *)mvFontWithSize18;
//17
+ (UIFont *)mvFontWithSize17;
//16 栏目标题
+ (UIFont *)mvFontWithSize16;
//15 表可进入状态的文字
+ (UIFont *)mvFontWithSize15;
//14 内容，正文
+ (UIFont *)mvFontWithSize14;
// 13
+ (UIFont *)mvFontWithSize13;
//12 备注等信息
+ (UIFont *)mvFontWithSize12;
+ (UIFont *)mvFontWithSize10;
@end