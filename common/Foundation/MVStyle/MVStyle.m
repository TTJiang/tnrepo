//
//  MVStyle.m
//  Pods
//
//  Created by JiangTeng on 15/12/9.
//
//

#import "MVStyle.h"
#import "UIColor+Ext.h"


@implementation UIColor (TenMovie)

+ (UIColor *)styleBackgroundColor{
    return [UIColor colorWithHexString:@"#3b383b"];
}

+ (UIColor *)styleFontColor{
    return [UIColor colorWithHexString:@"#748623"];
}


+ (UIColor *)defaultFontColor{
    
    return [UIColor colorWithHexString:@"#878787"];
}


+ (UIColor *)selectFontColor{
    
    return [UIColor colorWithHexString:@"#63bcdc"];
}


@end

@implementation UIFont (MVFontStyle)

//18pt
+ (UIFont *)headerTitleFont
{
    return [UIFont fontWithName:@"Helvetica" size:18];
}

//17pt
+ (UIFont *)buttonTitleFont
{
    return [UIFont fontWithName:@"Helvetica" size:17];
}

//11pt
+ (UIFont *)travelFooterFont
{
    return [UIFont fontWithName:@"Helvetica" size:11];
}

//10pt
+ (UIFont *)tabBarItemTitleFont
{
    return [UIFont fontWithName:@"Helvetica" size:10];
}

//17pt
+ (UIFont *)subTitleFont1
{
    return [UIFont fontWithName:@"Helvetica" size:17];
}

//15pt
+ (UIFont *)subTitleFont2
{
    return [UIFont fontWithName:@"Helvetica" size:15];
}

//13pt
+ (UIFont *)contentFont1
{
    return [UIFont fontWithName:@"Helvetica" size:13];
}

//12pt
+ (UIFont *)contentFont2
{
    return [UIFont fontWithName:@"Helvetica" size:12];
}

@end


@implementation UIColor (MVColorSytle)

// 255,132,0
+ (UIColor *)orangeStyleColor
{
    return [UIColor colorWithHexString:@"ff8400"];
}

// 245,207,168
+ (UIColor *)orangeStyleHighlightedColor
{
    return [UIColor colorWithRed:(245/255.f) green:(207/255.f) blue:(168/255.f) alpha:1];
}

// 50,50,50
+ (UIColor *)normalTitleColor
{
    return [UIColor colorWithHexString:@"#333"];
}

+ (UIColor *)orangeTitleColor
{
    return [UIColor colorWithHexString:@"#ff6633"];
}

// 135,135,135
+ (UIColor *)grayTitleColor1
{
    return [UIColor colorWithHexString:@"999999"];
}

+ (UIColor *)graySeperateLineColor
{
    return [UIColor colorWithRed:(200/255.f) green:(200/255.f) blue:(200/255.f) alpha:1.0];
}

+ (UIColor *)topLineColor
{
    return [UIColor colorWithHexString:@"d7d7d7"];
}

+ (UIColor *)middleLineColor
{
    return [UIColor colorWithHexString:@"e1e1e1"];
}

+ (UIColor *)bottomLineColor
{
    return [UIColor colorWithHexString:@"d7d7d7"];
}

+ (UIColor *)lightGraySeperateLineColor
{
    return [UIColor colorWithRed:(230/255.f) green:(230/255.f) blue:(230/255.f) alpha:1.0];
}


+ (UIColor *)viewBackgroundColor
{
    return [UIColor colorWithHexString:@"f0f0f0"];
}

+ (UIColor *)tableViewBackgroundColor
{
    return [UIColor viewBackgroundColor];
}

+ (UIColor *)tableViewCellSelectedBackgroundColor
{
    return [UIColor colorWithRed:0 green:0 blue:0 alpha:0.03f];
}

+ (UIColor *)grayHightedColor
{
    return [UIColor colorWithRed:(217/255.f) green:(217/255.f) blue:(217/255.f) alpha:1.0];
}

+ (UIColor *)scoreReviewColor
{
    return [UIColor colorWithRed:(228/255.f) green:(228/255.f) blue:(228/255.f) alpha:1.f];
}

+ (UIColor *)buttonTitleNormalColor
{
    //橙色
    return [UIColor colorWithRed:(255/255.f) green:(127/255.f) blue:(0/255.f) alpha:1.0];
}

+ (UIColor *)buttonTitleHighlightedColor
{
    //橙色
    return [UIColor colorWithRed:(255/255.f) green:(127/255.f) blue:(0/255.f) alpha:0.3];
}

+ (UIColor *)hotelHighLightRedColor
{
    return [UIColor colorWithHexString:@"#ff6633"];
    
}

+ (UIColor *)barTitleColor
{
    return [UIColor colorWithHexString:@"#666666"];
}

+ (UIColor *)grayBackgroundColor {
    return [UIColor colorWithRed:246/255.f green:246/255.f blue:246/255.f alpha:1];
}

#pragma mark -
#pragma mark Button Style

+ (UIColor *)orangeButtonBackColor
{
    return [UIColor colorWithHexString:@"f63"];
}

+ (UIColor *)whiteButtonBorderColor
{
    return [UIColor colorWithHexString:@"ccc"];
}

+ (UIColor *)whiteButtonTextColor
{
    return [UIColor colorWithHexString:@"666"];
}

+ (UIColor *)grayDisableButtonColor
{
    return [UIColor colorWithHexString:@"6ccc"];
}

+ (UIColor *)orangeDisableButtonColor
{
    return [UIColor colorWithHexString:@"6f63"];
}

#pragma mark -

@end

@implementation UIColor (Movie)

//ff6633 橙色
+(UIColor*)mvColorWithf63
{
    return [UIColor colorWithHexString:@"ff6633"];
}

//333 黑色
+(UIColor*)mvColorWith333
{
    return [UIColor colorWithHexString:@"333333"];
}

//666
+(UIColor*)mvColorWith666
{
    return [UIColor colorWithHexString:@"666666"];
}

//999 灰色
+(UIColor*)mvColorWith999
{
    return [UIColor colorWithHexString:@"999999"];
}

//ccc
+(UIColor*)mvColorWithccc
{
    return [UIColor colorWithHexString:@"cccccc"];
}

//80b57e 绿色
+(UIColor*)mvColorWith80b57e
{
    return [UIColor colorWithHexString:@"80b57e"];
    
}

//bbb
+(UIColor*)mvColorWithbbb
{
    return [UIColor colorWithHexString:@"bbbbbb"];
}

//65c07a
+(UIColor*)mvColorWith65c07a
{
    return [UIColor colorWithHexString:@"65c07a"];
}

+(UIColor*)mvColorWithff8400
{
    return [UIColor colorWithHexString:@"ff8400"];
}

+(UIColor*)mvColorWIth3388bb
{
    return [UIColor colorWithHexString:@"3388bb"];
}

+(UIColor*)mvColorWithadadad
{
    return [UIColor colorWithHexString:@"adadad"];
}

+(UIColor*)mvColorWithf0
{
    return [UIColor colorWithHexString:@"f0f0f0"];
}

@end

@implementation UIFont (Movie)

//40 加粗
+ (UIFont *)mvFontWithSizeBold40
{
    return [UIFont fontWithName:@"Helvetica-Bold"  size:40];
}

//18 加粗
+ (UIFont *)mvFontWithSizeBold18
{
    return [UIFont fontWithName:@"Helvetica-Bold"  size:18];
}

//18
+ (UIFont *)mvFontWithSize18;
{
    return [UIFont fontWithName:@"Helvetica"  size:18];
}

//17
+ (UIFont *)mvFontWithSize17;
{
    return [UIFont fontWithName:@"Helvetica"  size:17];
}

//16
+ (UIFont *)mvFontWithSize16
{
    return [UIFont fontWithName:@"Helvetica"  size:16];
}

//15
+ (UIFont *)mvFontWithSize15;
{
    return [UIFont fontWithName:@"Helvetica"  size:15];
}

//14
+ (UIFont *)mvFontWithSize14
{
    return [UIFont fontWithName:@"Helvetica"  size:14];
}

+ (UIFont *)mvFontWithSize13
{
    return [UIFont fontWithName:@"Helvetica"  size:13];
}

//12
+ (UIFont *)mvFontWithSize12
{
    return [UIFont fontWithName:@"Helvetica"  size:12];
}

//10
+ (UIFont *)mvFontWithSize10
{
    return [UIFont fontWithName:@"Helvetica"  size:10];
}
@end