//
//  NVUIActionBlock.h
//  Nova
//
//  Created by dawei on 12/16/13.
//  Copyright (c) 2013 dianping.com. All rights reserved.
//

#ifndef Nova_NVUIActionBlock_h
#define Nova_NVUIActionBlock_h
typedef void (^NVUIBlock)(void);
#endif

#ifndef Nova_NVActionBlock_h
#define Nova_NVActionBlock_h
typedef void (^NVActionBlock)(id sender);
#endif
