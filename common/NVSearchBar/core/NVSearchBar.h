//
//  NVSearchBar
//  Dianping
//
//  Created by Linyi on 13-8-29.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol NVSearchBarDelegate;
@class UICustomTextField;

@interface NVSearchBar : UIView

/**
 *  Default NO , hide the scopeIcon
 */
@property (readwrite, nonatomic) BOOL scopeIconHidden;

@property (nonatomic, strong) UIImage *barSearchImage;
@property (readwrite, nonatomic, strong) UIImage *barBackgroundImage;

@property (readwrite, nonatomic) UIEdgeInsets preferredEdgeInsets;

@property (readwrite, nonatomic, weak) id<NVSearchBarDelegate> searchBarDelegate;

@property (readwrite, nonatomic, weak) NSString *fieldPlaceHolder;

@property (readwrite, nonatomic, weak) NSString *searchText;

@property (nonatomic, assign) NSTextAlignment textAlignment;

@property (nonatomic, assign) UIReturnKeyType returnType;

@property (nonatomic, assign) BOOL enablesReturnKey;

@property (nonatomic, assign) CGRect    preferredFrame;

@property (nonatomic, assign) BOOL centerAlignPlaceHolder;  //提示文本是否居中，默认不居中

- (id)initWithBarDelegate:(id<NVSearchBarDelegate>)delegate;

- (void)fieldBecomeFirstResponder;

- (void)fieldResignFirstResponder;

- (BOOL)fieldIsFirstResponder;

- (UICustomTextField*)geneTextField;

@end


@interface UICustomTextField : UITextField

@property (nonatomic, assign) BOOL customPlaceholder;

@end



@protocol NVSearchBarDelegate<NSObject>

@optional

- (BOOL)searchBar:(NVSearchBar *)bar textFieldShouldBeginEditing:(UITextField *)textField;
- (BOOL)searchBar:(NVSearchBar *)bar textFieldShouldClear:(UITextField *)textField;
- (BOOL)searchBar:(NVSearchBar *)bar textFieldShouldReturn:(UITextField *)textField;
- (BOOL)searchBar:(NVSearchBar *)bar textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
@end