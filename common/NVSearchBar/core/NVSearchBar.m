//
//  NVSearchBar
//  Dianping
//
//  Created by Linyi on 13-8-29.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import "NVSearchBar.h"

@implementation UICustomTextField

- (void) drawPlaceholderInRect:(CGRect)rect {
    if (self.customPlaceholder) {
        
        [[UIColor colorWithRed:180/255.f green:180/255.f blue:180/255.f alpha:1] setFill];
        UIImage * searchIcon = [UIImage imageNamed:@"searchBar_Icon_Search"];
        rect = self.bounds;
        CGFloat placeholderWidth = [self nvSizeWithString:self.placeholder Font:self.font constrainedToSize:CGSizeMake(rect.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByClipping].width, iconWidth = searchIcon.size.width;

        CGRect rectToDraw = (CGRect){(rect.size.width - placeholderWidth - iconWidth) / 2, rect.origin.y, placeholderWidth, rect.size.height};
//        if (IPHONE_OS_7()) {
            rectToDraw.origin.y += 8;
//        }
        [self.placeholder drawInRect:rectToDraw withAttributes:@{NSFontAttributeName:self.font}];

        rectToDraw.origin.x -= iconWidth;
        rectToDraw.origin.y += 0.5f;
        rectToDraw.size = searchIcon.size;
        [searchIcon drawInRect:rectToDraw];
    } else {
        [super drawPlaceholderInRect:rect];
    }
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return [self textRectForBounds:bounds];
}

- (CGRect)textRectForBounds:(CGRect)bounds
{
    CGFloat margin = 10, clearButtonPadding = 16;

    bounds.origin.x += margin;
    bounds.size.width -= margin * 2 + clearButtonPadding;
    return bounds;
}

-(CGSize)nvSizeWithString:(NSString *)str Font:(UIFont *)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)mode
{
    if (!self || str.length == 0) {
        return CGSizeZero;
    }
    NSDictionary * attrs = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    CGSize sbSize = [str boundingRectWithSize:size options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
    sbSize.height = ceilf(sbSize.height);
    sbSize.width = ceilf(sbSize.width);
    return sbSize;
}
@end

@interface NVSearchBar ()<UITextFieldDelegate>

@property (readwrite, nonatomic, strong) UIImageView *bgImageView;
@property (readwrite, nonatomic, strong) UICustomTextField *searchTextField;

@end

@implementation NVSearchBar
@synthesize textAlignment;

- (void)internalInit
{
    self.bgImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.bgImageView.clipsToBounds = YES;
    self.bgImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self geneTextField];
    self.searchTextField.returnKeyType = UIReturnKeySearch;
    self.searchTextField.delegate = self;
    
    [self addSubview:self.bgImageView];
    [self addSubview:self.searchTextField];
    
    self.preferredEdgeInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    self.barBackgroundImage = [[UIImage imageNamed:@"searchBar"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)];

}

- (UICustomTextField*)geneTextField {
    if (self.searchTextField == nil) {
        self.searchTextField = [[UICustomTextField alloc] initWithFrame:CGRectZero];
    }
    return self.searchTextField;
}

- (id)initWithBarDelegate:(id<NVSearchBarDelegate>)delegate;
{
	self  = [super init];
	if(self) {
		self.searchBarDelegate = delegate;
        [self internalInit];
		[self awakeFromNib];
	}
	return self;
}

- (id)initWithFrame:(CGRect)aRect
{
	self = [super initWithFrame:aRect];
	if(self) {
        [self internalInit];
		[self awakeFromNib];
	}
	return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
	self = [super initWithCoder:decoder];
	if(self) {
        [self internalInit];
	}
	return self;
}

- (void)awakeFromNib
{
	[super awakeFromNib];
//    self.layer.borderColor = [UIColor yellowColor].CGColor;
//    self.layer.borderWidth = 1;
//    self.searchTextField.layer.borderColor = [UIColor greenColor].CGColor;
//    self.searchTextField.layer.borderWidth = 1;
    
    self.backgroundColor = [UIColor clearColor];
    self.clipsToBounds = YES;
    
    [self configSubviewsFrame];

    self.searchTextField.font = [UIFont systemFontOfSize:14];
    self.searchTextField.backgroundColor = [UIColor clearColor];
    self.searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.searchTextField.delegate = self;
    self.searchTextField.placeholder = @"请输入...";
    self.searchTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.searchTextField.enablesReturnKeyAutomatically = YES;
    self.scopeIconHidden = YES;
}

- (void)layoutSubviews
{
	[super layoutSubviews];
    [self configSubviewsFrame];
    if(self.centerAlignPlaceHolder) {
        self.searchTextField.textAlignment = NSTextAlignmentCenter;
    }
}

- (void)configSubviewsFrame
{
    CGFloat h = CGRectGetHeight(self.bounds) - self.preferredEdgeInsets.top - self.preferredEdgeInsets.bottom;
    CGFloat w = CGRectGetWidth(self.bounds) - self.preferredEdgeInsets.right - self.preferredEdgeInsets.left;
    h = h>0?h:0;
    w = w>0?w:0;
    self.bgImageView.frame = CGRectMake(self.preferredEdgeInsets.left,
                                        self.preferredEdgeInsets.top,
                                        w,
                                        h);
    self.bgImageView.image = self.barBackgroundImage;
    
    CGRect textFieldFrame = CGRectMake(CGRectGetMinX(self.bgImageView.frame) + 5,
                                       CGRectGetMidY(self.bgImageView.frame) - 0.5* CGRectGetHeight(self.bgImageView.frame),
									   CGRectGetWidth(self.bgImageView.frame) - 6,
                                       CGRectGetHeight(self.bgImageView.frame));
    
    self.searchTextField.frame = textFieldFrame;
}

- (void)fieldBecomeFirstResponder
{
	[self.searchTextField becomeFirstResponder];
}

- (void)fieldResignFirstResponder
{
	[self.searchTextField resignFirstResponder];
}

- (BOOL)fieldIsFirstResponder
{
	return [self.searchTextField isFirstResponder];
}

#pragma mark - Property

- (void)setReturnType:(UIReturnKeyType)returnType
{
    self.searchTextField.returnKeyType = returnType;
}

- (UIReturnKeyType)returnType
{
    return self.searchTextField.returnKeyType;
}

- (void)setEnablesReturnKey:(BOOL)enablesReturnKey{
    self.searchTextField.enablesReturnKeyAutomatically = enablesReturnKey;
}

- (BOOL)enablesReturnKey{
    return self.searchTextField.enablesReturnKeyAutomatically;
}

- (void)setBarSearchImage:(UIImage *)barSearchImage {
    _barSearchImage = barSearchImage;
    UIImageView *imageview = [[UIImageView alloc] initWithImage:barSearchImage];
    imageview.contentMode = UIViewContentModeScaleToFill;
    self.searchTextField.leftView = imageview;
}

- (void)setScopeIconHidden:(BOOL)scopeIconHidden
{
    if(scopeIconHidden) {
        self.searchTextField.leftView = nil;
        self.searchTextField.leftViewMode = UITextFieldViewModeNever;
    } else {
        self.searchTextField.leftView = [[UIImageView alloc] initWithImage:self.barSearchImage?:[UIImage imageNamed:@"home_topbar_icon_search"]];
        self.searchTextField.leftViewMode = UITextFieldViewModeUnlessEditing;
    }
}

- (BOOL)scopeIconHidden
{
    return (self.searchTextField.leftView == nil);
}

- (void)setTextAlignment:(NSTextAlignment)alignment
{
    textAlignment = alignment;
    switch (textAlignment) {
        case NSTextAlignmentLeft:
            self.scopeIconHidden = NO;
            break;
        case NSTextAlignmentCenter:
        {
            self.scopeIconHidden = YES;
            self.searchTextField.textAlignment = textAlignment;
            self.searchTextField.customPlaceholder = YES;
            break;
        }
        default:
            break;
    }
}

- (NSTextAlignment)textAlignment
{
    return textAlignment;
}

- (NSString *)fieldPlaceHolder
{
	return self.searchTextField.placeholder;
}

- (void)setFieldPlaceHolder:(NSString *)theStr
{
	self.searchTextField.placeholder = theStr;
}

- (NSString *)searchText
{
	return self.searchTextField.text;
}

- (void)setSearchText:(NSString *)theStr
{
	self.searchTextField.text = theStr;
}
#pragma mark - TextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.textAlignment = NSTextAlignmentLeft;
	if(self.searchBarDelegate && [self.searchBarDelegate respondsToSelector:@selector(searchBar:textFieldShouldBeginEditing:)]) {
		return [self.searchBarDelegate searchBar:self textFieldShouldBeginEditing:textField];
	}
	return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
	if(self.searchBarDelegate && [self.searchBarDelegate respondsToSelector:@selector(searchBar:textFieldShouldClear:)]) {
		return [self.searchBarDelegate searchBar:self textFieldShouldClear:textField];
	}
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	if(self.searchBarDelegate && [self.searchBarDelegate respondsToSelector:@selector(searchBar:textFieldShouldReturn:)]) {
		return [self.searchBarDelegate searchBar:self textFieldShouldReturn:textField];
	}
	return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	if(self.searchBarDelegate && [self.searchBarDelegate respondsToSelector:@selector(searchBar:textField:shouldChangeCharactersInRange:replacementString:)]) {
		return [self.searchBarDelegate searchBar:self textField:textField shouldChangeCharactersInRange:range replacementString:string];
	}

	return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if(self.centerAlignPlaceHolder) {
        textField.textAlignment = NSTextAlignmentLeft;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if(self.centerAlignPlaceHolder && textField.text.length ==  0) {
        textField.textAlignment = NSTextAlignmentCenter;
    }
}

- (void)setFrame:(CGRect)frame
{
    if (!CGRectEqualToRect(self.preferredFrame, CGRectZero)) {
        [super setFrame:self.preferredFrame];
    }
    else {
        [super setFrame:frame];
    }
}

- (void)dealloc
{
	self.searchBarDelegate = nil;
}

@end
