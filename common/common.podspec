Pod::Spec.new do |s|

  s.name         = "common"
  s.version      = "0.0.1"
  s.summary      = "A short description of movie-common."

  s.description  = <<-DESC
  DESC

  s.homepage     = "http://jiangteng/"

  s.license      = "MIT (example)"

  s.author             = { "jiangteng" => "jiangteng.cn@gmail.com" }

  s.source       = { :git => "git@code.ji.git", :tag => s.version }

  s.source_files  = "Classes", "Classes/**/*.{h,m}"
  s.exclude_files = "Classes/Exclude"


 # s.subspec 'Navigator' do |sp|
  #  sp.source_files = "Navigator/**/*.{h,m}"
   # sp.resources =  "Navigator/**/*.{png,jpg}"
  #end

  s.subspec 'Foundation' do |sp|
    sp.source_files = "Foundation/**/*.{h,m}"
  end

s.subspec 'DrawerViewController' do |sp|
    sp.source_files = "DrawerViewController/**/*.{h,m}"
  end

s.subspec 'ViewPagerController' do |sp|
    sp.source_files = "ViewPagerController/**/*.{h,m}"
  end

s.subspec 'FontIcon' do |sp|
    sp.source_files = "FontIcon/**/*.{h,m}"
end

s.subspec 'NVSearchBar' do |sp|
  sp.source_files = "NVSearchBar/**/*.{h,m}"
   sp.resources =  "NVSearchBar/**/*.{png,jpg}"
end

s.subspec 'PhotoList' do |sp|
    sp.source_files = "PhotoList/**/*.{h,m}"
    sp.resources =  "PhotoList/**/*.{png,jpg}"
end

#s.subspec 'FilterController' do |sp|
#   sp.source_files = "FilterController/**/*.{h,m}"
#end

# s.subspec 'NVBase' do |sp|
#     sp.source_files = "NVBase/**/*.{h,m}"
#   end

end
