Pod::Spec.new do |s|
s.name         = "PhotoList"
s.version      = "0.0.1"
s.summary      = "PhotoList"

s.description  = <<-DESC
PhotoList
DESC

s.homepage     = "http://code.jiangteng.com/"
s.license      = 'MIT'
s.author             = { "teng.jiang" => "jiangteng.cn@gmail.com" }
s.platform     = :ios, "6.0"
s.source       = {:git => "http://code", :tag => s.version}

s.source_files  = 'PhotoList/**/*.{h,m}'

s.requires_arc = true
end
