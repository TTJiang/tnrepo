Pod::Spec.new do |s|
s.name         = "NVSearchBar"
s.version      = "0.0.1"
s.summary      = "NVSearchBar"

s.description  = <<-DESC
NVSearchBar
DESC

s.homepage     = "http://code.jiangteng.com/"
s.license      = 'MIT'
s.author             = { "teng.jiang" => "jiangteng.cn@gmail.com" }
s.platform     = :ios, "6.0"
s.source       = {:git => "http://code", :tag => s.version}

s.source_files  = 'NVSearchBar/**/*.{h,m}'

s.requires_arc = true
end
