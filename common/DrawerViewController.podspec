Pod::Spec.new do |s|
s.name         = "DrawerViewController"
s.version      = "0.0.1"
s.summary      = "DrawerViewController"

s.description  = <<-DESC
DrawerViewController
DESC

s.homepage     = "http://code.jiangteng.com/"
s.license      = 'MIT'
s.author             = { "teng.jiang" => "jiangteng.cn@gmail.com" }
s.platform     = :ios, "6.0"
s.source       = {:git => "http://code", :tag => s.version}

s.source_files  = 'DrawerViewController/**/*.{h,m}'

s.requires_arc = true
end
