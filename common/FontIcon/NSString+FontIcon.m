//
//  NSString+FontIcon.m
//  Pods
//
//  Created by JiangTeng on 16/1/16.
//
//

#import "NSString+FontIcon.h"

@implementation NSString (FontIcon)
#pragma mark - Public API
+ (FIIcon)fontAwesomeEnumForIconIdentifier:(NSString*)string {
    NSDictionary *enums = [self enumDictionary];
    return [enums[string] integerValue];
}

+ (NSString*)fontAwesomeIconStringForEnum:(FIIcon)value {
    return [NSString fontAwesomeUnicodeStrings][value];
}

+ (NSString*)fontAwesomeIconStringForIconIdentifier:(NSString*)identifier {
    return [self fontAwesomeIconStringForEnum:[self fontAwesomeEnumForIconIdentifier:identifier]];
}


#pragma mark - Data Initialization
+ (NSArray *)fontAwesomeUnicodeStrings {
    
    static NSArray *fontAwesomeUnicodeStrings;
    
    static dispatch_once_t unicodeStringsOnceToken;
    dispatch_once(&unicodeStringsOnceToken, ^{
        
        fontAwesomeUnicodeStrings = @[@"\ue800",@"\ue801",@"\ue802",@"\ue803",@"\ue804",@"\ue805",@"\ue806",@"\ue807",@"\ue808",@"\ue809",@"\ue80a",@"\ue80b",@"\ue80c",@"\ue80d",@"\ue80e",@"\ue80f",@"\ue810",@"\ue811"];
        
    });
    
    return fontAwesomeUnicodeStrings;
}

+ (NSDictionary *)enumDictionary {
    
    static NSDictionary *enumDictionary;
    
    static dispatch_once_t enumDictionaryOnceToken;
    dispatch_once(&enumDictionaryOnceToken, ^{
        
        NSMutableDictionary *tmp = [[NSMutableDictionary alloc] init];
        
        tmp[@"fiUserOutline"]		= @(FIIconUserOutline);
        tmp[@"fiEmpire"]            = @(FIIconEmpire);
        tmp[@"fiUser"]				= @(FIIconUser);
        tmp[@"fiSearch"]			= @(FIIconSearch);
        tmp[@"fiPlus"]              = @(FIIconPlus);
        tmp[@"fiDownload"]			= @(FIIconDownload);
        tmp[@"fiHistory"]			= @(FIIconHistory);
        tmp[@"fiLeftOpenMini"]		= @(FIIconLeftOpenMini);
        tmp[@"fiRightOpenMini"]		= @(FIIconRightOpenMini);
        tmp[@"fiLeftOpenBig"]		= @(FIIconLeftOpenBig);
        tmp[@"fiRightOpenBig"]		= @(FIIconRightOpenBig);
        tmp[@"fiUpOpenBig"]			= @(FIIconUpOpenBig);
        tmp[@"fiDownOpenBig"]		= @(FIIconDownOpenBig);
        tmp[@"fiPlayOutline"]		= @(FIIconPlayOutline);
        tmp[@"fiPauseOutline"]		= @(FIIconPauseOutline);
        tmp[@"fiHomeOutline"]		= @(FIIconHomeOutline);
        tmp[@"fiHome"]              = @(FIIconHome);
        tmp[@"fiMonitor"]           = @(FIIconMonitor);

        enumDictionary = tmp;
    });
    
    return enumDictionary;
}
@end
