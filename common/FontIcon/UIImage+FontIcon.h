//
//  UIImage+FontIcon.h
//  Pods
//
//  Created by JiangTeng on 16/1/16.
//
//

#import <UIKit/UIKit.h>
#import "NSString+FontIcon.h"

@interface UIImage (FontIcon)

/**
 *	This method generates an UIImage with a given FontAwesomeIcon and format parameters
 *
 *	@param	identifier	NSString that identifies the icon
 *	@param	bgColor     UIColor for background image Color
 *	@param	iconColor	UIColor for icon color
 *	@param	scale       Scale factor between the image size and the icon size
 *	@param	fontSize	Font size used to be generate the image
 *
 *	@return	Image to be used wherever you want
 */
+(UIImage*)imageWithIcon2:(FIIcon)value backgroundColor:(UIColor*)bgColor iconColor:(UIColor*)iconColor fontSize:(int)fontSize;

/**
 *	This method generates an UIImage with a given FontAwesomeIcon and format parameters
 *
 *	@param	identifier	NSString that identifies the icon
 *	@param	bgColor     UIColor for background image Color
 *	@param	iconColor   UIColor for icon color
 *	@param	scale       Scale factor between the image size and the icon size
 *	@param	size        Size of the image to be generated
 *
 *	@return	Image to be used wherever you want
 */
+(UIImage*)imageWithIcon:(NSString*)identifier backgroundColor:(UIColor*)bgColor iconColor:(UIColor*)iconColor andSize:(CGSize)size;
/**
 *	This method generates an UIImage with a given FontAwesomeIcon and format parameters
 *
 *	@param	identifier	NSString that identifies the icon
 *	@param	bgColor     UIColor for background image Color
 *	@param	iconColor	UIColor for icon color
 *	@param	scale       Scale factor between the image size and the icon size
 *	@param	fontSize	Font size used to be generate the image
 *
 *	@return	Image to be used wherever you want
 */
+(UIImage*)imageWithIcon:(NSString*)identifier backgroundColor:(UIColor*)bgColor iconColor:(UIColor*)iconColor fontSize:(int)fontSize;
@end
