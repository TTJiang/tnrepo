//
//  NSString+FontIcon.h
//  Pods
//
//  Created by JiangTeng on 16/1/16.
//
//

#import <Foundation/Foundation.h>

static NSString *const kFontIconFamilyName = @"fontello";

/**
 @abstract FontAwesome Icons.
 */
typedef NS_ENUM(NSInteger, FIIcon) {
    FIIconUserOutline,
    FIIconEmpire,
    FIIconUser,
    FIIconSearch,
    FIIconPlus,
    FIIconDownload,
    FIIconHistory,
    FIIconLeftOpenMini,
    FIIconRightOpenMini,
    FIIconLeftOpenBig,
    FIIconRightOpenBig,
    FIIconUpOpenBig,
    FIIconDownOpenBig,
    FIIconPlayOutline,
    FIIconPauseOutline,
    FIIconHomeOutline,
    FIIconHome,
    FIIconMonitor
};



@interface NSString (FontIcon)

/**
 @abstract Returns the correct enum for a font-awesome icon.
 @discussion The list of identifiers can be found here: http://fortawesome.github.io/Font-Awesome/icons
 */
+ (FIIcon)fontAwesomeEnumForIconIdentifier:(NSString*)string;

/**
 @abstract Returns the font-awesome character associated to the icon enum passed as argument 
 */
+ (NSString*)fontAwesomeIconStringForEnum:(FIIcon)value;

/* 
 @abstract Returns the font-awesome character associated to the font-awesome identifier.
 @discussion The list of identifiers can be found here: http://fortawesome.github.io/Font-Awesome/icons
 */
+ (NSString*)fontAwesomeIconStringForIconIdentifier:(NSString*)identifier;

@end
