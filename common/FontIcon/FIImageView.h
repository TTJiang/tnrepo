//
//  FIImageView.h
//  Pods
//
//  Created by JiangTeng on 16/1/16.
//
//

#import <UIKit/UIKit.h>
#import "NSString+FontIcon.h"

@interface FIImageView : UIImageView

/* The background color for the default view displayed when the image is missing */
@property (nonatomic, strong) UIColor *defaultIconColor UI_APPEARANCE_SELECTOR;

/* Set the icon using the fontawesome icon's identifier */
@property (nonatomic, strong) NSString *defaultIconIdentifier;

/* Set the icon using the icon enumerations */
@property (nonatomic, assign) FIIcon defaultIcon;

/* The view that is displayed when the image is set to nil */
@property (nonatomic, strong) UILabel *defaultView;
@end
