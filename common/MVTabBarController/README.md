# MVTabBarController

[![CI Status](http://img.shields.io/travis/MVTabBarController/MVTabBarController.svg?style=flat)](https://travis-ci.org/MVTabBarController/MVTabBarController)
[![Version](https://img.shields.io/cocoapods/v/MVTabBarController.svg?style=flat)](http://cocoapods.org/pods/MVTabBarController)
[![License](https://img.shields.io/cocoapods/l/MVTabBarController.svg?style=flat)](http://cocoapods.org/pods/MVTabBarController)
[![Platform](https://img.shields.io/cocoapods/p/MVTabBarController.svg?style=flat)](http://cocoapods.org/pods/MVTabBarController)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MVTabBarController is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "MVTabBarController"
```

## Author

MVTabBarController, jiangteng.cn@gmail.com

## License

MVTabBarController is available under the MIT license. See the LICENSE file for more info.
