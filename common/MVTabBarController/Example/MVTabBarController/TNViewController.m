//
//  TNViewController.m
//  MVTabBarController
//
//  Created by MVTabBarController on 03/07/2016.
//  Copyright (c) 2016 MVTabBarController. All rights reserved.
//

#import "TNViewController.h"
#import "TNTestViewController.h"

@interface TNViewController ()<UITabBarControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) TNTestViewController *homeViewController;
@property (nonatomic, strong) TNTestViewController *userCenterViewController;
@property (nonatomic, strong) TNTestViewController *settingViewController;
@end

@implementation TNViewController


- (NSArray *)rootControllers {
    return @[
             self.homeViewController ?: [NSNull null],
             [self userCenterViewController] ?: [NSNull null],
             ];
}

- (id)init {
    self = [super init];
    if (self) {
        self.homeViewController = [[TNTestViewController alloc] init];
        [self.homeViewController.view setBackgroundColor:[UIColor redColor]];
        self.homeViewController.tabBarItem = [[UITabBarItem alloc] init];
        self.homeViewController.tabBarItem.title = @"主页";
        self.homeViewController.navigationItem.title = @"主页2";
        self.homeViewController.tabBarItem.titlePositionAdjustment = (UIOffset){0, -2};
        
        self.userCenterViewController = [[TNTestViewController alloc] init];
        [self.userCenterViewController.view setBackgroundColor:[UIColor brownColor]];
        self.userCenterViewController.tabBarItem = [[UITabBarItem alloc] init];
        self.userCenterViewController.tabBarItem.title = @"我的";
        self.userCenterViewController.navigationItem.title = @"我的2";
        self.userCenterViewController.tabBarItem.titlePositionAdjustment = (UIOffset){0, -2};
        
        self.settingViewController = [[TNTestViewController alloc] init];
        [self.userCenterViewController.view setBackgroundColor:[UIColor blueColor]];
        self.settingViewController.tabBarItem = [[UITabBarItem alloc] init];
        self.settingViewController.tabBarItem.title = @"设置";
        self.settingViewController.navigationItem.title = @"设置2";
        self.settingViewController.tabBarItem.titlePositionAdjustment = (UIOffset){0, -2};
        
        self.delegate = self;
    }
    
    return self;
}

- (NSArray *)normalIcons
{
    return @[@[@"home_icon", @"home_icon_blue"],
             @[@"mine_icon", @"mine_icon_blue"],
             @[@"setting_icon", @"setting_icon_blue"]];
}

- (NSArray *)tabControllers
{
    return  @[self.homeViewController,
              self.userCenterViewController,
              self.settingViewController];
}

- (void)setTabbarImages
{
    NSArray * controllers = [self tabControllers];
    for (UIViewController * controller in controllers) {
        NSArray * icon = [self normalIcons][[controllers indexOfObject:controller]];
        [controller.tabBarItem setImage:[UIImage imageNamed:[icon firstObject]] withHighLightedImage:[UIImage imageNamed:[icon lastObject]]];
    }
}

- (void)setupSubViewControllers
{
    NSMutableArray *vcArray = [[NSMutableArray alloc] init];
    [vcArray addObject:self.homeViewController];
    
    UINavigationController *nv1 =[[UINavigationController alloc] initWithRootViewController:self.userCenterViewController];
    nv1.navigationBar.translucent = NO;
    [vcArray addObject:nv1];
    
    [vcArray addObject:[[UINavigationController alloc] initWithRootViewController:self.settingViewController]];
    
    [self setViewControllers:vcArray];
    self.selectedItemTitleColor = [UIColor redColor];
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    
    return YES;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.isTabBarBlur = NO;
    [self setTabbarImages];
    [self setupSubViewControllers];
    self.automaticallyAdjustsScrollViewInsets = NO;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
