//
//  TNAppDelegate.h
//  MVTabBarController
//
//  Created by MVTabBarController on 03/07/2016.
//  Copyright (c) 2016 MVTabBarController. All rights reserved.
//

@import UIKit;

@interface TNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
