//
//  main.m
//  MVTabBarController
//
//  Created by MVTabBarController on 03/07/2016.
//  Copyright (c) 2016 MVTabBarController. All rights reserved.
//

@import UIKit;
#import "TNAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TNAppDelegate class]));
    }
}
