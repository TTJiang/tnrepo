//
//  MVTabBarController.h
//  Pods
//
//  Created by JiangTeng on 15/12/10.
//
//

#import <UIKit/UIKit.h>

@interface MVTabBarController : UIViewController

@property(nonatomic, strong) UIColor    *normalItemTitleColor;
@property(nonatomic, strong) UIColor    *selectedItemTitleColor;
@property (nonatomic, readwrite) BOOL   isTabBarBlur;

- (void)setItemImage:(UIImage *)normalImage withHighLightedImage:(UIImage *)highLightedImage withTitle:(NSString *)title atIndex:(NSInteger)index;

- (void)clearBadgeOnItemIndex:(NSInteger)index;
- (void)setBadgeOnItemIndex:(NSInteger)index withString:(NSString *)badgeValue;

- (UIButton *)createBadgeViewAtIndex:(NSInteger)index withString:(NSString *) badgeValue;

#pragma -mark  UITabBarController 属性/方法：

@property(nonatomic,readonly) UITabBar *tabBar NS_AVAILABLE_IOS(3_0);
@property(nonatomic,assign) id<UITabBarControllerDelegate> delegate;
@property(nonatomic,assign) UIViewController *selectedViewController;
@property(nonatomic) NSUInteger selectedIndex;

- (void)setViewControllers:(NSArray *)viewControllers;

- (void)setViewControllers:(NSArray *)viewControllers animated:(BOOL)animated;

- (NSArray *)viewControllers;
@end

@interface UITabBarItem (MVTabBar)
- (void)setImage:(UIImage *)image withHighLightedImage:(UIImage *)highlightedImg;
@end
