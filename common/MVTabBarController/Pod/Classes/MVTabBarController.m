//
//  MVTabBarController.m
//  Pods
//
//  Created by JiangTeng on 15/12/10.
//
//

#import "MVTabBarController.h"

double TABBAR_IPHONE_OS_MAIN_VERSION() {
    static double tabbar__iphone_os_main_version = 0.0;
    if(tabbar__iphone_os_main_version == 0.0) {
        NSString *sv = [[UIDevice currentDevice] systemVersion];
        NSScanner *sc = [[NSScanner alloc] initWithString:sv];
        if(![sc scanDouble:&tabbar__iphone_os_main_version])
            tabbar__iphone_os_main_version = -1.0;
    }
    return tabbar__iphone_os_main_version;
}

@implementation MVTabBarController
{
    UITabBarController  *_tabBarController;
}

- (id)init
{
    if (self = [super init]) {
        _tabBarController = [[UITabBarController alloc] init];
        [self addChildViewController:_tabBarController];
    }
    return self;
}

- (void)loadView
{
    [super loadView];
    _tabBarController.view.frame = self.view.bounds;
    _tabBarController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:_tabBarController.view];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNormalItemTitleColor:self.normalItemTitleColor ?: [UIColor grayColor]];
    [self setSelectedItemTitleColor:self.selectedItemTitleColor ?: self.normalItemTitleColor ?: [UIColor grayColor]];
    //#fff0f0f0
    UIColor *color = [UIColor colorWithRed:((CGFloat)255 / 255.f) green:((CGFloat)240 / 255.f) blue:((CGFloat)240 / 255.f) alpha:((CGFloat)240 / 255.f)];
    self.tabBar.backgroundImage = [[self imageWithColor:color] stretchableImageWithLeftCapWidth:0 topCapHeight:0];
    self.tabBar.selectionIndicatorImage = [UIImage imageNamed:@"ng_item_indicator"];
}

- (void)setIsTabBarBlur:(BOOL)isTabBarBlur
{
    UIColor *color;
    if (isTabBarBlur) {
        //#e0f0f0f0
        color = [UIColor colorWithRed:((CGFloat)224 / 255.f) green:((CGFloat)240 / 255.f) blue:((CGFloat)240 / 255.f) alpha:((CGFloat)240 / 255.f)];
    }else{
        color = [UIColor colorWithRed:((CGFloat)255 / 255.f) green:((CGFloat)240 / 255.f) blue:((CGFloat)240 / 255.f) alpha:((CGFloat)240 / 255.f)];
    }
    self.tabBar.backgroundImage = [[self imageWithColor:color] stretchableImageWithLeftCapWidth:0 topCapHeight:0];
}

- (void)setNormalItemTitleColor:(UIColor *)aNormalItemTitleColor {
    _normalItemTitleColor = aNormalItemTitleColor;
    for (UIViewController *vc in self.viewControllers) {
        [vc.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObject:aNormalItemTitleColor forKey:UITextAttributeTextColor] forState:UIControlStateNormal];
    }
}

- (void)setSelectedItemTitleColor:(UIColor *)aSelectedItemTitleColor {
    _selectedItemTitleColor = aSelectedItemTitleColor;
    for (UIViewController *vc in self.viewControllers) {
        [vc.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObject:aSelectedItemTitleColor forKey:UITextAttributeTextColor] forState:UIControlStateSelected];
    }
    self.tabBar.tintColor = aSelectedItemTitleColor;
}

- (void)setItemImage:(UIImage *)normalImage withHighLightedImage:(UIImage *)highLightedImage withTitle:(NSString *)title atIndex:(NSInteger)index
{
    if (index < 0 || index >= self.viewControllers.count) {
        return;
    }
    UIViewController *vc = (UIViewController *)self.viewControllers[index];
    vc.tabBarItem = [[UITabBarItem alloc] init];
    vc.tabBarItem.title = title;
    [vc.tabBarItem setImage:normalImage withHighLightedImage:highLightedImage];
}

- (BOOL)isPureInteger:(NSString *)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    NSInteger val;
    return [scan scanInteger:&val] && [scan isAtEnd];
}

#define kBadgeViewTag	8080
- (UIButton *)createBadgeViewAtIndex:(NSInteger)index withString:(NSString *) badgeValue{
    UIButton *_badge;
    UIImage *badgeImage;
    
    UITabBarItem *item = self.tabBar.items[index];
    UIView *view = [item valueForKey:@"view"];
    CGFloat itemRight = view.frame.origin.x + view.frame.size.width;
    CGFloat itemWidth = view.frame.size.width;
    
    if ([self isPureInteger:badgeValue]) {
        if (badgeValue.integerValue > 0 && badgeValue.integerValue < 10) {
            _badge = [[UIButton alloc] initWithFrame:CGRectMake(itemRight - itemWidth / 2 + 6, 6.0, 16, 16)];
            badgeImage = [UIImage imageNamed:@"footerbar_tips_reddigit_s"];
            [_badge setTitle:badgeValue forState:UIControlStateNormal];
        }else if (badgeValue.integerValue >= 10 && badgeValue.integerValue <= 99) {
            _badge = [[UIButton alloc] initWithFrame:CGRectMake(itemRight - itemWidth / 2 + 6, 6.0, 22, 16)];
            badgeImage = [UIImage imageNamed:@"footerbar_tips_reddigit"];
            [_badge setTitle:badgeValue forState:UIControlStateNormal];
        }
    }else {
        if (badgeValue.length > 0) {
            _badge = [[UIButton alloc] initWithFrame:CGRectMake(itemRight - itemWidth / 4.f - 6, 3.0, 12.0, 12.0)];
            badgeImage = [[UIImage imageNamed:@"home_footerbar_fan"] stretchableImageWithLeftCapWidth:4 topCapHeight:0];
            [_badge setTitle:badgeValue forState:UIControlStateNormal];
            
            CGRect newRect, originRect;
            newRect = originRect = _badge.frame;
            CGFloat badgeLenth = badgeValue.length * 10 + 6;
            newRect.size.width = badgeLenth;
            newRect.origin.x = CGRectGetMaxX(originRect) - badgeLenth / 2.f;
            _badge.frame = newRect;
        } else {
            _badge = [[UIButton alloc] initWithFrame:CGRectMake(itemRight - itemWidth / 2 + 13, 6.0, 8.0, 8.0)];
            badgeImage = [UIImage imageNamed:@"home_footerbar_dot"];
        }
    }
    
    [_badge setBackgroundImage:badgeImage forState:UIControlStateNormal];
    _badge.titleLabel.font = [UIFont systemFontOfSize:10];
    _badge.showsTouchWhenHighlighted = NO;
    _badge.adjustsImageWhenDisabled = NO;
    _badge.adjustsImageWhenHighlighted = NO;
    _badge.reversesTitleShadowWhenHighlighted = NO;
    _badge.userInteractionEnabled = NO;
    _badge.tag = kBadgeViewTag + index;
    return _badge;
}

- (void)setBadgeOnItemIndex:(NSInteger)index withString:(NSString *)badgeValue
{
    if (index < 0 || index >= self.viewControllers.count) {
        return;
    }
    
    UIButton *badgeView = (UIButton *)[self.view viewWithTag:kBadgeViewTag + index];
    if (badgeView) {
        [badgeView removeFromSuperview];
    }
    badgeView = [self createBadgeViewAtIndex:index withString:badgeValue];
    [self.tabBar addSubview:badgeView];
    //    [((UIViewController *)self.viewControllers[index]).tabBarItem setBadgeValue:badgeValue];
}

- (void)clearBadgeOnItemIndex:(NSInteger)index
{
    if (index < 0 || index >= self.viewControllers.count) {
        return;
    }
    
    UIButton *badgeView = (UIButton *)[self.view viewWithTag:kBadgeViewTag + index];
    if (badgeView) {
        [badgeView removeFromSuperview];
    }
}



#pragma mark -
#pragma -mark  UITabBarController 方法：

- (void)setViewControllers:(NSArray *)viewControllers
{
    [self setViewControllers:viewControllers animated:NO];
}

- (void)setViewControllers:(NSArray *)viewControllers animated:(BOOL)animated
{
    [_tabBarController setViewControllers:viewControllers animated:animated];
    
    if (_normalItemTitleColor) {
        [self setNormalItemTitleColor:_normalItemTitleColor];
    }
    if (_selectedItemTitleColor) {
        [self setSelectedItemTitleColor:_selectedItemTitleColor];
    }
}

- (NSArray *)viewControllers
{
    return _tabBarController.viewControllers;
}

- (UITabBar *)tabBar
{
    return _tabBarController.tabBar;
}

- (void)setDelegate:(id<UITabBarControllerDelegate>)delegate
{
    [_tabBarController setDelegate:delegate];
}

- (id<UITabBarControllerDelegate>)delegate
{
    return _tabBarController.delegate;
}

- (void)setSelectedIndex:(NSUInteger)index
{
    [_tabBarController setSelectedIndex:index];
}

- (NSUInteger)selectedIndex
{
    return _tabBarController.selectedIndex;
}

- (void)setSelectedViewController:(UIViewController *)viewController
{
    [_tabBarController setSelectedViewController:viewController];
}

- (UIViewController *)selectedViewController
{
    return _tabBarController.selectedViewController;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end

@implementation UITabBarItem (MVTabBar)

- (void)setImage:(UIImage *)image withHighLightedImage:(UIImage *)highlightedImg
{
    if (TABBAR_IPHONE_OS_MAIN_VERSION() >= 7.0) {
        [self setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
        [self setSelectedImage:[highlightedImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
//        [self setImage:image];
//        [self setSelectedImage:highlightedImg];
    }else {
        [self setFinishedSelectedImage:highlightedImg withFinishedUnselectedImage:image];
    }
}
@end

