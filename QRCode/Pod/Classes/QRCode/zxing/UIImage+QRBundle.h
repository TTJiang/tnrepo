//
//  UIImage+Bundle.h
//  ZXingWidget
//
//  Created by chenwang on 13-9-18.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (QRBundle)

+ (UIImage *)imageNamed:(NSString *)name bundleName:(NSString *)bundleName;

@end
