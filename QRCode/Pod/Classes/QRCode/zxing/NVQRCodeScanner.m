//
//  NVQRCodeScanner.m
//  Nova
//
//  Created by chen yuan on 1/7/14.
//  Copyright (c) 2014 dianping.com. All rights reserved.
//

#import "NVQRCodeScanner.h"
#import "NVAVCaptureScanViewController.h"

@interface NVQRCodeScanner()<CaptureScanViewDelegate>

@property (nonatomic, weak) id<QRScannerDelegate> delegate;

@end

@implementation NVQRCodeScanner
static NVQRCodeScanner * _instance;

+ (NVQRCodeScanner *)instance
{
    @synchronized(self) {
        if (!_instance) {
            _instance = [[NVQRCodeScanner alloc] init];
        }
        return _instance;
    }
}

- (UIViewController *)getQRCodeScannerWithDelegate:(id<QRScannerDelegate>)delegate
{
    self.delegate = delegate;
    if ([self version] >= 7.0) {
        NVAVCaptureScanViewController * avCaptureScanViewController = [[NVAVCaptureScanViewController alloc] init];
        avCaptureScanViewController.delegate = self;
        return avCaptureScanViewController;
    } else {
        return nil;
    }
}

- (CGFloat)version {
    static double __iphone_os_main_version = 0.0;
	if(__iphone_os_main_version == 0.0) {
		NSString *sv = [[UIDevice currentDevice] systemVersion];
		NSScanner *sc = [[NSScanner alloc] initWithString:sv];
		if(![sc scanDouble:&__iphone_os_main_version])
			__iphone_os_main_version = -1.0;
	}
    return __iphone_os_main_version;
}

- (void)captureScanViewController:(UIViewController *)controller didScanResult:(NSString *)result
{
    [self.delegate scanViewController:controller didScanResult:result];
}

- (void)captureScanViewControllerFailed:(UIViewController *)controller
{
    [self.delegate scanViewControllerFailed:controller];
}

- (void)dealloc
{
    _delegate = nil;
    _instance = nil;
}

@end
