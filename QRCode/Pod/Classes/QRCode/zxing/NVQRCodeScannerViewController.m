//
//  NVQRCodeScannerViewController.m
//  Pods
//
//  Created by hzc on 15/7/20.
//
//

#import "NVQRCodeScannerViewController.h"
#import "NVAVCaptureScanViewController.h"
//#import "MVNavigator.h"
//#import "MVNavigationController.h"

@interface NVQRCodeScannerViewController ()<CaptureScanViewDelegate>
@property (nonatomic, strong) NVAVCaptureScanViewController *avCaptureScanViewController;
@property (nonatomic, assign) BOOL notAnimation;
@end

@implementation NVQRCodeScannerViewController

- (void)dealloc {
    _avCaptureScanViewController.delegate = nil;
    _avCaptureScanViewController = nil;
}

//- (BOOL)handleWithURLAction:(MVURLAction *)urlAction {
//    if (urlAction.animation == NVNaviAnimationNone) {
//        _notAnimation = YES;
//    }
//    if (![[urlAction stringForKey:@"utm"] isEqualToString:@"force_touch"]) {
//        _notAnimation = YES;
//    }
//    return [super handleWithURLAction:urlAction];
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"扫一扫";
    if (self.notAnimation) {
//        [self drawerAnimationDidEnd:(MVNavigationController *)self.navigationController];
    }
}

- (void)drawerAnimationDidEnd:(UINavigationController *)navigationController
{
    if (!self.avCaptureScanViewController) {
        self.avCaptureScanViewController = [[NVAVCaptureScanViewController alloc] init];
        self.avCaptureScanViewController.delegate = self;
        [self.view addSubview:self.avCaptureScanViewController.view];
        [self addChildViewController:self.avCaptureScanViewController];
    }
}

- (void)captureScanViewController:(UIViewController *)controller didScanResult:(NSString *)result
{
    if ([self checkScanResult:result]) {
        [self handleScanResult:result];
    } else {
        [self showErrorQRCode];
    }
}

- (void)captureScanViewControllerFailed:(UIViewController *)controller
{
    [self showErrorQRCode];
}

- (BOOL)checkScanResult:(NSString *)result {
    if (![result hasPrefix:@"http://"] && ![result hasPrefix:@"https://"] && ![result hasPrefix:@"dianping://"]) {
        return NO;
    }
    return YES;
}

- (void)showErrorQRCode
{
    // 如果二维码不正确，则弹出提示
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"暂时无法识别" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
    [alert show];
}

- (void)handleScanResult:(NSString *)result {
    NSURL *url = [NSURL URLWithString:result];
    if ([result hasPrefix:@"http://dpurl.cn/m/s"]) {
        NSString *shopID = [result substringFromIndex:[@"http://dpurl.cn/m/s" length]];
        NSString *uriString = [NSString stringWithFormat:@"dianping://shopinfo?id=%@", shopID];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:uriString]];
        return;
    }
    
//    [MVNavigator navigator].preAction = ^ NSArray * {
//        UIViewController *vc = [(UINavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController popViewControllerAnimated:NO];
//        if (vc) {
//            return [NSArray arrayWithObject:vc];
//        } else {
//            return nil;
//        }
//    };
//    [[MVNavigator navigator] openURLAction:[MVURLAction actionWithURL:url]];
}


@end
