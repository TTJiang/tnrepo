//
//  NVQRCodeScanner.h
//  Nova
//
//  Created by chen yuan on 1/7/14.
//  Copyright (c) 2014 dianping.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol QRScannerDelegate <NSObject>

- (void)scanViewController:(UIViewController *)controller didScanResult:(NSString *)result;
- (void)scanViewControllerFailed:(UIViewController *)controller;

@end

@interface NVQRCodeScanner : NSObject

+ (NVQRCodeScanner *)instance;
- (UIViewController *)getQRCodeScannerWithDelegate:(id<QRScannerDelegate>)delegate;

@end
