//
//  NVAVCaptureScanViewController.h
//  Nova
//
//  Created by chen yuan on 1/7/14.
//  Copyright (c) 2014 dianping.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CaptureScanViewDelegate <NSObject>

@required
- (void)captureScanViewController:(UIViewController *)controller didScanResult:(NSString *)result;

@optional
- (void)captureScanViewControllerFailed:(UIViewController *)controller;

@end

#if !TARGET_IPHONE_SIMULATOR
#define HAS_AVFF 1
#endif

@interface NVAVCaptureScanViewController : UIViewController

@property (nonatomic, assign) id<CaptureScanViewDelegate> delegate;

@end