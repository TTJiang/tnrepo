//
//  UIImage+Bundle.m
//  ZXingWidget
//
//  Created by chenwang on 13-9-18.
//
//

#import "UIImage+QRBundle.h"

@implementation UIImage (QRBundle)

+ (UIImage *)imageNamed:(NSString *)name bundleName:(NSString *)bundleName
{
    NSString *pathExtension = [name pathExtension];
    if (pathExtension && pathExtension.length > 0) {
        name = [name substringToIndex:name.length - pathExtension.length - 1];
    }
    NSString *path = nil;
    NSBundle *bundle = [NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:bundleName withExtension:@"bundle"]];
    if ((NSInteger)[UIScreen mainScreen].scale == 3) {
        path = [bundle pathForResource:[NSString stringWithFormat:@"%@@3x", name] ofType:@"png"];
        if (path == nil) {
            path = [bundle pathForResource:[NSString stringWithFormat:@"%@@2x", name] ofType:@"png"];
        }
    }
    else {
        path = [bundle pathForResource:[NSString stringWithFormat:@"%@@2x", name] ofType:@"png"];
    }
    if (path == nil) {
        path = [bundle pathForResource:[NSString stringWithFormat:@"%@", name] ofType:@"png"];
    }
    return [UIImage imageWithContentsOfFile:path];
}

@end
