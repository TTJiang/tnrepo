//
//  QREncoder.h
//  Nova
//
//  Created by chenwang on 13-5-27.
//  Copyright (c) 2013年 dianping.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QREncoder : NSObject

// 生成指定大小的二维码图片
+ (UIImage *)encode:(NSString *)string imageSize:(CGFloat)size;

@end
