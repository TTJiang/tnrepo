//
//  QREncoder.m
//  Nova
//
//  Created by chenwang on 13-5-27.
//  Copyright (c) 2013年 dianping.com. All rights reserved.
//

#import "QREncoder.h"
#import "QREncoder_internal.h"

@implementation QREncoder

+ (UIImage *)encode:(NSString *)string imageSize:(CGFloat)size
{
    DataMatrix* qrMatrix = [QREncoder_internal encodeWithECLevel:QR_ECLEVEL_H version:QR_VERSION_AUTO string:string];
    return [QREncoder_internal renderDataMatrix:qrMatrix imageDimension:size];
}

@end
