//
//  QRDefinition.h
//  Nova
//
//  Created by chenwang on 13-5-27.
//  Copyright (c) 2013年 dianping.com. All rights reserved.
//

#ifndef QRDefinition
#define QRDefinition

#define QR_LEVEL_L	0
#define QR_LEVEL_M	1
#define QR_LEVEL_Q	2
#define QR_LEVEL_H	3

#endif