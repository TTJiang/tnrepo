//
//  TNAppDelegate.h
//  QRCode
//
//  Created by QRCode on 03/06/2016.
//  Copyright (c) 2016 QRCode. All rights reserved.
//

@import UIKit;

@interface TNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
