//
//  TNViewController.m
//  QRCode
//
//  Created by QRCode on 03/06/2016.
//  Copyright (c) 2016 QRCode. All rights reserved.
//

#import "TNViewController.h"
#import "NVQRCodeScanner.h"

@interface TNViewController ()<QRScannerDelegate>

@end

@implementation TNViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeContactAdd];
    btn.frame = CGRectMake(50, 50, 50, 50);
    [btn addTarget:self action:@selector(tapScanPay) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)tapScanPay{
    
    
//    MVNavigationController *rootNaviVC = (MVNavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController;
    
    [self.navigationController pushViewController:[[NVQRCodeScanner instance] getQRCodeScannerWithDelegate:self] animated:YES];
}

#define kErrorQRCodeURL	@"此商户不存在，请扫描正确的二维码哦！"

- (void)scanViewController:(UIViewController *)controller didScanResult:(NSString *)result
{
    if ([self checkScanResult:result]) {
        self.navigationItem.rightBarButtonItem.enabled = YES;
        [self handleScanResult:result];
    } else if (result && result.length > 0) {
        //        NVQRScanViewController *qrscan = [[NVQRScanViewController alloc] init];
        //        qrscan.scanContent = result;
        //        NVNavigationController *rootNaviVC = (NVNavigationController *)[UIApplication sharedApplication].keyWindow.rootViewController;
        //        [rootNaviVC pushViewController:qrscan animated:YES];
    } else {
        [self showErrorQRCode];
    }
}

- (void)scanViewControllerFailed:(UIViewController *)controller
{
    [self showErrorQRCode];
}

- (void)showErrorQRCode
{
    // 如果二维码不正确，则弹出提示
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"暂时无法识别" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"确定", nil];
    //    alert.tag = EAVT_BarcodeError;
    [alert show];
}

- (BOOL)checkScanResult:(NSString *)result {
    if (![result hasPrefix:@"http://"] && ![result hasPrefix:@"https://"] && ![result hasPrefix:@"dianping://"]) {
        return NO;
    }
    return YES;
}

- (NSString *)checkMockDomain:(NSString *)result
{
    // 仅debug模式下才生效
    //    if (![[NVEnvironment defaultEnvironment] isDebug]) {
    //        return nil;
    //    }
    //    NSURL *testURL = [NSURL URLWithNoNilString:result];
    //    if (testURL && [testURL.query hasPrefix:@"_=0__0&uid="]) {
    //        return [NSString stringWithFormat:@"%@:%@", testURL.host, testURL.port];
    //    }
    return nil;
}

- (NSString *)checkSwitchAlphaDomain:(NSString *)result
{
    // 仅debug模式下才生效
    //    if (![[NVEnvironment defaultEnvironment] isDebug]) {
    //        return nil;
    //    }
    //    NSURL *testURL = [NSURL URLWithNoNilString:result];
    //    if (testURL && [testURL.query hasPrefix:@"_=*__*"]) {
    //        NSString *String1=result;
    //        String1=[String1 substringFromIndex:@"http://".length];
    //        String1=[String1 substringToIndex:String1.length-@"?_=*__*".length];
    //        NSLog(@"String1:%@",String1);
    //        return String1;
    //    }
    return nil;
}

- (BOOL)checkLogin:(NSString *)result
{
    // 仅debug模式下才生效
    //    if (![[NVEnvironment defaultEnvironment] isDebug]) {
    //        return NO;
    //    }
    //    NSURL *testURL = [NSURL URLWithNoNilString:result];
    //    if (testURL &&
    //        [testURL.query hasPrefix:@"_=x_x"] &&
    //        [result hasPrefix:@"http://m.dper.com/weblogincallback"]) {
    //        return YES;
    //    }
    
    return NO;
}

- (void)handleScanResult:(NSString *)result {
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
