#
# Be sure to run `pod lib lint QRCode.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "QRCode"
  s.version          = "0.1.0"
  s.summary          = "A short description of QRCode."

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = <<-DESC
                       DESC

  s.homepage         = "https://github.com/<GITHUB_USERNAME>/QRCode"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "QRCode" => "jiangteng.cn@gmail.com" }
  s.source           = { :git => "https://github.com/<GITHUB_USERNAME>/QRCode.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '7.0'

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'QRCode' => ['Pod/Assets/*.png']
  }


   non_arc_files = ["Pod/Classes/QREncode/QR_Encode.{h,cpp}",
    "Pod/Classes/QREncode/DataMatrix.{h,mm}",
    "Pod/Classes/QREncode/QREncoder_internal.{h,mm}",
    "Pod/Classes/QREncode/QRDefinition.h"]


    s.requires_arc = true
    s.exclude_files =  non_arc_files
    s.resources = 'QRCode/zxing/ZXingWidgetBundle.bundle'
    s.frameworks = 'AVFoundation'

    s.subspec 'no-arc' do |sp|
    sp.source_files =  non_arc_files
    sp.requires_arc = false
    end

end
