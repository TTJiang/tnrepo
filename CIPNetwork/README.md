# CIPNetwork

[![CI Status](http://img.shields.io/travis/CIPNetwork/CIPNetwork.svg?style=flat)](https://travis-ci.org/CIPNetwork/CIPNetwork)
[![Version](https://img.shields.io/cocoapods/v/CIPNetwork.svg?style=flat)](http://cocoapods.org/pods/CIPNetwork)
[![License](https://img.shields.io/cocoapods/l/CIPNetwork.svg?style=flat)](http://cocoapods.org/pods/CIPNetwork)
[![Platform](https://img.shields.io/cocoapods/p/CIPNetwork.svg?style=flat)](http://cocoapods.org/pods/CIPNetwork)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CIPNetwork is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "CIPNetwork"
```

## Author

CIPNetwork, jiangteng.cn@gmail.com

## License

CIPNetwork is available under the MIT license. See the LICENSE file for more info.
