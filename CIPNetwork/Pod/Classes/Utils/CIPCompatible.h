//
//  DPCompatible.h
//  DPScope
//
//  Created by Tu Yimin on 10-2-24.
//  Copyright 2010 dianping.com. All rights reserved.
//

#import <UIKit/UIKit.h>

double CIP_IPHONE_OS_MAIN_VERSION();

BOOL CIP_IPHONE_OS_3();

BOOL CIP_IPHONE_OS_4();

BOOL CIP_IPHONE_OS_4_2();

BOOL CIP_IPHONE_OS_4_3();

BOOL CIP_IPHONE_OS_5();

BOOL CIP_IPHONE_OS_6();

BOOL CIP_IPHONE_OS_7();

BOOL CIP_IPHONE_OS_8();

BOOL CIP_IPHONE_OS_9();

//BOOL IPHONE_OS_EQUILORHIGHERTHAN(NSString *compareVersionString);

BOOL CIP_IPHONE_OS_SUPPORTMULTITASK();
