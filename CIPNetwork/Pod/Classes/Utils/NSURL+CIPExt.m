//
//  NSURL+CIPExt.m
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/27.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "NSURL+CIPExt.h"

@implementation NSURL (CIPExt)
+ (id)URLWithNoNilString:(NSString *)URLString {
    return URLString?[NSURL URLWithString:URLString]:nil;
}

+ (id)URLWithNoNilString:(NSString *)URLString relativeToURL:(NSURL *)baseURL {
    return URLString?[NSURL URLWithString:URLString relativeToURL:baseURL]:nil;
}
@end
