//
//  DPCompatible.m
//  DPScope
//
//  Created by Tu Yimin on 10-2-24.
//  Copyright 2010 dianping.com. All rights reserved.
//

#import "CIPCompatible.h"
//#import "NVVersion.h"

double CIP_IPHONE_OS_MAIN_VERSION() {
    static double __iphone_os_main_version = 0.0;
    if(__iphone_os_main_version == 0.0) {
        NSString *sv = [[UIDevice currentDevice] systemVersion];
        NSScanner *sc = [[NSScanner alloc] initWithString:sv];
        if(![sc scanDouble:&__iphone_os_main_version])
            __iphone_os_main_version = -1.0;
    }
    return __iphone_os_main_version;
}


BOOL CIP_IPHONE_OS_3() {
    return CIP_IPHONE_OS_MAIN_VERSION() >= 3.0;
}


BOOL CIP_IPHONE_OS_4() {
    return CIP_IPHONE_OS_MAIN_VERSION() >= 4.0;
}


BOOL CIP_IPHONE_OS_4_2() {
    return CIP_IPHONE_OS_MAIN_VERSION() >= 4.2;
}

BOOL CIP_IPHONE_OS_4_3() {
    return CIP_IPHONE_OS_MAIN_VERSION() >= 4.3;
}

BOOL CIP_IPHONE_OS_5() {
    return CIP_IPHONE_OS_MAIN_VERSION() >= 5.0;
}

BOOL CIP_IPHONE_OS_6() {
    return CIP_IPHONE_OS_MAIN_VERSION() >= 6.0;
}

BOOL CIP_IPHONE_OS_7() {
    return CIP_IPHONE_OS_MAIN_VERSION() >= 7.0;
}

BOOL CIP_IPHONE_OS_8() {
    return CIP_IPHONE_OS_MAIN_VERSION() >= 8.0;
}

BOOL CIP_IPHONE_OS_9() {
    return CIP_IPHONE_OS_MAIN_VERSION() >= 9.0;
}

//BOOL IPHONE_OS_EQUILORHIGHERTHAN(NSString *compareVersionString) {
//    NVVersion *osVersion = [NVVersion osVersion];
//    NVVersion *compVersion = [[NVVersion alloc] initWithVersion:compareVersionString];
//    return ![compVersion isNewerThan:osVersion];
//}

BOOL CIP_IPHONE_OS_SUPPORTMULTITASK() {
    UIDevice *device = [UIDevice currentDevice];
    if ([device respondsToSelector:@selector(isMultitaskingSupported)] && [device isMultitaskingSupported]) {
        return YES;
    } else {
        return NO;
    }
    
}
