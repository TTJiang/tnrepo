//
//  NSData+CIPExt.h
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/27.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (CIPExt)

- (NSString *)hexString;

// GZIP
- (NSData *)encodeGZip;
- (NSData *)decodeGZip;

// Mobile
- (NSData *)decodeMobileData;
- (NSData *)encodeMobileData;

// Returns range [start, null byte), or (NSNotFound, 0).
- (NSRange)rangeOfNullTerminatedBytesFrom:(int)start;

// Standard Base64 encoder
- (NSString *)base64String;

// URL Base64 encoder for url only (avoid url escape)
- (NSString *)urlBase64String;
@end
