//
//  NSString+CIPExt.h
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/27.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (CIPExt)
+ (instancetype)generateUUID;

- (NSData *)decodeHexString;

- (NSString *)md5;

- (NSString *)stringByAddingPercentEscapesUsingEncodingExt:(NSStringEncoding)enc;
- (NSString *)stringByReplacingPercentEscapesUsingEncodingExt:(NSStringEncoding)enc;

/**
 url escape
 */
- (NSString *)stringByAddingPercentEscapes;
/**
 url unescape
 */
- (NSString *)stringByReplacingPercentEscapes;

- (BOOL)isValidEmail;

/**
 去掉头尾的冒号，包括中文和英文
 [@"价格：" trimColon] = @"价格"
 */
- (NSString *)trimColon;

// Standard Base64 decoder
- (NSData *)decodeBase64String;

// URL Base64 decoder (avoid url escape)
- (NSData *)decodeUrlBase64String;


- (NSString *)urlEncode;

- (NSString *)urlDecode;
@end
