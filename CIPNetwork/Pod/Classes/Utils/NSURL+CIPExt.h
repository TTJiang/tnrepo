//
//  NSURL+CIPExt.h
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/27.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (CIPExt)
// iOS<4.0，如果传入URLWithString:的参数为nil，程序会崩溃。
+ (id)URLWithNoNilString:(NSString *)URLString;
+ (id)URLWithNoNilString:(NSString *)URLString relativeToURL:(NSURL *)baseURL;
@end
