//
//  CIPNetworkConfiguration.m
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/30.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "CIPNetworkConfiguration.h"

@implementation CIPNetworkConfiguration

- (NSTimeInterval)defaultTimeout{
    return _defaultTimeout;
}

- (BOOL)isDebug{
    return _isDebug;
}

- (nullable NSDictionary *)requestHeaders{
    return _requestHeaders;
}

- (CIPSessionType)sessionTypeForRequest:(nonnull NSURLRequest *)request{
    return _sessionType;
}

- (BOOL)disableStatistics{
    return _disableStatistics;
}

- (instancetype)copyWithZone:(NSZone *)zone {
    CIPNetworkConfiguration *networkConfiguration = [[[self class] allocWithZone:zone] init];
    networkConfiguration.isDebug = self.isDebug;
    networkConfiguration.defaultTimeout = self.defaultTimeout;
    networkConfiguration.sessionType = self.sessionType;
    networkConfiguration.disableStatistics = self.disableStatistics;
    networkConfiguration.requestHeaders = self.requestHeaders;
    
    return networkConfiguration;
}
@end
