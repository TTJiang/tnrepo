//
//  CIPNetworkConfiguration.h
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/30.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CIPURLSessionDataTask.h"

@interface CIPNetworkConfiguration : NSObject <NSCopying>
/*
 可以通过赋值或重写方式进行配置
 network的配置会 复制到每个task中
 */
@property (nonatomic, assign) NSTimeInterval defaultTimeout;           //超时时间设定
@property (nonatomic, assign) BOOL isDebug;                 //是否是debug环境
@property (nonatomic, strong, nullable) NSDictionary *requestHeaders; //头部
@property (nonatomic, assign) CIPSessionType sessionType;   //通道的选择策略
@property (nonatomic, assign) BOOL disableStatistics;       //是否禁止cat的统计


- (NSTimeInterval)defaultTimeout;
- (BOOL)isDebug;
- (nullable NSDictionary *)requestHeaders;
- (CIPSessionType)sessionTypeForRequest:(nonnull NSURLRequest *)request;
- (BOOL)disableStatistics;
@end
