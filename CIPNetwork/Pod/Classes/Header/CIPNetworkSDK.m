//
//  CIPNetworkSDK.m
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/24.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "CIPNetworkSDK.h"
#import "CIPTaskManager.h"
#import "CIPSessionManager.h"


#define CIPHttpMethodGet         @"GET"
#define CIPHttpMethodPost        @"POST"

@interface CIPNetworkSDK()

@property (nonatomic, strong) NSString *baseURL;
@property (nonatomic, strong) CIPNetworkConfiguration *configuration;
@end

@implementation CIPNetworkSDK

//- (instancetype)initWithAppID:(int)appID
//                andAppVersion:(NSString *)appVersion
//                   andChannel:(NSString *)channel{
//    if (self = [super init]) {
//
//    }
//
//    return self;
//}
+ (nullable instancetype)sdk
{
    static CIPNetworkSDK * instance = nil;
    static  dispatch_once_t networkSdkonce;
    dispatch_once(&networkSdkonce, ^{
        instance = [[[self class] alloc] initWithBaseURL:nil];
    });
    
    return instance;
}

- (instancetype)initWithBaseURL:(NSString *)url {
    return [self initWithBaseURL:url sessionConfiguration:nil];
}

- (instancetype)initWithSessionConfiguration:(CIPNetworkConfiguration *)configuration {
    return [self initWithBaseURL:nil sessionConfiguration:configuration];
}

- (instancetype)initWithBaseURL:(NSString *)url
           sessionConfiguration:(CIPNetworkConfiguration *)configuration
{
    
    if(self = [super init]){
        [[CIPSessionManager sharedInstance] setSessionThread:[CIPTaskManager threadForTaskMgr]];
    }
    self.baseURL = url;
    self.configuration = configuration;
    self.requestSerializer = [CIPHttpRequestSerializer serializer];
    self.responseSerializer = [CIPHttpResponseSerializer serializer];
    self.completionQueue = dispatch_get_main_queue();
    return self;
}

#pragma mark normal task

- (nullable CIPTask *)syncUrl:(nonnull NSString *)urlString
               parameters:(nullable id)parameters
                   method:(CIPHttpMethodType)httpMethodType
                  timeout:(NSTimeInterval)timeout
        requestSerializer:(nullable CIPHttpRequestSerializer *)requestSerializer{
    
    return [self url:urlString
          parameters:parameters
              method:httpMethodType
             timeout:timeout
   requestSerializer:requestSerializer
             success:nil
             failure:nil];
}

- (nullable CIPTask *)url:(nonnull NSString *)urlString
               parameters:(nullable id)parameters
                   method:(CIPHttpMethodType)httpMethodType
                  timeout:(NSTimeInterval)timeout
        requestSerializer:(nullable CIPHttpRequestSerializer *)requestSerializer
                  success:(nullable void (^)(CIPTask * _Nullable task, id _Nullable result))success
                  failure:(nullable void (^)(CIPTask * _Nullable task, NSError * _Nullable error))failure{

    NSData *pData = nil;
    id sendParametr = parameters;
    if ([parameters isKindOfClass:[NSData class]] && httpMethodType == CIPHttpMethodTypePost) {
        pData = parameters;
        sendParametr = nil;
    }
    
    return [self url:urlString
          parameters:sendParametr
              method:httpMethodType
             timeout:timeout
            postData:pData
   requestSerializer:requestSerializer
             success:success
             failure:failure];
}

- (nullable CIPTask *)url:(nonnull NSString *)urlString
               parameters:(nullable id)parameters
                   method:(CIPHttpMethodType)httpMethodType
                  timeout:(NSTimeInterval)timeout
                 postData:(NSData *)postData
        requestSerializer:(nullable CIPHttpRequestSerializer *)requestSerializer
                  success:(nullable void (^)(CIPTask * _Nullable task, id _Nullable result))success
                  failure:(nullable void (^)(CIPTask * _Nullable task, NSError * _Nullable error))failure{
    
    NSString *completeUrl = self.baseURL?[self.baseURL stringByAppendingString:urlString]:urlString;
    CIPHttpRequestSerializer *reqSerializer = nil;
    if (requestSerializer != nil) {
        reqSerializer = requestSerializer;
    }else{
        reqSerializer = self.requestSerializer;
    }
    
    NSTimeInterval httpTimeout = 0.0;
    if (timeout <= 0) {
        httpTimeout = [self.configuration defaultTimeout];
    }else{
        httpTimeout = timeout;
    }
    
    NSString *httpMethod = httpMethodType == CIPHttpMethodTypePost ? CIPHttpMethodPost:CIPHttpMethodGet;
    NSError *serializationError = nil;
    
    NSMutableURLRequest *request = [reqSerializer requestWithMethod:httpMethod
                                                          URLString:completeUrl
                                                         parameters:parameters
                                                            timeout:httpTimeout
                                                           postData:postData
                                                            headers:[self.configuration requestHeaders]
                                                              error:&serializationError];
    
    if (serializationError) {
        if (failure) {
            dispatch_async(self.completionQueue?:dispatch_get_main_queue(), ^{
                failure(nil, serializationError);
            });
        }
        return nil;
    }
    
    CIPTask *task = [CIPTask task];
    task.configuration = [self.configuration copy];
    task.responseSerializer = [self.responseSerializer copy];
    task.timeout = httpTimeout;
    task.success = success;
    task.failure = failure;
    task.request = request;
    task.completionQueue = self.completionQueue;
    return task;
}
@end
