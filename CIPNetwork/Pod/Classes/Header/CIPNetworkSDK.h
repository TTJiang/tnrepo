//
//  CIPNetworkSDK.h
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/24.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CIPCache.h"
#import "CIPTask.h"
#import "CIPNetworkConfiguration.h"
#import "CIPHttpRequestSerializer.h"
#import "CIPHttpResponseSerializer.h"

@interface CIPNetworkSDK : NSObject

typedef NS_ENUM(NSInteger, CIPHttpMethodType){
    CIPHttpMethodTypeGet = 0,
    CIPHttpMethodTypePost = 1,
};

@property (nonatomic, strong, nonnull) CIPHttpRequestSerializer <CIPRequestSerialization> *requestSerializer;
@property (nonatomic, strong, nonnull) CIPHttpResponseSerializer <CIPResponseSerialization> *responseSerializer;
/*
 返回结果的队列，为空时则在主线程中返回
 */
@property (nonatomic, assign, nullable) dispatch_queue_t completionQueue;

/*! @brief 初始化WnsSDK。
 *
 * @param appid 第三方应用的标识ID
 * @param appVersion 第三方应用的版本号
 * @param channel 第三方应用发布渠道
 */
//- (instancetype)initWithAppID:(int)appID
//                andAppVersion:(NSString *)appVersion
//                   andChannel:(NSString *)channel;

+ (nullable instancetype)sdk;

- (nullable instancetype)initWithBaseURL:(nullable NSString *)url;

- (nullable instancetype)initWithSessionConfiguration:(nullable CIPNetworkConfiguration *)configuration;

- (nullable instancetype)initWithBaseURL:(nullable NSString *)url
                    sessionConfiguration:(nullable CIPNetworkConfiguration *)configuration;

/*! @brief 异步请求接口
 *
 *通过[task resume] 触发，结果通过block返回
 *
 */
- (nullable CIPTask *)url:(nonnull NSString *)urlString
               parameters:(nullable id)parameters
                   method:(CIPHttpMethodType)httpMethodType
                  timeout:(NSTimeInterval)timeout
        requestSerializer:(nullable CIPHttpRequestSerializer *)requestSerializer
                  success:(nullable void (^)(CIPTask * _Nullable task, id _Nullable result))success
                  failure:(nullable void (^)(CIPTask * _Nullable task, NSError * _Nullable error))failure;

/*! @brief 同步请求接口
 *
 *通过[task startSyncError:erro] 触发，结果直接返回
 *
 */
- (nullable CIPTask *)syncUrl:(nonnull NSString *)urlString
                   parameters:(nullable id)parameters
                       method:(CIPHttpMethodType)httpMethodType
                      timeout:(NSTimeInterval)timeout
            requestSerializer:(nullable CIPHttpRequestSerializer *)requestSerializer;

/*! @brief 发送请求(HTTP协议接口)。
 *
 * @param urlString 请求的url,设置了baseURL只需要传后半段
 * @param parameters 请求参数支持包含NSArray,NSDictionary,NSSet三种类型数据
 * @param httpMethodType 请求类型，目前只有CIPHttpMethodTypeGet、CIPHttpMethodTypePost二种
 * @param timeout 请求超时时间，timeout = 0使用默认的超时时间
 * @param postData 只有post请求才有postData
 * @param success 回调Block，参数task表示请求，参数result表示请求结果
 * @param failure 回调Block，参数task表示请求，参数error表示错误标识
 * @return 成功返回请求task，失败返回nil。
 */
- (nullable CIPTask *)url:(nonnull NSString *)urlString
               parameters:(nullable id)parameters
                   method:(CIPHttpMethodType)httpMethodType
                  timeout:(NSTimeInterval)timeout
                 postData:(nullable NSData *)postData
        requestSerializer:(nullable CIPHttpRequestSerializer *)requestSerializer
                  success:(nullable void (^)(CIPTask * _Nullable task, id _Nullable result))success
                  failure:(nullable void (^)(CIPTask * _Nullable task, NSError * _Nullable error))failure;
@end
