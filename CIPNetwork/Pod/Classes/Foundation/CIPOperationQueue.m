//
//  DPOperationQueue.m
//  DPLib
//
//  Created by Tu Yimin on 10-1-8.
//  Copyright 2010 dianping.com. All rights reserved.
//

#import "CIPOperationQueue.h"
#import <libkern/OSAtomic.h>


@interface CIPOperationQueue () {
    NSCondition *cond;
    NSMutableArray *operations;
    NSArray *threads;
    volatile int32_t busyThreads;
    NSCondition *waitStop;
    BOOL stoped;
    BOOL fifo; // first in first out
}

- (void)main;

@end


@implementation CIPOperationQueue

- (id)init {
    return [self initWithThreadCount:1 withFIFO:NO];
}
- (id)initWithThreadCount:(NSInteger)count {
    return [self initWithThreadCount:count withFIFO:NO];
}
- (id)initWithThreadCount:(NSInteger)count withFIFO:(BOOL)isFIFO {
    if(self = [super init]) {
        cond = [[NSCondition alloc] init];
        operations = [[NSMutableArray alloc] init];
        fifo = isFIFO;
        NSThread *tarr[count];
        for(int i = 0; i < count; i++) {
            NSThread *t = [[NSThread alloc] initWithTarget:self selector:@selector(main) object:nil];
            tarr[i] = t;
        }
        threads = [[NSArray alloc] initWithObjects:tarr count:count];
        for(int i = 0; i < count; i++) {
            NSThread *t = tarr[i];
            [t start];
        }
    }
    return self;
}

- (void)addOperation:(id<CIPOperation>)operation {
    if(stoped)
        return;
    [cond lock];
    if ([operation respondsToSelector:@selector(setOperationQueue:)]) {
        [operation performSelector:@selector(setOperationQueue:) withObject:self];
    }
    NSInteger count = [operations count];
    [operations addObject:operation];
    if(count == 0)
        [cond broadcast];
    [cond unlock];
}

- (NSArray *)operations {
    NSArray *result = nil;
    [cond lock];
    result = [operations copy];
    [cond unlock];
    return result;
}

- (NSInteger)busyThreads {
    return busyThreads;
}

- (void)cancelAllOperations {
    [cond lock];
    //stoped = YES;
    [operations removeAllObjects];
    [cond unlock];
}

- (void)waitUntilAllOperationsAreFinished {
    if(busyThreads == 0 && [operations count] == 0)
        return;
    waitStop = [[NSCondition alloc] init];
    [waitStop lock];
    while (busyThreads > 0 || [operations count] > 0) {
        [waitStop wait];
    }
    [waitStop unlock];
    waitStop = nil;
}

- (NSInteger)threadCount {
    return [threads count];
}

- (void)main {
    while (!stoped) {
        CIPOperationQueue *retainSelf = self;
        @autoreleasepool {
            @try {
                id<CIPOperation> op = nil;
                BOOL isLast = NO;
                [cond lock];
                while ([operations count] == 0) {
                    [cond wait];
                }
                if (fifo) {
                    op = [operations objectAtIndex:0];
                    [operations removeObjectAtIndex:0];
                } else {
                    op = [operations lastObject];
                    [operations removeLastObject];
                }
                
                isLast = [operations count] == 0;
                [cond unlock];
                
                OSAtomicIncrement32(&busyThreads);
                [op main];
                if(OSAtomicDecrement32(&busyThreads) == 0) {
                    if(waitStop && [operations count] == 0) {
                        [waitStop signal];
                    }
                }
            }
            @catch (NSException * e) {
#ifdef _DEBUG
                NSLog(@"Exception on operation queue:\n\t%@", e);
#endif
            }
        }
        retainSelf = nil;
    }
}

@end