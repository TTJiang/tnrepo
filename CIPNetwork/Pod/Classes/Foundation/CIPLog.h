//
//  NVLog.h
//  Core
//
//  Created by Yimin Tu on 12-6-30.
//  Copyright (c) 2012年 dianping.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CIPLOG(format, ...) if(1) {__CIPLog(@__FILE__, __LINE__, [NSString stringWithFormat:format, ## __VA_ARGS__]);}
#define DEGLOG(format, ...) NVLOG(format, ## __VA_ARGS__)

void __CIPLog(NSString *file, NSInteger line, NSString * content);
