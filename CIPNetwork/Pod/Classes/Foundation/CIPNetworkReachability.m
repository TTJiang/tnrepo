//
//  NVNetworkType.m
//  Core
//
//  Created by Yimin Tu on 12-7-1.
//  Copyright (c) 2012年 dianping.com. All rights reserved.
//

#import "CIPNetworkReachability.h"
#import "CoreTelephony/CTTelephonyNetworkInfo.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import <netinet/in.h>

#import <sys/socket.h>
#import <netinet/in.h>
#import <netinet6/in6.h>
#import <arpa/inet.h>
#import <ifaddrs.h>
#import <netdb.h>
#import "CIPCompatible.h"

#import <CoreFoundation/CoreFoundation.h>

static SCNetworkReachabilityRef __reachability = nil;

CIPNetworkReachability CIPGetNetworkReachability() {
    if(!__reachability) {
        struct sockaddr_in zeroAddress;
        bzero(&zeroAddress, sizeof(zeroAddress));
        zeroAddress.sin_len = sizeof(zeroAddress);
        zeroAddress.sin_family = AF_INET;
        __reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr*)&zeroAddress);
    }
    
    SCNetworkReachabilityFlags flags;
    if (__reachability && SCNetworkReachabilityGetFlags(__reachability, &flags)) {
        if ((flags & kSCNetworkReachabilityFlagsReachable) == 0)
        {
            // if target host is not reachable
            return CIPNetworkReachabilityNone;
        }
        
        CIPNetworkReachability retVal = CIPNetworkReachabilityNone;
        
        if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0)
        {
            // if target host is reachable and no connection is required
            //  then we'll assume (for now) that your on Wi-Fi
            retVal = CIPNetworkReachabilityWifi;
        }
        
        
        if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
             (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0))
        {
            // ... and the connection is on-demand (or on-traffic) if the
            //     calling application is using the CFSocketStream or higher APIs
            
            if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
            {
                // ... and no [user] intervention is needed
                retVal = CIPNetworkReachabilityWifi;
            }
        }
        
        if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
        {
            // ... but WWAN connections are OK if the calling application
            //     is using the CFNetwork (CFSocketStream?) APIs.
            retVal = CIPNetworkReachabilityMobile;
        }
        return retVal;
    }
    return CIPNetworkReachabilityNone;
}

static CTTelephonyNetworkInfo * __telephonyNetworkInfo;
CIPNetworkReachability CIPGetAccurateNetworkReachability()
{
    CIPNetworkReachability reachability = CIPGetNetworkReachability();
    if (reachability == CIPNetworkReachabilityMobile) {
        if (CIP_IPHONE_OS_7()) {
            if (!__telephonyNetworkInfo) {
                __telephonyNetworkInfo = [CTTelephonyNetworkInfo new];
            }
            NSString * radioAccessTechnology = __telephonyNetworkInfo.currentRadioAccessTechnology;
            if ([radioAccessTechnology isEqualToString:CTRadioAccessTechnologyGPRS] ||
                [radioAccessTechnology isEqualToString:CTRadioAccessTechnologyEdge] ||
                [radioAccessTechnology isEqualToString:CTRadioAccessTechnologyCDMA1x])
                reachability = CIPNetworkReachabilityMobile2G;
            else if ([radioAccessTechnology isEqualToString:CTRadioAccessTechnologyLTE])
                reachability = CIPNetworkReachabilityMobile4G;
            else
                reachability = CIPNetworkReachabilityMobile3G;
        } else {
            SCNetworkReachabilityFlags flags;
            SCNetworkReachabilityGetFlags(__reachability, &flags);
            return ((flags & kSCNetworkReachabilityFlagsTransientConnection) == kSCNetworkReachabilityFlagsTransientConnection) ? CIPNetworkReachabilityMobile2G : CIPNetworkReachabilityMobile3G;
        }
    }
    return reachability;
}