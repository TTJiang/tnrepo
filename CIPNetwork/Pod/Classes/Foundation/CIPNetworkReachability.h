//
//  NVNetworkType.h
//  Core
//
//  Created by Yimin Tu on 12-7-1.
//  Copyright (c) 2012年 dianping.com. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 未公开函数，请勿直接调用
 */
typedef enum {
    CIPNetworkReachabilityNone = 0,
    CIPNetworkReachabilityWifi = 1,
    CIPNetworkReachabilityMobile = 2,
    CIPNetworkReachabilityMobile2G = 3,
    CIPNetworkReachabilityMobile3G = 4,
    CIPNetworkReachabilityMobile4G = 5,
} CIPNetworkReachability;

CIPNetworkReachability CIPGetNetworkReachability();

CIPNetworkReachability CIPGetAccurateNetworkReachability();