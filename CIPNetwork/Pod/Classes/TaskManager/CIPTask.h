//
//  CIPTask.h
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/24.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CIPURLSessionDataTask.h"
#import "CIPCache.h"
#import "CIPLog.h"
#import "CIPNetworkConfiguration.h"
#import "CIPHttpResponseSerializer.h"

@class CIPTask;
typedef void (^CIPTaskSuccessBlock)(CIPTask * _Nullable task, id _Nullable result);
typedef void (^CIPTaskFailBlock)(CIPTask * _Nullable task, NSError * _Nullable error);

typedef void (^CIPTaskFailWithRetry)(CIPTask * _Nullable task, NSError * _Nullable error);
typedef void (^CIPTaskWillRetry)(CIPTask * _Nullable task);

typedef void (^CIPTaskUpload)(CIPTask * _Nullable task,NSInteger sendedBytes,NSInteger totalBytes);
typedef void (^CIPTaskDownload)(CIPTask * _Nullable task,NSInteger recvedBytes,NSInteger totalBytes);

@interface CIPTask : NSObject

@property (nonatomic, assign, readonly) NSInteger seqNo;                      // 当前请求的序列号
@property (nonatomic, copy, nullable) CIPTaskSuccessBlock success;
@property (nonatomic, copy, nullable) CIPTaskFailBlock failure;
@property (nonatomic, strong, nullable) NSURLRequest *request;

/**
 请求失败，但是5秒后会重试
 （不是最终失败）
 */
@property (nonatomic, copy, nullable) CIPTaskFailWithRetry failAndRetryBlock;

/**
 请求即将重试
 （重试前会等待5秒，不包括等待的时间）
 */
@property (nonatomic, copy, nullable) CIPTaskWillRetry willRetryBlock;

/**
 请求已经发出字节
 */
@property (nonatomic, copy, nullable) CIPTaskUpload uploadBlock;

/**
 请求已经接受字节
 totalData可能不提供
 */
@property (nonatomic, copy, nullable) CIPTaskDownload downloadBlock;

/*
 请求执行时间
 */
@property (nonatomic, assign) NSTimeInterval httpTime;
/*
 等待wait后再执行请求
 */
@property (nonatomic, assign) NSTimeInterval wait;
/**
 设置最大重试次数
 默认为0，不重试
 -1为永久重试
 */
@property (nonatomic, assign) NSInteger maxRetryTimes;
/**
 配置
 */
@property (nonatomic, strong, nullable) CIPNetworkConfiguration *configuration;

/**
 cache
 */
@property (nonatomic, assign) CIPCacheType cacheType;         // 缓存类型
/**
 数据是否来自缓存
 */
@property (nonatomic, assign, readonly) BOOL isDataFromCache;
/**
 数据的缓存时间(1448603166).cachedTime = 0,不是缓存数据
 */
@property (nonatomic, assign, readonly) time_t cachedTime;
/**
 超时时间
 */
@property (nonatomic, assign) NSTimeInterval timeout;
/**
 通道类型
 */
@property (nonatomic, assign) CIPSessionType sessionType;
/*
 返回结果的队列，为空时则在主线程中返回
 */
@property (nonatomic, assign, nullable) dispatch_queue_t completionQueue;
/**
 请求网络拿到的数据，但可能不是最终返回给上层的结果
 */
@property (nonatomic, strong, nullable) NSData *data;

/**
 返回的最终结果
*/
@property (nonatomic, strong, nullable) id result;
/**
 错误
 */
@property (nonatomic, strong, nullable) NSError *error;
/**
 结果系列化
 */
@property (nonatomic, strong, nonnull) CIPHttpResponseSerializer <CIPResponseSerialization> *responseSerializer;

/**
 创建一个新的Task
 由于Task是可变对象，所有Task的属性都可以用属性设置
 */
+ (nonnull instancetype)task;
/**
 开始同步任务
 一个任务只能开始一次
 同步请求
 直接返回结果,block都将失效
 */
- (nullable id)startSyncError:(NSError * _Nullable __autoreleasing * __nullable)err;

/**
 开始异步任务
 一个任务只能开始一次
 异步请求
 通过block返回结果
 */
- (void)resume;

- (void)cancel;

@end
