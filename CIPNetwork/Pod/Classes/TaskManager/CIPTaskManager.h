//
//  CIPTaskManger.h
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/24.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CIPTask.h"

@interface CIPTaskManager : NSObject
+ (CIPTaskManager *)sharedInstance;
+ (NSThread *)threadForTaskMgr;
+ (void)releaseCurrentThread;
+ (long)generateTaskNo;

- (void)enqueueAndStartSession:(CIPTask *)task;
- (void)cancelTask:(NSNumber *)seqNo;
- (void)removeTask:(NSNumber *)seqNo;
@end
