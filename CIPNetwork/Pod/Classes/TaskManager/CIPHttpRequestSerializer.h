//
//  CIPRequestSerialization.h
//  Pods
//
//  Created by JiangTeng on 16/1/2.
//
//

#import <Foundation/Foundation.h>

@protocol CIPRequestSerialization <NSObject, NSCopying>

/**
 Returns a request with the specified parameters encoded into a copy of the original request.
 
 @param request The original request.
 @param parameters The parameters to be encoded.
 @param error The error that occurred while attempting to encode the request parameters.
 
 @return A serialized request.
 */
- (nullable NSURLRequest *)requestBySerializingRequest:(nonnull NSURLRequest *)request
                                        withParameters:(nullable id)parameters
                                              postData:(nullable NSData *)postData
                                                 error:( NSError * _Nullable __autoreleasing * __nullable)error;

@end


@interface CIPHttpRequestSerializer : NSObject <CIPRequestSerialization>

@property (nonatomic, strong, nullable) NSSet <NSString *> *HTTPMethodsEncodingParametersInURI;

@property (nonatomic, assign) NSStringEncoding stringEncoding;

@property (nonatomic, strong, nullable) NSMutableDictionary *mutableHTTPRequestHeaders;


+ (nullable instancetype)serializer;

- (nullable NSMutableURLRequest *)requestWithMethod:(nonnull NSString *)method
                                          URLString:(nonnull NSString *)URLString
                                         parameters:(nullable id)parameters
                                            timeout:(NSTimeInterval) timeout
                                           postData:(nullable NSData *)postData
                                            headers:(nullable NSDictionary *)headers
                                              error:(NSError * _Nullable __autoreleasing * __nullable)error;

- (nullable NSString *)queryStringFromParameters:(nullable id)parameters;
@end


#pragma mark -

/**
 `AFJSONRequestSerializer` is a subclass of `AFHTTPRequestSerializer` that encodes parameters as JSON using `NSJSONSerialization`, setting the `Content-Type` of the encoded request to `application/json`.
 */
@interface CIPJSONRequestSerializer : CIPHttpRequestSerializer

/**
 Options for writing the request JSON data from Foundation objects. For possible values, see the `NSJSONSerialization` documentation section "NSJSONWritingOptions". `0` by default.
 */
@property (nonatomic, assign) NSJSONWritingOptions writingOptions;

/**
 Creates and returns a JSON serializer with specified reading and writing options.
 
 @param writingOptions The specified JSON writing options.
 */
+ (instancetype)serializerWithWritingOptions:(NSJSONWritingOptions)writingOptions;

@end
