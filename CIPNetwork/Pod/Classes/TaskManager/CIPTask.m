//
//  CIPTask.m
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/24.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "CIPTask.h"
#import "CIPTaskManager.h"
#import "CIPSessionManager.h"
#import "NSString+CIPExt.h"
#import "CIPNetworkReachability.h"
#import "CIPMonitorCenter.h"
#import "NSURL+CIPExt.h"

typedef NS_ENUM(NSInteger,CIPExecType) {
    
    // 异步请求
    CIPExecTypeAsync = 0,
    
    // 同步请求
    CIPExecTypeSync  = 1
    
} ;

@interface CIPTask()
@property (nonatomic, strong) NSCondition *syncLock;    // 同步事件
@property (nonatomic, assign) CIPExecType execType;
@property (nonatomic, assign, readwrite) NSInteger seqNo;                      // 当前请求的序列号
@property (nonatomic, strong) CIPCache *cache;
@property (nonatomic, strong) CIPURLSessionDataTask *sessionTask;
@property (nonatomic, strong, nullable) NSData *cacheReadData;          //缓存数据
@property (nonatomic, assign, readwrite) BOOL isDataFromCache;
@property (nonatomic, assign, readwrite) time_t cachedTime;

// monitor
@property (nonatomic, assign) NSInteger postRawBytes; // monitor
@property (nonatomic, strong, nullable) NSDictionary *responseHeaders;
@property (nonatomic, strong, nullable) NSURLResponse *response;
@property (nonatomic, strong, nullable) NSError *subError;
@property (nonatomic, assign) NSInteger statusCode;
@property (nonatomic, assign) BOOL isCanceled;
@property (nonatomic, assign) BOOL isJustRefreshCache;
/**
 当前重试次数
 */
@property (nonatomic, assign) NSInteger retryTimes;
@end

@implementation CIPTask

#pragma mark - init
+ (nonnull instancetype)task{
    return [[CIPTask alloc] init];
}

- (id)init{
    
    if (self = [super init]) {
        [self initData];
    }
    
    return self;
}

- (void)initData{
    self.seqNo = [CIPTaskManager generateTaskNo]; //long to NSInteger??
    _cacheType = CIPCacheTypeDisabled;
    _sessionType = CIPSessionTypeNone;
    _isCanceled = NO;
    _isJustRefreshCache = NO;
    _maxRetryTimes = 0;
    _success = nil;
    _failure = nil;
    _request = nil;
    _cacheReadData = nil;
    _execType = CIPExecTypeAsync;
}

#pragma mark - task method
- (void)resume{
    self.execType = CIPExecTypeAsync;
    [[CIPTaskManager sharedInstance] performSelector:@selector(enqueueAndStartSession:)
                                            onThread:[CIPTaskManager threadForTaskMgr]
                                          withObject:self
                                       waitUntilDone:NO];
}

- (nullable id)startSyncError:(NSError * _Nullable __autoreleasing * __nullable)err{
    NSAssert(![[NSThread currentThread] isMainThread], @"同步请求不允许在主线程调用");
    if(self.isCanceled){
        if (err != NULL) {
            err = nil;
        }
        return nil;
    }
    
    self.execType = CIPExecTypeSync;
    [[CIPTaskManager sharedInstance] performSelector:@selector(enqueueAndStartSession:)
                                            onThread:[CIPTaskManager threadForTaskMgr]
                                          withObject:self
                                       waitUntilDone:NO];
    if (self.syncLock == nil) {
        self.syncLock = [[NSCondition alloc] init];
    }
    
    NSTimeInterval tout = self.timeout > 0 ? self.timeout : [self.configuration defaultTimeout]; // default timeout = 15 second
    tout += 5.0;
    NSTimeInterval startTime = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval nowTime = startTime;
    while (self.result == nil && self.error == nil && ((nowTime - startTime) < tout)) {
        [self.syncLock lock];
        tout -= nowTime - startTime;
        [self.syncLock waitUntilDate:[NSDate dateWithTimeIntervalSinceNow:tout]];
        [self.syncLock unlock];
        nowTime = [[NSDate date] timeIntervalSince1970];
    }
    if (err != NULL) {
        *err = self.error;
    }
    return self.result;
}

- (void)startTask{
    
    self.result = nil;
    self.data = nil;
    _cacheReadData = nil;
    _isCanceled = NO;
    _isJustRefreshCache = NO;
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    if (_cacheType != CIPCacheTypeDisabled && _cacheType != CIPCacheTypeCritical) {
        if (self.wait == 0) {
            [self readCacheData:nil];
        }else{
            [self performSelector:@selector(readCacheData:) withObject:nil afterDelay:self.wait];
        }
    }else{
        if (self.wait == 0) {
            [self send];
        }else{
            [self performSelector:@selector(send) withObject:nil afterDelay:self.wait];
        }
    }
}


- (void)sendRetry{
    
    if(self.isCanceled)
        return;
    NSThread *taskThread = [CIPTaskManager threadForTaskMgr];
    if (taskThread == [NSThread currentThread]) {
        self.statusCode = 0;
        self.result = nil;
        self.error = nil;
        self.data = nil;
        self.retryTimes++;
        self.sessionTask = nil;
        if (self.willRetryBlock != nil) {
            __weak typeof(self) __weakSelf = self;
            dispatch_async(self.completionQueue?:dispatch_get_main_queue(), ^{
                __strong typeof(__weakSelf) __strongSelf = __weakSelf;
                __strongSelf.willRetryBlock(__strongSelf);
            });
        }
        [self send];
    }else{
        [self performSelector:@selector(sendRetry) onThread:taskThread withObject:nil waitUntilDone:NO];
    }
}

- (void)send{
    
    self.timeout = self.timeout==0 ?: [self.configuration defaultTimeout];
    if (self.request == nil) {
        NSDictionary *userInfo = @{NSLocalizedFailureReasonErrorKey:@"request is null"};
        NSError *err = [NSError errorWithDomain:@"com.cip.request" code:-106 userInfo:userInfo];
        self.error = err;
        [self sessionFailure:err];
        return ;
    }
    
    //通道的选择
    if (self.sessionType == CIPSessionTypeNone) {
        self.sessionType = [self confirmSessionType];
    }
    __weak typeof(self) __weakSelf = self;
    self.sessionTask = [[CIPSessionManager sharedInstance] sendHTTPRequest:self.request
                                                                       cmd:nil
                                                                      type:self.sessionType
                                                                   timeout:self.timeout
                                                                    completion:^(NSString *cmd, NSURLResponse *response, NSData *data, NSError *error, NSError *subError) {
                                                                        __strong typeof(__weakSelf) __strongSelf = __weakSelf;
                                                                        @synchronized(__strongSelf) {
                                                                            __strongSelf.response = response;
                                                                            __strongSelf.data = data;
                                                                            __strongSelf.error = error;
                                                                            __strongSelf.subError = subError;
                                                                        }
                                                                        NSThread *thread = [CIPTaskManager threadForTaskMgr];
                                                                        if (thread == [NSThread currentThread]) {
                                                                            [__strongSelf sessionDidFinish];
                                                                        }else{
                                                                            [__strongSelf performSelector:@selector(sessionDidFinish) onThread:thread withObject:nil waitUntilDone:NO];
                                                                        }
                                                                        
                                                                    }];

    if (self.uploadBlock != nil) {
        self.sessionTask.uploadBlock = ^(NSInteger sendedBytes, NSInteger totalBytes) {
            __strong typeof(__weakSelf) __strongSelf = __weakSelf;
            if (__strongSelf.isJustRefreshCache) return ;
            dispatch_async(__strongSelf.completionQueue?:dispatch_get_main_queue(), ^{
                    __strongSelf.uploadBlock(__strongSelf,sendedBytes, totalBytes);
            });
        };
    }
    
    if (self.downloadBlock != nil) {
        self.sessionTask.downloadBlock =^(NSInteger recvedBytes, NSInteger totalBytes) {
            __strong typeof(__weakSelf) __strongSelf = __weakSelf;
            if (__strongSelf.isJustRefreshCache) return ;
            dispatch_async(__strongSelf.completionQueue?:dispatch_get_main_queue(), ^{
                    __strongSelf.downloadBlock(__strongSelf,recvedBytes, totalBytes);
            });
        };
    }
    
    [self.sessionTask start];
    self.httpTime = -[[NSDate date] timeIntervalSince1970];
    self.sessionTask.taskIdentifier = self.seqNo;
}

- (void)cancel{
    NSThread *thread = [CIPTaskManager threadForTaskMgr];
    if (thread == [NSThread currentThread]) {
        [self cancelTask];
    }else{
        [self performSelector:@selector(cancelTask) onThread:thread withObject:nil waitUntilDone:NO];
    }
}

- (void)cancelTask{
    self.isCanceled = YES;
    self.result = nil;
    self.failure = nil;
    self.success = nil;
    self.willRetryBlock = nil;
    self.failAndRetryBlock = nil;
    self.uploadBlock = nil;
    self.downloadBlock = nil;
    if (self.sessionTask) {
        [self.sessionTask cancel];
    }
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

- (void)finishTask{
    
    NSNumber *numSeq = [NSNumber numberWithInteger:self.seqNo];
    NSThread *thread = [CIPTaskManager threadForTaskMgr];
    if (thread == [NSThread currentThread]) {
        [[CIPTaskManager sharedInstance] removeTask:numSeq];
    }else{
        [[CIPTaskManager sharedInstance] performSelector:@selector(removeTask:)
                                                onThread:thread
                                              withObject:numSeq
                                           waitUntilDone:NO];
    }
}

#pragma mark - report result

- (void)didReceiveResponse:(NSURLResponse *)response{
    
    NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *)response;
    self.statusCode = [httpResponse statusCode];
    
    self.responseHeaders = [httpResponse allHeaderFields];
}

- (void)sessionDidFinish{
    
    if(self.httpTime < 0) {
        self.httpTime += [[NSDate date] timeIntervalSince1970];
        if(self.httpTime < 0)
            self.httpTime = 0;
    }
    if (self.error == nil) {
        [self didReceiveResponse:self.response];
        [self sessionSuccess:self.data];
    }else{
        NSError *errorInfo = [self.responseSerializer errorCodeFailRequest:self error:self.error];
        [self sessionFailure:errorInfo];
        [self pvCode:(int)errorInfo.code];
        NSString *tunnel = self.sessionType == CIPSessionTypeNone?@"HTTP":(self.sessionType == CIPSessionTypeDP?@"TUNNELDP":@"TUNNELWNS");
        CIPLOG(@"FAIL:[%@] time:[%f] error:[%@] url:[%@]",tunnel, self.httpTime *1000,self.error, [self.request.URL absoluteString]);
    }
}

- (void)sessionSuccess:(NSData *)data{
    NSError *serializationError = nil;
    self.result = [self.responseSerializer responseObjectForResponse:self.response task:self data:data error:&serializationError];
    
    NSString *tunnel = self.sessionType == CIPSessionTypeNone?@"HTTP":(self.sessionType == CIPSessionTypeDP?@"TUNNELDP":@"TUNNELWNS");
    //解析数据时出错
    if (serializationError) {
        if (self.error == nil) {
            self.error = serializationError;
        }
        [self sessionFailure:serializationError];
        [self pvCode:(int)serializationError.code];
        CIPLOG(@"解析数据时出错 FAIL:[%@] time:[%f] error:[%@] url:[%@]",tunnel, self.httpTime *1000,serializationError, [self.request.URL absoluteString]);
        return ;
    }
    [self pvCode:(int)self.statusCode];
    CIPLOG(@"FINISH:[%@] code:[%ld] time:[%f] url:[%@]",tunnel, (long)self.statusCode,self.httpTime * 1000, [self.request.URL absoluteString]);
    [self reportSuccess:self.result];
    if(self.result) {
        @autoreleasepool{
            NSData *decodeData = data;
            // write back to cache
            if ([self.responseSerializer respondsToSelector:@selector(dataForNetData:error:)]) {
                decodeData = [self.responseSerializer dataForNetData:data error:NULL];
            }
            [self writeCacheData:decodeData];
        }
    }
}

- (void)sessionFailure:(NSError *)error{
    
    // do not retry when 500. avoid server overload failure
    // do not retry when 404. pointless
    if((self.maxRetryTimes < 0 || self.retryTimes < self.maxRetryTimes)
       && self.statusCode / 100 != 5 && self.statusCode != 404) {
        
        [self performSelector:@selector(sendRetry) withObject:nil afterDelay:5];
        if (self.failAndRetryBlock != nil) {
            __weak typeof(self)  __weakSelf = self;
            dispatch_async(self.completionQueue?:dispatch_get_main_queue(), ^{
                __strong typeof(__weakSelf) __strongSelf = __weakSelf;
                __strongSelf.failAndRetryBlock(__strongSelf, error);
            });
        }
        CIPLOG(@"失败重试 FAIL[%@] w/t RETRY:%ld/%ld", [self.request.URL absoluteString],(long)self.retryTimes,(long)self.maxRetryTimes );
    } else if (![self networkFailureUseCacheData]) {
        [self reportError:error];
    }
}
- (void)reportError:(NSError *)error{
    
    if (self.isJustRefreshCache) return;//只是刷新缓存，不需要上报结果
    if (self.error == nil) {
        NSError *err = [[NSError alloc] initWithDomain:error.domain ?: @"default-error-domain" code:error.code userInfo:error.userInfo];
        self.error = err;
    }
    
    if (self.execType == CIPExecTypeSync) {//同步请求不走block形式上报。
        [self signalSyncLockAndFinishReq];
        return ;
    }
    __weak typeof(self) __weakSelf = self;
    dispatch_async(self.completionQueue?: dispatch_get_main_queue(), ^{
        __strong typeof(__weakSelf) __strongSelf = __weakSelf;
        
        [__strongSelf dispatchFromQueueError:error];
        [__strongSelf performSelector:@selector(finishTask) onThread:[CIPTaskManager threadForTaskMgr] withObject:nil waitUntilDone:NO];
    });
}

- (void)reportSuccess:(id)result{
    
    if (self.isJustRefreshCache) return ;//只是刷新缓存，不需要上报结果
    
    if (self.execType == CIPExecTypeSync) {//同步请求不走block形式上报。
        [self signalSyncLockAndFinishReq];
        return ;
    }
    __weak typeof(self) __weakSelf = self;
    dispatch_async(self.completionQueue?: dispatch_get_main_queue(), ^{
        __strong typeof(__weakSelf) __strongSelf = __weakSelf;
        [__strongSelf dispatchFromQueueSuccess:result];
        [__strongSelf performSelector:@selector(finishTask) onThread:[CIPTaskManager threadForTaskMgr] withObject:nil waitUntilDone:NO];
    });
}

- (void)dispatchFromQueueError:(NSError *)error{
    if (self.failure) {
        self.failure(self, error);
    }
}

- (void)dispatchFromQueueSuccess:(id)result{
    if (self.success) {
        self.success(self, result);
    }
}

- (void)signalSyncLockAndFinishReq{
    
    __weak typeof(self) __weakSelf = self;
    static dispatch_queue_t taskSignalQueue= nil;
    static  dispatch_once_t taskSyncPred;
    dispatch_once(&taskSyncPred, ^{
        taskSignalQueue = dispatch_queue_create("com.CIPNetwork.signalQueue", DISPATCH_QUEUE_SERIAL);
    });
    dispatch_async(taskSignalQueue, ^{
        __strong typeof(__weakSelf) __strongSelf = __weakSelf;
        [__strongSelf.syncLock lock];
        [__strongSelf.syncLock signal];
        [__strongSelf.syncLock unlock];
        [__strongSelf performSelector:@selector(finishTask)
                             onThread:[CIPTaskManager threadForTaskMgr]
                           withObject:nil
                        waitUntilDone:NO];
    });
}

#pragma mark - cached

- (void)readCacheData:(NSDictionary *)argDiction{
    
    
    __weak typeof(self) __weakSelf = self;
    self.cache.success = ^(CIPCache *cache,NSData *data){
        
        __strong typeof(__weakSelf) __strongSelf = __weakSelf;
        [__strongSelf dealCacheData:cache data:data cachedTime:cache.cachedTime error:nil];
    };
    
    self.cache.failure = ^(CIPCache *cache,NSData *data,NSError *error){
        
        __strong typeof(__weakSelf) __strongSelf = __weakSelf;
        [__strongSelf dealCacheData:cache data:data cachedTime:cache.cachedTime error:error];
    };
    [self.cache doCache];
}

- (void)writeCacheData:(NSData *)data{
    
    self.cache.writeData = data;
    [self.cache doCache];
}

- (void)dealCacheData:(CIPCache *)cache data:(NSData *)data cachedTime:(time_t)cachedTime error:(NSError *)error{
    
    @synchronized(self) {
        self.cachedTime = cachedTime;
        self.cacheReadData = data;
        [self performSelector:@selector(finishCache:) onThread:[CIPTaskManager threadForTaskMgr] withObject:error waitUntilDone:NO];
    }
}

- (void)finishCache:(NSError *)error{
    
    if (error == nil) {
        [self useCacheData];
        if (_cacheType == CIPCacheTypePersistent) {
            time_t dtime = time(0) - self.cachedTime;
            if(dtime > 3600 * 24) {// more than a day, refresh
                self.isJustRefreshCache = YES;
                [self send];
            }
        }
    }else{
        if (_cacheType == CIPCacheTypeCritical) {
            //已经走过网络 所以直接上报失败
            [self reportError:self.error];
        }else{
            [self send];
        }
    }
}

- (void)useCacheData{
    self.isDataFromCache = YES;
    self.error = nil; //确保error为空，防止上层从error判断请求成功与失败
    self.result = [self.responseSerializer resultWithData:self.cacheReadData error:NULL];
    [self reportSuccess:self.result];
}

// 网络请求失败时，走缓存规则取数据
- (BOOL)networkFailureUseCacheData{
    
    BOOL b = NO;
    if (self.cacheType != CIPCacheTypeDisabled &&
        self.cacheType != CIPCacheTypeCritical &&
        self.cacheType != CIPCacheTypePersistent) {
        if (self.cacheReadData) { //网络失败后，判断是否有缓存数据。有的话取缓存数据
            [self useCacheData];
            b = YES;
        }
    }else if (self.cacheType == CIPCacheTypeCritical) {
        //先走网络，网络失败后走缓存
        [self readCacheData:nil];
        b = YES;
    }
    return b;
}

- (CIPCache *)cache{
    if (_cache == nil) {
        _cache = [[CIPCache alloc] init];
        _cache.type = self.cacheType;
        _cache.key  = [self.request.URL absoluteString];//TODO:what`s cache key?
    }
    return _cache;
}

#pragma mark monitor
- (void)pvCode:(int)code{
    
    if ([self.configuration disableStatistics]) return;
    CIPMonitorCenter *monitor = [CIPMonitorCenter defaultCenter];
    if (monitor) {
        [monitor pvWithCommand:[monitor commandWithUrl:[self.request.URL absoluteString]]
                       network:0
                          code:code
                        tunnel:self.sessionType
                  requestBytes:(int)[self.request.HTTPBody length]
                 responseBytes:(int)[self.data length]
                  responseTime:(int)(self.httpTime*1000)
                            ip:[self getHost:[self.request.URL absoluteString]]];
        [monitor flush];
    }
}

- (NSString *)getHost:(NSString *)u {
    NSRange r = [u rangeOfString:@"://"];
    if(r.location == NSNotFound) {
        return nil;
    }
    NSRange r2 = [u rangeOfString:@"/" options:0 range:NSMakeRange(r.location + r.length, [u length] - r.location - r.length)];
    if(r2.location == NSNotFound) {
        return [u substringFromIndex:r.location + r.length];
    } else{
        return [u substringWithRange:NSMakeRange(r.location + r.length, r2.location - r.location - r.length)];
    }
}

#pragma mark confirm session type
- (CIPSessionType)confirmSessionType{
    return [self.configuration sessionTypeForRequest:self.request];
}
@end
