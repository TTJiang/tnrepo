//
//  CIPHttpResponseSerializer.h
//  Pods
//
//  Created by JiangTeng on 16/1/2.
//
//

#import <Foundation/Foundation.h>
@class CIPTask;
@protocol CIPResponseSerialization <NSObject, NSCopying>

/**
 The response object decoded from the data associated with a specified response.
 
 @param response The response to be processed.
 @param data The response data to be decoded.
 @param error The error that occurred while attempting to decode the response data.
 
 @return The object decoded from the specified response data.
 */
- (nullable id)responseObjectForResponse:(nullable NSURLResponse *)response
                                    task:(nullable CIPTask *)task
                                    data:(nullable NSData *)data
                                   error:(NSError * _Nullable __autoreleasing * __nullable)error;
/*
 网络请求失败后，对error处理后返回处理后的error
 */
- (nullable NSError *)errorCodeFailRequest:(nullable CIPTask *)task error:(nullable NSError *)error;
/*
 把数据转化成对象，把缓存数据取出转化成相应的对象，如:data -> json
 */
- (nullable id)resultWithData:(nonnull NSData *)data error:(NSError * _Nullable __autoreleasing * __nullable)error;
@optional
/*
 把网络数据做一些处理，如:解密，解压
 */
- (nullable NSData *)dataForNetData:(nullable NSData *)data
                              error:(NSError * _Nullable __autoreleasing * __nullable)error;
@end

@interface CIPHttpResponseSerializer : NSObject <CIPResponseSerialization>

/**
 The string encoding used to serialize data received from the server, when no string encoding is specified by the response. `NSUTF8StringEncoding` by default.
 */
@property (nonatomic, assign) NSStringEncoding stringEncoding;


@property (nonatomic, copy, nullable) NSIndexSet *acceptableStatusCodes;

/**
 The acceptable MIME types for responses. When non-`nil`, responses with a `Content-Type` with MIME types that do not intersect with the set will result in an error during validation.
 */
@property (nonatomic, copy, nullable) NSSet <NSString *> *acceptableContentTypes;


/**
 Creates and returns a serializer with default configuration.
 */
+ (nullable instancetype)serializer;


@end

#pragma mark -


/**
 `AFJSONResponseSerializer` is a subclass of `AFHTTPResponseSerializer` that validates and decodes JSON responses.
 
 By default, `AFJSONResponseSerializer` accepts the following MIME types, which includes the official standard, `application/json`, as well as other commonly-used types:
 
 - `application/json`
 - `text/json`
 - `text/javascript`
 */
@interface CIPJSONResponseSerializer : CIPHttpResponseSerializer

- (nullable instancetype)init;

/**
 Options for reading the response JSON data and creating the Foundation objects. For possible values, see the `NSJSONSerialization` documentation section "NSJSONReadingOptions". `0` by default.
 */
@property (nonatomic, assign) NSJSONReadingOptions readingOptions;

/**
 Whether to remove keys with `NSNull` values from response JSON. Defaults to `NO`.
 */
@property (nonatomic, assign) BOOL removesKeysWithNullValues;

/**
 Creates and returns a JSON serializer with specified reading and writing options.
 
 @param readingOptions The specified JSON reading options.
 */
+ (nullable instancetype)serializerWithReadingOptions:(NSJSONReadingOptions)readingOptions;

@end

#pragma mark -

/**
 `AFXMLParserResponseSerializer` is a subclass of `AFHTTPResponseSerializer` that validates and decodes XML responses as an `NSXMLParser` objects.
 
 By default, `AFXMLParserResponseSerializer` accepts the following MIME types, which includes the official standard, `application/xml`, as well as other commonly-used types:
 
 - `application/xml`
 - `text/xml`
 */
@interface CIPXMLParserResponseSerializer : CIPHttpResponseSerializer

@end

/**
 `AFPropertyListResponseSerializer` is a subclass of `AFHTTPResponseSerializer` that validates and decodes XML responses as an `NSXMLDocument` objects.
 
 By default, `AFPropertyListResponseSerializer` accepts the following MIME types:
 
 - `application/x-plist`
 */
@interface CIPPropertyListResponseSerializer : CIPHttpResponseSerializer

- (nullable instancetype)init;

/**
 The property list format. Possible values are described in "NSPropertyListFormat".
 */
@property (nonatomic, assign) NSPropertyListFormat format;

/**
 The property list reading options. Possible values are described in "NSPropertyListMutabilityOptions."
 */
@property (nonatomic, assign) NSPropertyListReadOptions readOptions;

/**
 Creates and returns a property list serializer with a specified format, read options, and write options.
 
 @param format The property list format.
 @param readOptions The property list reading options.
 */
+ (nullable instancetype)serializerWithFormat:(NSPropertyListFormat)format
                         readOptions:(NSPropertyListReadOptions)readOptions;

@end
