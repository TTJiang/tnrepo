//
//  CIPTaskManger.m
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/24.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "CIPTaskManager.h"

static BOOL useMainThread = NO;
static NSThread *processTaskThread;
static long taskNo;

#define MAX_REQUEST_SIZE    400

@interface CIPTask()

- (void)startTask;
- (void)cancelTask;
@end

@interface CIPTaskManager ()
{
    NSMutableDictionary * _dictRequest;
    NSRecursiveLock *_lock;
}

@end

@implementation CIPTaskManager

#pragma mark - Public
+ (CIPTaskManager*)sharedInstance
{
    static CIPTaskManager * taskManager = nil;
    static  dispatch_once_t once;
    dispatch_once(&once, ^{
        taskManager = [[CIPTaskManager alloc] init];
    });
    
    return taskManager;
}

+ (NSThread *)threadForTaskMgr
{
    if (useMainThread)
    {
        return [NSThread mainThread];
    }
    else
    {
        BOOL bInit = NO;
        if (processTaskThread == nil) {
            @synchronized(self)
            {
                if (processTaskThread == nil)
                {
                    processTaskThread = [[NSThread alloc] initWithTarget:self selector:@selector(taskLoop) object:nil];
                    bInit = YES;
                }
            }
        }
        
        if (bInit)
        {
            [processTaskThread setName:@"NetworkTaskThread"];
            [processTaskThread start];
        }
        
        return processTaskThread;
    }
}

+ (void)releaseCurrentThread
{
    processTaskThread = nil;
}

+ (void)taskLoop
{
    CFRunLoopSourceContext context = {0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};
    CFRunLoopSourceRef source = CFRunLoopSourceCreate(kCFAllocatorDefault, 0, &context);
    CFRunLoopAddSource(CFRunLoopGetCurrent(), source, kCFRunLoopDefaultMode);
    
    @autoreleasepool {
        CFRunLoopRun();
    }
    
    CFRunLoopRemoveSource(CFRunLoopGetCurrent(), source, kCFRunLoopDefaultMode);
    CFRelease(source);
}

+ (long)generateTaskNo
{
    
    static NSRecursiveLock * seqNolocker = nil;
    static  dispatch_once_t pred;
    
    dispatch_once(&pred, ^{
        seqNolocker = [[NSRecursiveLock alloc] init];
    });
    long seqNo;
    [seqNolocker lock];
    seqNo = ++taskNo;
    [seqNolocker unlock];
    
    return seqNo;
}

#pragma mark -instance method
- (id)init{
    self = [super init];
    if (self) {
        _dictRequest = [NSMutableDictionary dictionary];
        _lock = [[NSRecursiveLock alloc] init];
    }
    return self;
}

#pragma mark --
- (void)enqueueAndStartSession:(CIPTask *)task{
    
    [_lock lock];
    if ([_dictRequest count] > MAX_REQUEST_SIZE)
    {
        //阻塞了，需要清除所有请求
        NSArray *allTasks = [_dictRequest allValues];
        for (CIPTask * task in allTasks)
        {
            [task cancelTask];
        }
    }
    [_dictRequest setObject:task forKey:[NSNumber numberWithInteger:task.seqNo]];
    [_lock unlock];
    
    NSThread *thread = [CIPTaskManager threadForTaskMgr];
    if (thread == [NSThread currentThread]) {
        [task startTask];
    }else{
        [task performSelector:@selector(startTask)  onThread:[CIPTaskManager threadForTaskMgr] withObject:nil waitUntilDone:NO];
    }
}

- (void)cancelTask:(NSNumber *)seqNo{
    
    [_lock lock];
    CIPTask *task = [_dictRequest objectForKey:seqNo];
    if (task) {
        [task cancelTask];
    }
    [_lock unlock];
}

- (void)removeTask:(NSNumber *)seqNo
{
    [_lock lock];
    {
        CIPTask * task = [_dictRequest objectForKey:seqNo];
        if(task){
            [_dictRequest removeObjectForKey:seqNo];
        }
    }
    [_lock unlock];
}
@end
