//
//  NVMonitorCenter.m
//  Core
//
//  Created by ZhouHui on 14-5-19.
//  Copyright (c) 2014年 dianping.com. All rights reserved.
//

#import "CIPMonitorCenter.h"
#import <sys/time.h>
#import "CIPLog.h"
#import "CIPNetworkReachability.h"
#import "NSString+CIPExt.h"

static CIPMonitorCenter *__monitor = nil;

void CIPInternalSetDefaultMonitorCenter(CIPMonitorCenter *monitor) {
    __monitor = monitor;
}

@interface CIPMonitorCenter ()
@property (nonatomic, assign) NSInteger versionCode;
@property (nonatomic, retain) NSThread *thread;
@property (nonatomic, retain) NSCondition *condition;
@property (nonatomic, assign) BOOL stop;
@property (nonatomic, assign) BOOL force;
@end

@implementation CIPMonitorCenter {
    NSURL *_serverURL;
    NSMutableArray *_buffer;
}

+ (instancetype)defaultCenter {
    
    if (__monitor == nil) {
        __monitor = [[CIPMonitorCenter alloc] initWithServerURL:@"http://114.80.165.63/broker-service/api/batch?"];
    }
    return __monitor;
}

- (id)initWithServerURL:(NSString *)serverURLString {
    self = [super init];
    if (self) {
        self.versionCode = [self getVersionCode];
        if (serverURLString.length<1) {
            _serverURL = nil;
        } else {
            _serverURL = [NSURL URLWithString:serverURLString];
        }
        _thread = [[NSThread alloc] initWithTarget:self selector:@selector(run) object:nil];
        _condition = [[NSCondition alloc] init];
        _buffer = [NSMutableArray arrayWithCapacity:16];
        [_thread start];
    }
    return self;
}

- (NSString *)serverURL {
    return nil;
}

// 6.8 = 680, 6.8.5 = 685
- (NSInteger)getVersionCode {
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    NSScanner *scan = [NSScanner scannerWithString:build];
    NSInteger a=1, b=0, c=0; // aabbc
    do {
        if(![scan scanInt:(int *)&a]) {
            break;
        }
        if(![scan scanString:@"." intoString:NULL]) {
            break;
        }
        if(![scan scanInt:(int *)&b]) {
            break;
        }
        if(![scan scanString:@"." intoString:NULL]) {
            break;
        }
        [scan scanInt:(int *)&c];
    } while(0);
    return a*100 + (b%10)*10 + (c%10);
}

- (NSString *)commandWithUrl:(NSString *)url {
    if(!url)
        return @"";
    NSRange r = [url rangeOfString:@"?"];
    if(r.location == NSNotFound) {
    } else {
        url = [url substringToIndex:r.location];
    }
    r = [url rangeOfString:@"/" options:NSBackwardsSearch];
    if(r.location == NSNotFound) {
        return url;
    } else {
        return [url substringFromIndex:r.location + 1];
    }
}

- (void)pvWithCommand:(NSString *)cmd network:(int)network code:(int)code requestBytes:(int)reqBytes responseBytes:(int)respBytes responseTime:(int)respTime {
    [self pvWithCommand:cmd network:network code:code requestBytes:reqBytes responseBytes:respBytes responseTime:respTime ip:nil];
}

- (void)pvWithCommand:(NSString *)cmd network:(int)network code:(int)code requestBytes:(int)reqBytes responseBytes:(int)respBytes responseTime:(int)respTime ip:(NSString *)ip {
    [self pvWithCommand:cmd network:network code:code tunnel:0 requestBytes:reqBytes responseBytes:respBytes responseTime:respTime ip:ip];
}

- (void)pvWithCommand:(NSString *)cmd network:(int)network code:(int)code tunnel:(int)tunnel requestBytes:(int)reqBytes responseBytes:(int)respBytes responseTime:(int)respTime ip:(NSString *)ip {
    if (!_serverURL) {
        return;
    }
    NSMutableString *pv = [NSMutableString string];
    
    [pv appendFormat:@"%lld\t", (int64_t)([[NSDate date] timeIntervalSince1970] * 1000.0)];
    
    if(!network) {
        CIPNetworkReachability re = CIPGetAccurateNetworkReachability();
        switch (re) {
            case CIPNetworkReachabilityWifi:
                network = 1;
                break;
            case CIPNetworkReachabilityMobile2G:
                network = 2;
                break;
            case CIPNetworkReachabilityMobile3G:
                network = 3;
                break;
            case CIPNetworkReachabilityMobile4G:
                network = 4;
                break;
            default:
                network = 0;
                break;
        }
    }
    [pv appendFormat:@"%d\t", network];
    
    [pv appendFormat:@"%@\t", @(self.versionCode)];
    
    [pv appendFormat:@"%d\t", tunnel]; // tunnel
    
    [pv appendFormat:@"%@\t", [cmd urlEncode]];
    
    [pv appendFormat:@"%d\t", code];
    
    [pv appendString:@"2\t"]; // ios
    
    [pv appendFormat:@"%d\t", reqBytes];
    
    [pv appendFormat:@"%d\t", respBytes];
    
    [pv appendFormat:@"%d\t", respTime];
    
    if(ip) {
        [pv appendFormat:@"%@", ip];
    }
    
    CIPLOG(@"CAT: %@", pv);
    
    NSUInteger n;
    @synchronized (_buffer) {
        n = _buffer.count;
        while (n > 16) {
            [_buffer removeObjectAtIndex:0];
            n--;
        }
        [_buffer addObject:pv];
    }
    
    if (n == 0) {
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(uploadNow) object:nil];
        [self performSelector:@selector(uploadNow) withObject:nil afterDelay:15];
    } else if(n > 15) {
        [self uploadNow];
    }
}

- (void)uploadNow {
    [_condition signal];
}

- (void)flush {
    _force = YES;
	[_condition signal];
}

- (void)run {
    int retryTimes = 0;
	while (!_stop) {
        @autoreleasepool {
            [_condition lock];
            while(!_force && _buffer.count == 0) {
                [_condition wait];
            }
            [_condition unlock];
            if(_stop)
                return;
            _force = NO;
            
            NSInteger lines = 0;
//            NVEnvironment *env = [NVEnvironment defaultEnvironment];
//            NSString *dpid = [env dpid];
            NSString *dpid = nil;
            if (!dpid) {
                dpid = @"";
            }
            NSMutableArray *backlog = [NSMutableArray arrayWithCapacity:_buffer.count];
            NSMutableString *body = [NSMutableString stringWithFormat:@"v=3&dpid=%@&c=\n", dpid];
            
            @synchronized (_buffer) {
				if (_buffer.count > 0) {
					for (NSString *s in _buffer) {
                        [body appendString:s];
                        [body appendString:@"\n"];
                        lines++;
					}
                    [backlog addObjectsFromArray:_buffer];
					[_buffer removeAllObjects];
				} else {
					continue;
				}
			}
            
            CIPLOG(@"CAT BODY: %@", body);
            
            NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:_serverURL cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:15];
            [req setHTTPMethod:@"POST"];
            [req addValue:@"Content-Type" forHTTPHeaderField:@"application/x-www-form-urlencoded"];
            [req setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
            
//            NVLOG(@"CAT MONITOR URL: %@", _serverURL);
//            NVLOG(@"CAT MONITOR BODY: %@", body);
            NSHTTPURLResponse *resp = nil;
            if([NSURLConnection sendSynchronousRequest:req returningResponse:&resp error:NULL] && [resp statusCode] / 100 == 2) {
                retryTimes = 0;
                CIPLOG(@"CAT MONITOR UPLOAD FINISHED: COUNT = %@", @(lines));
            } else {
                
                // write things back to buffer
                BOOL isEmpty;
                @synchronized (_buffer) {
                    isEmpty = _buffer.count<1;
                    for (; backlog.count > 0 && _buffer.count < 16;) {
                        id lastObj = backlog.lastObject;
                        [backlog removeLastObject];
                        [_buffer insertObject:lastObj atIndex:0];
                    }
                }
                
                retryTimes++;
                CIPLOG(@"CAT MONITOR UPLOAD FAILED(%@): RETRYTIMES = %d, WAITING4UPLOAD=%@", @([resp statusCode]), retryTimes, @(_buffer.count));
                NSTimeInterval sleepTime = retryTimes>15?15:0;
                [NSThread sleepForTimeInterval:sleepTime];
            }
        }
    }
}

- (void)dealloc {
    _stop = YES; // dead code? -- yimin
}

@end
