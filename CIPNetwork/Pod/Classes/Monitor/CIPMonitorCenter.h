//
//  NVMonitorCenter.h
//  Core
//
//  Created by ZhouHui on 14-5-19.
//  Copyright (c) 2014年 dianping.com. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * 前端监控服务
 * <p>
 * 监控对象为API的接口调用，包括时长，状态码，错误等<br>
 * 监控日志批量上传，15秒一次<br>
 * 日志缓存为16条，超出部分丢弃只保留最近16条。日志缓存在内存中，进程关闭既消失
 *
 * @author zhouhui
 *
 */

@interface CIPMonitorCenter : NSObject

/**
 NVMonitorCenter以单例模式运行
 具体的App运行环境需要复写defaultCenter入口
 */
+ (instancetype)defaultCenter;

/**
 * API访问的PV日志
 *
 * @param command
 *            标示符，一般用url里最后的path表示，如“shop.bin”
 * @param network
 *            网络类型，1=Wifi, 3=移动网络，传0表示使用当前网络状态
 * @param code
 *            状态码，>1000表示业务错误码，<0表示自定义错误码，其余使用HTTP状态码
 * @param code
 *            连接通道，1为长连，0为短连
 * @param requestBytes
 *            请求字节数
 * @param responseBytes
 *            返回字节数
 * @param responseTime
 *            端到端响应时间
 * @param ip
 *            当前使用的IP
 */

- (void)pvWithCommand:(NSString *)cmd network:(int)network code:(int)code tunnel:(int)tunnel requestBytes:(int)reqBytes responseBytes:(int)respBytes responseTime:(int)respTime ip:(NSString *)ip;

- (void)pvWithCommand:(NSString *)cmd network:(int)network code:(int)code requestBytes:(int)reqBytes responseBytes:(int)respBytes responseTime:(int)respTime ip:(NSString *)ip;

- (void)pvWithCommand:(NSString *)cmd network:(int)network code:(int)code requestBytes:(int)reqBytes responseBytes:(int)respBytes responseTime:(int)respTime;

/**
 获取URL中的Command，比如http://m.api.dianping.com/shop.bin?id=1234的Command=shop.bin
 */
- (NSString *)commandWithUrl:(NSString *)url;

/**
 强制上传当前栈中的数据
 */
- (void)flush;

@end
