//
//  CIPLocalDNS.h
//  Pods
//
//  Created by JiangTeng on 16/1/6.
//
//

#import <Foundation/Foundation.h>

@protocol CIPLocalDNSProtocol <NSObject>

- (NSString *)localAnalyzeDNS:(NSString *)url;
@end

@interface CIPLocalDNS : NSObject<CIPLocalDNSProtocol>

- (NSURLRequest *)requestDNSAnalyze:(NSURLRequest *)request;
@end
