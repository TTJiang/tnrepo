
//
//  CIPLocalDNS.m
//  Pods
//
//  Created by JiangTeng on 16/1/6.
//
//

#import "CIPLocalDNS.h"

@implementation CIPLocalDNS

- (NSURLRequest *)requestDNSAnalyze:(NSURLRequest *)request{
    
    if([request.URL.scheme isEqualToString:@"https"]) return request;//https 暂不做dns解析,https可能是对域名绑定证书
    NSMutableURLRequest * mutableURLRequest = [request mutableCopy];
    mutableURLRequest.URL = [NSURL URLWithString:[self localAnalyzeDNS:request.URL.absoluteString]];
    return mutableURLRequest;
}

- (NSString *)localAnalyzeDNS:(NSString *)url{
    return url;
}
@end
