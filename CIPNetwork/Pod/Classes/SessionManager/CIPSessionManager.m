//
//  CIPSessionManger.m
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/25.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "CIPSessionManager.h"
#import "CIPHttpSession.h"
#import "CIPLog.h"
#import <UIKit/UIKit.h>

@interface CIPSessionManager() {
    
    NSThread *_sessionThread;
}
@end

@implementation CIPSessionManager

+ (CIPSessionManager*)sharedInstance
{
    static CIPSessionManager * sessionManager = nil;
    static  dispatch_once_t once;
    dispatch_once(&once, ^{
        sessionManager = [[CIPSessionManager alloc] init];
    });
    
    return sessionManager;
}

- (id)init{
    
    if (self = [super init]) {
        self.localDns = [[CIPLocalDNS alloc] init];
        [self addApplicationNotification];
    }
    return self;
}

- (void)setSessionThread:(NSThread *)thread{
    _sessionThread = thread;
}

- (void)addApplicationNotification{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)applicationEnterBackground:(NSNotification *)n {
    
    NSThread *thread = nil;
    if (_sessionThread) {
        thread = _sessionThread;
    }else{
        thread = [NSThread mainThread];
    }
    [self performSelector:@selector(setApplicationBackground:) onThread:thread withObject:@(YES) waitUntilDone:NO];
}

- (void)applicationWillEnterForeground:(NSNotification *)n {
    
    NSThread *thread = nil;
    if (_sessionThread) {
        thread = _sessionThread;
    }else{
        thread = [NSThread mainThread];
    }
    [self performSelector:@selector(setApplicationBackground:) onThread:thread withObject:@(NO) waitUntilDone:NO];
}

- (void)applicationDidBecomeActive:(NSNotification *)n {
   
    NSThread *thread = nil;
    if (_sessionThread) {
        thread = _sessionThread;
    }else{
        thread = [NSThread mainThread];
    }
    [self performSelector:@selector(applicationActive) onThread:thread withObject:nil waitUntilDone:NO];
}

- (void)setApplicationBackground:(NSNumber *)nBackground{
}

- (void)applicationActive{
}

- (CIPURLSessionDataTask *)sendHTTPRequest:(NSURLRequest *)request
                                       cmd:(NSString *)cmd
                                      type:(CIPSessionType)type
                                   timeout:(NSTimeInterval)timeout
                                completion:(void (^)(NSString *cmd, NSURLResponse* response, NSData* data, NSError* error, NSError *subError))completion{
    
    CIPURLSessionDataTask *dataTask = nil;
    switch (type) {
        case CIPSessionTypeNone:{
            NSURLRequest *dnsRequest = [self.localDns requestDNSAnalyze:request];
            CIPHttpSession *httpSession = [[CIPHttpSession alloc] init];
            dataTask = [httpSession sendHTTPRequest:dnsRequest cmd:cmd timeout:timeout completion:completion];
            if (dataTask != nil) {
                dataTask.urlSession = httpSession;
            }
        }
            break;
        case CIPSessionTypeDP:
            break;
            
        default:
            break;
    }
    
    if (dataTask == nil) {
        CIPLOG(@"---------init %@ fail,use httpSession",type == CIPSessionTypeDP?@"DPSession":(type == CIPSessionTypeWNS?@"WNSSession":@"HttpSession"));
        NSURLRequest *dnsRequest = [self.localDns requestDNSAnalyze:request];
        CIPHttpSession *httpSession = [[CIPHttpSession alloc] init];
        dataTask = [httpSession sendHTTPRequest:dnsRequest cmd:cmd timeout:timeout completion:completion];
        dataTask.urlSession = httpSession;
    }
    return dataTask;
}

- (void)cancelTunnelSession:(NSString *)tunnelUid {
}


@end
