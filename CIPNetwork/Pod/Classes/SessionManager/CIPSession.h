//
//  CIPSession.h
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/25.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CIPURLSessionDataTask;

typedef void (^CIPSessionUpload)(NSInteger sendedBytes,NSInteger totalBytes);
typedef void (^CIPSessionDownload)(NSInteger recvedBytes,NSInteger totalBytes);

@interface CIPSession : NSObject

@property (nonatomic, strong) NSString *cmd;
@property (nonatomic, copy) CIPSessionUpload uploadBlock;
@property (nonatomic, copy) CIPSessionDownload downloadBlock;

- (CIPURLSessionDataTask *)sendHTTPRequest:(NSURLRequest *)request
                                       cmd:(NSString *)cmd
                                   timeout:(NSTimeInterval)timeout
                                completion:(void (^)(NSString *cmd, NSURLResponse* response, NSData* data, NSError* error, NSError *subError))completion;

//app 进入前后台，background=YES 进行后台，否则进行前台
- (void)applicationEnterBackground:(BOOL)background;

//app 激活
- (void)applicationActive;

- (void)start;

- (void)cancel:(CIPURLSessionDataTask *)task;
@end
