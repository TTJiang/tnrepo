//
//  CIPSessionManger.h
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/25.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CIPURLSessionDataTask.h"
#import "CIPLocalDNS.h"

typedef void (^CIPSessionCompletionBlock)(NSString *cmd, NSURLResponse* response, NSData* data, NSError* error, NSError *subError);

@interface CIPSessionManager : NSObject

+ (CIPSessionManager *)sharedInstance;

@property (nonatomic, strong) CIPLocalDNS <CIPLocalDNSProtocol> *localDns;

/**
 * 设置请求线程
 */
- (void)setSessionThread:(NSThread *)thread;

/*! @brief 发送请求(HTTP协议接口)。
 *
 * @param request 第三方应用应用层HTTP请求
 * @param cmd 第三方应用应用层请求命令字
 * @param timeout 请求超时时间
 * @param completion 回调Block，参数data表示服务器应答数据，参数error表示内部错误，参数subError表示子类错误。当请求被取消时，该block也会被调用
 * @return 成功返回请求实例，失败返回nil。
 */
- (CIPURLSessionDataTask *)sendHTTPRequest:(NSURLRequest *)request
                                       cmd:(NSString *)cmd
                                      type:(CIPSessionType)type
                                   timeout:(NSTimeInterval)timeout
                                completion:(void (^)(NSString *cmd, NSURLResponse* response, NSData* data, NSError* error, NSError *subError))completion;
@end
