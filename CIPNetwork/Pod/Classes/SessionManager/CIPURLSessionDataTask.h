//
//  CIPURLSessionDataTask.h
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/25.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CIPSession.h"

typedef NS_OPTIONS(NSInteger, CIPSessionType){
    // 未使用长连
    CIPSessionTypeNone  = 0,
    /**
     DP长连通道
     */
    CIPSessionTypeDP    = 1,
    /**
     WNS长连通道
     */
    CIPSessionTypeWNS   = 4,
    /**
     HTTPS通道
     */
    CIPSessionTypeHTTPS = 8
};

typedef void (^CIPDataTaskUpload)(NSInteger sendedBytes,NSInteger totalBytes);
typedef void (^CIPDataTaskDownload)(NSInteger recvedBytes,NSInteger totalBytes);

@interface CIPURLSessionDataTask : NSObject

@property (nonatomic, assign) CIPSessionType type;
@property (nonatomic, assign) NSInteger taskIdentifier;
@property (nonatomic, strong) CIPSession *urlSession;
@property (nonatomic, strong) NSString *cmd;
@property (nonatomic, strong) NSString *tunnelUid;

@property (nonatomic, copy) CIPDataTaskUpload uploadBlock;
@property (nonatomic, copy) CIPDataTaskDownload downloadBlock;

/*
 目前只有http请求有效，其他的请求直接开始
*/
- (void)start;

- (void)cancel;
@end
