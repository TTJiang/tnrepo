//
//  CIPURLSessionDataTask.m
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/25.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "CIPURLSessionDataTask.h"
#import "CIPSessionManager.h"

@implementation CIPURLSessionDataTask

- (void)start{
    if (self.urlSession != nil){
        if (self.urlSession != nil) {
            self.urlSession.uploadBlock = self.uploadBlock;
        }
        
        if (self.downloadBlock != nil) {
            self.urlSession.downloadBlock = self.downloadBlock;
        }
        
        [self.urlSession start];
    }
}
- (void)cancel{

    if (self.urlSession != nil) {
        [self.urlSession cancel:self];
    }    
}
@end
