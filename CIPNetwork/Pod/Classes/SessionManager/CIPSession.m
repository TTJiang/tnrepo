
//
//  CIPSession.m
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/25.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "CIPSession.h"
#import "CIPURLSessionDataTask.h"

@implementation CIPSession

- (CIPURLSessionDataTask *)sendHTTPRequest:(NSURLRequest *)request
                                       cmd:(NSString *)cmd
                                   timeout:(NSTimeInterval)timeout
                                completion:(void (^)(NSString *cmd, NSURLResponse* response, NSData* data, NSError* error, NSError *subError))completion{
    self.cmd = cmd;
    CIPURLSessionDataTask *task = [[CIPURLSessionDataTask alloc] init];
    task.cmd = cmd;
    return task;
}

- (void)applicationEnterBackground:(BOOL)background{
    //具体的业务由子类实现
}

- (void)applicationActive{
    //具体的业务由子类实现
}

- (void)start{
    //具体的业务由子类实现
}

- (void)cancel:(CIPURLSessionDataTask *)task{
    //具体的业务由子类实现
}
@end
