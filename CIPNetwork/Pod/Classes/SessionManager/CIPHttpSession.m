//
//  CIPHttpSession.m
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/25.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "CIPHttpSession.h"
#import "CIPSessionManager.h"

@interface CIPHttpSession()<NSURLConnectionDataDelegate, NSURLConnectionDelegate>

@property (nonatomic, strong) NSURLConnection *connection;
@property (nonatomic, strong) NSHTTPURLResponse *response;
@property (nonatomic, strong) NSDictionary *responseHeaders;
@property (nonatomic, assign) NSInteger expectedLength;
@property (nonatomic, strong) NSMutableData *receivedData;
@property (nonatomic, copy) CIPSessionCompletionBlock completion;
@end

@implementation CIPHttpSession


- (CIPURLSessionDataTask *)sendHTTPRequest:(NSURLRequest *)request
                                       cmd:(NSString *)cmd
                                   timeout:(NSTimeInterval)timeout
                                completion:(void (^)(NSString *cmd, NSURLResponse* response, NSData* data, NSError* error, NSError *subError))completion{
    
    CIPURLSessionDataTask *task = [super sendHTTPRequest:request cmd:cmd timeout:timeout completion:completion];
    NSURLConnection *connection2 = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
    self.connection = connection2;
    self.completion = completion;
    
    task.type = CIPSessionTypeNone;
    return task;
}

- (void)start{
    [self.connection start];
}
- (void)cancel:(CIPURLSessionDataTask *)task{
    [self.connection cancel];
}
#pragma mark - NSURLConnectionDelegate
- (void)            connection: (NSURLConnection *)conn
            didReceiveResponse: (NSURLResponse *)response {
    
    self.response = (NSHTTPURLResponse *)response;
    NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *)response;
    self.responseHeaders = [httpResponse allHeaderFields];
    self.expectedLength = [[self.responseHeaders objectForKey:@"Content-Length"] intValue];
    if (self.expectedLength > 0) {
        self.receivedData = [[NSMutableData alloc] initWithCapacity:self.expectedLength];
    }else{
        self.receivedData = [[NSMutableData alloc] init];
    }
}

- (void)            connection: (NSURLConnection *)conn
                didReceiveData: (NSData *)d {
    
    [self.receivedData appendData:d];
    
    if (self.downloadBlock != nil) {
        self.downloadBlock([self.receivedData length],self.expectedLength);
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)conn {
    if (self.completion) {
        self.completion(self.cmd, self.response, self.receivedData, nil, nil);
    }
}

- (void)            connection: (NSURLConnection *)conn
              didFailWithError: (NSError *)err {
    if (self.completion) {
        self.completion(self.cmd, self.response, self.receivedData, err, nil);
    }
}

- (void)            connection: (NSURLConnection *)conn
               didSendBodyData: (NSInteger)bytesWritten
             totalBytesWritten: (NSInteger)totalBytesWritten
     totalBytesExpectedToWrite: (NSInteger)totalBytesExpectedToWrite {
    if (self.uploadBlock != nil) {
        self.uploadBlock(totalBytesWritten, totalBytesExpectedToWrite);
    }
}

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    NSString * host = challenge.protectionSpace.host;
    SecTrustRef trust = challenge.protectionSpace.serverTrust;
    BOOL auth = NO;
    if ([host rangeOfString:@"51ping"].location != NSNotFound) {
        auth = YES;
    } else {
        SecTrustResultType trustResult;
        OSStatus status = SecTrustEvaluate(trust, &trustResult);
        auth = status == errSecSuccess && (trustResult == kSecTrustResultProceed || trustResult == kSecTrustResultUnspecified);
    }
    if (auth) {
        NSURLCredential *cred = [NSURLCredential credentialForTrust:trust];
        [challenge.sender useCredential:cred forAuthenticationChallenge:challenge];
    } else {
        [challenge.sender cancelAuthenticationChallenge:challenge];
    }
}

@end
