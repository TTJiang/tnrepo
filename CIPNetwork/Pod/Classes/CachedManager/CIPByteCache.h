//
//  CIPByteCache.h
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/24.
//  Copyright © 2015年 dianping. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface CIPByteCache : NSObject

- (id)initWithFile:(NSString *)path name:(NSString *)tableName;

- (NSData *)fetch:(NSString *)url timestamp:(time_t *)time;

- (NSData *)fetch:(NSString *)url timestamp:(time_t *)time location:(CLLocation **)location;

- (BOOL)push:(NSData *)data forKey:(NSString *)url;

- (BOOL)push:(NSData *)data location:(CLLocation *)location accuracy:(NSInteger)accuracy forKey:(NSString *)url;

- (BOOL)push:(NSData *)data timestamp:(time_t)time forKey:(NSString *)url;

- (BOOL)push:(NSData *)data timestamp:(time_t)time location:(CLLocation *)location accuracy:(NSInteger)accuracy forKey:(NSString *)url;

- (BOOL)remove:(NSString *)url;

- (BOOL)trimToTimestamp:(time_t)time;
@end
