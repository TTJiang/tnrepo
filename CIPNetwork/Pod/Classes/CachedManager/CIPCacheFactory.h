//
//  CIPCacheFactory.h
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/24.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CIPCache.h"

@interface CIPCacheModel : NSObject

@property (nonatomic, strong) NSDictionary *argDic;
@property (nonatomic, assign) CIPCacheType cacheType;

- (id)initWithArgs:(NSDictionary *)argDic cacheType:(CIPCacheType )cacheType;
@end

@interface CIPCacheFactory : NSObject

+ (CIPCache *)createCache:(CIPCacheModel *)cacheData;
@end

