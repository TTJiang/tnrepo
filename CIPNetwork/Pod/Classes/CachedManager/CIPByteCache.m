//
//  CIPByteCache.m
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/24.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "CIPByteCache.h"
#import <pthread.h>
#import "sqlite3.h"


@interface CIPByteCache () {
    NSString *filename;
    NSString *name;
    
    sqlite3 *db;
    pthread_rwlock_t rwlock;
}

@end

@implementation CIPByteCache

- (id)init {
    return nil;
}

- (id)initWithFile:(NSString *)path name:(NSString *)tableName {
    if(self = [super init]) {
        db = NULL;
        if(sqlite3_libversion_number() < 3006012) {
            if(sqlite3_open([path UTF8String], &db) != SQLITE_OK) {
                sqlite3_close(db);
                return nil;
            }
        } else {
            if(sqlite3_open_v2([path UTF8String], &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_FULLMUTEX, NULL) != SQLITE_OK) {
                sqlite3_close(db);
                return nil;
            }
        }
        NSString *sqlstr = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (URL TEXT PRIMARY KEY, TIME INTEGER, SIZE INTEGER, DATA BOLB, LOCATION TEXT);", tableName];
        const char *sql = [sqlstr UTF8String];
        if(sqlite3_exec(db, sql, NULL, NULL, NULL) != SQLITE_OK) {
            sqlite3_close(db);
            return nil;
        }
        // add extra column
        sqlstr = [NSString stringWithFormat:@"ALTER TABLE %@ ADD LOCATION TEXT;", tableName];
        const char *sqlAlter = [sqlstr UTF8String];
        sqlite3_exec(db, sqlAlter, NULL, NULL, NULL);
        
        if(pthread_rwlock_init(&rwlock, NULL)) {
            return nil;
        }
        filename = path;
        name = tableName;
    }
    return self;
}

- (NSData *)fetch:(NSString *)url timestamp:(time_t *)ti {
    return [self fetch:url timestamp:ti location:nil];
}

- (NSData *)fetch:(NSString *)url timestamp:(time_t *)ti location:(CLLocation **)location {
    NSString *sql = [NSString stringWithFormat:@"SELECT DATA, TIME, LOCATION FROM %@ WHERE URL = ?;", name];
    if(pthread_rwlock_rdlock(&rwlock))
        return nil;
    sqlite3_stmt *stmt = NULL;
    NSData *data = nil;
    if(sqlite3_prepare_v2(db, [sql UTF8String], -1, &stmt, NULL) != SQLITE_OK) {
        pthread_rwlock_unlock(&rwlock);
        return nil;
    }
    
    sqlite3_bind_text(stmt, 1, [url UTF8String], (int)[url length], NULL);
    
    if(SQLITE_ROW == sqlite3_step(stmt)) {
        time_t t = sqlite3_column_int(stmt, 1);
        if(ti) {
            *ti = t;
        }
        const char *l = (const char *)sqlite3_column_text(stmt, 2);
        if (location) {
            if (l!=NULL && strlen(l)>0) {
                NSString *locationString = [NSString stringWithCString:l encoding:NSUTF8StringEncoding];
                NSArray *arr = [locationString componentsSeparatedByString:@"&"];
                NSString *lat = nil;
                NSString *lng = nil;
                NSString *accuracy = nil;
                for (NSString *str in arr) {
                    NSArray *kv = [str componentsSeparatedByString:@"="];
                    if (kv.count == 2) {
                        if ([kv[0] isEqualToString:@"lat"]) {
                            lat = kv[1];
                        } else if ([kv[0] isEqualToString:@"lng"]) {
                            lng = kv[1];
                        } else if ([kv[0] isEqualToString:@"accuracy"]) {
                            accuracy = kv[1];
                        }
                    }
                }
                if (lat.length>0 && lng.length>0) {
                    *location = [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[lng doubleValue]];
                } else {
                    *location = nil;
                }
            }
        }
        data = [[NSData alloc] initWithBytes:sqlite3_column_blob(stmt, 0) length:sqlite3_column_bytes(stmt, 0)];
    }
    
    sqlite3_finalize(stmt);
    pthread_rwlock_unlock(&rwlock);
    return data;
}

- (BOOL)push:(NSData *)data forKey:(NSString *)url {
    return [self push:data timestamp:time(0) forKey:url];
}

- (BOOL)push:(NSData *)data location:(CLLocation *)location accuracy:(NSInteger)accuracy forKey:(NSString *)url {
    return [self push:data timestamp:time(0) location:location accuracy:accuracy forKey:url];
}

- (BOOL)push:(NSData *)data timestamp:(time_t)time forKey:(NSString *)url {
    return [self push:data timestamp:time location:nil accuracy:0 forKey:url];
}

- (BOOL)push:(NSData *)data timestamp:(time_t)time location:(CLLocation *)location accuracy:(NSInteger)accuracy forKey:(NSString *)url {
    NSString *sqlstr = [NSString stringWithFormat:@"INSERT OR REPLACE INTO %@ (URL, TIME, SIZE, DATA, LOCATION) VALUES(?, ?, ?, ?, ?);", name];
    const char *sql = [sqlstr UTF8String];
    if(pthread_rwlock_wrlock(&rwlock))
        return NO;
    sqlite3_stmt *stmt = NULL;
    if(sqlite3_prepare_v2(db, sql, -1, &stmt, NULL) != SQLITE_OK) {
        pthread_rwlock_unlock(&rwlock);
        return NO;
    }
    
    sqlite3_bind_text(stmt, 1, [url UTF8String], (int)[url length], NULL);
    
    sqlite3_bind_int(stmt, 2, (int)time);
    
    sqlite3_bind_int(stmt, 3, (int)[data length]);
    
    if(sqlite3_bind_blob(stmt, 4, [data bytes], (int)[data length], NULL) != SQLITE_OK) {
        sqlite3_finalize(stmt);
        pthread_rwlock_unlock(&rwlock);
        return NO;
    }
    
    if (location) {
        NSString *locationString = [NSString stringWithFormat:@"lat=%f&lng=%f&accuracy=%@", location.coordinate.latitude, location.coordinate.longitude, @(accuracy)];
        sqlite3_bind_text(stmt, 5, [locationString UTF8String], (int)[locationString length], NULL);
    } else {
        sqlite3_bind_text(stmt, 5, NULL, 0, NULL);
    }
    
    BOOL result = (SQLITE_DONE == sqlite3_step(stmt));
    sqlite3_finalize(stmt);
    
    pthread_rwlock_unlock(&rwlock);
    return result;
}

- (BOOL)remove:(NSString *)url {
    NSString *sqlstr = [NSString stringWithFormat:@"DELETE FROM %@ WHERE URL = \"%@\";", name, url];
    if(pthread_rwlock_wrlock(&rwlock))
        return NO;
    BOOL result = SQLITE_OK == sqlite3_exec(db, [sqlstr UTF8String], NULL, NULL, NULL);
    pthread_rwlock_unlock(&rwlock);
    return result;
}

- (BOOL)trimToTimestamp:(time_t)time {
    BOOL result = YES;
    if(pthread_rwlock_wrlock(&rwlock))
        return NO;
    if(sqlite3_exec(db, "BEGIN TRANSACTION", NULL, NULL, NULL) != SQLITE_OK) {
        pthread_rwlock_unlock(&rwlock);
        return NO;
    }
    do {
        NSString *sqlstr = [[NSString alloc] initWithFormat:@"DELETE FROM %@ WHERE TIME <= %@", name, @(time)];
        if(SQLITE_OK != sqlite3_exec(db, [sqlstr UTF8String], NULL, NULL, NULL)) {
            result = NO;
            break;
        }
    } while(NO);
    
    if(result && sqlite3_exec(db, "COMMIT", NULL, NULL, NULL) != SQLITE_OK)
        result = NO;
    
    if(result) {
    } else {
        sqlite3_exec(db, "ROLLBACK", NULL, NULL, NULL);
    }
    
    pthread_rwlock_unlock(&rwlock);
    return result;
}

- (void)dealloc {
    sqlite3_close(db);
    pthread_rwlock_destroy(&rwlock);
}
@end