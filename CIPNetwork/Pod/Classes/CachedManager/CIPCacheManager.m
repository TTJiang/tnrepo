//
//  CIPCacheManager.m
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/24.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "CIPCacheManager.h"
#import <UIKit/UIKit.h>

@interface CIPCacheManager()

@property (nonatomic, strong) CIPOperationQueue *cacheQueue;
@property (nonatomic, strong) CIPByteCache *normalCache;
@property (nonatomic, strong) CIPByteCache *persistentCache;
@end

@implementation CIPCacheManager

+ (CIPCacheManager*)sharedInstance
{
    static CIPCacheManager * cacheManager = nil;
    static  dispatch_once_t once;
    dispatch_once(&once, ^{
        cacheManager = [[CIPCacheManager alloc] init];
        
    });
    
    return cacheManager;
}

- (id)init{
    if (self = [super init]) {
        [self addApplicationNotifications];
    }
    return self;
}

- (CIPByteCache *)normalCache {
    if(!_normalCache) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *path = [paths objectAtIndex:0];
        path = [path stringByAppendingPathComponent:@"apicache.db"];
        _normalCache = [[CIPByteCache alloc] initWithFile:path name:@"NORMAL"];
    }
    return _normalCache;
}

- (CIPByteCache *)persistentCache {
    if(!_persistentCache) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *path = [paths objectAtIndex:0];
        path = [path stringByAppendingPathComponent:@"fastcache3.db"];
        _persistentCache = [[CIPByteCache alloc] initWithFile:path name:@"PERSISTENCE"];
    }
    return _persistentCache;
}

- (CIPOperationQueue *)cacheQueue {
    
    if(!_cacheQueue) {
        _cacheQueue = [[CIPOperationQueue alloc] initWithThreadCount:1];
    }
    return _cacheQueue;
}
#pragma mark - notification

- (void)addApplicationNotifications{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willTerminate:) name:UIApplicationWillTerminateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(memoryWarning:) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
}

- (void)didEnterBackground:(NSNotification *)n {
        if(_normalCache) {//thread safe?
            [_normalCache trimToTimestamp:time(0) - 300];
        }
}
- (void)willTerminate:(NSNotification *)n {
    
}
- (void)memoryWarning:(NSNotification *)n {
    
}
@end