//
//  CIPCache.m
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/24.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "CIPCache.h"
#import "CIPCacheManager.h"
#import "CIPByteCache.h"
#import "CIPLog.h"


@interface CIPCache()

@property (nonatomic, strong) NSData *readData;
@end

@implementation CIPCache

- (void)prepareFetch{
    
}

- (void)doCache{
    [self prepareFetch];
    [[CIPCacheManager sharedInstance].cacheQueue addOperation:self];
}

- (void)main{
    // run in background
    if(self.writeData) {
        [self writeData2Cache];
        return ;
    }
    
    // just load cache
//    if(isCanceled)
//        return;
    [self readDataFromCache];
    
}

- (void)writeData2Cache{
    // just write to cache
    CIPByteCache *cache;
    switch (self.type) {
        case CIPCacheTypeNormal:
        case CIPCacheTypeDaily:
            cache = [[CIPCacheManager sharedInstance] normalCache];
            //                if (self.defaultCacheLocationScope>0) {
            //                    CLLocation *location = nil;
            //                    int accuracy = 0;
            //                    if ([self url:url getLocation:&location accuracy:&accuracy]) {
            //                        NSString *dimUrl = [self urlByRemoveLocation:url];
            //                        [cache push:writeToCacheData location:location accuracy:accuracy forKey:dimUrl];
            //                    } else {
            //                        [cache push:writeToCacheData forKey:url];
            //                    }
            //                } else {
            [cache push:self.writeData forKey:self.key];
            //                }
            break;
        case CIPCacheTypePersistent:
        case CIPCacheTypeCritical:
            cache = [[CIPCacheManager sharedInstance] persistentCache];
            //                if (self.defaultCacheLocationScope>0) {
            //                    CLLocation *location = nil;
            //                    int accuracy = 0;
            //                    if ([self url:url getLocation:&location accuracy:&accuracy]) {
            //                        NSString *dimUrl = [self urlByRemoveLocation:url];
            //                        [cache push:writeToCacheData location:location accuracy:accuracy forKey:dimUrl];
            //                    } else {
            //                        [cache push:writeToCacheData forKey:url];
            //                    }
            //                } else {
            [cache push:self.writeData forKey:self.key];
            //                }
            break;
        default:
            break;
    }
    self.writeData = nil;
}

- (void)readDataFromCache{
    CIPByteCache *cache;
    switch (self.type) {
        case CIPCacheTypeNormal:
        case CIPCacheTypeDaily:
            cache = [[CIPCacheManager sharedInstance] normalCache];
            //            if (self.defaultCacheLocationScope>0) {
            //                CLLocation *l = nil;
            //                cachedData = [cache fetch:[self urlByRemoveLocation:url] timestamp:&cachedTime location:&l];
            //                cachedLocation = l;
            //            } else {
            self.readData = [cache fetch:self.key timestamp:&_cachedTime];
            //            }
            break;
        case CIPCacheTypePersistent:
        case CIPCacheTypeCritical:
            cache = [[CIPCacheManager sharedInstance] persistentCache];
            //            if (self.defaultCacheLocationScope>0) {
            //                CLLocation *l = nil;
            //                cachedData = [cache fetch:[self urlByRemoveLocation:url] timestamp:&cachedTime location:&l];
            //                cachedLocation = l;
            //            } else {
            self.readData = [cache fetch:self.key timestamp:&_cachedTime];
            //            }
            break;
        default:
            break;
    }
    [self cacheDidLoaded];
}

- (void)cacheDidLoaded {
//    if(isCanceled)
//        return;
//    FILTER_ZOMBIE_DELEGATE;
    
    time_t dtime = time(0) - _cachedTime;
    switch (self.type) {
        case CIPCacheTypeNormal:
            if(self.readData && dtime > 0 && dtime < 300) {
                // cache hit
                [self hitCache];
            } else {
                // no cache
                [self noHitCache:@"no cache"];
            }
            break;
        case CIPCacheTypeDaily:
            ;
            NSTimeInterval today = [[NSDate date] timeIntervalSince1970];
            today += 8 * 3600;
            today = (int)today - (int)today % (24 * 3600);
            today -= 8 * 3600;
            if(self.readData && dtime > 0 && self.cachedTime > today) {
                // cache hit
                [self hitCache];
//                // statistics
//                if(!disableStatistics) {
//                    NSArray *extras = [NSArray arrayWithObjects:@"network", @"cache", @"tag", @"CACHE", nil];
//                    [[NVStatisticsCenter defaultCenter] pageViewWithUrl:url extras:extras];
//                }
            } else {
                // no cache
                [self noHitCache:@"no cache"];
            }
            break;
        case CIPCacheTypePersistent:
            if(self.readData) {
                // cache hit
                [self hitCache];
//                // statistics
//                if(!disableStatistics) {
//                    NSArray *extras = [NSArray arrayWithObjects:@"network", @"cache", @"tag", @"CACHE", nil];
//                    [[NVStatisticsCenter defaultCenter] pageViewWithUrl:url extras:extras];
//                }
            } else {
                // no cache
                [self noHitCache:@"no cache"];
            }
            break;
        case CIPCacheTypeCritical:
            if(self.readData) {
                // cache hit
                [self hitCache];
                // statistics
//                if(!disableStatistics) {
//                    NSArray *extras = [NSArray arrayWithObjects:@"network", @"cache", @"tag", @"CACHE", nil];
//                    [[NVStatisticsCenter defaultCenter] pageViewWithUrl:url extras:extras];
//                }
            } else {
                // no cache, already requested, so fail
                [self noHitCache:@"no cache"];
//                if(statusCode) {
//                    NVLOG(@"FAIL: (%@, CRITICAL) %@", @(statusCode), ACTUAL_URL(url));
//                } else {
//                    NVLOG(@"FAIL: (TIMEOUT, CRITICAL) %@", ACTUAL_URL(url));
//                }
                
//                // statistics
//                if(!disableStatistics) {
//                    NSMutableArray *extras = [NSMutableArray arrayWithCapacity:8];
//                    [extras addObject:@"elapse"];
//                    [extras addObject:@"-1"];
//                    switch (NVGetNetworkReachability()) {
//                        case NVNetworkReachabilityWifi:
//                            [extras addObject:@"network"];
//                            [extras addObject:@"wifi"];
//                            break;
//                        case NVNetworkReachabilityMobile:
//                            [extras addObject:@"network"];
//                            [extras addObject:@"mobile"];
//                            break;
//                        default:
//                            break;
//                    }
//                    [extras addObject:@"tag"];
//                    [extras addObject:@"TIMEOUT"];
//                    if(retryTimes > 0) {
//                        [extras addObject:@"retry"];
//                        [extras addObject:[NSString stringWithFormat:@"%@", @(retryTimes)]];
//                    }
//                    [[NVStatisticsCenter defaultCenter] pageViewWithUrl:url extras:extras];
//                }
            }
        default:
            break;
    }
}

- (void)hitCache{
    if (self.success) {
        self.success(self, self.readData);
    }
    CIPLOG(@"cache hit! key:%@",self.key);
}

- (void)noHitCache:(NSString *)reason{
    
    if (self.failure) {
        NSDictionary *errorInfo = @{
                                    NSLocalizedDescriptionKey:reason,
                                    };
        NSError *error = [[NSError alloc] initWithDomain:@"cache.com" code:-999 userInfo:errorInfo];
        self.failure(self, self.readData, error);
    }
    CIPLOG(@"cache miss! key:%@",self.key);
}
@end