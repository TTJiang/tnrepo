//
//  CIPCache.h
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/24.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CIPOperationQueue.h"

typedef NS_ENUM(NSInteger, CIPCacheType) {
    // 不使用缓存
    CIPCacheTypeDisabled    = 0,
    
    // 标准缓存，过期会被清除 5 minuter
    CIPCacheTypeNormal      = 1,
    
    // 持久化缓存，不会过期，超过一定时间会自动更新
    // 已停用
    CIPCacheTypePersistent  = 2,
    
    // 关键业务缓存，不会过期，当网络失效的情况下才会命中
    CIPCacheTypeCritical    = 3,
    
    // 和NVCacheTypeNormal类似，但持续时间为当天，每天0:00:00过期
    CIPCacheTypeDaily       = 4
};

@class CIPCache;
typedef void (^DMCacheSuccessBlock)(CIPCache *cache, NSData *data);
typedef void (^DMCacheFailureBlock)(CIPCache *cache, NSData *data, NSError* error);

@interface CIPCache : NSObject<CIPOperation>

@property (nonatomic, copy) DMCacheSuccessBlock success;
@property (nonatomic, copy) DMCacheFailureBlock failure;
@property (nonatomic, assign) CIPCacheType type;
@property (nonatomic, strong) NSString *key;
@property (nonatomic, assign) time_t cachedTime;
@property (nonatomic, strong) NSData *writeData;

- (void)doCache;      //开始做缓存，读或者写
- (void)prepareFetch;
@end
