//
//  CIPCacheManager.h
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/24.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CIPCache.h"
#import "CIPByteCache.h"

@interface CIPCacheManager : NSObject
+ (CIPCacheManager*)sharedInstance;

- (CIPByteCache *)normalCache;
- (CIPByteCache *)persistentCache;
- (CIPOperationQueue *)cacheQueue;

@end
