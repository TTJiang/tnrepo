//
//  CIPCacheFactory.m
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/24.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "CIPCacheFactory.h"


@implementation CIPCacheModel

- (id)initWithArgs:(NSDictionary *)argDic cacheType:(CIPCacheType )cacheType{
    
    if (self = [super init]) {
        self.argDic = argDic;
        self.cacheType = cacheType;
    }
    return self;
}
@end

@implementation CIPCacheFactory
+ (CIPCache *)createCache:(CIPCacheModel *)cacheModel{
    
    if (nil == cacheModel || nil == cacheModel.argDic) {
        return nil;
    }
    
    NSNumber *numType = [cacheModel.argDic objectForKey:@"type"];
    if (nil == numType) {
        return nil;
    }
    int type = [numType intValue];
    switch (type) {
        case CIPCacheTypeNormal:{
            
            CIPCache *cache = [[CIPCache alloc] init];
            cache.type = cacheModel.cacheType;
            return cache;
        }
            break;
            
        default:
            break;
    }
    return nil;
}
@end
