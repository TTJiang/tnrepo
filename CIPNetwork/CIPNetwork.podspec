#
# Be sure to run `pod lib lint CIPNetwork.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "CIPNetwork"
  s.version          = "0.1.0"
  s.summary          = "A short description of CIPNetwork."

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = <<-DESC
                       DESC

  s.homepage         = "https://github.com/<GITHUB_USERNAME>/CIPNetwork"
  s.license          = 'MIT'
  s.author           = { "CIPNetwork" => "jiangteng.cn@gmail.com" }
  s.source           = { :git => "https://github.com/<GITHUB_USERNAME>/CIPNetwork.git", :tag => s.version.to_s }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'CIPNetwork' => ['Pod/Assets/*.png']
  }

    s.frameworks = "SystemConfiguration", "AdSupport", "CoreTelephony", "UIKit", "CFNetwork", "Security"
    s.libraries = "sqlite3", "z", "stdc++.6", "c++"
end
