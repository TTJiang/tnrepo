//
//  TNAppDelegate.h
//  CIPNetwork
//
//  Created by CIPNetwork on 03/06/2016.
//  Copyright (c) 2016 CIPNetwork. All rights reserved.
//

@import UIKit;

@interface TNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
