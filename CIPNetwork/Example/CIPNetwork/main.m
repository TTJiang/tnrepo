//
//  main.m
//  CIPNetwork
//
//  Created by CIPNetwork on 03/06/2016.
//  Copyright (c) 2016 CIPNetwork. All rights reserved.
//

@import UIKit;
#import "TNAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TNAppDelegate class]));
    }
}
