//
//  CIPNovaTask.h
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/27.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "CIPTask.h"
#import "CIPHttpRequestSerializer.h"
#import "CIPHttpResponseSerializer.h"


@interface CIPMApiRequestSeializer : CIPHttpRequestSerializer

@end


@interface CIPMApiResponseSerializer : CIPHttpResponseSerializer

@end
