//
//  CIPNovaTask.m
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/27.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "CIPMApiSerializer.h"
#import "NSString+CIPExt.h"
#import "NSData+CIPExt.h"
#import "NSURL+CIPExt.h"
#import "NVObject.h"
#import "NVSimpleMsg.h"

@implementation CIPMApiRequestSeializer

+ (instancetype)serializer {
    return [[self alloc] init];
}

#pragma mark - AFURLRequestSerialization

- (NSURLRequest *)requestBySerializingRequest:(NSURLRequest *)request
                               withParameters:(id)parameters
                                     postData:(nullable NSData *)postData
                                        error:(NSError *__autoreleasing *)error
{
    NSParameterAssert(request);
    
    NSMutableURLRequest *mutableRequest = [request mutableCopy];    
    
    NSString *query = nil;
    if (parameters) {
        
        query = [self queryStringFromParameters:parameters];
    }
    if ([self.HTTPMethodsEncodingParametersInURI containsObject:[[request HTTPMethod] uppercaseString]]) {
        if (query) {
            mutableRequest.URL = [NSURL URLWithString:[[mutableRequest.URL absoluteString] stringByAppendingFormat:mutableRequest.URL.query ? @"&%@" : @"?%@", query]];
        }
    } else {

        if (![mutableRequest valueForHTTPHeaderField:@"Content-Type"]) {
            [mutableRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        }
        // #2864: an empty string is a valid x-www-form-urlencoded payload
        if (query) {
            [mutableRequest setHTTPBody:[[query dataUsingEncoding:self.stringEncoding] encodeMobileData]];
        }else if (postData){
            [mutableRequest setHTTPBody:postData];
        }else{
            [mutableRequest setHTTPBody:[@"" dataUsingEncoding:self.stringEncoding]];
        }
    }
    
    return mutableRequest;
}


#pragma mark - NSCopying

- (instancetype)copyWithZone:(NSZone *)zone {
    CIPMApiRequestSeializer *serializer = [super copyWithZone:zone];
    
    return serializer;
}
@end

@interface CIPMApiResponseSerializer ()

@property (nonatomic, assign) NSInteger statusCode;
@property (nonatomic, assign) NSInteger errorCode;
@property (nonatomic, strong, nullable) NSDictionary *responseHeaders;
@property (nonatomic, assign) NSInteger expectedLength;
@property (nonatomic, strong, nullable) id result;
@property (strong, nonatomic, readwrite) NVSimpleMsg *message;
@end


@implementation CIPMApiResponseSerializer

- (nullable id)responseObjectForResponse:(nullable NSURLResponse *)response
                                    data:(nullable NSData *)data
                                   error:(NSError * _Nullable __autoreleasing * __nullable)error{
        [self didReceiveResponse:response];
        NSData *decoData = [self dataForNetData:data error:NULL];
        [self sessionSuccess:decoData];
        if (self.result == nil) {
            NSDictionary *errorInfo = @{
                                        NSLocalizedDescriptionKey: self.message.content ?: @"",
                                        NSLocalizedFailureReasonErrorKey: self.message.title ?: @"",
                                        };
            NSError *err = [[NSError alloc] initWithDomain:@"com.CIPMApi.responseSerializer" code:self.errorCode userInfo:errorInfo];
            *error = err;
        }
    return self.result;
}

- (instancetype)copyWithZone:(NSZone *)zone {
    CIPMApiResponseSerializer *serializer = [super copyWithZone:zone];;
    return serializer;
}

- (void)didReceiveResponse: (NSURLResponse *)response{
    
    NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *)response;
    self.statusCode = [httpResponse statusCode];
    
    self.responseHeaders = [httpResponse allHeaderFields];
    self.expectedLength = [[self.responseHeaders objectForKey:@"Content-Length"] intValue];
    
    
    if(self.statusCode == 401) {
        static NSString *dismissedToken = nil;
        //        NSString *token = [[NVAccountManager sharedInstance] token];TODO:accountManger
        NSString *token = @"token";
        if(token && ![token isEqualToString:dismissedToken]) {
            dismissedToken = token;
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"NVNetworkDismissToken" object:nil];
            });
        }
    }
}

- (nullable NSData *)dataForNetData:(nullable NSData *)data
                              error:(NSError * _Nullable __autoreleasing *)error{
    return [data decodeMobileData];
}
- (void)sessionSuccess:(NSData *)data{
    
    BOOL isMalformed = NO;
    NSData *data2 = data;
    if(self.statusCode / 100 == 2 || self.statusCode / 100 == 4) {
        if(data2) {
            if(self.statusCode / 100 == 2) {
                self.result = [self resultWithData:data2 error:NULL];
                self.message = nil;
                if([self.result isKindOfClass:[NVObject class]] && [(NVObject *)self.result isClassHash:0x909d]) { // SimpleMsg
                    NVSimpleMsg *msg = [[NVSimpleMsg alloc] initWithNVObject:self.result];
                    if(msg.statusCode > 0) {
                        self.statusCode = msg.statusCode;
                        if(self.statusCode / 100 != 2) {
                            self.result = nil;
                            self.message = msg;
                        }
                    }
                }
            } else {
                self.result = nil;
                NVObject *obj = [[NVObject alloc] initWithData:data2];
                self.message = [[NVSimpleMsg alloc] initWithNVObject:obj];
            }
        } else {
            isMalformed = self.statusCode == 200 || self.statusCode == 400;
            self.result = nil;
            self.message =  [self.class malformDataMessage:self.statusCode];
        }
    } else {
        self.result = nil;
        self.message = [[self class] serverErrorMessage:self.statusCode];
    }
    
    int code = 0;
    if(isMalformed) {
        if(__CIPIsText(data)) {
            code = self.statusCode == 200 ? -109 : -111;
        } else {
            code = self.statusCode == 200 ? -108 : -110;
        }
        
    } else {
        code = (int)self.statusCode;
    }
    self.errorCode = code;
    NSString *newToken = [self.responseHeaders objectForKey:@"pragma-newtoken"];
    if([newToken length]) {
        // TODO: NVAccountManager's neoToken
        [[NSUserDefaults standardUserDefaults] setObject:newToken forKey:@"MyNewToken"];
    }
}

- (nullable id)resultWithData:(nonnull NSData *)data error:(NSError * _Nullable __autoreleasing *)error{
    uint8_t *bytes = (uint8_t *)[data bytes];
    size_t length = [data length];
    uint8_t b = length > 0 ? bytes[0] : 0;
    if(b == 'A') {
        NSArray *arr = [NVObject arrayWithData:data];
        return arr;
    } else if(b == 'S' && length > 2) {
        size_t bufLen = ((0xFF & bytes[1]) << 8) | (0xFF & bytes[2]);
        if(bufLen == 0) {
            return @"";
        } else if(2 + bufLen < length) {
            NSString *str = [[NSString alloc] initWithBytes:bytes+3 length:bufLen encoding:NSUTF8StringEncoding];
            return str;
        } else {
            return @"";
        }
    } else {
        NVObject *obj = [[NVObject alloc] initWithData:data];
        return obj;
    }
}

+ (NVSimpleMsg *)malformDataMessage:(NSInteger)statusCode {
    return [[NVSimpleMsg alloc] initWithTitle:@"点小评醉了" content:[self promptForStatusCode:statusCode] flag:0];
}

+ (NVSimpleMsg *)serverErrorMessage:(NSInteger)statusCode {
    return [[NVSimpleMsg alloc] initWithTitle:@"出错了:(" content:[self promptForStatusCode:statusCode] flag:0];
}
+ (NSDictionary *)statusCodeDescriptions {
    static dispatch_once_t onceToken;
    static NSDictionary *mapping;
    dispatch_once(&onceToken, ^{
        mapping = @{
                    @(-152): @"点小评去吃三鲜丸子了",
                    @(-151): @"点小评去吃蒸南瓜了",
                    @(-111): @"点小评去吃烩蟹肉了",
                    @(-110): @"点小评去吃炒蟹肉了",
                    @(-109): @"点小评去吃熘蟹肉了",
                    @(-108): @"点小评去吃锅烧白菜了",
                    @(-107): @"点小评去吃焖黄鳝了",
                    @(-106): @"点小评去吃烩鸭条了",
                    @(-105): @"点小评去吃松花小肚了",
                    @(-104): @"点小评去吃烧子鹅了",
                    @(-103): @"点小评去吃糖醋排骨了",
                    @(-102): @"点小评去吃烧花鸭了",
                    @(-101): @"点小评去吃黄焖鸡了",
                    @(-100): @"点小评去吃香辣五花肉了",
                    @(-250): @"点小评去吃酱牛肉了",
                    @(0): @"点小评去吃四喜丸子了",
                    @(400): @"点小评去吃炒子蟹了",
                    @(401): @"点小评去吃炸子蟹了",
                    @(403): @"点小评去吃水晶肘子了",
                    @(404): @"点小评去吃小肚儿了",
                    @(405): @"点小评去吃红肘子了",
                    @(406): @"点小评去吃白肘子了",
                    @(407): @"点小评去吃冰糖肘子了",
                    @(408): @"点小评去吃蜜蜡肘子了",
                    @(409): @"点小评去吃烧烀肘子了",
                    @(418): @"点小评去吃炒虾仁儿了",
                    @(450): @"点小评去吃炒腰花儿了",
                    @(500): @"点小评去吃清蒸鸡了",
                    @(501): @"点小评去吃酱羊肉了",
                    @(502): @"点小评去吃香酥鸡了",
                    @(503): @"点小评去吃软炸里脊了",
                    @(504): @"点小评去吃什锦豆腐了",
                    @(706): @"点小评去吃樱桃肉了",
                    };
    });
    return mapping;
}

+ (NSString *)promptForStatusCode:(NSInteger)statusCode {
    return [[self statusCodeDescriptions] objectForKey:@(statusCode)] ?: @"点小评去吃满汉全席了";
}

- (int)checkTunnelErrorCode:(NSInteger )errcode type:(CIPSessionType )type{
    
    int code = -105;
    switch (type) {
        case CIPSessionTypeDP:{
            if (errcode / 100 == -1) {
                code = (int)errcode;
            }else{
                code = -105;
            }
        }
            break;
        case CIPSessionTypeWNS:{
            if (errcode / 100 == -2) {
                code = (int)errcode;
            }else{
                code = -105;
            }
        }
            break;
        default:
            code = -105;
            break;
    }
    return code;
}

static const int LEAD_BYTE = 0;

static const int TRAIL_BYTE_1 = 1;

static const int TRAIL_BYTE = 2;

static const int bytesFromUTF8[] =
{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0,
    // trail bytes
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3,
    3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5
};

BOOL __CIPIsText(NSData*data){
    if(![data length])
        return NO;
    int count = 0;
    const int start=0;
    NSUInteger len=data.length;
    int leadByte = 0;
    int length = 0;
    int state = LEAD_BYTE;
    Byte *bytes = (Byte *)[data bytes];
    while (count < start+len) {
		      int aByte =bytes[count]& 0xFF;
		      switch (state) {
                  case LEAD_BYTE:
                      leadByte = aByte;
                      length = bytesFromUTF8[aByte];
                      
                      switch (length) {
                          case 0: // check for ASCII
                              if (leadByte > 0x7F)
                                  return NO;
                              break;
                          case 1:
                              if (leadByte < 0xC2 || leadByte > 0xDF)
                                  return NO;
                              state = TRAIL_BYTE_1;
                              break;
                          case 2:
                              if (leadByte < 0xE0 || leadByte > 0xEF)
                                  return NO;
                              state = TRAIL_BYTE_1;
                              break;
                          case 3:
                              if (leadByte < 0xF0 || leadByte > 0xF4)
                                  return NO;
                              state = TRAIL_BYTE_1;
                              break;
                          default:
                              // too long! Longest valid UTF-8 is 4 bytes (lead + three)
                              // or if < 0 we got a trail byte in the lead byte position
                              return NO;
                      } // switch (length)
                      break;
                      
                  case TRAIL_BYTE_1:
                      if (leadByte == 0xF0 && aByte < 0x90)
                          return false;
                      if (leadByte == 0xF4 && aByte > 0x8F)
                          return false;
                      if (leadByte == 0xE0 && aByte < 0xA0)
                          return false;
                      if (leadByte == 0xED && aByte > 0x9F)
                          return false;
                      // falls through to regular trail-byte test!!
                  case TRAIL_BYTE:
                      if (aByte < 0x80 || aByte > 0xBF)
                          return NO;
                      if (--length == 0) {
                          state = LEAD_BYTE;
                      } else {
                          state = TRAIL_BYTE;
                      }
                      break;
              } // switch (state)
		      count++;
    }
    return YES;
}
@end
