//
//  CIPAppNetAPIClient.m
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/30.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "CIPAppNetAPIClient.h"
#import "CIPAppConfiguration.h"
#import "CIPMApiSerializer.h"

@implementation CIPAppNetAPIClient

+ (nullable instancetype)sharedInstance
{
    static CIPAppNetAPIClient * instance = nil;
    static  dispatch_once_t appNetOnce;
    dispatch_once(&appNetOnce, ^{
        CIPAppConfiguration *configuration = [[CIPAppConfiguration alloc] init];
        configuration.defaultTimeout = 45;
        configuration.isDebug = YES;
        configuration.sessionType = CIPSessionTypeNone;
        configuration.disableStatistics = YES;
//        NSString *baseUrl = @"https://api.app.net/stream/";
        instance = [[CIPAppNetAPIClient alloc] initWithBaseURL:nil sessionConfiguration:configuration];
    });
    
    return instance;
}

- (nullable CIPTask *)postNormal:(nonnull NSString *)urlString
             parameters:(nullable id)parameters
                success:(nullable void (^)(CIPTask * task, id result))success
                failure:(nullable void (^)(CIPTask * task, NSError *error))failure{
    
    CIPTask *task = [self url:urlString
                   parameters:parameters
                       method:CIPHttpMethodTypePost
                      timeout:0
            requestSerializer:[CIPHttpRequestSerializer serializer]
                      success:success
                      failure:failure];
    
    task.responseSerializer = [CIPJSONResponseSerializer serializer];
    return task;
}

- (nullable CIPTask *)getNormal:(nonnull NSString *)urlString
            parameters:(nullable id)parameters
               success:(nullable void (^)(CIPTask * task, id result))success
               failure:(nullable void (^)(CIPTask * task, NSError *error))failure{
    
    CIPTask *task = [self url:urlString
                   parameters:parameters
                       method:CIPHttpMethodTypeGet
                      timeout:0
            requestSerializer:[CIPHttpRequestSerializer serializer]
                      success:success
                      failure:failure];
    
    task.responseSerializer = [CIPJSONResponseSerializer serializer];
    return task;
}

#pragma mark MApi task

- (nullable CIPTask *)getMApi:(nonnull NSString *)urlString
          parameters:(nullable id)parameters
             success:(nullable void (^)(CIPTask * task, id result))success
             failure:(nullable void (^)(CIPTask * task, NSError *error))failure{
    
    CIPTask *task = [self url:urlString
                   parameters:parameters
                       method:CIPHttpMethodTypeGet
                      timeout:0
            requestSerializer:[CIPMApiRequestSeializer serializer]
                      success:success
                      failure:failure];
    task.responseSerializer = [CIPMApiResponseSerializer serializer];
    task.sessionType = CIPSessionTypeDP;
    return task;
}

- (nullable CIPTask *)postMApi:(nonnull NSString *)urlString
           parameters:(nullable id)parameters
              success:(nullable void (^)(CIPTask * task, id result))success
              failure:(nullable void (^)(CIPTask * task, NSError *error))failure{
    
    CIPTask *task = [self url:urlString
                   parameters:parameters
                       method:CIPHttpMethodTypePost
                      timeout:0
            requestSerializer:[CIPMApiRequestSeializer serializer]
                      success:success
                      failure:failure];
    task.responseSerializer = [CIPMApiResponseSerializer serializer];
    task.sessionType = CIPSessionTypeDP;
    return task;
}
@end
