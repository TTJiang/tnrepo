//
//  TNViewController.m
//  CIPNetwork
//
//  Created by CIPNetwork on 03/06/2016.
//  Copyright (c) 2016 CIPNetwork. All rights reserved.
//

#import "TNViewController.h"
#import "CIPTask.h"
#import "CIPAppNetAPIClient.h"
#import "NSData+CIPExt.h"

@interface TNViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) NSMutableArray *taskArray;
@property (nonatomic, strong) UITableView *taskTableView;
@end


@implementation TNViewController

- (id)init{
    if (self = [super init]) {
    }
    return self;
}

- (void)viewDidLoad {
    self.taskArray = [[NSMutableArray alloc] init];
    [self initSubview];
    [super viewDidLoad];
    [self doconnection];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.taskTableView.frame = self.view.bounds;
}

- (void)initSubview{
    self.taskTableView = [[UITableView alloc] init];
    self.taskTableView.dataSource =self;
    self.taskTableView.delegate = self;
    
    [self.view addSubview:self.taskTableView];
}

- (void)doconnection{
    //    for (int i = 0; i <10; i++) {
    [self testNormalGET];
    //    [self testMApiGET1];
    //    [self testMApiGET2];
    [self testMApiPOST1];
    //    [self testBackgroungThread];
    
    [self performSelector:@selector(doconnection) withObject:nil afterDelay:2];
    //    }
}

- (void)testNormalGET{
    NSString *url = @"https://api.app.net/stream/0/posts/stream/global";
    CIPTask *task = [[CIPAppNetAPIClient sharedInstance] getNormal:url parameters:nil success:^(CIPTask *task, id result) {
        //        NSLog(@"=================success,task[%@],data=%@",task,[result class]);
        [self finishedTask:task];
    } failure:^(CIPTask *task, NSError *error) {
        //        NSLog(@"=================failure,task[%@],error=%@",task,error);
        [self finishedTask:task];
    }];
    //    task.cacheType = CIPCacheTypeNormal;
    task.maxRetryTimes = 4;
    if ([NSThread isMainThread]) {
        [task resume];
    }else{
        [task startSyncError:NULL];
    }
    [self addTask:task];
}

- (void)testMApiGET1{
    NSString *url2 = @"http://m.api.dianping.com/appconfig.bin";
    CIPTask *task = [[CIPAppNetAPIClient sharedInstance] getMApi:url2 parameters:nil success:^(CIPTask *task, id result) {
        //        NSLog(@"=================success,task[%@],result=%@",task,[result class]);
        [self finishedTask:task];
    } failure:^(CIPTask *task, NSError *error) {
        //        NSLog(@"=================failure,task[%@],error=%@",task,error);
        [self finishedTask:task];
    }];
    task.cacheType = CIPCacheTypeNormal;
    [task resume];
    [self addTask:task];
}
- (void)testMApiGET2{
    NSLog(@"-------start  -----");
    NSString *url2 = @"http://m.api.dianping.com/common/cityinfo.bin";
    NSDictionary *parameters = @{@"cityid":@(1)};
    CIPTask *task = [[CIPAppNetAPIClient sharedInstance] getMApi:url2
                                                      parameters:parameters
                                                         success:^(CIPTask *task, id result) {
                                                             //        NSLog(@"=================success,task[%@],data=%@",task,[result class]);
                                                             [self finishedTask:task];
                                                         } failure:^(CIPTask *task, NSError *error) {
                                                             //        NSLog(@"=================failure,task[%@],error=%@",task,error);
                                                             [self finishedTask:task];
                                                         }];
    //    task.cacheType = CIPCacheTypeNormal;
    [task resume];
    [self addTask:task];
}

- (void)testMApiPOST1{
    NSString *url2 = @"http://l.api.dianping.com/rgc.bin";
    NSDictionary *parameters = @{@"accuracy":@(0),
                                 @"cityid":@(1),
                                 @"lat":@(31.21587),
                                 @"lng":@(121.41910),
                                 @"wifi":@""};
    NSArray *parametersArray = @[@"accuracy",@(0),
                                 @"cityid",@(1),
                                 @"lat",@(31.21587),
                                 @"lng",@(121.41910),
                                 @"wifi",@""];
    NSMutableString *param = [NSMutableString stringWithCapacity:50];
    [param appendFormat:@"accuracy=%@&", @(0)];
    [param appendFormat:@"cityid=%@&", @(1)];
    [param appendFormat:@"lat=%@&", @(31.21587)];
    [param appendFormat:@"lng=%@&", @(121.41910)];
    [param appendFormat:@"type=%@", @""];
    NSData *parameterData = [[param dataUsingEncoding:NSUTF8StringEncoding] encodeMobileData];
    CIPTask *task = [[CIPAppNetAPIClient sharedInstance] postMApi:url2 parameters:parameterData success:^(CIPTask *task, id result) {
        //        NSLog(@"=================success,task[%@],data=%@",task,[result class]);
        [self finishedTask:task];
    } failure:^(CIPTask *task, NSError *error) {
        //        NSLog(@"=================failure,task[%@],error=%@",task,error);
        [self finishedTask:task];
    }];
    [task resume];
    [self addTask:task];
}

- (void)testBackgroungThread{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self testNormalGET];
        [self testMApiGET1];
        [self testMApiGET2];
        [self testMApiPOST1];
    });
}

- (void)finishedTask:(CIPTask *)task{
    
    @synchronized(self.taskArray) {
        if (task.error == nil) {
            [self.taskArray removeObject:task];
            [self.taskArray addObject:task];
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.taskTableView reloadData];
    });
}

- (void)addTask:(CIPTask *)task{
    @synchronized(self.taskArray) {
        NSMutableArray *array = self.taskArray;
        NSArray *resultArray;
        if ([array count] > 100) {
            NSRange rage;
            rage.location = 0;
            rage.length = 10;
            resultArray = [array subarrayWithRange:rage];
            self.taskArray = [NSMutableArray arrayWithArray:resultArray];
        }
        [self.taskArray insertObject:task atIndex:0];
    }
    if ([NSThread isMainThread]) {
        [self.taskTableView reloadData];
    }else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.taskTableView reloadData];
        });
    }
}

#pragma mark table view delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSMutableArray *array = self.taskArray;
    return [array count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"cellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    NSMutableArray *array ;
    @synchronized(self.taskArray) {
        array = [[NSMutableArray alloc] initWithArray:self.taskArray];
    }
    
    if ([array count] <= indexPath.row) {
        return cell;
    }
    CIPTask *task = array[indexPath.row];
    NSString *text = nil;
    UIColor *color = [UIColor whiteColor];
    if (task.error == nil) {
        if (task.result) {
            color = [UIColor greenColor];
            text = [NSString stringWithFormat:@"success:%@ re%lu",[self commandWithUrl:[task.request.URL absoluteString]],[task.data length]];
        }else{
            text = [NSString stringWithFormat:@"send:%@",[self commandWithUrl:[task.request.URL absoluteString]]];
        }
    }else{
        color = [UIColor redColor];
        text = [NSString stringWithFormat:@"failure!!!:%@",[task.error description]];
    }
    cell.text = text;
    cell.backgroundColor = color;
    return cell;
}

- (NSString *)commandWithUrl:(NSString *)url {
    if(!url)
        return @"";
    NSRange r = [url rangeOfString:@"?"];
    if(r.location == NSNotFound) {
    } else {
        url = [url substringToIndex:r.location];
    }
    r = [url rangeOfString:@"/" options:NSBackwardsSearch];
    if(r.location == NSNotFound) {
        return url;
    } else {
        return [url substringFromIndex:r.location + 1];
    }
}
@end
