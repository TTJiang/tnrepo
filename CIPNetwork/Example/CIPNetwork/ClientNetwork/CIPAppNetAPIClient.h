//
//  CIPAppNetAPIClient.h
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/30.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "CIPNetworkSDK.h"

#define kCount 15
@interface CIPAppNetAPIClient : CIPNetworkSDK
+ (nullable instancetype)sharedInstance;
- (nullable CIPTask *)postNormal:(nonnull NSString *)urlString
                      parameters:(nullable id)parameters
                         success:(nullable void (^)(CIPTask * _Nullable task, id _Nullable result))success
                         failure:(nullable void (^)(CIPTask * _Nullable task, NSError * _Nullable error))failure;

- (nullable CIPTask *)getNormal:(nonnull NSString  *)urlString
                     parameters:(nullable id)parameters
                        success:(nullable void (^)(CIPTask * _Nullable task, id _Nullable result))success
                        failure:(nullable void (^)(CIPTask * _Nullable task, NSError * _Nullable error))failure;
@end
