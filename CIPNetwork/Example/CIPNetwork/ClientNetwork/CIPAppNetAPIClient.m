//
//  CIPAppNetAPIClient.m
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/30.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "CIPAppNetAPIClient.h"
#import "CIPAppConfiguration.h"

@implementation CIPAppNetAPIClient

+ (nullable instancetype)sharedInstance
{
    static CIPAppNetAPIClient * instance = nil;
    static  dispatch_once_t appNetOnce;
    dispatch_once(&appNetOnce, ^{
        CIPAppConfiguration *configuration = [[CIPAppConfiguration alloc] init];
        configuration.defaultTimeout = 45;
        configuration.isDebug = YES;
        configuration.sessionType = CIPSessionTypeNone;
        configuration.disableStatistics = NO;
        NSString *baseUrl = @"http://zhichang.nat123.net:80/";
        instance = [[CIPAppNetAPIClient alloc] initWithBaseURL:baseUrl sessionConfiguration:configuration];
    });
    
    return instance;
}

- (nullable CIPTask *)postNormal:(nonnull NSString *)urlString
             parameters:(nullable id)parameters
                success:(nullable void (^)(CIPTask * task, id result))success
                failure:(nullable void (^)(CIPTask * task, NSError *error))failure{
    
    CIPTask *task = [self url:urlString
                   parameters:parameters
                       method:CIPHttpMethodTypePost
                      timeout:0
            requestSerializer:[CIPJSONRequestSerializer serializer]
                      success:success
                      failure:failure];
    
    task.responseSerializer = [CIPJSONResponseSerializer serializer];
    task.cacheType = CIPCacheTypeDisabled;
    return task;
}

- (nullable CIPTask *)getNormal:(nonnull NSString *)urlString
            parameters:(nullable id)parameters
               success:(nullable void (^)(CIPTask * task, id result))success
               failure:(nullable void (^)(CIPTask * task, NSError *error))failure{
    
    CIPTask *task = [self url:urlString
                   parameters:parameters
                       method:CIPHttpMethodTypeGet
                      timeout:0
            requestSerializer:[CIPJSONRequestSerializer serializer]
                      success:success
                      failure:failure];
    
    task.responseSerializer = [CIPJSONResponseSerializer serializer];
    task.cacheType = CIPCacheTypeDisabled;
    return task;
}
@end
