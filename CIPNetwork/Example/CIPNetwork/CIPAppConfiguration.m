//
//  CIPAppConfiguration.m
//  CIPNetworkDemo
//
//  Created by JiangTeng on 15/12/30.
//  Copyright © 2015年 dianping. All rights reserved.
//

#import "CIPAppConfiguration.h"

@implementation CIPAppConfiguration

- (CIPSessionType)sessionTypeForRequest:(NSURLRequest *)request
{
    return CIPSessionTypeNone;
}

- (nullable NSDictionary *)requestHeaders{
    NSMutableDictionary *mutableDictionary = [[NSMutableDictionary alloc] init];
    
    static NSString *userAgent = @"MApi 1.1 (dpscope 8.0.0 appstore; iPhone 9.2 x86_64; a0d0)";
    [mutableDictionary setObject:@"wifi" forKey:@"network-type"];
    [mutableDictionary setObject:userAgent forKey:@"User-Agent"];
    [mutableDictionary setObject:userAgent forKey:@"pragma-os"];
    [mutableDictionary setObject:[NSBundle mainBundle].bundleIdentifier ?: @"" forKey:@"pragma-apptype"];
    
    NSString *did = @"395262871f16cb062662001768f8e9b1daad10bc";
    if(did) {
        [mutableDictionary setObject:did forKey:@"pragma-device"];
    }
    NSString *dpid = @"-4252934710078942683";
    if(dpid) {
        [mutableDictionary setObject:dpid forKey:@"pragma-dpid"];
    }
    NSString *aid = @"351091731";
    if (aid) {
        [mutableDictionary setObject:aid forKey:@"pragma-appid"];
    }
    return  mutableDictionary;
}

@end
