//
//  TNViewController.m
//  Navigator
//
//  Created by Navigator on 03/06/2016.
//  Copyright (c) 2016 Navigator. All rights reserved.
//

#import "TNViewController.h"
#import "Navigator.h"

@interface TNViewController ()

@end

@implementation TNViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    UIButton *btun = [UIButton buttonWithType:UIButtonTypeContactAdd];
    [btun addTarget:self action:@selector(tapBtn) forControlEvents:UIControlEventTouchUpInside];
    btun.frame = CGRectMake(50, 50, 50, 50);
    [self.view addSubview:btun];
	// Do any additional setup after loading the view, typically from a nib.
}


- (void)tapBtn{
    
    MVURLAction *action;
    action = [[MVURLAction alloc] initWithURL:[NSURL URLWithString:@"ten://two"]];
    [[MVNavigator navigator] openURLAction:action];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
