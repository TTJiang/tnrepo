//
//  main.m
//  Navigator
//
//  Created by Navigator on 03/06/2016.
//  Copyright (c) 2016 Navigator. All rights reserved.
//

@import UIKit;
#import "TNAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TNAppDelegate class]));
    }
}
