//
//  TNTwoViewController.m
//  Navigator
//
//  Created by JiangTeng on 16/3/6.
//  Copyright © 2016年 Navigator. All rights reserved.
//

#import "TNTwoViewController.h"

@interface TNTwoViewController ()

@end

@implementation TNTwoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor redColor]];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    UIImage *image = [UIImage imageNamed:@"ng_detail_topbar_icon_back"];
    imageView.image = image;
    imageView.frame = CGRectMake(50, 50, 50, 50);
    [self.view addSubview:imageView];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
