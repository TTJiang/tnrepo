//
//  TNAppDelegate.h
//  Navigator
//
//  Created by Navigator on 03/06/2016.
//  Copyright (c) 2016 Navigator. All rights reserved.
//
#import "Navigator.h"
@import UIKit;

@interface TNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic, readonly) MVCoreNavigationController *navigationController;
@end
