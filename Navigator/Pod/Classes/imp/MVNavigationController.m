//
//  MVNavigationController.m
//  Pods
//
//  Created by JiangTeng on 15/12/6.
//
//

#import "MVNavigationController.h"
#import <objc/runtime.h>
#import "MVNavigator.h"
#import <QuartzCore/QuartzCore.h>
//#import "NVViewController.h"
#import <objc/runtime.h>
//#import "UIBarButtonItem+Addition.h"
//#import "MVStyle.h"
//#import "EXTScope.h"
//#import "NVLogDiskManager.h"
//#import "NVDefinition.h"
//#import "UIScreen+Adaptive.h"
//#import "MVPopoverView.h"
//#import "NVCompatible.h"
//#import "UIResponder+informer.h"
//#import "RACSignal+Operations.h"

#import "MVPresentTransitionManager.h"
#import "MVPopTransitionManager.h"

#define NAVIG_SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define NAVIG_SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
#define NV_FLIP_TRANSITION_DURATION 0.4
#define naviMargin 15
#define naviButtonWidth 40
#define naviOrgMargin 30

typedef enum {
    CustomViewAnimationTransitionNone,
    CustomViewAnimationTransitionFlipFromLeft,
    CustomViewAnimationTransitionFlipFromRight,
    CustomViewAnimationTransitionCurlUp,
    CustomViewAnimationTransitionCurlDown,
    CustomViewAnimationTransitionFadeIn,
    CustomViewAnimationTransitionMoveIn,
    CustomViewAnimationTransitionPush,
    CustomViewAnimationTransitionReveal
} CustomViewAnimationTransition;

#define CustomViewAnimationSubtypeFromRight kCATransitionFromRight
#define CustomViewAnimationSubtypeFromLeft kCATransitionFromLeft
#define CustomViewAnimationSubtypeFromTop kCATransitionFromTop
#define CustomViewAnimationSubtypeFromBottom kCATransitionFromBottom


@interface MVNavigationController () <UINavigationControllerDelegate>

@property (nonatomic, strong) MVPresentTransitionManager    *presentTransitionManager;
@property (nonatomic, strong) MVPopTransitionManager        *popTransitionManager;

- (MVNavigationStyle)navigationStyleOfViewController:(UIViewController *)viewConroller;
@end

typedef enum {
    ButtonHighLightStyleLight,
    ButtonHighLightStyleDark,
    ButtonHighLightStyleGray
} ButtonHighLightStyle;

@interface NSString (Equal)

+ (BOOL)isString:(NSString *)stringA equalTo:(NSString *)stringB;
@end

@implementation NSString (Equal)

+ (BOOL)isString:(NSString *)stringA equalTo:(NSString *)stringB
{
    if (stringA == nil) {
        return stringB == nil;
    }
    return [stringA isEqualToString:stringB];
}
@end

@interface UIBarButtonItem (Addition)

+ (id)itemWithFlexibleSpace;
+ (id)itemWithFixedSpace;
+ (id)itemWithBarButonSystemItem:(UIBarButtonSystemItem)systemItem target:(id)target action:(SEL)action;

+ (id)backItemWithTarget:(id)target action:(SEL)action;
+ (id)cancelItemWithTarget:(id)target action:(SEL)action;
+ (id)submitItemWithTarget:(id)target action:(SEL)action;

+ (id)leftItemWithIcon:(NSString *)icon target:(id)target action:(SEL)action;
+ (id)leftItemWithText:(NSString *)text target:(id)target action:(SEL)action;
+ (id)leftItemWithText:(NSString *)text withTextColor:(UIColor *)color target:(id)target action:(SEL)action;
+ (id)leftItemWithView:(UIView *)aView;
+ (id)backItemWithTarget:(id)target imageString:(NSString *)imageString action:(SEL)action;


+ (id)rightItemWithIcon:(NSString *)icon target:(id)target action:(SEL)action;
+ (id)rightItemWithText:(NSString *)text target:(id)target action:(SEL)action;
+ (id)rightItemWithText:(NSString *)text withTextColor:(UIColor *)color target:(id)target action:(SEL)action;
+ (id)rightItemWithView:(UIView *)aView;
@end

@interface UIImage (HighLight)

- (UIImage *)highLightedImage:(ButtonHighLightStyle)highLightStyle;

@end

@interface UIButton (HighLight)

- (void)setButtonHighLightStyle:(ButtonHighLightStyle)highLightStyle;

- (void)setImage:(UIImage *)image withHighLightStyle:(ButtonHighLightStyle)highLightStyle;

- (void)setBackgroundImage:(UIImage *)image withHighLightStyle:(ButtonHighLightStyle)highLightStyle;

@end

@interface NSString (height)

-(CGFloat) heightWithFont:(UIFont*) font lineBreakMode:(NSLineBreakMode) mode withWidth:(CGFloat)width;
-(CGSize) nvSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)mode;
-(CGSize) nvSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size;
-(CGSize) nvSizeWithFont:(UIFont *)font forWidth:(CGFloat)width lineBreakMode:(NSLineBreakMode)lineBreakMode;
-(CGSize) nvSizeWithFont:(UIFont *)font;
@end

@implementation UIViewController (NVNavigationController)

@dynamic backTimer;
@dynamic backImage;

static char const * const BackTimerTag = "BackTimerTag";

- (void)setBackTimer:(NSTimer *)backTimer
{
    objc_setAssociatedObject(self, BackTimerTag, backTimer, OBJC_ASSOCIATION_RETAIN);
}

- (NSTimer *)backTimer
{
    NSTimer *timer = objc_getAssociatedObject(self, BackTimerTag);
    return timer;
}

static char const * const BackImageTag = "BackImageTag";

- (void)setBackImage:(UIView *)backImage
{
    objc_setAssociatedObject(self, BackImageTag, backImage, OBJC_ASSOCIATION_RETAIN);
}

- (UIView *)backImage
{
    UIView *image = objc_getAssociatedObject(self, BackImageTag);
    return image;
}

static char const * const EnableDrawViewTag = "EnableDrawViewTag";

- (void)setEnableDrawView:(BOOL)enableDrawView
{
    NSNumber *number = [NSNumber numberWithBool:enableDrawView];
    objc_setAssociatedObject(self, EnableDrawViewTag, number, OBJC_ASSOCIATION_RETAIN);
}

- (BOOL)enableDrawView
{
    NSNumber *number = objc_getAssociatedObject(self, EnableDrawViewTag);
    return [number boolValue];
}

static char const * const ModalGroupTag = "ModalGroupTag";

- (void)setModalGroup:(NSString *)modalGroup
{
    objc_setAssociatedObject(self, ModalGroupTag, modalGroup, OBJC_ASSOCIATION_RETAIN);
}

- (NSString *)modalGroup
{
    NSString *modalGroup = objc_getAssociatedObject(self, ModalGroupTag);
    return modalGroup;
}

static char const * const HideNavigationBarTag = "HideNavigationBarTag";

- (void)setHideNavigationBar:(BOOL)hideNavigationBar
{
    NSNumber *number = [NSNumber numberWithBool:hideNavigationBar];
    objc_setAssociatedObject(self, HideNavigationBarTag, number, OBJC_ASSOCIATION_RETAIN);
}

- (BOOL)hideNavigationBar
{
    NSNumber *number = objc_getAssociatedObject(self, HideNavigationBarTag);
    return [number boolValue];
}

- (BOOL)isDrawerView
{
    if (!self.enableDrawView) {
        return NO;
    }
    if ([self.navigationController isKindOfClass:[MVNavigationController class]]) {
        MVNavigationStyle style = [((MVNavigationController *)self.navigationController) navigationStyleOfViewController:self];
        if (style == MVNavigationStyleSystem) {
            return NO;
        }
    }
    return YES;
}

- (BOOL)canSlideBack
{
    return YES;
}

- (BOOL)isModalView
{
    return NO;
}

- (BOOL)handleWithURLAction:(MVURLAction *)urlAction
{
    return YES;
}

- (MVNavigationStyle)navigationStyle
{
    return MVNavigationStyleIOS7;
}

- (UIBarButtonItem *)backBarButtonItem {
    return [self backBarButtonItemWithCurrenttViewController:nil leftIcon:nil];
}

- (UIBarButtonItem *)backBarButtonItemWithCurrenttViewController:(UIViewController *)curVC leftIcon:(NSString *)lefticnString {
    UIBarButtonItem *btnItem;
    if (![NSString isString:self.modalGroup equalTo:curVC.modalGroup]) {
        btnItem = [UIBarButtonItem cancelItemWithTarget:self action:@selector(backButtonClicked)];
    }
    else {
        btnItem = [UIBarButtonItem backItemWithTarget:self imageString:lefticnString action:@selector(backButtonClicked)];
        UIButton *btn = (UIButton *)btnItem.customView;
        if (btn) {
            //            UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(backToHome)];
            //            [btn addGestureRecognizer:longPressRecognizer];
            [btn addTarget:self action:@selector(startB2ITimer) forControlEvents:UIControlEventTouchDown];
            [btn addTarget:self action:@selector(stopB2ITimer) forControlEvents:UIControlEventTouchUpOutside];
            [btn addTarget:self action:@selector(stopB2ITimer) forControlEvents:UIControlEventTouchDragExit];
        }
    }
    return btnItem;
}

- (void)backButtonClicked
{
    [self stopB2ITimer];
    
    [self backToPreviousViewController];
}

- (void)backToPreviousViewController {
    UINavigationController *naviController = nil;
    if (self.navigationController) {
        naviController = self.navigationController;
    } else {
        naviController = (UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController;
    }
    
    [naviController popViewControllerAnimated:YES];
    
//    [self ga_logEventType:NVGaEventTypeTap forLabel:@"back" userInfo:nil];111
}

- (void)cancelBackToPreviousViewController
{
    UINavigationController *naviController = nil;
    if (self.navigationController) {
        naviController = self.navigationController;
    } else {
        naviController = (UINavigationController *)[UIApplication sharedApplication].delegate.window.rootViewController;
    }
    
    if (![naviController isKindOfClass:[MVNavigationController class]]) {
        return;
    }
    
    MVNavigationController *nvNavController = (MVNavigationController *)naviController;
    [nvNavController cancelPopWithAnimation];
}

- (void)startB2ITimer
{
    self.backTimer = [NSTimer scheduledTimerWithTimeInterval:1.5f target:self selector:@selector(backToHome) userInfo:nil repeats:NO];
}

- (void)stopB2ITimer
{
    [self.backTimer invalidate];
    self.backTimer = nil;
}

- (void)backToHome
{
    if (self.navigationController == [UIApplication sharedApplication].delegate.window.rootViewController) {
        if (self.modalGroup.length > 0) {
            return;
        }
        
        [self cancelBackToPreviousViewController];
        NSString *scheme = [[MVNavigator navigator] handleableURLScheme];
        MVURLAction *action = [MVURLAction actionWithURLString:[NSString stringWithFormat:@"%@://home",scheme]];
        [[MVNavigator navigator] openURLAction:action];
    }
}

- (void)openUrlInNextLoop:(MVURLAction *)action
{
    [[MVNavigator navigator] performSelector:@selector(openURLAction:) withObject:action afterDelay:0];
}

@end

@implementation MVNavigationController
{
    BOOL                                isShowingAnimation;
    UIImageView                         *img_shadow;
    UIImageView                         *_img_shadow_up;
    UIImageView                         *_img_shadow_down;
    UIImageView                         *_img_shadow_right;
    //    UIView                            *_backgroundView;
    UIView                              *_rootViewBackImage;
}

- (id)init
{
    self = [super init];
    if (self) {
        [self customNavigationBar];
        self.interactivePopGestureRecognizer.delegate = self;
        self.nextNavigationStyle = MVNavigationStyleNone;
        self.delegate = self;
        for (UIView *view in self.view.subviews) {
            view.backgroundColor = [UIColor clearColor];
        }
    }
    return self;
}

- (id)initWithRootViewController:(UIViewController *)rootViewController {
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        [self customNavigationBar];
        self.interactivePopGestureRecognizer.delegate = self;
        self.nextNavigationStyle = MVNavigationStyleNone;
        self.delegate = self;
        for (UIView *view in self.view.subviews) {
            view.backgroundColor = [UIColor clearColor];
        }
    }
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////


- (void)coreAnimationWithPushBlock:(void (^)(void))pushBlock {
    // Take screenshot and scale
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, YES, [[UIScreen mainScreen] scale]);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *lastImage = UIGraphicsGetImageFromCurrentImageContext();
    UIImageView * lastView = [[UIImageView alloc] initWithImage:lastImage];
    [[UIApplication sharedApplication].delegate.window addSubview:lastView];
    
    CGRect sf = self.view.frame;
    CGRect vf = self.view.bounds;
    // Present view animated
    self.view.frame = CGRectMake(0, vf.size.height, vf.size.width, vf.size.height);
    
    pushBlock();
    
    [[UIApplication sharedApplication].delegate.window bringSubviewToFront:self.view];
    
    [UIView animateWithDuration:NV_FLIP_TRANSITION_DURATION animations:^{
        self.view.frame = sf;
    } completion:^(BOOL finished) {
        if(finished){
            [lastView removeFromSuperview];
        }
    }];
}

- (void)setNeedPop2Root:(BOOL)needPop2Root
{
    if (needPop2Root) {
        __weak typeof(self) weakSelf = self;
        self.preAction = ^NSArray *{
            __strong typeof(weakSelf) strongSelf = weakSelf;
            return [strongSelf popToRootViewControllerAnimated:NO];
        };
    }
    else {
        self.preAction = NULL;
    }
}

- (void)setPreActionN:(void (^)())preActionN
{
    if (preActionN != NULL) {
        self.preActionV = ^UIView *{
            preActionN();
            return nil;
        };
    }
    else {
        self.preActionV = NULL;
    }
}

- (void)setPreAction:(NSArray *(^)())preAction
{
    if (preAction != NULL) {
        self.preActionV = ^UIView *{
            NSArray *popped = preAction();
            if (popped.count > 0) {
                return ((UIViewController *)popped.firstObject).backImage;
            }
            return nil;
        };
    }
    else {
        self.preActionV = NULL;
    }
}

- (void)checkAndRunPostAction
{
    if (self.postAction != nil) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (self.postAction != nil) {
                self.postAction();
                self.postAction = nil;
            }
        });

//        }];
    }
    self.nextNavigationStyle = MVNavigationStyleNone;
}

- (MVNavigationStyle)navigationStyleOfViewController:(UIViewController *)viewConroller
{
    return [self navigationStyleOfViewController:viewConroller withLastViewController:nil];
}

- (MVNavigationStyle)navigationStyleOfViewController:(UIViewController *)viewConroller withLastViewController:(UIViewController *)lastViewController
{
    if (self.nextNavigationStyle) {
        return self.nextNavigationStyle;
    }
    
    if (lastViewController != nil && ![NSString isString:viewConroller.modalGroup equalTo:lastViewController.modalGroup]) {
        return MVNavigationstylePresented;
    }
    return viewConroller.navigationStyle;
}

- (void)processNavigationBar:(UIViewController *)curVC withTarVC:(UIViewController *)tarVC animated:(BOOL)animated
{
    if (curVC.hideNavigationBar && !tarVC.hideNavigationBar) {
        [self setNavigationBarHidden:NO animated:animated];
    }
    else if (!curVC.hideNavigationBar && tarVC.hideNavigationBar) {
        [self setNavigationBarHidden:YES animated:animated];
    }
}

- (void)processNavigationBar:(UIViewController *)curVC withTarVC:(UIViewController *)tarVC
{
    [self processNavigationBar:curVC withTarVC:tarVC animated:YES];
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (![NSThread isMainThread]) {
        NSLog(@"Cannot push a view controller (%@) in the background thread.", viewController);
        return;
    }
    
    if (isShowingAnimation) {
        
        __weak typeof(self) __weakSelf = self;
        if (self.postAction) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                __strong typeof(__weakSelf) __strongSelf = __weakSelf;
                [__strongSelf pushViewController:viewController animated:animated];
            });
        }else{
            self.postAction = ^(){
                __strong typeof(__weakSelf) __strongSelf = __weakSelf;
                [__strongSelf pushViewController:viewController animated:animated];
            };
        }
//        TODO:ten
//        @weakify(self);
//        if (self.postAction) {
//            [[[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
//                [subscriber sendCompleted];
//                return nil;
//            }] delay:.3] subscribeCompleted:^ {
//                @strongify(self)
//                [self pushViewController:viewController animated:animated];
//            }];
//        }
//        else {
//            self.postAction = ^(){
//                @strongify(self);
//                [self pushViewController:viewController animated:animated];
//            };
//        }
        return;
    }
    
    UIViewController *lastViewController = self.topViewController;
    // 提供默认的返回按键样式
    if (self.viewControllers.count > 0 && viewController.navigationItem.leftBarButtonItem == nil) {
        //        if (IPHONE_OS_7()) {
        //            self.topViewController.navigationItem.backBarButtonItem = [viewController backBarButtonItem];
        //        } else {
        [[viewController navigationItem] setLeftBarButtonItem:[viewController backBarButtonItemWithCurrenttViewController:lastViewController leftIcon:self.ngleftimagestring]];
        //        }
        
    }
    
    UIView *curView = [self view];
    [curView endEditing:YES];
    UIViewController *curVC = self.topViewController;
    
    BOOL modelViewAnimation = [viewController isModalView];
    
    if (animated) {
        if (modelViewAnimation) {
            
            [self coreAnimationWithPushBlock:^{
                [super pushViewController:viewController animated:NO];
            }];
            
            [self recordTopViewControllerWithOperation:@"PUSHMOD"];
            return;
        }
        else if ([viewController isDrawerView] && [self.viewControllers count] > 0) {
            [self initDrawerView:viewController];
            _dDelegate = viewController;
            
            if (_preActionV != NULL) {
                UIView *lastImg = _preActionV();
                _rootViewBackImage = lastImg;
                curVC = self.topViewController;
            }
            [self initBackImage:viewController.backImage];
            
            if (_imageView.subviews.count == 0) {
                [super pushViewController:viewController animated:YES];
                [self recordTopViewControllerWithOperation:@"PUSH"];
                return;
            }
            else {
                if (_dDelegate && [_dDelegate respondsToSelector:@selector(drawerAnimationWillShow:)]) {
                    [_dDelegate drawerAnimationWillShow:self];
                }
                isShowingAnimation = YES;
                [self processNavigationBar:curVC withTarVC:viewController animated:NO];
                switch ([self navigationStyleOfViewController:viewController withLastViewController:lastViewController]) {
                    case MVNavigationStyleIOS7:
                    case MVNavigationStyleDrawer:
                        [_imageView removeFromSuperview];
                        [self.view.superview insertSubview:_imageView belowSubview:self.view];
                        [self removeRightShadow];
                        [self addLeftShadow2View:curView];
                        [curView setTransform:CGAffineTransformMakeTranslation(NAVIG_SCREEN_WIDTH, 0)];
                        break;
                    case MVNavigationStyleCascade:
                        [_imageView removeFromSuperview];
                        [self.view.superview addSubview:_imageView];
                        [self removeLeftShadow];
                        [self addRightShadow];
                        break;
                    case MVNavigationStyleIOS7Pop:
                        [curView setTransform:CGAffineTransformMakeTranslation(-NAVIG_SCREEN_WIDTH / 2.f, 0)];
                        [_imageView removeFromSuperview];
                        [self.view.superview addSubview:_imageView];
                        [self addLeftShadow2View:_imageView];
                        [self removeRightShadow];
                        break;
                    case MVNavigationstylePresented:
                        [_imageView removeFromSuperview];
                        [self.view.superview insertSubview:_imageView belowSubview:self.view];
                        [curView setTransform:CGAffineTransformMakeTranslation(0, NAVIG_SCREEN_HEIGHT)];
                        
                        break;
                        
                    default:
                        break;
                }
                [super pushViewController:viewController animated:NO];
                MVNavigationStyle curNavigationStyle = [self navigationStyleOfViewController:viewController withLastViewController:lastViewController];
                self.nextNavigationStyle = MVNavigationStyleNone;
                [UIView animateWithDuration:0.25
                                      delay:0
                                    options:UIViewAnimationOptionCurveEaseInOut
                                 animations:^(void) {
                                     switch (curNavigationStyle) {
                                         case MVNavigationStyleIOS7:
                                             [_imageView setTransform:CGAffineTransformMakeTranslation(-NAVIG_SCREEN_WIDTH / 2.f, 0)];
                                             [curView setTransform:CGAffineTransformMakeTranslation(0, 0)];
                                             break;
                                         case MVNavigationStyleDrawer:
                                             [_imageView setTransform:CGAffineTransformMakeScale(0.95, 0.95)];
                                             _imageView.alpha = 0.6;
                                             [curView setTransform:CGAffineTransformMakeTranslation(0, 0)];
                                             break;
                                         case MVNavigationStyleCascade:
                                             [_imageView setTransform:CGAffineTransformConcat(CGAffineTransformMakeScale(0.75, 0.75), CGAffineTransformMakeTranslation(-(NAVIG_SCREEN_WIDTH * 3.f / 4.f), 0))];
                                             break;
                                         case MVNavigationStyleIOS7Pop:
                                             [_imageView setTransform:CGAffineTransformMakeTranslation(NAVIG_SCREEN_WIDTH, 0)];
                                             [curView setTransform:CGAffineTransformIdentity];
                                             break;
                                         case MVNavigationstylePresented:
                                             [curView setTransform:CGAffineTransformMakeTranslation(0, 0)];
                                             break;
                                             
                                         default:
                                             break;
                                     }
                                 }
                                 completion:^(BOOL finish) {
                                     isShowingAnimation = NO;
                                     [self removeLeftShadow];
                                     switch (curNavigationStyle) {
                                         case MVNavigationStyleIOS7Pop:
                                             [_imageView removeFromSuperview];
                                             [self.view.superview insertSubview:_imageView belowSubview:self.view];
                                             [_imageView setTransform:CGAffineTransformIdentity];
                                             break;
                                             
                                         default:
                                             break;
                                     }
                                     if (_preActionV != NULL && _rootViewBackImage) {
                                         self.topViewController.backImage = _rootViewBackImage;
                                         [self updateBackImage:_rootViewBackImage];
                                         _rootViewBackImage = nil;
                                     }
                                     _preActionV = NULL;
                                     [self checkAndRunPostAction];
                                     if (_dDelegate && [_dDelegate respondsToSelector:@selector(drawerAnimationDidEnd:)]) {
                                         [_dDelegate drawerAnimationDidEnd:self];
                                     }
                                     [self recordTopViewControllerWithOperation:@"PUSH"];
                                 }
                 ];
            }
            return;
        }
    }
    if (!modelViewAnimation && [viewController isDrawerView] && [self.viewControllers count] > 0) {
        [self initDrawerView:viewController];
        [self initBackImage:viewController.backImage];
        _dDelegate = viewController;
    }
    
    if (_preActionV != NULL) {
        UIView *lastImg = _preActionV();
        _rootViewBackImage = lastImg;
    }
    
    [super pushViewController:viewController animated:animated];
    
    if (_preActionV != NULL && _rootViewBackImage) {
        self.topViewController.backImage = _rootViewBackImage;
        [self updateBackImage:_rootViewBackImage];
        _rootViewBackImage = nil;
    }
    _preActionV = NULL;
    
    [self checkAndRunPostAction];
    
    [self processNavigationBar:curVC withTarVC:viewController];
    [self recordTopViewControllerWithOperation:@"PUSH"];
}

- (id)coreAnimationWithPopBlock:(id (^)(void))popBlock {
    // Take screenshot and scale
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, YES, [[UIScreen mainScreen] scale]);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *lastImage = UIGraphicsGetImageFromCurrentImageContext();
    UIImageView * lastView = [[UIImageView alloc] initWithImage:lastImage];
    CGFloat statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
    lastView.contentMode = UIViewContentModeBottom;
    lastView.frame = CGRectMake(0, statusBarHeight, CGRectGetWidth(lastView.frame), CGRectGetHeight(lastView.frame)-statusBarHeight);
    lastView.clipsToBounds = YES;
    [[UIApplication sharedApplication].delegate.window addSubview:lastView];
    
    id result = popBlock();
    
    [UIView animateWithDuration:NV_FLIP_TRANSITION_DURATION animations:^{
        lastView.frame = CGRectMake(0, CGRectGetHeight(lastView.frame)+statusBarHeight, CGRectGetWidth(lastView.frame), CGRectGetHeight(lastView.frame));
    } completion:^(BOOL finished) {
        if(finished){
            [lastView removeFromSuperview];
        }
    }];
    return result;
}

- (void)preparePopAnimation:(UIView *)image
{
    if (image != nil) {
        [self updateBackImage:image];
    }
    UIViewController *viewController = [self topViewController];
    UIViewController *lastViewController = self.viewControllers.count > 1 ? [self.viewControllers objectAtIndex:self.viewControllers.count - 2] : nil;
    switch ([self navigationStyleOfViewController:viewController withLastViewController:lastViewController]) {
        case MVNavigationStyleIOS7: {
            if (CGAffineTransformIsIdentity(_imageView.transform)) {
                [_imageView setTransform:CGAffineTransformMakeTranslation(-NAVIG_SCREEN_WIDTH / 2.f, 0)];
            }
            [_imageView removeFromSuperview];
            [self.view.superview insertSubview:_imageView belowSubview:self.view];
            [self addLeftShadow2View:self.view];
            break;
        }
        case MVNavigationStyleDrawer: {
            if (CGAffineTransformIsIdentity(_imageView.transform)) {
                _imageView.alpha = 0.6;
                [_imageView setTransform:CGAffineTransformMakeScale(0.95, 0.95)];
            }
            [_imageView removeFromSuperview];
            [self.view.superview insertSubview:_imageView belowSubview:self.view];
            [self addLeftShadow2View:self.view];
            break;
        }
        case MVNavigationStyleCascade:
            [_imageView removeFromSuperview];
            [self.view.superview addSubview:_imageView];
            [self addRightShadow];
            break;
        case MVNavigationStyleIOS7Pop:
            [_imageView setTransform:CGAffineTransformMakeTranslation(-NAVIG_SCREEN_WIDTH / 2.f, 0)];
            [_imageView removeFromSuperview];
            [self.view.superview insertSubview:_imageView belowSubview:self.view];
            [self addLeftShadow2View:self.view];
        case MVNavigationstylePresented:
            [_imageView setTransform:CGAffineTransformIdentity];
            [_imageView removeFromSuperview];
            [self.view.superview insertSubview:_imageView belowSubview:self.view];
            break;
            
        default:
            break;
    }
}

- (UIViewController*)popViewControllerAnimated:(BOOL)animated {
    if ([self presentedViewController] && ![[self presentedViewController] isKindOfClass:[UIAlertController class]]) {
        // 模态情况下禁止返回
        return nil;
    }
    
    [self updateBackImage:self.topViewController.backImage];
    
    [self.topViewController.view endEditing:YES];
//    [MVPopoverView dismissPopoverViewAnimated:NO];
    
    UIViewController *returnController = nil;
    if (animated) {
        UIViewController *viewController = self.topViewController;
        
        BOOL modelViewAnimation = [viewController isModalView];
        if (modelViewAnimation) {
            returnController = [self coreAnimationWithPopBlock:^id{
                return [super popViewControllerAnimated:NO];
            }];
            
            [self recordTopViewControllerWithOperation:@"POPMOD"];
            return returnController;
        }
    }
    
    if (animated && [self.topViewController isDrawerView]) {
        [self preparePopAnimation:nil];
        if (_imageView.subviews.count > 0) {
            returnController = self.topViewController;
            [self continuePopWithAnimation];
            
            [self recordTopViewControllerWithOperation:@"POP"];
            return returnController;
        }
    }
    returnController = [super popViewControllerAnimated:animated];
    if (self.topViewController.backImage) {
        [self updateBackImage:self.topViewController.backImage];
    }
    [self checkAndRunPostAction];
    [self processNavigationBar:returnController withTarVC:self.topViewController];
    
    [self recordTopViewControllerWithOperation:@"POP"];
    return returnController;
}


- (NSArray *)popToViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if ([self presentedViewController] && ![[self presentedViewController] isKindOfClass:[UIAlertController class]]) {
        // 模态情况下禁止返回
        return nil;
    }
    
    [self.topViewController.view endEditing:YES];
//    TODO:Ten
//    [NVPopoverView dismissPopoverViewAnimated:NO];
    
    NSArray *returnControllers = nil;
    if (animated) {
        if ([self.viewControllers containsObject:viewController]) {
            NSInteger index = [self.viewControllers indexOfObject:viewController];
            NSArray *subViewControllers = [self.viewControllers subarrayWithRange:NSMakeRange(index, self.viewControllers.count-index-1)];
            for (UIViewController *controller in subViewControllers) {
                if ([controller respondsToSelector:@selector(isModalView)] && [controller isModalView]) {
                    // 一旦返回的ViewController中有模态页面，则返回动画采用模态动画
                    returnControllers = [self coreAnimationWithPopBlock:^id{
                        return [super popToViewController:viewController animated:NO];
                    }];
                    
                    [self recordTopViewControllerWithOperation:@"POPMODTO"];
                    return returnControllers;
                }
            }
        }
    }
    
    NSAssert([self.viewControllers containsObject:viewController], @"Can't pop to a viewController (%@) that doesn't exist", [NSString stringWithUTF8String:object_getClassName(viewController)]);
    
    if (animated && [self.topViewController isDrawerView]) {
        UIView *image = nil;
        NSMutableArray *popedVC = [[NSMutableArray alloc] init];
        for (NSInteger i = self.viewControllers.count - 1; i >= 0; i--) {
            if (self.viewControllers[i] != viewController) {
                [popedVC insertObject:self.viewControllers[i] atIndex:0];
            }
            else {
                image = ((UIViewController *)[popedVC firstObject]).backImage;
                break;
            }
        }
        returnControllers = [NSArray arrayWithArray:popedVC];
        [self preparePopAnimation:image];
        if (_imageView.subviews.count > 0) {
            [self continuePopWithAnimation:^(BOOL animated){
                [super popToViewController:viewController animated:animated];
            }];
            
            [self recordTopViewControllerWithOperation:@"POPTO"];
            return returnControllers;
        }
    }
    returnControllers = [super popToViewController:viewController animated:animated];
    if (self.topViewController.backImage) {
        [self updateBackImage:self.topViewController.backImage];
    }
    [self checkAndRunPostAction];
    [self processNavigationBar:returnControllers.lastObject withTarVC:self.topViewController];
    
    [self recordTopViewControllerWithOperation:@"POPTO"];
    return returnControllers;
}

- (NSArray *)popToRootViewControllerAnimated:(BOOL)animated {
    if ([self presentedViewController] && ![[self presentedViewController] isKindOfClass:[UIAlertController class]]) {
        // 模态情况下禁止返回
        return nil;
    }
    
    [self.topViewController.view endEditing:YES];
//    TODO:Ten
//    [NVPopoverView dismissPopoverViewAnimated:NO];
    
    NSArray *returnControllers = nil;
    if (animated) {
        for (UIViewController *controller in self.viewControllers) {
            if ([controller respondsToSelector:@selector(isModalView)] && [controller isModalView]) {
                // 一旦返回的ViewController中有模态页面，则返回动画采用模态动画
                returnControllers = [self coreAnimationWithPopBlock:^id{
                    return [super popToRootViewControllerAnimated:NO];
                }];
                
                [self recordTopViewControllerWithOperation:@"POPMODROOT"];
                return returnControllers;
            }
        }
    }
    
    if (animated && [self.topViewController isDrawerView]) {
        UIView *image = nil;
        NSMutableArray *popedVC = [[NSMutableArray alloc] init];
        for (NSInteger i = self.viewControllers.count - 1; i > 0; i--) {
            [popedVC insertObject:self.viewControllers[i] atIndex:0];
        }
        returnControllers = [NSArray arrayWithArray:popedVC];
        image = ((UIViewController *)self.viewControllers.firstObject).backImage;
        if (image == nil) {
            image = ((UIViewController *)[popedVC firstObject]).backImage;
        }
        [self preparePopAnimation:image];
        if (_imageView.subviews.count > 0) {
            [self continuePopWithAnimation:^(BOOL animated){
                [super popToRootViewControllerAnimated:animated];
            }];
            
            [self recordTopViewControllerWithOperation:@"POPROOT"];
            return returnControllers;
        }
    }
    returnControllers = [super popToRootViewControllerAnimated:animated];
    [self checkAndRunPostAction];
    [self processNavigationBar:returnControllers.lastObject withTarVC:self.topViewController];
    
    [self recordTopViewControllerWithOperation:@"POPROOT"];
    return returnControllers;
}

- (void)recordTopViewControllerWithOperation:(NSString *)op {
    /*******************
     解耦移植暂时去掉以下代码
     *******************/
    //	ksRecordOperation([NSString stringWithFormat:@"[%@] %@", op, [self.topViewController objectDiscription]]);
}

- (void)setViewControllers:(NSArray *)viewControllers animated:(BOOL)animated
{
    if (isShowingAnimation) {
        
        __weak typeof(self) __weakSelf = self;
        if (self.postAction) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                __strong typeof(__weakSelf) __strongSelf = __weakSelf;
                [__strongSelf setViewControllers:viewControllers animated:animated];
            });
        }else{
            self.postAction = ^(){
                __strong typeof(__weakSelf) __strongSelf = __weakSelf;
                [__strongSelf setViewControllers:viewControllers animated:animated];
            };
        }
//        TODO: Ten
//        @weakify(self);
//        if (self.postAction) {
//            [[[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
//                [subscriber sendCompleted];
//                return nil;
//            }] delay:.3] subscribeCompleted:^ {
//                @strongify(self)
//                [self setViewControllers:viewControllers animated:animated];
//            }];
//        }
//        else {
//            self.postAction = ^(){
//                @strongify(self);
//                [self setViewControllers:viewControllers animated:animated];
//            };
//        }
        return;
    }
//    TODO:Ten
//    [MVPopoverView dismissPopoverViewAnimated:NO];
    
    NSArray *oldArray = self.viewControllers;
    for (NSInteger i = viewControllers.count - 1; i > 0; i--) {
        UIViewController *curVC = viewControllers[i];
        UIViewController *preVC = self.viewControllers[i - 1];
        NSInteger index = [oldArray indexOfObject:preVC];
        if (index >= 0 && index < oldArray.count - 1) {
            UIViewController *lastVC = oldArray[index + 1];
            curVC.backImage = lastVC.backImage;
        }
    }
    
    UIViewController *curLastVC = [viewControllers lastObject];
    UIViewController *oldLastVC = [oldArray lastObject];
    
    if (animated && [curLastVC isDrawerView]) {
        [self initDrawerView:oldLastVC];
        [self initBackImage:oldLastVC.backImage];
        
        UIView *curView = [self view];
        if (oldLastVC.backImage != nil) {
            if (_dDelegate && [_dDelegate respondsToSelector:@selector(drawerAnimationWillShow:)]) {
                [_dDelegate drawerAnimationWillShow:self];
            }
            isShowingAnimation = YES;
            [self processNavigationBar:oldLastVC withTarVC:curLastVC animated:NO];
            switch ([self navigationStyleOfViewController:curLastVC]) {
                case MVNavigationStyleIOS7:
                case MVNavigationStyleDrawer:
                    [_imageView removeFromSuperview];
                    [self.view.superview insertSubview:_imageView belowSubview:curView];
                    [self removeRightShadow];
                    [curView setTransform:CGAffineTransformMakeTranslation(NAVIG_SCREEN_WIDTH, 0)];
                    break;
                case MVNavigationStyleCascade:
                    [_imageView removeFromSuperview];
                    [self.view.superview addSubview:_imageView];
                    [self addRightShadow];
                    break;
                case MVNavigationStyleIOS7Pop:
                    [curView setTransform:CGAffineTransformMakeTranslation(-NAVIG_SCREEN_WIDTH / 2.f, 0)];
                    [_imageView removeFromSuperview];
                    [self.view.superview addSubview:_imageView];
                    [self addLeftShadow2View:_imageView];
                    break;
                case MVNavigationstylePresented:
                    [_imageView removeFromSuperview];
                    [self.view.superview insertSubview:_imageView belowSubview:curView];
                    [curView setTransform:CGAffineTransformMakeTranslation(0, NAVIG_SCREEN_HEIGHT)];
                    break;
                    
                default:
                    break;
            }
            [super setViewControllers:viewControllers animated:NO];
            _dDelegate = viewControllers.lastObject;
            [UIView animateWithDuration:0.25
                                  delay:0
                                options:UIViewAnimationOptionCurveEaseInOut
                             animations:^(void) {
                                 switch ([self navigationStyleOfViewController:curLastVC]) {
                                     case MVNavigationStyleIOS7:
                                         [_imageView setTransform:CGAffineTransformMakeTranslation(-NAVIG_SCREEN_WIDTH / 2.f, 0)];
                                         [curView setTransform:CGAffineTransformMakeTranslation(0, 0)];
                                         break;
                                     case MVNavigationStyleDrawer:
                                         [_imageView setTransform:CGAffineTransformMakeScale(0.95, 0.95)];
                                         _imageView.alpha = 0.6;
                                         [curView setTransform:CGAffineTransformMakeTranslation(0, 0)];
                                         break;
                                     case MVNavigationStyleCascade:
                                         [_imageView setTransform:CGAffineTransformConcat(CGAffineTransformMakeScale(0.75, 0.75), CGAffineTransformMakeTranslation(-(NAVIG_SCREEN_WIDTH * 3.f / 4.f), 0))];
                                         break;
                                     case MVNavigationStyleIOS7Pop:
                                         [_imageView setTransform:CGAffineTransformMakeTranslation(NAVIG_SCREEN_WIDTH, 0)];
                                         [curView setTransform:CGAffineTransformIdentity];
                                         break;
                                     case MVNavigationstylePresented:
                                         [curView setTransform:CGAffineTransformMakeTranslation(0, 0)];
                                         break;
                                         
                                     default:
                                         break;
                                 }
                             }
                             completion:^(BOOL finish) {
                                 //                                 curLastVC.backImage = lastImage;
                                 //                                 [self initBackImage:curLastVC.backImage];
                                 [self updateBackImage:self.topViewController.backImage];
                                 isShowingAnimation = NO;
                                 [self checkAndRunPostAction];
                                 if (_dDelegate && [_dDelegate respondsToSelector:@selector(drawerAnimationDidEnd:)]) {
                                     [_dDelegate drawerAnimationDidEnd:self];
                                 }
                                 [self recordTopViewControllerWithOperation:@"SETVC"];
                             }
             ];
            return;
        }
    }
    else {
        [self initBackImage:curLastVC.backImage];
    }
    [super setViewControllers:viewControllers animated:animated];
    if (self.topViewController.backImage) {
        [self updateBackImage:self.topViewController.backImage];
    }
    [self checkAndRunPostAction];
    [self processNavigationBar:oldLastVC withTarVC:curLastVC animated:YES];
    [self recordTopViewControllerWithOperation:@"SETVC"];
}

- (void)dismissModalGroupAnimated:(BOOL)animated completion:(void (^)(void))completion
{
    if (self.topViewController.modalGroup.length == 0) {
        return;
    }
    
    self.postAction = completion;
    self.nextNavigationStyle = MVNavigationstylePresented;
    NSString *curModalGroup = self.topViewController.modalGroup;
    for (NSInteger i = self.viewControllers.count - 1; i >= 0; i--) {
        UIViewController *vc = [self.viewControllers objectAtIndex:i];
        if (![curModalGroup isEqualToString:vc.modalGroup]) {
            [self popToViewController:vc animated:animated];
            break;
        }
    }
}

- (void)updateBackImage:(UIView *)backImage
{
    for (UIView *subview in _imageView.subviews) {
        [subview removeFromSuperview];
    }
    backImage.frame = _imageView.bounds;
    [_imageView addSubview:backImage];
}

- (void)initDrawerView:(UIViewController *)viewController
{
    //    UIView *curView = [self view];
    //
    //    UIGraphicsBeginImageContextWithOptions(curView.frame.size, NO, 0.0);
    //
    //    if (!IPHONE_OS_7() || [self isWebViewController:viewController]) {
    //        [curView.layer renderInContext:UIGraphicsGetCurrentContext()];
    //    }
    //    else {
    //        [curView drawViewHierarchyInRect:curView.frame afterScreenUpdates:YES];
    //    }
    //    //[curView.layer renderInContext:UIGraphicsGetCurrentContext()];
    //    UIImage *lastViewImage = UIGraphicsGetImageFromCurrentImageContext();
    //    UIGraphicsEndImageContext();
    //    [viewController setBackImage:lastViewImage];
    
    if (self.nextNavigationStyle == MVNavigationStyleSystem) {
        viewController.backImage = nil;
    }
    else {
        UIView *snap = [self.view snapshotViewAfterScreenUpdates:YES];
        [viewController setBackImage:snap];
    }
}

- (void)initBackImage:(UIView *)backImage
{
    if (!_imageView && self.viewControllers.count > 0) {
        _imageView = [[UIView alloc] init];
        _imageView.frame  = self.view.frame;
        _imageView.backgroundColor = [UIColor blackColor];
        backImage.frame = _imageView.frame;
        [_imageView addSubview:backImage];
        [self.view.superview insertSubview:_imageView belowSubview:self.view];
    }
    else {
        [self updateBackImage:backImage];
        [_imageView setTransform:CGAffineTransformMakeScale(1, 1)];
        _imageView.alpha = 1;
        [_imageView setTransform:CGAffineTransformMakeTranslation(0, 0)];
    }
    
    if (self.viewControllers.count == 1) {
        [self.viewControllers[0] setBackImage:backImage];
    }
}

- (void)addLeftShadow2View:(UIView *)superView
{
    if (!img_shadow.superview) {
        CGRect screenFrame = [[UIScreen mainScreen] bounds];
        superView.clipsToBounds = NO;
        [superView addSubview:img_shadow];
        [img_shadow setFrame:CGRectMake(-6 , 0, 6, screenFrame.size.height)];
    }
}

- (void)removeLeftShadow
{
    [img_shadow removeFromSuperview];
}

- (void)addRightShadow
{
    if (_img_shadow_down.superview || _img_shadow_right.superview || _img_shadow_up.superview) {
        return;
    }
    _imageView.clipsToBounds = NO;
    _img_shadow_up.frame = CGRectMake(0, -36, _imageView.frame.size.width, 36);
    [_imageView addSubview:_img_shadow_up];
    _img_shadow_down.frame = CGRectMake(0, _imageView.frame.size.height, _imageView.frame.size.width, 36);
    [_imageView addSubview:_img_shadow_down];
    _img_shadow_right.frame = CGRectMake(_imageView.frame.size.width, -36, 20, _imageView.frame.size.height + 36 * 2);
    [_imageView addSubview:_img_shadow_right];
}

- (void)removeRightShadow
{
    [_img_shadow_down removeFromSuperview];
    [_img_shadow_right removeFromSuperview];
    [_img_shadow_up removeFromSuperview];
}

- (void)HandlePan:(UIPanGestureRecognizer*)panGestureRecognizer{
    UIView *curView = [self view];
    
    CGPoint translation = [panGestureRecognizer translationInView:self.imageView];
    //    NSLog(@"x:%.2f", translation.x);
    
    if ([[self viewControllers] count] > 1) {
        if (translation.x > 0) {
            if (!isShowingAnimation) {
                if (_dDelegate && [_dDelegate respondsToSelector:@selector(drawerAnimationWillShow:)]) {
                    [_dDelegate drawerAnimationWillShow:self];
                }
                isShowingAnimation = YES;
                //                curView.layer.shadowOffset = CGSizeMake(-4, 0);
                //                curView.layer.shadowColor = [[UIColor blackColor] CGColor];
                //                curView.layer.shadowOpacity = 0.5;
                [self addLeftShadow2View:curView];
            }
            
            UIViewController *viewController = [self topViewController];
            UIViewController *lastViewController = self.viewControllers.count > 1 ? [self.viewControllers objectAtIndex:self.viewControllers.count - 2] : nil;
            switch ([self navigationStyleOfViewController:viewController withLastViewController:lastViewController]) {
                case MVNavigationStyleIOS7: {
                    double translatedX = translation.x / 2.0f - NAVIG_SCREEN_WIDTH / 2.f;
                    [_imageView setTransform:CGAffineTransformMakeTranslation(translatedX, 0)];
                    [curView setTransform:CGAffineTransformMakeTranslation(translation.x, 0)];
                    break;
                }
                case MVNavigationStyleDrawer: {
                    double scale = MIN(1.0f, 0.95 + translation.x / 4000);
                    [_imageView setTransform:CGAffineTransformMakeScale(scale, scale)];
                    double alpha = MIN(1.0f, 0.6 + translation.x / 500);
                    _imageView.alpha = alpha;
                    [curView setTransform:CGAffineTransformMakeTranslation(translation.x, 0)];
                    break;
                }
                case MVNavigationStyleCascade: {
                    double scale = MIN(1.0f, 0.75f + translation.x / NAVIG_SCREEN_WIDTH * 0.25f);
                    [_imageView setTransform:CGAffineTransformConcat(CGAffineTransformMakeScale(scale, scale), CGAffineTransformMakeTranslation(translation.x - NAVIG_SCREEN_WIDTH + 80, 0))];
                    break;
                }
                    
                default:
                    break;
            }
        }
        if (panGestureRecognizer.state == UIGestureRecognizerStateEnded) {
            if (translation.x > 100) {
                [self.topViewController backToPreviousViewController];
            }else{
                [self cancelPopWithAnimation];
            }
        }
    }
}

- (void)continuePopWithAnimation
{
    [self continuePopWithAnimation:^(BOOL animated) {
        [super popViewControllerAnimated:animated];
    }];
}

- (void)continuePopWithAnimation:(void(^)(BOOL animated))action
{
    UIView *curView = [self view];
    UIViewController *viewController = [self topViewController];
    UIViewController *lastViewController = self.viewControllers.count > 1 ? [self.viewControllers objectAtIndex:self.viewControllers.count - 2] : nil;
    if (_imageView.subviews.count == 0) {
        isShowingAnimation = NO;
        action(YES);
        return;
    }
    if (_dDelegate && [_dDelegate respondsToSelector:@selector(drawerAnimationWillShow:)]) {
        [_dDelegate drawerAnimationWillShow:self];
    }
    isShowingAnimation = YES;
    MVNavigationStyle curNavigationStyle = [self navigationStyleOfViewController:viewController withLastViewController:lastViewController];
    self.nextNavigationStyle = MVNavigationStyleNone;
    [UIView animateWithDuration:0.25
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(void){
                         switch (curNavigationStyle) {
                             case MVNavigationStyleIOS7:
                             case MVNavigationStyleIOS7Pop:
                                 [_imageView setTransform:CGAffineTransformMakeTranslation(0, 0)];
                                 [curView setTransform:CGAffineTransformMakeTranslation(NAVIG_SCREEN_WIDTH, 0)];
                                 break;
                             case MVNavigationStyleDrawer:
                                 _imageView.alpha = 1;
                                 [_imageView setTransform:CGAffineTransformMakeScale(1.0, 1.0)];
                                 [curView setTransform:CGAffineTransformMakeTranslation(NAVIG_SCREEN_WIDTH, 0)];
                                 break;
                             case MVNavigationStyleCascade:
                                 [_imageView setTransform:CGAffineTransformIdentity];
                                 break;
                             case MVNavigationstylePresented:
                                 [curView setTransform:CGAffineTransformMakeTranslation(0,NAVIG_SCREEN_HEIGHT)];
                                 break;
                                 
                             default:
                                 break;
                         }
                     }completion:^(BOOL finish){
                         [curView setTransform:CGAffineTransformMakeTranslation(0, 0)];
                         UIViewController *oldViewController = self.topViewController;
                         action(NO);
                         UIViewController *viewController = [self topViewController];
                         _dDelegate = viewController;
                         [self checkAndRunPostAction];
                         [self processNavigationBar:oldViewController withTarVC:viewController animated:NO];
                         switch ([self navigationStyleOfViewController:viewController withLastViewController:lastViewController]) {
                             case MVNavigationStyleIOS7:
                             case MVNavigationStyleDrawer:
                             case MVNavigationStyleIOS7Pop:
                             case MVNavigationstylePresented:
                                 [_imageView removeFromSuperview];
                                 [self.view.superview insertSubview:_imageView belowSubview:self.view];
                                 [self removeRightShadow];
                                 [self removeLeftShadow];
                                 isShowingAnimation = NO;
                                 [self updateBackImage:viewController.backImage];
                                 if (_dDelegate && [_dDelegate respondsToSelector:@selector(drawerAnimationDidEnd:)]) {
                                     [_dDelegate drawerAnimationDidEnd:self];
                                 }
                                 break;
                             case MVNavigationStyleCascade: {
                                 [_imageView removeFromSuperview];
                                 [self.view.superview addSubview:_imageView];
                                 if (self.viewControllers.count == 1) {
                                     isShowingAnimation = NO;
                                     if (_dDelegate && [_dDelegate respondsToSelector:@selector(drawerAnimationDidEnd:)]) {
                                         [_dDelegate drawerAnimationDidEnd:self];
                                     }
                                     return;
                                 }
                                 [self addRightShadow];
                                 [_imageView setTransform:CGAffineTransformConcat(CGAffineTransformMakeScale(0.67, 0.67), CGAffineTransformMakeTranslation(-NAVIG_SCREEN_WIDTH, 0))];
                                 [UIView animateWithDuration:0.25
                                                       delay:0
                                                     options:UIViewAnimationOptionCurveEaseInOut
                                                  animations:^(void){
                                                      [_imageView setTransform:CGAffineTransformConcat(CGAffineTransformMakeScale(0.75, 0.75), CGAffineTransformMakeTranslation(-(NAVIG_SCREEN_WIDTH * 3.f / 4.f), 0))];
                                                  } completion:^(BOOL finish){
                                                      isShowingAnimation = NO;
                                                      [self updateBackImage:viewController.backImage];
                                                      if (_dDelegate && [_dDelegate respondsToSelector:@selector(drawerAnimationDidEnd:)]) {
                                                          [_dDelegate drawerAnimationDidEnd:self];
                                                      }
                                                  }];
                                 break;
                             }
                                 
                             default:
                                 isShowingAnimation = NO;
                                 break;
                         }
                     }];
}

- (void)cancelPopWithAnimation
{
    UIView *curView = [self view];
    UIViewController *viewController = [self topViewController];
    UIViewController *lastViewController = self.viewControllers.count > 1 ? [self.viewControllers objectAtIndex:self.viewControllers.count - 2] : nil;
    if ([self navigationStyleOfViewController:viewController withLastViewController:lastViewController] != MVNavigationStyleCascade && CGAffineTransformIsIdentity(curView.transform)) {
        return;
    }
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^(void){
                         switch ([self navigationStyleOfViewController:viewController withLastViewController:lastViewController]) {
                             case MVNavigationStyleIOS7:
                             case MVNavigationStyleIOS7Pop:
                                 [_imageView setTransform:CGAffineTransformMakeTranslation(-NAVIG_SCREEN_WIDTH / 2.f, 0)];
                                 [curView setTransform:CGAffineTransformMakeTranslation(0, 0)];
                                 break;
                             case MVNavigationStyleDrawer:
                                 _imageView.alpha = 0.95;
                                 [_imageView setTransform:CGAffineTransformMakeScale(0.95, 0.95)];
                                 [curView setTransform:CGAffineTransformMakeTranslation(0, 0)];
                                 break;
                             case MVNavigationStyleCascade:
                                 [_imageView setTransform:CGAffineTransformConcat(CGAffineTransformMakeScale(0.75, 0.75), CGAffineTransformMakeTranslation(-(NAVIG_SCREEN_WIDTH * 3.f / 4.f), 0))];
                                 break;
                             case MVNavigationstylePresented:
                                 // [_imageView setTransform:CGAffineTransformMakeTranslation(0, 0)];
                                 [curView setTransform:CGAffineTransformMakeTranslation(0, 0)];
                                 break;
                                 
                             default:
                                 break;
                         }
                     }completion:^(BOOL finish){
                         isShowingAnimation = NO;
                         if (_dDelegate && [_dDelegate respondsToSelector:@selector(drawerAnimationDidEnd:)]) {
                             [_dDelegate drawerAnimationDidEnd:self];
                         }
                     }];
}


- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    UIViewController *curVC = self.topViewController;
    UIViewController *lastVC = self.viewControllers.count > 1 ? [self.viewControllers objectAtIndex:self.viewControllers.count - 2] : nil;
    
    if (![curVC canSlideBack] || [self navigationStyleOfViewController:curVC withLastViewController:lastVC] == MVNavigationstylePresented) {
        return NO;
    }
    
    if ([gestureRecognizer isKindOfClass:[UIScreenEdgePanGestureRecognizer class]] && self.viewControllers.count > 1) {
        return YES;
    }
    return NO;
}


- (void)dealloc
{
    if (_imageView) {
        [_imageView removeFromSuperview];
        _imageView = nil;
    }
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                   animationControllerForOperation:(UINavigationControllerOperation)operation
                                                fromViewController:(UIViewController *)fromVC
                                                  toViewController:(UIViewController *)toVC
{
    MVNavigationStyle style = [self navigationStyleOfViewController:(operation == UINavigationControllerOperationPush ? toVC : fromVC) withLastViewController:(operation == UINavigationControllerOperationPush ? fromVC : toVC)];
    switch (style) {
        case MVNavigationstylePresented:
            if (!self.presentTransitionManager) {
                self.presentTransitionManager = [MVPresentTransitionManager new];
            }
            self.presentTransitionManager.operation = operation;
            return self.presentTransitionManager;
            break;
        case MVNavigationStyleIOS7Pop:
            if (operation == UINavigationControllerOperationPop) {
                return nil;
            }
            if (!self.popTransitionManager) {
                self.popTransitionManager = [MVPopTransitionManager new];
            }
            self.popTransitionManager.operation = operation;
            return self.popTransitionManager;
            break;
            
        default:
            break;
    }
    return nil;
}

@end


@implementation UINavigationController (Custom)

- (void)customNavigationBar {
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                          [UIColor colorWithRed:(51 / 255.f) green:(51 / 255.f) blue:(51 / 255.f) alpha:1], NSForegroundColorAttributeName,
                                                          [UIFont fontWithName:@"Helvetica" size:18], NSFontAttributeName,
                                                          nil]];
    [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"ng_navibar_bg"]
                             forBarMetrics:UIBarMetricsDefault];
    [self.navigationBar setShadowImage:[UIImage imageNamed:@"ng_navibar_bg_shadow"]];
}

@end

//private
@implementation UIBarButtonItem (Addition)

//+ (id)itemWithFlexibleSpace {
//    return  [self itemWithBarButonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//}
//
//+ (id)itemWithFixedSpace {
//    return  [self itemWithBarButonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
//}
//
//+ (id)itemWithBarButonSystemItem:(UIBarButtonSystemItem)systemItem target:(id)target action:(SEL)action {
//    return [[self alloc] initWithBarButtonSystemItem:systemItem target:target action:action];
//}

+ (UIButton *)bgButtonWithTarget:(id)target action:(SEL)action isLeft:(BOOL)left {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return button;
}
//
+ (UIButton *)bgTextButtonWithTarget:(id)target action:(SEL)action isLeft:(BOOL)left {
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

+ (id)backItemWithTarget:(id)target imageString:(NSString *)imageString action:(SEL)action {
    NSString *string ;
    if ([imageString length] >0) {
        string = imageString;
    }else{
        string =@"ng_detail_topbar_icon_back";
    }
    return [self leftItemWithIcon:string inBundle:@"LegoBundle" target:target	action:action];
}

+ (id)cancelItemWithTarget:(id)target action:(SEL)action {
    return [self leftItemWithIcon:@"ng_navibar_icon_cross" inBundle:@"LegoBundle" target:target action:action];
}

+ (id)submitItemWithTarget:(id)target action:(SEL)action {
    return [self rightItemWithIcon:@"ng_navibar_icon_tick" inBundle:@"LegoBundle" target:target action:action];
}

+ (id)leftItemWithIcon:(NSString *)icon target:(id)target action:(SEL)action {
    return [self leftItemWithIcon:icon inBundle:nil target:target action:action];
}

+ (id)leftItemWithIcon:(NSString *)icon inBundle:(NSString *)bundle target:(id)target action:(SEL)action {
    UIButton *button = [self bgButtonWithTarget:target action:action isLeft:YES];
    
    UIImage *image;
    if (bundle.length > 0) {
        image = [UIImage imageNamed:icon];
    }
    else {
        image = [UIImage imageNamed:icon];
    }
    [button setImage:image withHighLightStyle:ButtonHighLightStyleLight];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    button.exclusiveTouch = YES;
    
    button.frame = CGRectMake(0, 0, naviButtonWidth, naviButtonWidth);
    UIBarButtonItem *item = [[self alloc] initWithCustomView:button];
    
    return item;
}

+ (id)leftItemWithText:(NSString *)text withTextColor:(UIColor *)color target:(id)target action:(SEL)action {
    UIButton *button = [self bgTextButtonWithTarget:target action:action isLeft:YES];
    UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
    button.titleLabel.font = font;
    button.titleLabel.minimumScaleFactor = 8.f / 15.f;
    
    [button setTitle:text forState:UIControlStateNormal];
    [button setTitleColor:color forState:UIControlStateNormal];
    
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    button.exclusiveTouch = YES;
    
    CGSize size = [text nvSizeWithFont:font constrainedToSize:CGSizeMake(140, 34)];
    button.frame = CGRectMake(0, 0, size.width, size.height);
    UIBarButtonItem *item = [[self alloc] initWithCustomView:button];
    
    return item;
}

+ (id)leftItemWithText:(NSString *)text target:(id)target action:(SEL)action {
    return [self leftItemWithText:text withTextColor:[UIColor colorWithRed:(255 / 255.f) green:(103 / 255.f) blue:(51 / 255.f) alpha:1] target:target action:action];
}

+ (id)leftItemWithView:(UIView *)aView {
    UIBarButtonItem *item = [[self alloc] initWithCustomView:aView];
    
    aView.exclusiveTouch = YES;
    
    return item;
}

+ (id)rightItemWithIcon:(NSString *)icon target:(id)target action:(SEL)action {
    return [self rightItemWithIcon:icon inBundle:nil target:target action:action];
}

+ (id)rightItemWithIcon:(NSString *)icon inBundle:(NSString *)bundle target:(id)target action:(SEL)action {
    UIButton *button = [self bgButtonWithTarget:target action:action isLeft:NO];
    
    UIImage *image;
    if (bundle.length > 0) {
        image = [UIImage imageNamed:icon];
    }
    else {
        image = [UIImage imageNamed:icon];
    }
    [button setImage:image withHighLightStyle:ButtonHighLightStyleLight];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    button.exclusiveTouch = YES;
    
    button.frame = CGRectMake(0, 0, naviButtonWidth, naviButtonWidth);
    UIBarButtonItem *item = [[self alloc] initWithCustomView:button];
    return item;
}

+ (id)rightItemWithText:(NSString *)text withTextColor:(UIColor *)color target:(id)target action:(SEL)action {
    UIButton *button = [self bgTextButtonWithTarget:target action:action isLeft:NO];
    UIFont *font = [UIFont fontWithName:@"Helvetica-Bold" size:15.0];
    button.titleLabel.font = font;
    button.titleLabel.minimumScaleFactor = 8.f / 15.f;
    
    CGSize size = [text nvSizeWithFont:font constrainedToSize:CGSizeMake(140, 34)];
    [button setTitle:text forState:UIControlStateNormal];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    [button setTitleColor:color forState:UIControlStateNormal];
    
    button.exclusiveTouch = YES;
    
    button.frame = CGRectMake(0, 0, size.width, size.height);
    UIBarButtonItem *item = [[self alloc] initWithCustomView:button];
    
    return item;
}

+ (id)rightItemWithText:(NSString *)text target:(id)target action:(SEL)action {
    return [self rightItemWithText:text withTextColor:[UIColor colorWithRed:(255 / 255.f) green:(103 / 255.f) blue:(51 / 255.f) alpha:1] target:target action:action];
}

+ (id)rightItemWithView:(UIView *)aView {
    UIBarButtonItem *item = [[self alloc] initWithCustomView:aView];
    
    aView.exclusiveTouch = YES;
    
    return item;
}

@end

@implementation UIImage (HighLight)
+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
- (UIImage *)highLightedImage:(ButtonHighLightStyle)highLightStyle
{
    UIImage *newImage = nil;
    
    switch (highLightStyle) {
        case ButtonHighLightStyleGray:
            newImage = [UIImage imageWithColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.03f]];
            break;
            
        case ButtonHighLightStyleLight:
        {
            CGFloat brightness = 0.3;
            newImage = [self coverImageWithColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:brightness]];
        }
            break;
            
        case ButtonHighLightStyleDark:
        {
            newImage = [self coverImageWithColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.1]];
        }
            break;
            
        default:
            break;
    }
    
    if (!UIEdgeInsetsEqualToEdgeInsets(self.capInsets, UIEdgeInsetsZero)) {
        return [newImage resizableImageWithCapInsets:self.capInsets];
    }
    return newImage;
}

- (UIImage *)coverImageWithColor:(UIColor *)color
{
    UIGraphicsBeginImageContextWithOptions(self.size, NO, self.scale);
    CGRect imageRect = CGRectMake(0, 0, self.size.width, self.size.height);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    [self drawInRect:imageRect];
    
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextAddRect(context, imageRect);
    CGContextFillPath(context);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
@implementation UIButton (HighLight)

- (void)setButtonHighLightStyle:(ButtonHighLightStyle)highLightStyle
{
    UIImage *img = [self backgroundImageForState:UIControlStateNormal];
    if (img) {
        [self setBackgroundImage:img withHighLightStyle:highLightStyle];
    }
    img = [self imageForState:UIControlStateNormal];
    if (img) {
        [self setImage:img withHighLightStyle:highLightStyle];
    }
}

- (void)setImage:(UIImage *)image withHighLightStyle:(ButtonHighLightStyle)highLightStyle
{
    [self setImage:image forState:UIControlStateNormal];
    [self setImage:[image highLightedImage:highLightStyle] forState:UIControlStateHighlighted];
}

- (void)setBackgroundImage:(UIImage *)image withHighLightStyle:(ButtonHighLightStyle)highLightStyle
{
    [self setBackgroundImage:image forState:UIControlStateNormal];
    [self setBackgroundImage:[image highLightedImage:highLightStyle] forState:UIControlStateHighlighted];
}

@end

@implementation NSString (height)

-(CGFloat) heightWithFont:(UIFont*)font lineBreakMode:(NSLineBreakMode) mode withWidth:(CGFloat)width {
    if(self == nil || [self length] == 0)
        return 0;
    
    CGSize constraint = CGSizeMake(width, 9999.0f);
    CGSize theSize = [self nvSizeWithFont:font constrainedToSize:constraint lineBreakMode:mode];
    return theSize.height;
}

-(CGSize) nvSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)mode
{
    if (!self || self.length == 0) {
        return CGSizeZero;
    }
    NSDictionary * attrs = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    //IOS7 测量字符串问题，需向下取整
    CGSize sbSize = [self boundingRectWithSize:size options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
    sbSize.height = ceilf(sbSize.height);
    sbSize.width = ceilf(sbSize.width);
    return sbSize;
}

-(CGSize) nvSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size
{
    return [self nvSizeWithFont:font constrainedToSize:size lineBreakMode:NSLineBreakByWordWrapping];
}

-(CGSize) nvSizeWithFont:(UIFont *)font forWidth:(CGFloat)width lineBreakMode:(NSLineBreakMode)lineBreakMode
{
    return [self nvSizeWithFont:font constrainedToSize:CGSizeMake(width, MAXFLOAT) lineBreakMode:lineBreakMode];
}

-(CGSize) nvSizeWithFont:(UIFont *)font
{
    return [self nvSizeWithFont:font constrainedToSize:CGSizeMake(MAXFLOAT, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
}

@end