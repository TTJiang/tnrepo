//
//  MVNavigationController.h
//  Pods
//
//  Created by JiangTeng on 15/12/6.
//
//

#import "MVCoreNavigationController.h"
#import "MVURLAction.h"

typedef enum {
    MVNavigationStyleNone,
    MVNavigationStyleIOS7,
    MVNavigationStyleDrawer,
    MVNavigationStyleCascade,
    MVNavigationStyleIOS7Pop,
    MVNavigationstylePresented,
    MVNavigationStyleSystem,
} MVNavigationStyle;

@class MVNavigationController;
@protocol MVDrawerFrameDelegate <NSObject>
@optional
- (void)drawerAnimationWillShow:(MVNavigationController *)navigationController;
- (void)drawerAnimationDidEnd:(MVNavigationController *)navigationController;

@end

@interface MVNavigationController : MVCoreNavigationController<UIGestureRecognizerDelegate>

@property (nonatomic, strong) UIView *imageView;
@property (nonatomic, weak) id<MVDrawerFrameDelegate> dDelegate;
@property (nonatomic, copy) UIView * (^preActionV)();
@property (nonatomic, copy) void (^postAction)();
@property (nonatomic, assign) MVNavigationStyle nextNavigationStyle;
@property (nonatomic, strong) NSString *ngleftimagestring;


- (void)setNeedPop2Root:(BOOL)needPop2Root;
- (void)setPreActionN:(void (^)())preActionN;
- (void)setPreAction:(NSArray *(^)())preAction;

- (void)continuePopWithAnimation;
- (void)cancelPopWithAnimation;

- (void)dismissModalGroupAnimated:(BOOL)animated completion:(void (^)(void))completion;
@end

@interface UINavigationController (Custom)

- (void)customNavigationBar;
@end

@interface UIViewController (NVNavigationController) <MVDrawerFrameDelegate>

@property (nonatomic, strong) UIView    *backImage;
@property (nonatomic, strong) NSTimer   *backTimer;
@property (nonatomic, readwrite) BOOL   enableDrawView;
@property (nonatomic, assign) BOOL      hideNavigationBar;

//模态分组
@property (nonatomic, strong) NSString  *modalGroup;

//是否支持抽屉视图。
- (BOOL)isDrawerView;
//是否允许滑动返回。
- (BOOL)canSlideBack;

- (BOOL)isModalView;

- (BOOL)handleWithURLAction:(MVURLAction *)urlAction;

- (MVNavigationStyle)navigationStyle;

- (UIBarButtonItem *)backBarButtonItem;
- (void)backButtonClicked;
- (void)backToPreviousViewController;
- (void)cancelBackToPreviousViewController;
- (void)backToHome;
- (void)openUrlInNextLoop:(MVURLAction *)action;
@end



