//
//  MVPopTransitionManager.h
//  Pods
//
//  Created by JiangTeng on 15/12/9.
//
//

#import <Foundation/Foundation.h>

@interface MVPopTransitionManager : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) UINavigationControllerOperation   operation;
@end
