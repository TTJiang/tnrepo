//
//  MVPresentTransitionManager.h
//  Pods
//
//  Created by JiangTeng on 15/12/6.
//
//

#import <Foundation/Foundation.h>

@interface MVPresentTransitionManager : NSObject <UIViewControllerAnimatedTransitioning>

@property (nonatomic, assign) UINavigationControllerOperation   operation;
@end
