//
//  MVNavigator.h
//  Pods
//
//  Created by JiangTeng on 15/12/8.
//
//
#import "MVBaseNavigator.h"

@class MVNavigator;
extern void NVInternalSetNavigator(MVNavigator *navigator);

@protocol MVNavigatorDelegate <NSObject>

@optional

/**
 询问是否应该打开urlAction
 @param urlAction 待打开的urlAction
 */
- (BOOL)navigator:(MVNavigator *)navigator shouldOpenURLAction:(MVURLAction *)urlAction;

/**
 将要打开urlAction
 @param urlAction 待打开的urlAction
 */
- (void)navigator:(MVNavigator *)navigator willOpenURLAction:(MVURLAction *)urlAction;

/**
 urlAction将要使用外部程序打开
 @param urlAction 将要外部打开的urlAction
 */
- (void)navigator:(MVNavigator *)navigator willOpenExternal:(MVURLAction *)urlAction;

/**
 遇到了无法处理的urlAction
 @param urlAction 无法处理的urlAction
 */
- (void)navigator:(MVNavigator *)navigator onMatchUnhandledURLAction:(MVURLAction *)urlAction;

/**
 找到了映射的Class
 */
- (void)navigator:(MVNavigator *)navigator onMatchViewController:(UIViewController *)controller withURLAction:(MVURLAction *)urlAction;
@end


@interface MVNavigator : MVBaseNavigator
@property (nonatomic, weak) id <MVNavigatorDelegate> delegate;

/**
 具体由不同的App来定义。
 一般在业务层中需要重载提供自定义的NVNavigator（使用NVInternal.h NVInternalSetNavigator），并且子类必须实现NVNavigator
 */
+ (MVNavigator *)navigator;

/**
 设置程序的主导航控制器
 所有的页面跳转都会在mainNavigationContorller中进行
 */
- (void)setMainNavigationController:(MVCoreNavigationController *)mainNavigationContorller;

/**
 设置可以处理的URL Scheme
 默认是：@"dianping"
 */
- (void)setHandleableURLScheme:(NSString *)scheme;

/**
 设置URL mapping文件名称
 url mapping文件只能是包含在工程项目中的文件
 例如：
 @[@"dpmapping.dat", @"dptuanmapping.dat"]
 */
- (void)setFileNamesOfURLMapping:(NSArray *)fileNames;


/////////////////////////////////////////////////////////////////////////////////////////

/**
 在当前的NVNavigator栈中打开新的URL，
 简写方法是: NVOpenURL(NSURL *url)
 
 例如：
 [[NVNavigator navigator] openURL:[NSURL URLWithString:@"dianping://shop?id=123"]]
 或
 NVOpenURL([NSURL URLWithString:@"dianping://shop?id=123"])
 */
- (UIViewController *)openURL:(NSURL *)url fromViewController:(UIViewController *)controller;

/**
 在当前的NVNavigator栈中打开新的URL，
 简写方法是: NVOpenURLString(NSString *urlString)
 
 例如:
 [[NVNavigator navigator] openURLString:@"dianping://shop?id=123"]
 或
 NVOpenURL(@"dianping://shop?id=123")
 */
- (UIViewController *)openURLString:(NSString *)urlString fromViewController:(UIViewController *)controller;

/**
 在当前的NVNavigator栈中打开新的URL
 简写方法是: NVOpenURLAction(NVURLAction *urlAction)
 
 可以向NVURLAction中传入制定的参数，参数可以为integer, double, string, NVObject四种类型
 bool的参数可以用0和1表示
 如果希望传入任意对象，可以使用setAnyObject:forKey:方法
 
 URL中附带的参数和setXXX:forKey:所设置的参数等价，
 例如下面两种写法是等价的：
 NVURLAction *a = [NVURLAction actionWithURL:@"dianping://shop?id=1"];
 和
 NVURLAction *a = [NVURLAction actionWithURL:@"dianping://shop"];
 [a setInteger:1 forKey:@"id"]
 
 在获取参数时，调用[a integerForKey:@"id"]，返回值均为1
 */
- (UIViewController *)openURLAction:(MVURLAction *)urlAction fromViewController:(UIViewController *)controller;

- (UIViewController *)openURLAction:(MVURLAction *)urlAction;

- (void)popCurrentUrlAnimated:(BOOL)animated;
@end

@interface UIViewController (NVNavigator)
- (UIViewController *)openURLHost:(NSString *)urlHost;
- (UIViewController *)openURL:(NSURL *)url;
- (UIViewController *)openURLString:(NSString *)urlString;
- (UIViewController *)openHttpURLString:(NSString *)httpURLString;
- (UIViewController *)openURLFormat:(NSString *)urlFormat, ...;
- (UIViewController *)openURLAction:(MVURLAction *)urlAction;
- (void)openURLList:(NSArray *)urlList;
@end