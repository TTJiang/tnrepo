//
//  MVBaseNavigator.m
//  Pods
//
//  Created by JiangTeng on 15/12/8.
//
//

#import "MVBaseNavigator.h"
#import "MVURLPattern.h"
#import "MVNavigationController.h"


static MVURLAction *gWait4LoginURLAction = nil;
static __weak UIViewController *gLoginController = nil;

@interface MVCoreNavigationController ()
- (void)pushViewController:(UIViewController *)viewController withAnimation:(BOOL)animated;
@end

@implementation MVBaseNavigator {
    NSTimer *_checkBlockTimer; // 检查堵塞模式消失的事件
    id<MVNavigatorAccountViewControllerProtocal> _accountManager;
}
@synthesize mainNavigationContorller = _mainNavigationContorller;
@synthesize handleableURLScheme = _handleableURLScheme;
@synthesize fileNamesOfURLMapping = _fileNamesOfURLMapping;

- (id)init {
    self = [super init];
    if (self) {
        _urlActionWaitingList = [NSMutableArray array];
        _handleableURLScheme = @"navigator";
    }
    return self;
}

- (BOOL)animating {
    return self.mainNavigationContorller.inAnimating;
}

- (BOOL)inBlockMode {
    return [self.mainNavigationContorller presentedViewController] || self.animating;
}

- (void)checkTimerBlockModeDismiss {
    if (_urlActionWaitingList.count<1) {
        [_checkBlockTimer invalidate];
        _checkBlockTimer = nil;
        return;
    }
    if (![self inBlockMode]) {
        [_checkBlockTimer invalidate];
        _checkBlockTimer = nil;
        [self flush];
    }
}

- (UIViewController *)handleOpenURLAction:(MVURLAction *)urlAction {
    if (!urlAction || !urlAction.url || !_mainNavigationContorller) {
        return nil;
    }
    
    if ([self inBlockMode]) {
        // in block mode, url action will send to waiting list
        [_urlActionWaitingList addObject:urlAction];
        if (!_checkBlockTimer) {
            _checkBlockTimer = [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(checkTimerBlockModeDismiss) userInfo:nil repeats:YES];
        }
        return nil;
    }
    
    if (urlAction.openExternal) {
        [self willOpenExternal:urlAction];
        [[UIApplication sharedApplication] openURL:urlAction.url];
        return nil;
    }
    
    if (![self shouldOpenURLAction:urlAction]) {
        return nil;
    }
    
    [self willOpenURLAction:urlAction];
    
    NSURL *url = urlAction.url;
    NSString *scheme = url.scheme;
    // check unhandleable url scheme
    if (![self.handleableURLScheme caseInsensitiveCompare:scheme] == NSOrderedSame) {
        [self willOpenExternal:urlAction];
        [[UIApplication sharedApplication] openURL:url];
        return nil;
    }
    
    // check unhandled url host
    MVURLPattern *pattern = [self matchPatternWithURLAction:urlAction];
    if (!pattern) {
        [self onMatchUnhandledURLAction:urlAction];
        return nil;
    }
    
    urlAction.urlPattern = pattern;
    // check unhandled class
    UIViewController *controller = [self obtainControllerWithPattern:pattern];
    if (!controller) {
        [self onMatchUnhandledURLAction:urlAction];
        return nil;
    }
    // notify match view controller
    [self onMatchViewController:controller withURLAction:urlAction];
    
    // handle login action
    if ([self handleLoginAction:urlAction]) {
        // login action handled, so should not to continue.
        return nil;
    }
    
    // handle needs login action
    if ([self handleNeedsLoginAction:urlAction withController:controller]) {
        // needs login action handled, so should not to continue.
        return nil;
    }
    
    // check should open viewcontroller
    if (![controller isKindOfClass:[UIViewController class]]) {
        return nil;
    }
    if ([controller respondsToSelector:@selector(shouldShow:)]) {
        if (![((id <MVNavigatorViewControllerProtocal>)controller) shouldShow:urlAction]) {
            return nil;
        }
    }
    // open view controller
    [self openViewController:controller withURLAction:urlAction];
    
    return controller;
}

- (void)openViewController:(UIViewController *)controller withURLAction:(MVURLAction *)urlAction {
    NSString *modalGroup = [urlAction stringForKey:@"modalgroup"];
    if (modalGroup.length > 0) {
        controller.modalGroup = modalGroup;
        [urlAction removeObjectForKey:@"modalgroup"];
    }
    
    controller.urlAction = urlAction;
    BOOL isSingleton = NO;
    if ([[controller class] respondsToSelector:@selector(isSingleton)]) {
        isSingleton = [[controller class] isSingleton];
    }
    if ([controller respondsToSelector:@selector(handleWithURLAction:)]) {
        if (![((id<MVNavigatorViewControllerProtocal>)controller) handleWithURLAction:urlAction]) {
            // handleWithURLAction returns NO
            return;
        }
    }
    if (isSingleton) {
        [self pushSingletonViewController:controller withURLAction:urlAction];
    } else {
        [self pushViewController:controller withURLAction:urlAction];
    }
}

- (void)pushSingletonViewController:(UIViewController *)controller withURLAction:(MVURLAction *)urlAction {
    if (!controller) {
        return;
    }
    
    NSMutableArray *controllers = [NSMutableArray arrayWithArray:self.mainNavigationContorller.viewControllers];
    //    if ([controllers existObjectMatch:^BOOL(UIViewController *obj) {
    //        return (obj == controller);
    //    }])
    if ([controllers containsObject:controller]){
        if (controller != [controllers lastObject]) {
            //            NVNaviAnimation animation = NVNaviAnimationNone;
            //            if (_urlActionWaitingList.count>0) {
            //                animation = NVNaviAnimationNone;
            //            } else {
            //                animation = urlAction.animation;
            //            }
            //            [self.mainNavigationContorller popToViewController:controller animated:(animation!=NVNaviAnimationNone)];
            
            
            //manipulate the view controllers in another array incase it sends unnecessary viewController messages
            //pop all the viewController above the object , including object it self
            
            //the bottom one is always HomeMainViewController, can't be removed
            while (controllers.count > 1) {
                [controllers removeLastObject];
                if(controller == [controllers lastObject]){
                    break;
                }
            }
            //[controllers removeObject:controller];
            
            //self.mainNavigationContorller.viewControllers = controllers;
            
            NVNaviAnimation animation = NVNaviAnimationNone;
            if (_urlActionWaitingList.count>0) {
                animation = NVNaviAnimationNone;
            } else {
                animation = urlAction.animation;
            }
            [self.mainNavigationContorller setViewControllers:controllers animated:(animation!=NVNaviAnimationNone)];
            //[self pushViewController:controller withURLAction:urlAction];
        }
        else {
            [controller viewWillAppear:NO];
            [controller viewDidAppear:NO];
        }
    } else {
        [self pushViewController:controller withURLAction:urlAction];
    }
}

- (void)pushViewController:(UIViewController *)controller withURLAction:(MVURLAction *)urlAction {
    // 如果是处理堵塞的页面，一次性压入所有页面，只有最后一个页面使用动画
    NVNaviAnimation animation = NVNaviAnimationNone;
    if (_urlActionWaitingList.count>0) {
        animation = NVNaviAnimationNone;
    } else {
        animation = urlAction.animation;
    }
    
    [self.mainNavigationContorller pushViewController:controller withAnimation:(animation!=NVNaviAnimationNone)];
}

- (BOOL)pop2Scheme:(NSString *)scheme
{
    return [self pop2AnyScheme:[NSArray arrayWithObject:scheme]];
}

- (BOOL)pop2AnyScheme:(NSArray *)schemeArray
{
    if (schemeArray.count == 0 || !_mainNavigationContorller) {
        return NO;
    }
    
    if ([self inBlockMode]) {
        return NO;
    }
    for (NSInteger i = self.mainNavigationContorller.viewControllers.count - 1; i >= 0; i--) {
        for (NSString *scheme in schemeArray) {
            MVURLPattern *pattern = [_urlMapping objectForKey:[scheme lowercaseString]];
            Class class = pattern.targetClass;
            if (class == nil) {
                return NO;
            }
            if ([self.mainNavigationContorller.viewControllers[i] isKindOfClass:class]) {
                [self.mainNavigationContorller popToViewController:self.mainNavigationContorller.viewControllers[i] animated:YES];
                return YES;
            }
            
        }
    }
    return NO;
}

- (BOOL)isFromScheme:(NSString *)scheme
{
    MVURLPattern *pattern = [_urlMapping objectForKey:[scheme lowercaseString]];
    Class class = pattern.targetClass;
    if (class == nil) return NO;
    
    id target = nil;
    for (id obj in [self.mainNavigationContorller viewControllers]) {
        if ([obj isKindOfClass:class]) {
            target = obj;
            break;
        }
    }
    
    return target != nil;
}

- (void)flush {
    while (_urlActionWaitingList.count>0) {
        if ([self inBlockMode]) {
            if (!_checkBlockTimer) {
                _checkBlockTimer = [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(checkTimerBlockModeDismiss) userInfo:nil repeats:YES];
            }
            return;
        }
        MVURLAction *urlAction = _urlActionWaitingList[0];
        [_urlActionWaitingList removeObject:urlAction];
        [self handleOpenURLAction:urlAction];
    }
}

- (MVURLPattern *)matchPatternWithURLAction:(MVURLAction *)urlAction {
    if (urlAction.url.host.length<1) {
        return nil;
    }
    return [_urlMapping objectForKey:[urlAction.url.host lowercaseString]];
}

- (Class)matchClassWithURLAction:(MVURLAction *)urlAction {
    MVURLPattern *pattern = [self matchPatternWithURLAction:urlAction];
    if (pattern) {
        return pattern.targetClass;
    } else {
        return NULL;
    }
}

- (UIViewController *)obtainControllerWithPattern:(MVURLPattern *)pattern {
    if (pattern.targetClass == nil) return nil;
    Class class = pattern.targetClass;
    if ([class respondsToSelector:@selector(isSingleton)] && [class isSingleton]) {
        
        UIViewController *viewController = nil;
        for (UIViewController *vc in [self.mainNavigationContorller viewControllers]) {
            if ([vc isKindOfClass:class]) {
                viewController = vc;
                break;
            }
        }
        
        return viewController ?: [class new];
    }
    return [class new];
}

- (UIViewController *)handlePopToURLAction:(MVURLAction *)urlAction {
    return nil;
}

- (NSMutableDictionary *)loadPattern {
    if (_urlMapping) {
        [_urlMapping removeAllObjects];
    } else {
        _urlMapping = [NSMutableDictionary dictionary];
    }
    
    for (int i=0; i<self.fileNamesOfURLMapping.count; i++) {
        NSString *fileName = self.fileNamesOfURLMapping[i];
        NSString *path = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
        NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
        if (content) {
            NSArray *eachLine = [content componentsSeparatedByString:@"\n"];
            for (NSString *aString in eachLine) {
                NSString *lineString = [aString stringByReplacingOccurrencesOfString:@" " withString:@""];
                if (lineString.length<1) {
                    // 空行
                    continue;
                }
                NSRange commentRange = [lineString rangeOfString:@"#"];
                if (commentRange.location == 0) {
                    // #在开头，表明这一行是注释
                    continue;
                }
                if (commentRange.location != NSNotFound) {
                    // 其后有注释，需要去除后面的注释
                    lineString = [lineString substringToIndex:commentRange.location];
                }
                if ([lineString rangeOfString:@":"].location != NSNotFound) {
                    NSString *omitString = [lineString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    NSArray *kv = [omitString componentsSeparatedByString:@":"];
                    if (kv.count == 2) {
                        // got it
                        NSString *host = [kv[0] lowercaseString];
                        NSString *className = kv[1];
                        [_urlMapping setObject:[MVURLPattern patternWithClassName:className withKey:host] forKey:host];
                    }
                }
            }
        } else {
            NSLog(@"[url mapping error] file(%@) is empty!!!!", fileName);
        }
    }
    
    return _urlMapping;
}

- (BOOL)isLogined {
    if (_accountManager) return [_accountManager isLogined];
    
    return NO;
}

- (void)presentLoginViewController:(UIViewController *)loginController {
    if (!loginController) {
        return;
    }
    [self.mainNavigationContorller presentViewController:loginController animated:YES completion:nil];
}

- (void)dismissLoginViewController:(void (^)(void))completedBlock {
    [self.mainNavigationContorller dismissViewControllerAnimated:YES completion:completedBlock];
}

///////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - call subclass
- (void)onMatchUnhandledURLAction:(MVURLAction *)urlAction {
}

- (void)willOpenExternal:(MVURLAction *)urlAction {
}

- (void)onMatchViewController:(UIViewController *)controller withURLAction:(MVURLAction *)urlAction {
}

- (BOOL)shouldOpenURLAction:(MVURLAction *)urlAction {
    return YES;
}

- (void)willOpenURLAction:(MVURLAction *)urlAction {
}

//- (void)setNeedPop2Root:(BOOL)needPop2Root
//{
//    [self.mainNavigationContorller setNeedPop2Root:needPop2Root];
//}
//
//- (void)setPreActionN:(void (^)())preActionN
//{
//    [self.mainNavigationContorller setPreActionN:preActionN];
//}
//
//- (void)setPreAction:(NSArray *(^)())preAction
//{
//    [self.mainNavigationContorller setPreAction:preAction];
//}
//
//- (void)setPreActionV:(UIView *(^)())preActionV
//{
//    [self.mainNavigationContorller setPreActionV:preActionV];
//}
//
//- (void)setPostAction:(void (^)())postAction
//{
//    [self.mainNavigationContorller setPostAction:postAction];
//}

#pragma mark - login
- (void)setAccountObject:(id)account{
    _accountManager = account;
}
- (BOOL)isShowingLogin {
    
    NSArray *array = [self.mainNavigationContorller viewControllers];
    return [array containsObject:[_accountManager createLoginViewController]];
//        return !!gLoginController;
//    return NO;
}

- (BOOL)isLoginURLAction:(MVURLAction *)action {
    return [@[@"login", @"weblogin"] containsObject:[action.url host]];
}

- (MVURLAction *)loginGotoURLAction:(MVURLAction *)originURLAction {
    NSString *gotoURL = [originURLAction stringForKey:@"goto"];
    return [MVURLAction actionWithURLString:gotoURL];
}

- (void)accountChanged {
    if ([self isLogined]) {
        if (gWait4LoginURLAction) {
            [self handleOpenURLAction:gWait4LoginURLAction];
            gWait4LoginURLAction = nil;
        }
    }
}

- (void)cancelLoginAction {
    gLoginController = nil;
    gWait4LoginURLAction = nil;
    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:NVAccountSwitched object:nil];
}

- (BOOL)showLogin {
    
    //    UIViewController *__loginController = [[NVAccountManager sharedInstance] createLoginViewControllerWithTarget:self cancelAction:@selector(cancelLoginAction)];
    
    UIViewController *__loginController = [_accountManager createLoginViewController];
    
    
    if (!__loginController || ![__loginController isKindOfClass:[UIViewController class]]) {
        NSLog(@"[log in error] %@ is not a kind of UIViewController class", NSStringFromClass([__loginController class]));
        return NO;
    }
    
    gLoginController = __loginController;
    if ([gLoginController conformsToProtocol:@protocol(NVLoginProtocol)]) {
        id<NVLoginProtocol> controller = (id<NVLoginProtocol>)gLoginController;
        controller.didLoginBlock = ^(){[self accountChanged];};
        controller.didCancelLoginBlock = ^(){[self cancelLoginAction];};
    }
    
    [self.mainNavigationContorller pushViewController:__loginController animated:YES];
    
    //    [self presentLoginViewController:__loginController];
    //        [self.mainNavigationContorller presentViewController:__loginController animated:YES completion:nil];
    return YES;
}

- (BOOL)handleLoginAction:(MVURLAction *)urlAction {
    // handle login host
    if (![self isLoginURLAction:urlAction]) {
        return NO;
    }
    
    if ([self isLogined]) {
        // get goto urlaction
        [self handleOpenURLAction:[self loginGotoURLAction:urlAction]];
        return NO;
    }
    
    if ([self isShowingLogin]) {
        // already showing login, in model mode, the url action showed add to _urlActionWaitingList
//        [self handleOpenURLAction:urlAction];TODO:_urlActionWaitingList
    } else {
        // show login
        if ([self showLogin]) {
            gWait4LoginURLAction = [self loginGotoURLAction:urlAction];
        }
    }
    
    return YES;
}

- (BOOL)handleNeedsLoginAction:(MVURLAction *)urlAction withController:(UIViewController *)controller {
    if ([[controller class] respondsToSelector:@selector(needsLogin:)]) {
        if ([[controller class] needsLogin:urlAction]) {
            if (![self isLogined]) {
                if ([self isShowingLogin]) {
                    // already showing login, in model mode, the url action showed add to _urlActionWaitingList
                    [self handleOpenURLAction:urlAction];
                } else {
                    // show login
                    if ([self showLogin]) {
                        gWait4LoginURLAction = urlAction;
                    }
                    return YES;
                }
            }
        }
    }
    return NO;
}
@end