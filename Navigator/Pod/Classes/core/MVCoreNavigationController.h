//
//  MVCoreNavigationController.h
//  Pods
//
//  Created by JiangTeng on 15/12/6.
//
//

#import <UIKit/UIKit.h>

@interface MVCoreNavigationController : UINavigationController
@property (nonatomic) BOOL inAnimating;
@end
