//
//  MVNavigatorAccountViewControllerProtocal.h
//  Pods
//
//  Created by JiangTeng on 15/12/8.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol MVNavigatorAccountViewControllerProtocal <NSObject>
@required
- (BOOL)isLogined;
- (UIViewController *)createLoginViewController;
@optional

@end
