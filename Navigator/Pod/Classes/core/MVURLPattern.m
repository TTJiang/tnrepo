//
//  MVURLPattern.m
//  Pods
//
//  Created by JiangTeng on 15/12/7.
//
//

#import "MVURLPattern.h"

static Class defaultWebViewControllerClass = NULL;
@implementation MVURLPattern {
    NSString *_patternString;
}

+ (void)setDefaultWebViewControllerClass:(Class)webControllerClass {
    defaultWebViewControllerClass = webControllerClass;
}

+ (MVURLPattern *)patternWithClassName:(NSString *)className withKey:(NSString *)key {
    return [[self alloc] initWithString:className type:NVURLPatternTypeClass withKey:key];
}

//+ (NVURLPattern *)patternWithHost:(NSString *)host withKey:(NSString *)key {
//    return [[self alloc] initWithString:host type:NVURLPatternTypeHost withKey:key];
//}

+ (MVURLPattern *)patternWithHttp:(NSString *)url withKey:(NSString *)key {
    return [[self alloc] initWithString:url type:NVURLPatternTypeHttp withKey:key];
}

+ (MVURLPattern *)patternWithHtmlZip:(NSString *)urlZip withKey:(NSString *)key {
    return [[self alloc] initWithString:urlZip type:NVURLPatternTypeHtmlZip withKey:key];
}

- (id)initWithString:(NSString *)string type:(NVURLPatternType)type withKey:(NSString *)key {
    self = [super init];
    if (self) {
        if (string.length<1 || key.length<1) {
            return nil;
        }
        _type = type;
        _key = key;
        _patternString = string;
    }
    return self;
}

- (Class)targetClass {
    if (_type == NVURLPatternTypeHttp || _type == NVURLPatternTypeHtmlZip) {
        return defaultWebViewControllerClass;
    }
    
    if (_patternString.length<1) {
        return NULL;
    }
    if (_type == NVURLPatternTypeClass) {
        return NSClassFromString(_patternString);
    } else {
        return NULL;
    }
}

@end
