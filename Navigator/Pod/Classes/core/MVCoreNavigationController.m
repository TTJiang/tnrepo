//
//  MVCoreNavigationController.m
//  Pods
//
//  Created by JiangTeng on 15/12/6.
//
//

#import "MVCoreNavigationController.h"
#import "MVURLAction.h"

@interface MVCoreNavigationController ()

@end

@implementation MVCoreNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (!animated && viewController.urlAction.animation == NVNaviAnimationNone) {
        // 无动画
        [super pushViewController:viewController animated:animated];
        _inAnimating = NO;
        return;
    }
    
    _inAnimating = YES;
    [super pushViewController:viewController animated:animated];
    __weak typeof(self) __weakSelf = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int)0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        __strong typeof(__weakSelf) __strongSelf = __weakSelf;
        __strongSelf.inAnimating = NO;
    });
}

- (void)pushViewController:(UIViewController *)viewController withAnimation:(BOOL)animated {
    [self pushViewController:viewController animated:animated];
    if (!animated) {
        // 无动画
        _inAnimating = NO;
    }
}
@end
