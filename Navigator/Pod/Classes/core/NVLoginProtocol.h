//
//  NVLoginProtocol.h
//  Nova
//
//  Created by Johnson Zhang on 14/12/11.
//  Copyright (c) 2014年 dianping.com. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, NVLoginType){
    NVLoginTypeQuick = 0,
    NVLoginTypeNormal = 1,
    NVLoginTypeAuto
};

typedef void (^NVUIBlock)(void);
typedef void (^NVActionBlock)(id sender);

@protocol NVLoginCallback <NSObject>

@property (nonatomic, copy) NVUIBlock didLoginBlock;
@property (nonatomic, copy) NVUIBlock didCancelLoginBlock;

@end

@protocol NVLoginProtocol <NVLoginCallback>

/**
 *  登录后重定向到URL
 */
@property (nonatomic, copy) NSString *gotoUrl;

/**
 *  登录HTML标题
 */
@property (nonatomic, copy) NSString *loginTitle;

@end
