//
//  MVURLPattern.h
//  Pods
//
//  Created by JiangTeng on 15/12/7.
//
//

#import <Foundation/Foundation.h>

typedef enum {
    NVURLPatternTypeClass = 0,  // host对应创建Class
    NVURLPatternTypeHttp = 1,       // host对应创建webviewController
    NVURLPatternTypeHtmlZip = 2     // host对应离线下载html zip包
    //    NVURLPatternTypeHost,       // host对应替换Host
} NVURLPatternType;

@interface MVURLPattern : NSObject

@property (nonatomic, strong, readonly) NSString *key;
@property (nonatomic, readonly) NVURLPatternType type;
@property (nonatomic, strong, readonly) NSString *patternString;
@property (nonatomic, readonly) Class targetClass;
@property (nonatomic) int version;

+ (void)setDefaultWebViewControllerClass:(Class)webControllerClass;

+ (MVURLPattern *)patternWithClassName:(NSString *)className withKey:(NSString *)key;
+ (MVURLPattern *)patternWithHttp:(NSString *)url withKey:(NSString *)key;
+ (MVURLPattern *)patternWithHtmlZip:(NSString *)urlZip withKey:(NSString *)key;
@end
