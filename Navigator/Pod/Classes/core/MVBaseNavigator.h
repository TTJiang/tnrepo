//
//  MVBaseNavigator.h
//  Pods
//
//  Created by JiangTeng on 15/12/8.
//
//

#import <Foundation/Foundation.h>
#import "MVURLAction.h"
#import "MVNavigatorViewControllerProtocal.h"
#import "MVCoreNavigationController.h"
#import "NVLoginProtocol.h"
#import "MVNavigatorAccountViewControllerProtocal.h"

@interface MVBaseNavigator : NSObject{
    MVCoreNavigationController *_mainNavigationContorller;
    NSString *_handleableURLScheme;
    NSArray *_fileNamesOfURLMapping;
    
    // url actions waiting for handle
    NSMutableArray *_urlActionWaitingList;
    
    NSMutableDictionary *_urlMapping; // host与NVURLPattern的映射关系，host为key，NVURLPattern为value
}

/**
 所有的页面跳转都会在mainNavigationContorller中进行
 */
@property (nonatomic, readonly) MVCoreNavigationController *mainNavigationContorller;
@property (nonatomic, readonly) NSString *handleableURLScheme;
@property (nonatomic, readonly) NSArray *fileNamesOfURLMapping;
@property (nonatomic, readonly) BOOL animating;

- (void)setNeedPop2Root:(BOOL)needPop2Root;
- (void)setPreActionN:(void (^)())preActionN;
- (void)setPreAction:(NSArray *(^)())preAction;
- (void)setPreActionV:(UIView *(^)())preActionV;
- (void)setPostAction:(void (^)())postAction;

- (Class)matchClassWithURLAction:(MVURLAction *)urlAction;
- (NSMutableDictionary *)loadPattern;

- (void)presentLoginViewController:(UIViewController *)loginController;
- (void)dismissLoginViewController:(void (^)(void))completedBlock;

- (BOOL)pop2Scheme:(NSString *)scheme;

- (BOOL)pop2AnyScheme:(NSArray *)schemeArray;

/**
 *  判断当前页面是否从某个特定的页面跳转而来
 *
 *  @param scheme 某个页面的scheme
 *
 *  @return 是否从这个页面跳转而来
 */
- (BOOL)isFromScheme:(NSString *)scheme;

/**
 程序现在是不是正在展示登录窗口
 */
- (BOOL)isShowingLogin;

- (void)setAccountObject:(id<MVNavigatorAccountViewControllerProtocal>)account;
/**
 显示登录窗口
 */
- (BOOL)showLogin;

- (BOOL)handleLoginAction:(MVURLAction *)urlAction;
- (BOOL)handleNeedsLoginAction:(MVURLAction *)urlAction withController:(UIViewController *)controller;
@end
