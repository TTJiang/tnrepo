//
//  MVNavigator.m
//  Pods
//
//  Created by JiangTeng on 15/12/8.
//
//

#import "MVNavigator.h"

static MVNavigator *gNavigator = nil;

void NVInternalSetNavigator(MVNavigator *navigator) {
    gNavigator = navigator;
}

@interface MVBaseNavigator (Private)

- (UIViewController *)handleOpenURLAction:(MVURLAction *)urlAction;
- (UIViewController *)openHttpURLAction:(MVURLAction *)urlAction;
- (void)handleUtmFlag:(MVURLAction *)urlAction withParamDic:(NSDictionary *)dict;
@end

@implementation MVNavigator

+ (void)initialize {
    gNavigator = [[self alloc] init];
}

#pragma mark - public methods
+ (id)navigator {
    return gNavigator;
}

- (void)setMainNavigationController:(MVCoreNavigationController *)mainViewContorller {
    _mainNavigationContorller = mainViewContorller;
}

- (void)setHandleableURLScheme:(NSString *)scheme {
    _handleableURLScheme = scheme;
}

- (void)setFileNamesOfURLMapping:(NSArray *)fileNames {
    _fileNamesOfURLMapping = fileNames;
    
    [self loadPattern];
}

- (UIViewController *)openURL:(NSURL *)url fromViewController:(UIViewController *)controller {
    if (!url) {
        return nil;
    }
    return [self openURLAction:[MVURLAction actionWithURL:url] fromViewController:controller];
}

- (UIViewController *)openURLString:(NSString *)urlString fromViewController:(UIViewController *)controller {
    if (urlString.length<1) {
        return nil;
    }
    return [self openURLAction:[MVURLAction actionWithURL:[NSURL URLWithString:urlString]] fromViewController:controller];
}

- (UIViewController *)openURLAction:(MVURLAction *)urlAction fromViewController:(UIViewController *)controller {
    if (![urlAction isKindOfClass:[MVURLAction class]]) {
        NSLog(@"*****************[open url action error] urlAction(%@) is not a kind of NVURLAction", NSStringFromClass([urlAction class]));
        return nil;
    }
    return [self openURLAction:urlAction];
}

- (UIViewController *)openURLAction:(MVURLAction *)urlAction {
    return [self handleOpenURLAction:urlAction];
}

- (void)popCurrentUrlAnimated:(BOOL)animated {
    [_mainNavigationContorller popViewControllerAnimated:animated];
}

- (NSArray *)urlActions {
    if (self.mainNavigationContorller) {
        NSArray *viewControllers = [self.mainNavigationContorller viewControllers];
        if (viewControllers.count>0) {
            NSMutableArray *array = [NSMutableArray array];
            for (UIViewController *controller in viewControllers) {
                if (controller.urlAction) {
                    [array addObject:controller.urlAction];
                } else {
                    [array addObject:[NSNull null]];
                }
            }
        }
    }
    return nil;
}

- (NSString *)description {
    if (self.mainNavigationContorller) {
        NSArray *viewControllers = [self.mainNavigationContorller viewControllers];
        if (viewControllers.count>0) {
            NSMutableString *log = [NSMutableString stringWithString:@"NVNavigator ("];
            for (UIViewController *controller in viewControllers) {
                [log appendFormat:@"\n  %@ - %@", NSStringFromClass([controller class]), controller.urlAction];
            }
            [log appendString:@"\n)"];
            return log;
        }
    }
    return [super description];
}


/////////////////////////////////////////////////////////////////////////////
- (void)onMatchUnhandledURLAction:(MVURLAction *)urlAction {
    if ([self.delegate respondsToSelector:@selector(navigator:onMatchUnhandledURLAction:)]) {
        [self.delegate navigator:self onMatchUnhandledURLAction:urlAction];
    }
}

- (void)onMatchViewController:(UIViewController *)controller withURLAction:(MVURLAction *)urlAction {
    if ([self.delegate respondsToSelector:@selector(navigator:onMatchViewController:withURLAction:)]) {
        [self.delegate navigator:self onMatchViewController:controller withURLAction:urlAction];
    }
}

- (BOOL)shouldOpenURLAction:(MVURLAction *)urlAction {
    if ([self.delegate respondsToSelector:@selector(navigator:shouldOpenURLAction:)]) {
        return [self.delegate navigator:self shouldOpenURLAction:urlAction];
    }
    return YES;
}

- (void)willOpenURLAction:(MVURLAction *)urlAction {
    if ([self.delegate respondsToSelector:@selector(navigator:willOpenURLAction:)]) {
        [self.delegate navigator:self willOpenURLAction:urlAction];
    }
}

- (void)willOpenExternal:(MVURLAction *)urlAction {
    if ([self.delegate respondsToSelector:@selector(navigator:willOpenExternal:)]) {
        [self.delegate navigator:self willOpenExternal:urlAction];
    }
}

@end


////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
@implementation UIViewController (NVNavigator)

- (UIViewController *)openURLHost:(NSString *)urlHost {
    NSString *scheme = [[MVNavigator navigator] handleableURLScheme];
    if (scheme.length<1 || urlHost.length<1) {
        return nil;
    }
    return [[MVNavigator navigator] openURLString:[NSString stringWithFormat:@"%@://%@", scheme, urlHost] fromViewController:self];
}

- (UIViewController *)openURL:(NSURL *)url {
    return [[MVNavigator navigator] openURL:url fromViewController:self];
}

- (UIViewController *)openURLString:(NSString *)urlString {
    return [[MVNavigator navigator] openURLString:urlString fromViewController:self];
}

- (UIViewController *)openHttpURLString:(NSString *)httpURLString {
    NSString *scheme = [[MVNavigator navigator] handleableURLScheme];
    if (scheme.length<1 || httpURLString.length<1) {
        return nil;
    }
    return [[MVNavigator navigator] openURLString:[NSString stringWithFormat:@"%@://web?url=%@", scheme, [self stringByAddingPercentEscapes:httpURLString]] fromViewController:self];
}

- (void)openURLList:(NSArray *)urlList {
    NSString *scheme = [[MVNavigator navigator] handleableURLScheme];
    if (scheme.length<1 || urlList.count<1) {
        return;
    }
    NSMutableString *newUrlString = [NSMutableString stringWithFormat:@"%@://list?", scheme];
    for (int i=1; i<=urlList.count; i++) {
        NSURL *url = urlList[i-1];
        NSString *urlString = nil;
        if ([url isKindOfClass:[NSURL class]]) {
            urlString = [url absoluteString];
        } else if ([url isKindOfClass:[NSString class]]) {
            urlString = (NSString *)url;
        } else {
            NSLog(@"openURLList error: %@ is not a kind of NSURL or NSString", url);
            return;
        }
        if (i != urlList.count) {
            [newUrlString appendFormat:@"url%d=%@&", i, [self stringByAddingPercentEscapes:urlString]];
        } else {
            [newUrlString appendFormat:@"url%d=%@", i, [self stringByAddingPercentEscapes:urlString]];
        }
    }
    [[MVNavigator navigator] openURLString:newUrlString fromViewController:self];
}

- (UIViewController *)openURLFormat:(NSString *)urlFormat, ... {
    if (urlFormat.length<1) {
        return nil;
    }
    va_list ap;
    va_start(ap, urlFormat);
    NSString *urlString = [[NSString alloc] initWithFormat:urlFormat arguments:ap];
    va_end(ap);
    return [[MVNavigator navigator] openURLString:urlString fromViewController:self];
}

- (UIViewController *)openURLAction:(MVURLAction *)urlAction {
    return [[MVNavigator navigator] openURLAction:urlAction fromViewController:self];
}


- (NSString *)stringByAddingPercentEscapes:(NSString *)string {
    return [self stringByAddingPercentEscapesUsing:string encoding:NSUTF8StringEncoding];
}

- (NSString *)stringByAddingPercentEscapesUsing:(NSString *)string encoding:(NSStringEncoding)enc
{
    
    NSString * newString = (__bridge_transfer NSString *)
    CFURLCreateStringByAddingPercentEscapes(NULL,
                                            (__bridge CFStringRef)string,
                                            NULL,
                                            (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                            CFStringConvertNSStringEncodingToEncoding(enc));
    
    return newString;
    
}
@end
