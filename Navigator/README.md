# Navigator

[![CI Status](http://img.shields.io/travis/Navigator/Navigator.svg?style=flat)](https://travis-ci.org/Navigator/Navigator)
[![Version](https://img.shields.io/cocoapods/v/Navigator.svg?style=flat)](http://cocoapods.org/pods/Navigator)
[![License](https://img.shields.io/cocoapods/l/Navigator.svg?style=flat)](http://cocoapods.org/pods/Navigator)
[![Platform](https://img.shields.io/cocoapods/p/Navigator.svg?style=flat)](http://cocoapods.org/pods/Navigator)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Navigator is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "Navigator"
```

## Author

Navigator, jiangteng.cn@gmail.com

## License

Navigator is available under the MIT license. See the LICENSE file for more info.
