//
//  TNAppDelegate.h
//  MVImageView
//
//  Created by MVImageView on 03/06/2016.
//  Copyright (c) 2016 MVImageView. All rights reserved.
//

@import UIKit;

@interface TNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
