//
//  main.m
//  MVImageView
//
//  Created by MVImageView on 03/06/2016.
//  Copyright (c) 2016 MVImageView. All rights reserved.
//

@import UIKit;
#import "TNAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TNAppDelegate class]));
    }
}
