# MVImageView

[![CI Status](http://img.shields.io/travis/MVImageView/MVImageView.svg?style=flat)](https://travis-ci.org/MVImageView/MVImageView)
[![Version](https://img.shields.io/cocoapods/v/MVImageView.svg?style=flat)](http://cocoapods.org/pods/MVImageView)
[![License](https://img.shields.io/cocoapods/l/MVImageView.svg?style=flat)](http://cocoapods.org/pods/MVImageView)
[![Platform](https://img.shields.io/cocoapods/p/MVImageView.svg?style=flat)](http://cocoapods.org/pods/MVImageView)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MVImageView is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "MVImageView"
```

## Author

MVImageView, jiangteng.cn@gmail.com

## License

MVImageView is available under the MIT license. See the LICENSE file for more info.
