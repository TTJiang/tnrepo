//
//  DPImageCache.h
//  DPLib
//
//  Created by Tu Yimin on 09-12-30.
//  Copyright 2009 dianping.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <pthread.h>
#import "sqlite3.h"
@class TMCache;
typedef void (^CacheObjectBlock)(NSString *key, id object);

@protocol NVImageCacheProtocol <NSObject>
- (void)fetch:(NSString *)url block:(CacheObjectBlock)block;
- (NSData *)fetch:(NSString *)url;
- (BOOL)push:(NSData *)data forKey:(NSString *)url;
- (BOOL)cleanUp;
@end


@interface NVImageCacheManager : NSObject

+ (NVImageCacheManager *)sharedInstance;

@property (nonatomic, strong, readonly) TMCache *cache;

- (id<NVImageCacheProtocol>)thumbCache;
- (id<NVImageCacheProtocol>)photoCache;

@end

