//
//  DPImageCache.m
//  DPLib
//
//  Created by Tu Yimin on 09-12-30.
//  Copyright 2009 dianping.com. All rights reserved.
//

#import "NVImageCache.h"
#import "TMCache.h"
#import <UIKit/UIKit.h>


@interface TMCache (nvcache)<NVImageCacheProtocol>
@end

@implementation TMCache (nvcache)

- (void)fetch:(NSString *)url block:(CacheObjectBlock)block
{
    [self objectForKey:url block:^(TMCache *cache, NSString *key, id object) {
        block(key, object);
    }];
}

- (NSData *)fetch:(NSString *)url {
    return [self objectForKey:url];
}

- (BOOL)push:(NSData *)data forKey:(NSString *)url {
    [self setObject:data forKey:url];
    return YES;
}

- (BOOL)cleanUp {
    [self removeAllObjects];
    return YES;
}
@end


@interface NVImageCacheManager ()
@property (nonatomic, strong) TMCache *cache;
@end


@implementation NVImageCacheManager

+ (NVImageCacheManager *)sharedInstance {
    static NVImageCacheManager *instance  = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[NVImageCacheManager alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:instance selector:@selector(didEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    });
    
    return instance;
}

- (TMCache *)cache {
    if (_cache == nil) {
        _cache = [[TMCache alloc] initWithName:@"com.ImageView.image-cache"];
    }
    return _cache;
}

- (id<NVImageCacheProtocol>)thumbCache {
    return (id)self.cache;
}

- (id<NVImageCacheProtocol>)photoCache {
    return (id)self.cache;
}

- (NSTimeInterval)cacheDuration {
    return 3600 * 24 * 15;
}

- (NSInteger)maxImageCacheSize {
    return 1024 * 100;
}

- (void)didEnterBackground:(NSNotification *)n {
    NSTimeInterval interval = [[NSDate date] timeIntervalSince1970];
    [self.cache trimToDate:[NSDate dateWithTimeIntervalSince1970:(interval - [self cacheDuration])]];
    [self.cache.diskCache trimToSize:[self maxImageCacheSize]];
}

@end

