//
//  UIImage+Compress.m
//  Nova
//
//  Created by chen wang on 12-6-15.
//  Copyright (c) 2012年 dianping.com. All rights reserved.
//

#import "UIImage+Compress.h"
//#import "NVAppConfig.h"

#define MAX_IMAGEWIDTHPIX 1400.0
#define MAX_IMAGEHEIGHTPIX 1400.0

@implementation UIImage (Compress)

- (UIImage *)compressedImage {
    NSInteger widthPix = 100;
    NSInteger heightPix = 50;
    
    widthPix = (widthPix == 0 ? MAX_IMAGEWIDTHPIX : widthPix);
    heightPix = (heightPix == 0 ? MAX_IMAGEHEIGHTPIX : heightPix);
    
    return [self compressedImageWithMaxWidthPix:widthPix MaxHeightPix:heightPix];
}

- (UIImage *)compressedImageWithInMaxPix:(CGFloat)maxPIX
{
    return [self compressedImageWithMaxWidthPix:maxPIX MaxHeightPix:maxPIX];
}

- (UIImage *)compressedImageWithMaxWidthPix:(CGFloat)maxWidthPIX MaxHeightPix:(CGFloat)maxHeightPIX
{
    CGSize imageSize = self.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    
    if (width <= maxWidthPIX && height<= maxHeightPIX) {
        // no need to compress.
        return self;
    }
    
    if (width == 0 || height == 0) {
        // void zero exception
        return self;
    }
    
    UIImage *newImage = nil;
    CGFloat widthFactor = maxWidthPIX / width;
    CGFloat heightFactor = maxHeightPIX / height;
    CGFloat scaleFactor = 0.0;
    if (widthFactor > heightFactor)
        scaleFactor = heightFactor; // scale to fit height
    else
        scaleFactor = widthFactor; // scale to fit width
    CGFloat scaledWidth  = width * scaleFactor;
    CGFloat scaledHeight = height * scaleFactor;
    
    CGSize targetSize = CGSizeMake(scaledWidth, scaledHeight);
    
    UIGraphicsBeginImageContext(targetSize); // this will crop
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [self drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    
    return newImage;
}


- (UIImage *)compressedImageToSize:(CGSize)targetSize
{
    CGSize imageSize = self.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
	
	if (width == targetSize.width && height== targetSize.height) {
		// no need to compress.
		return self;
	}
	
	if (width == 0 || height == 0 || targetSize.width == 0 || targetSize.height == 0) {
		// void zero exception
		return self;
	}
	
    UIImage *newImage = nil;
    UIGraphicsBeginImageContext(targetSize); // this will crop
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.size.width  = targetSize.width;
    thumbnailRect.size.height = targetSize.height;
    
    [self drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
	
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    
    return newImage;
}


- (NSData *)compressedData:(CGFloat)compressionQuality {
	assert(compressionQuality<=1.0 && compressionQuality >=0);
	return UIImageJPEGRepresentation(self, compressionQuality);
}

- (CGFloat)compressionQuality {
	NSData *data = UIImageJPEGRepresentation(self, 1.0);
	NSUInteger dataLength = [data length];
	if(dataLength>50000.0) {
		return 1.0-50000.0/dataLength;
	} else {
		return 1.0;
	}
}

- (NSData *)compressedData {
//	CGFloat quality = [self compressionQuality];
//	return [self compressedData:quality];
    return [self compressedData:0.8];
}



//截取部分图像
-(UIImage*)cropImageInRect:(CGRect)rect
{
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 1);
    [self drawAtPoint:(CGPoint){-rect.origin.x, -rect.origin.y}];
    UIImage *croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return croppedImage;
//    CGImageRef subImageRef = CGImageCreateWithImageInRect(self.CGImage, rect);
//	CGRect smallBounds = CGRectMake(0, 0, CGImageGetWidth(subImageRef), CGImageGetHeight(subImageRef));
//	
//    UIGraphicsBeginImageContext(smallBounds.size);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    CGContextDrawImage(context, smallBounds, subImageRef);
//    UIImage* smallImage = [UIImage imageWithCGImage:subImageRef];
//    CFRelease(subImageRef);
//    UIGraphicsEndImageContext();
//    return smallImage;
}

- (UIImage *)imageWithImage:(UIImage *)image cropInRect:(CGRect)rect {
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 1);
    [image drawAtPoint:(CGPoint){-rect.origin.x, -rect.origin.y}];
    UIImage *croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return croppedImage;
}


@end