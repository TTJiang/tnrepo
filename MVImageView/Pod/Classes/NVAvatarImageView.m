//
//  NVAvatarImageView.m
//  NVScope
//
//  Created by Tu Yimin on 10-7-30.
//  Copyright 2010 dianping.com. All rights reserved.
//

#import "NVAvatarImageView.h"

@implementation NVAvatarImageView


- (UIImage *)noPicFrame {
	static UIImage *_empty96 = nil;
	static UIImage *_empty48 = nil;
	static UIImage *_empty32 = nil;
	CGRect rect = self.frame;
	if(rect.size.width >= 96) {
		if(_empty96 == nil) {
			_empty96 = [UIImage imageNamed:@""];
		}
		return _empty96;
	} else if(rect.size.width >= 48) {
		if(_empty48 == nil) {
			_empty48 = [UIImage imageNamed:@""];
		}
		return _empty48;
	} else {
		if(_empty32 == nil) {
			_empty32 = [UIImage imageNamed:@""];
		}
		return _empty32;
	}
}

- (void)setImageUrl:(NSURL *)url {
    [super setImageUrl:url];
    [self setLoadingCentralIconHidden:YES];
}


- (UIImage *)loadingFrame {
	static UIImage *_loading96 = nil;
	static UIImage *_loading48 = nil;
	static UIImage *_loading32 = nil;
	CGRect rect = self.frame;
	if(rect.size.width >= 96) {
		if(_loading96 == nil) {
			_loading96 = [UIImage imageNamed:@""];
		}
		return _loading96;
	} else if(rect.size.width >= 48) {
		if(_loading48 == nil) {
			_loading48 = [UIImage imageNamed:@""];
		}
		return _loading48;
	} else {
		if(_loading32 == nil) {
			_loading32 = [UIImage imageNamed:@""];
		}
		return _loading32;
	}
}

- (UIImage *)errorFrame {
	return [self noPicFrame];
}

@end
