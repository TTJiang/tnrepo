//
//  UIImage+Radius.h
//  Nova
//
//  Created by xiebohui on 5/6/15.
//  Copyright (c) 2015 dianping.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Radius)

- (UIImage *)radius:(CGFloat)cornerRadius;

@end
