//
//  UIImage+Compress.h
//  Nova
//
//  Created by chen wang on 12-6-15.
//  Copyright (c) 2012年 dianping.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Compress)

- (UIImage *)compressedImage;
- (UIImage *)compressedImageWithInMaxPix:(CGFloat)maxPIX;
- (UIImage *)compressedImageToSize:(CGSize)targetSize;

- (CGFloat)compressionQuality;
- (NSData *)compressedData;
- (NSData *)compressedData:(CGFloat)compressionQuality;

- (UIImage*)cropImageInRect:(CGRect)rect;

@end


