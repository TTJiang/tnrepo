#
# Be sure to run `pod lib lint MVImageView.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "MVImageView"
  s.version          = "0.1.0"
  s.summary          = "A short description of MVImageView."

  s.description      = <<-DESC
                       DESC

  s.homepage         = "https://github.com/<GITHUB_USERNAME>/MVImageView"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "MVImageView" => "jiangteng.cn@gmail.com" }
  s.source           = { :git => "https://github.com/<GITHUB_USERNAME>/MVImageView.git", :tag => s.version.to_s }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
  s.resource_bundles = {
    'MVImageView' => ['Pod/Assets/*.png']
  }

  s.dependency 'TMCache'
  s.dependency 'StoreKit'
end
