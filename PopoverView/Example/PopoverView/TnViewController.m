//
//  TnViewController.m
//  PopoverView
//
//  Created by PopoverView on 03/06/2016.
//  Copyright (c) 2016 PopoverView. All rights reserved.
//

#import "TnViewController.h"
#import "MVPopoverView.h"

@interface TnViewController ()

@end

@implementation TnViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeContactAdd];
    btn.frame = CGRectMake(50, 50, 50, 50);
    [btn addTarget:self action:@selector(tapBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)tapBtn{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 200, 200, 200)];
    [view setBackgroundColor:[UIColor redColor]];
    
    MVPopoverView *popview = [MVPopoverView sharedPopoverView];
    popview.componentsView = view;
    [popview showPopoverViewInView:self.view position:NVPopoverPositionScreenBottom animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
