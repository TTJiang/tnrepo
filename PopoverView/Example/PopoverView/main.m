//
//  main.m
//  PopoverView
//
//  Created by PopoverView on 03/06/2016.
//  Copyright (c) 2016 PopoverView. All rights reserved.
//

@import UIKit;
#import "TnAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TnAppDelegate class]));
    }
}
