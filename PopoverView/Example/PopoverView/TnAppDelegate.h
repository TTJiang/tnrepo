//
//  TnAppDelegate.h
//  PopoverView
//
//  Created by PopoverView on 03/06/2016.
//  Copyright (c) 2016 PopoverView. All rights reserved.
//

@import UIKit;

@interface TnAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
