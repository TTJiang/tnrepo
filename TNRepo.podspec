Pod::Spec.new do |s|
s.name         = "TNRepo"
s.version      = "0.0.1"
s.summary      = "TNRepo"

s.description  = <<-DESC
集成网络通信，等其他基础控件
DESC

s.homepage     = "http://ten12.xyz"
s.license      = 'MIT'
s.author             = { "teng.jiang" => "jiangteng.cn@gmail.com" }
s.platform     = :ios, "6.0"
s.source       = {:git => "git@bitbucket.org:TTJiang/tnrepo.git", :tag => s.version}
s.requires_arc = true
s.default_subspec = 'CIPNetwork', 'common', 'MVImageView', 'Navigator', 'PopoverView', 'QRCode', 'TMCache', 'StoreKit', 'MVTabBarController'


 s.subspec 'StoreKit' do |storekit|
        storekit.source_files = 'common/StoreKit/Pod/Classes/**/*'
        storekit.resource_bundles = {
          'StoreKit' => ['common/StoreKit/Pod/Assets/*.png']
        }
    end

s.subspec 'MVTabBarController' do |tabbarcontroller|
        tabbarcontroller.source_files = 'common/MVTabBarController/Pod/Classes/**/*'
        tabbarcontroller.resource_bundles = {
          'StoreKit' => ['common/MVTabBarController/Pod/Assets/*.png']
        }
    end

 s.subspec 'CIPNetwork' do |cipnetwork|
    cipnetwork.source_files = 'CIPNetwork/Pod/Classes/**/*'
    cipnetwork.resource_bundles = {
    'CIPNetwork' => ['CIPNetwork/Pod/Assets/*.png']
    }
    cipnetwork.frameworks = "SystemConfiguration", "AdSupport", "CoreTelephony", "UIKit", "CFNetwork", "Security"
    cipnetwork.libraries = "sqlite3", "z", "stdc++.6", "c++"
  end

 s.subspec 'MVImageView' do |mvimageview|
  mvimageview.source_files = 'MVImageView/Pod/Classes/**/*'
  mvimageview.resource_bundles = {
    'MVImageView' => ['MVImageView/Pod/Assets/*.png']
  }
  # mvimageview.dependency 'TMCache'
  # mvimageview.dependency 'StoreKit'
end

 s.subspec 'Navigator' do |navigator|
  navigator.source_files = 'Navigator/Pod/Classes/**/*'
  navigator.resources = 'Navigator/Pod/Assets/**/*.png'
end

 s.subspec 'PopoverView' do |popoverView|
  popoverView.source_files = 'PopoverView/Pod/Classes/**/*'
  popoverView.resource_bundles = {
    'PopoverView' => ['PopoverView/Pod/Assets/*.png']
  }
end

 s.subspec 'TMCache' do |tmcache|
   tmcache.source_files = 'TMCache/Pod/Classes/**/*'
  tmcache.resource_bundles = {
    'TMCache' => ['TMCache/Pod/Assets/*.png']
  }
end

 s.subspec 'QRCode' do |qrcode|
  qrcode.source_files = 'Pod/Classes/**/*'
  qrcode.resource_bundles = {
    'QRCode' => ['QRCode/Pod/Assets/*.png']
  }


   non_arc_files = ["QRCode/Pod/Classes/QREncode/QR_Encode.{h,cpp}",
    "QRCode/Pod/Classes/QREncode/DataMatrix.{h,mm}",
    "QRCode/Pod/Classes/QREncode/QREncoder_internal.{h,mm}",
    "QRCode/Pod/Classes/QREncode/QRDefinition.h"]

    qrcode.requires_arc = true
    qrcode.exclude_files =  non_arc_files
    qrcode.resources = 'QRCode/QRCode/zxing/ZXingWidgetBundle.bundle'
    qrcode.frameworks = 'AVFoundation'

    qrcode.subspec 'no-arc' do |qrcodesp|
    qrcodesp.source_files =  non_arc_files
    qrcodesp.requires_arc = false
    end
end



   s.subspec 'common' do |common|
    common.subspec 'Foundation' do |sp|
            sp.source_files = "common/Foundation/**/*.{h,m}"
     end

    common.subspec 'DrawerViewController' do |sp|
        sp.source_files = "common/DrawerViewController/**/*.{h,m}"
    end

    common.subspec 'ViewPagerController' do |sp|
        sp.source_files = "common/ViewPagerController/**/*.{h,m}"
  end

    common.subspec 'FontIcon' do |sp|
        sp.source_files = "common/FontIcon/**/*.{h,m}"
    end

    common.subspec 'NVSearchBar' do |sp|
         sp.source_files = "common/NVSearchBar/**/*.{h,m}"
         sp.resources =  "common/NVSearchBar/**/*.{png,jpg}"
    end

    common.subspec 'PhotoList' do |sp|
        sp.source_files = "common/PhotoList/**/*.{h,m}"
        sp.resources =  "common/PhotoList/**/*.{png,jpg}"
    end

 end


end

